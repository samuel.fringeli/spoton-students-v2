-  installer password-hash avec node (`npm install password-hash`)
-  créer test.js
```
const passwordHash = require("password-hash");
let pass = "GXBeD34T5"; // mot de passe quelconque à communiquer à l'utilisateur
let hashedPassword = passwordHash.generate(pass);  
console.log(hashedPassword); // mot de passe à sauver dans la base pour l'utilisateur
```
-  exécuter le script depuis le terminal (`node test.js`)
-  sauver hashedPassword dans la base de donnée sur prod dans la colonne token et utiliser GXBeD34T5 pour se logger.
- envoyer à l'utilisateur le mot de passe brut et lui demander de se logger puis de modifier son mot de passe depuis https://sooz.tic.heia-fr.ch/profil

