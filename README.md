# SooZ global project

You will need Docker and `docker-compose` to deploy the application in production.

---

## Launch the project on your computer

To launch SooZ on your computer, follow these steps :

1. Create a file named `.env` at the root of the project
2. Insert the following variables into the `.env` file :
```
APP_BASE_URL=localhost
MYSQL_PASSWORD="whatever_you_want"
MYSQL_ROOT_PASSWORD="whatever_you_want"
SESSION_PASSWORD="whatever_you_want"
PUBLIC_VAPID_KEY_BASE64="..."
PRIVATE_VAPID_KEY="..."
FCM_API_KEY="..."
```

Run the project with the command `docker-compose up`.
On M1 macs, you can use the `docker-compose.m1.yml` file with the command `docker-compose -f docker-compose.m1.yml up`.

This will expose the frontend on http://localhost:8080 and the backend on port http://localhost:3000. At the same time, file changes to the frontend and backend will become visible immediately. The database is exposed on port 3306.


### Account creation email
Please note that in production, when a user signs up, he will receive an email to confirm his account. 
As this email sending process is not working when you run the machine locally, we are bypassing that by setting the confirmation flag of the account to 1 during the creation.


### Connect to the DB

To connect to the DB, enter the following command :

`docker exec -it sooze-database-1 mysql -u root -p s00z_2_db`

Then, enter the root pasword you defined in your `.env` file.

### Rebuild the project from scratch

To rebuild the project from scratch, follow these steps :

1. Remove the folder `database_volume`
2. Run the command `docker-compose up --build` (on M1 macs, run the command `docker-compose -f docker-compose.m1.yml up --build`)

### Access the api docs (swagger)

By default, the api docs (swagger) are available on [http://localhost:3000/api-docs](http://localhost:3000/api-docs).

---

## Launch the production environment on a server

The production environment runs the frontend on a nginx webserver that supports HTTPS. This adds some extra steps to the deployment procedure. This section here describes the manual deployment. For automatic deployment, have a look at [Gitlab CD](#GitLab-CD) of this project.

### Connect to the server

Use `ssh` to connect to the server (e.g. `ssh your.user@sooz.tic.heia-fr.ch`).

Usually the Sooz source files are located at `/sooze` of the machines of HEIA, since it was not clear at the beginning how it will be named.

### Before first launch

To be able to use the app, the configuration of the application must be adapted to the environment. This includes principally the two following tasks.

**Configure env variables**

The CI/CD deploys automatically the project on a server configured in GitLab (settings => runners).
However, you need to configure the following variables ([settings => CI/CD](https://gitlab.forge.hefr.ch/sandy.ingram/sooze/-/settings/ci_cd) => variables) to make it work.


- ``APP_BASE_URL``
- ``MYSQL_PASSWORD``
- ``MYSQL_ROOT_PASSWORD``
- ``S3_PASSWORD``
- ``S3_USERNAME``
- ``SESSION_PASSWORD``
- ``PUBLIC_VAPID_KEY_BASE64``
- ``PRIVATE_VAPID_KEY``
- ``FCM_API_KEY``

**Generate the SSL certificate**

As this is a PWA, a connection over HTTPS to the server must be guaranteed. Thus, a SSL certificate is required. So far, the certificate is generated using [Let's Encrypt](https://letsencrypt.org/) and [Certbot](https://certbot.eff.org/). Since such a certificate needs to be renewed every 6 months, the production environment is setup to automate this task and followed this [Medium Post](https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71) with minor modifications.

Before the certificates are supported, the following steps shall be performed:

- Set the correct URL where the application is hosted (currently `soozdev.tic.heia-fr.ch`) throughout the project use the auxillary script `switch_server_url.sh`. Therefore, execute in the root folder of the project: `$ ./switch_server_url.sh <old_url> <new_url>`.
  In case that you don't know the old URL, look for a `.conf` file in the folder `/data/nginx/` which is preceeded by a URL.
- Run the script to initialize the necessary files: `./init_letsencrypt.sh`

### Deployment and Redeployment

The redeployment is automatically done with the Gitlab CI, by using the `docker-compose.ci.yml` file. 

---

## Notifications

The application is configured to send push notifictions to users every weekday at 17h00 if they have not yet answered a survey.

The configuration of the cronjob is in the file `backend/server/routes/notifications.js` which contains at the same time the options for the push notification sent.

A user is subscribed automatically on a device if he gives the browser the necessary permissions.

---

## Access to Sensor Dashboard

(using the sandy's BBData account - ask Sandy or Yael for those credentials and save them locally)

gestion admin
https://bbdata-admin.smartlivinglab.ch/

Visualisation et extraction on CSV
http://vmenergy.tic.heia-fr.ch/login

---

## User Privileges

There is a complete Privilege system included in the application, but so far used only in a single use case: performing overall analysis.
This allows an _admin_ or _superadmin_ to see the stats for entire enterprises. To assign a user a particular role, the change has to be made directly in the `user` table in the database, since no corresponding UI component exist.

The admin role is the role number 8.

---

## GitLab CD gitlab-runner

The machine where the gitlab-runner should run and finally, the application should be deployed must have docker installed. Please note, that so far this was always done as root on the server, if you use another user, you may run into permission issues (but prevent security flaws obvisouly).

**Install gitlab-runner**

Belows instructions to setup the docker-compose and gitlab-runner are coming from https://sean-bradley.medium.com/auto-devops-with-gitlab-ci-and-docker-compose-f931233f080f. The second part is specific to this project sooz here.

The GitLab runner, upon detecting changes to the chosen branches, will pull the latest commit and rebuild on the servers where I’ve installed the GitLab runners.

The commands used on my Ubuntu server where,

Add GitLab’s official repository:

```
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
```

Install the latest version of GitLab runner:

```
sudo apt-get install gitlab-runner
```

I also have another article that shows how to install GitLab Runner on your Ubuntu or Centos.

Register the runner. During registering, it will require a coordinator URL and CI token. You can find these in your GitLab repository → Settings → CI/CD → Runners page. Disable ‘Shared Runners’ while you are there. Take note of the details in the ‘Specific Runners’ section

```
sudo gitlab-runner register # [follow prompts] => define a custom tag, select the SHELL executor!
```

Open the nano editor on your Ubuntu server

```
sudo nano /etc/sudoers
```

add

```
gitlab-runner ALL=(ALL) NOPASSWD: ALL
```

to the bottom of the file, then save it.

**Important**: note that there is an open issue regarding starting the shell executor on Ubuntu 20.04 (https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26605). The workaround is to remove the `.bash_logout` file in the home directory of the gitlab-runner `/home/gitlab-runner`.

**Put in place configuration files**

To deploy the application, the configuration files for nginx and SSL certifiactes are needed, such as the `.env` file.

The SSL certificates have to be generated manually a first time. Therefore clone this repo in a temporary folder and follow [the corresponding instructions](#Before-first-launch) in this README.

After that, create a new folder at the root of the machine

```
mkdir /sooz_ci_artifacts
```

and move from your temporary folder the `data` directory and `.env` file to sooz_ci_artifacts.

```
cd <path/to/temporary/folder>
cp -r data /sooz_ci_artifacts/data
cp .env /sooz_ci_artifacts/.env
```

To distinguish the `.env` files for backend end frontend, a suffix is added to both while copying them.

```
cp frontend/.env.production /sooz_ci_artifacts/.env.production.frontend
cp backend/server/.env /sooz_ci_artifacts/.env.backend
```

The previously created temprorary folder can now be deleted.

**Include the runner in the pipeline**

The last step is updating of the pipeline to define when the runner should be triggered. Therefore, update the file `.gitlab-ci.ym` according to your needs. Make sure to use the tag previously defined while registrating the runner.

### How this pipeline works

Since `docker-compose` is used to deploy the application, the pipeline is a bit tricky. The list below outlines the basic steps performed:

1. Use a SHELL-executer for the pipeline
2. Clone the current commit to the server
3. Install `docker-compose`
4. Define the path to the shared directory between the host and the gitlab-runner executer to share configuration data and store database volumes
5. Copy the CI/CD variables to a local `.env` file
6. Install dependencies and create the build through commands (should be done through a stage and corresponding docker file)
7. Stop all running docker containers
8. Build new docker images
9. Start new docker containers


### Password reset procedure (until this feature is implemented)
https://gitlab.forge.hefr.ch/sandy.ingram/sooze/-/blob/master/PasswordResetProcedure.md
