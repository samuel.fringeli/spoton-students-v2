'use strict';

const {user} = require("../models");

const userAdmin = {
  firstName: 'Admin',
  lastName: 'Admin',
  username: 'admin',
  email: 'admin@sooz.tic.heia-fr.ch',
  token: 'sha1$a6e55ee8$1$b648d508794a5da8a4e0dafade5cdf8adcee9a4c', // s00z_Admin
  points: 0,
  activatePopup: 1,
  sex: "m",
  role: 8, // 8 is admin
  enterpriseId: 1,
  activated: 1
}

module.exports = {
	// knowing that seeds are run at every server restart, we check if the user already exists
  up: async queryInterface => {
      const adminInDBWithUsername = await user.findAll({ where: { username: userAdmin.username }});
      if (adminInDBWithUsername.length > 0) return await new Promise(r => r());
      const adminInDBWithEmail = await user.findAll({ where: { email: userAdmin.email }});
      if (adminInDBWithEmail.length > 0) return await new Promise(r => r());
      return await queryInterface.bulkInsert('user', [{ ...userAdmin }], {});
  },

  down: queryInterface => {
    return user.destroy({ where: { username: userAdmin.username } });
  }
};
