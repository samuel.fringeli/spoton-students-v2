
module.exports = (sequelize, DataTypes) => {
  const Rule = sequelize.define('rule', {
    periodicity: {
      allowNull: false,
      type: DataTypes.ENUM('daily', 'weekly', 'monthly')
    },
    operator: {
      allowNull: false,
      type: DataTypes.ENUM('<', '>')
    },
    participationRate: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    votingRate: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    voteType: {
      allowNull: false,
      type: DataTypes.ENUM('positive', 'negative', 'neutral')
    },
    rate: { // taux d'adéquation à la règle
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    messagePrefix: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    name: {
      allowNull: true,
      type: DataTypes.STRING
    },
    archived: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    timestamps: true,
  });

  Rule.associate = (models) => {
    Rule.belongsTo(models.user, {
      foreignKey: {
        name: 'personId',
        allowNull: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'user',
          key: 'id',
        },
        validate: {
          validateGroupId(value) {
            if (value && this.groupId) {
              throw new Error('Must have a personId or a groupId, but not both.');
            }
            if (!value && !this.groupId) {
              throw new Error('Must have a personId or a groupId.');
            }
          },
          validatePersonParticipationRate(value) {
            if (value && parseInt(this.participationRate) !== 100) {
              throw new Error('A person-related rule must have a participation rate of 100%.')
            }
          }
        }
      },
      onDelete: 'CASCADE',
    });

    Rule.belongsTo(models.factor, {
      foreignKey: {
        name: 'factorId',
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: 'factor',
          key: 'id',
        }
      },
      onDelete: 'CASCADE',
    });

    Rule.belongsTo(models.group, {
      foreignKey: {
        name: 'groupId',
        allowNull: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'group',
          key: 'id',
        },
        validate: {
          validateGroupId(value) {
            if (value && this.personId) {
              throw new Error('Must have a personId or a groupId, but not both.');
            }
            if (!value && !this.personId) {
              throw new Error('Must have a personId or a groupId.');
            }
          }
        }
      },
      onDelete: 'CASCADE',
    })
  };

  return Rule;
};
