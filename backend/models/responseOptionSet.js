
// This table is very basic and referenced only from other tables at the moment.
// However, it allows reusability of responses for multiple factors.
module.exports = (sequelize, DataTypes) => {
  const ResponseOptionSet = sequelize.define('responseOptionSet', {
  }, {
    timestamps: false,
  });
  ResponseOptionSet.associate = function (models) {
    ResponseOptionSet.hasMany(models.responseOption);
    ResponseOptionSet.hasMany(models.factor);
    ResponseOptionSet.hasMany(models.theme);
  };
  return ResponseOptionSet;
};
