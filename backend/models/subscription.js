
module.exports = (sequelize, DataTypes) => {
  const Subscription = sequelize.define('subscription', {
    subscription: DataTypes.JSON,
    date: DataTypes.DATE
  }, {
    timestamps: false,
  });
  Subscription.associate = function (models) {
    Subscription.belongsTo(models.user, {
      onDelete: 'CASCADE',
    });
  };
  return Subscription;
};