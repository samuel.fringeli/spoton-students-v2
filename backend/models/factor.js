module.exports = (sequelize, DataTypes) => {
  const Factor = sequelize.define('factor', {
    name: DataTypes.STRING
  }, {
    timestamps: false,
  });
  Factor.associate = function (models) {
    Factor.belongsTo(models.theme, {
      onDelete: "CASCADE",
    });
    Factor.belongsTo(models.responseOptionSet, {
      onDelete: "CASCADE",
    });
    Factor.hasMany(models.userFactorResponse);
    Factor.hasMany(models.userThemeResponse);
    Factor.hasMany(models.factorEnterprise,  { onDelete: 'CASCADE', hooks: true});
  };
  
  return Factor;
};
