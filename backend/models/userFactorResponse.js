const { Op } = require("sequelize");

module.exports = (sequelize, DataTypes, QueryTypes) => {
  const UserFactorResponse = sequelize.define('userFactorResponse', {
    date: {
      type: DataTypes.DATEONLY,
      defaultValue: DataTypes.NOW,
    },
    datetime: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    period: {
      type: DataTypes.ENUM('morning', 'afternoon', 'day')
    },
    isFromGuided: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    timestamps: false,
    indexes: [
      {
        unique: true,
        fields: ['date', 'userId', 'factorId', 'period'],
      },
    ]
  });

  UserFactorResponse.associate = function (models) {
    UserFactorResponse.belongsTo(models.responseOption, {
      onDelete: "CASCADE",
    });
    UserFactorResponse.belongsTo(models.room, {
      onDelete: "SET NULL",
    });
    UserFactorResponse.belongsTo(models.user, {
      onDelete: "CASCADE",
    });
    UserFactorResponse.belongsTo(models.factor, {
      onDelete: "CASCADE",
    });
  };

  function getFactorIfSelectedbyEnterprise(userId, factorId) {
    return sequelize.models.user.findOne({
      where: {
        id: userId
      }
    }).then((user) => sequelize.models.factor.findOne({
      where: {
        id: factorId
      },
      include: [{
        model: sequelize.models.factorEnterprise,
        required: true,
        where: {
          enterpriseId: user.enterpriseId
        }
      }]
    })).then((factor) => {
      if (factor)
        return factor;
      else
        throw "Cannot vote for this factor because the factor isn't selected !";
    });
  }

  function checkVotedFactorsByThemeDateUserAndPeriod(themeId, model, options) {
    return sequelize.models.factor.count({ // Check if a factor has already been voted for this theme
      where: {
        themeId: themeId
      },
      include: [{
        model: sequelize.models.userFactorResponse,
        where: {
          id: {
            [Op.ne]: model.id
          },
          date: model.date,
          period: model.period,
          userId: model.userId
        }
      }]
    }).then((count) => {
      if (count == 0) {
        return (model, options);
      } else {
        throw "Cannot vote for this factor because a factor has already been voted for this theme !"
      }
    });
  }

  UserFactorResponse.beforeCreate((model, options) => {
    return getFactorIfSelectedbyEnterprise(model.userId, model.factorId)
      .then((factor) => checkVotedFactorsByThemeDateUserAndPeriod(factor.themeId, model, options));
  });

  UserFactorResponse.beforeUpdate((model, options) => {
    return getFactorIfSelectedbyEnterprise(model.userId, model.factorId)
      .then((factor) => checkVotedFactorsByThemeDateUserAndPeriod(factor.themeId, model, options))
  });

  UserFactorResponse.beforeUpsert((values, options) => {
    //Cannot be used, because the primary keys do not relate to the keys: "date", "period" and "userId"
    return Promise.reject("UserFactorResponse: \"upsert\" cannot be used, please use \"create\" or \"update\" methods !");
  });

  return UserFactorResponse;
};
