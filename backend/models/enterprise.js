const factorEnterpriseTableName = 'factorEnterprise';
const factorTableName = 'factor';

module.exports = (sequelize, DataTypes) => {
  const Enterprise = sequelize.define('enterprise', {
    name: DataTypes.STRING,
  }, {
    timestamps: false,
    hooks: {
      afterCreate: (enterprise, options) => {
        if (!options.transaction) {
          enterprise.destroy(); //Destroy the enterprise if no transaction has been set !
          throw "No transaction is defined for the enterprise's creation ! The enterprise has been destroyed !";
        }

        // Select all factors for the enterprise with a single raw query
        return sequelize.query(
          `INSERT INTO ${factorEnterpriseTableName} (enterpriseId, factorId, createdAt)
           SELECT ${enterprise.id}, id, now() FROM ${factorTableName};`, {
          type: sequelize.QueryTypes.INSERT,
          transaction: options.transaction
        });

        // // Select all factor for the enterprise with two queries
        // return sequelize.models.factor.findAll({
        //   attributes: ["id"],
        //   transaction: options.transaction
        // }).map((factor) => { 
        //   return {
        //     enterpriseId: enterprise.id,
        //     factorId: factor.id
        //   };
        // }).then((values) => sequelize.models.factorEnterprise.bulkCreate(values, { transaction: options.transaction }));
      }
    }
  });

  Enterprise.associate = (models) => {
    Enterprise.hasMany(models.group);
    Enterprise.hasMany(models.user);


    // Enterprise.belongsToMany(models.factor, {
    //   through: 'factorEnterprise',
    //   foreignKey: 'enterpriseId',
    //   onDelete: 'CASCADE'
    // });
    Enterprise.hasMany(models.factorEnterprise);
  };
  return Enterprise;
};
