'use strict';

module.exports = (sequelize, DataTypes) => {
    const Pdf = sequelize.define('pdf', {
        pdfId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        groupId: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'userGroup',
                key: 'id'
            },
            onDelete: 'CASCADE',
            validate: {
                userGroupExclusive(value) {
                    if ((value === null && this.userId === null) ||
                        (value !== null && this.userId !== null)) {
                        throw new Error('Either groupId or userId must be set, but not both.');
                    }
                }
            }
        },
        userId: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'user',
                key: 'id'
            },
            onDelete: 'CASCADE',
            validate: {
                userGroupExclusive(value) {
                    if ((value === null && this.groupId === null) ||
                        (value !== null && this.groupId !== null)) {
                        throw new Error('Either userId or groupId must be set, but not both.');
                    }
                }
            }
        },
        url: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        createdAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        }
    });

    Pdf.associate = function(models) {
        // associations can be defined here
        Pdf.belongsTo(models.userGroup, {
            foreignKey: 'groupId',
            as: 'userGroup'
        });

        Pdf.belongsTo(models.user, {
            foreignKey: 'userId',
            as: 'user'
        });
    };

    return Pdf;
};
