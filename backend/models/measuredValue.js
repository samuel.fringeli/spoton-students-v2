
module.exports = (sequelize, DataTypes) => {
  // unused
  const MeasuredValue = sequelize.define('measuredValue', {
    date: DataTypes.DATE,
    value: DataTypes.INTEGER,
  }, {
    timestamps: false,
  });
  MeasuredValue.associate = function (models) {
    MeasuredValue.belongsTo(models.room, {
      onDelete: "CASCADE",
    });
    MeasuredValue.belongsTo(models.theme, {
      onDelete: "CASCADE",
    });
  };
  return MeasuredValue;
};
