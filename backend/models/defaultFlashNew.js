'use strict'

module.exports = (sequelize, DataTypes) => {
    const DefaultFlashNew = sequelize.define('defaultFlashNew', {
        analyseType: {
            type: DataTypes.ENUM('correlation', 'clustering'),
            allowNull: false,
            validate: {
                isCorrelation(value) {
                    if (value === 'correlation') {
                        if (!this.period2 || (!this.factorId2 || !this.themeId2) || (!this.userId2 || !this.groupId2)) {
                            throw new Error('All fields concerning period2 must be filled when analyseType is "correlation".');
                        }
                    }
                },
            },
        },
        title: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        frequency: {
            type: DataTypes.ENUM('daily', 'weekly', 'monthly'),
            allowNull: false
        },
        period1: {
            type: DataTypes.ENUM('daily', 'weekly', 'monthly'),
            allowNull: false
        },
        userGroupType1: {
            type: DataTypes.ENUM('user', 'group'),
            allowNull: false
        },
        factorId1: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'factor',
                key: 'id'
            },
            validate: {
                exclusiveFactorTheme(value) {
                    if (value === null && this.themeId1 === null) {
                        throw new Error('One of factorId1 or themeId1 must be set');
                    }
                    if (value !== null && this.themeId1 !== null) {
                        throw new Error('Cannot set both factorId1 and themeId1 at the same time');
                    }
                },
            },
        },
        themeId1: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'theme',
                key: 'id'
            },
            validate: {
                exclusiveFactorTheme(value) {
                    if (value === null && this.factorId1 === null) {
                        throw new Error('One of themeId1 or factorId1 must be set');
                    }
                    if (value !== null && this.factorId1 !== null) {
                        throw new Error('Cannot set both factorId1 and themeId1 at the same time');
                    }
                },
            },
        },
        period2: {
            type: DataTypes.ENUM('daily', 'weekly', 'monthly'),
            allowNull: true
        },
        userGroupType2: {
            type: DataTypes.ENUM('user', 'group'),
            allowNull: false
        },
        factorId2: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'factor',
                key: 'id'
            },
            validate: {
                exclusiveFactorTheme(value) {
                    if (value !== null && this.themeId2 !== null) {
                        throw new Error('Cannot set both factorId2 and themeId2 at the same time');
                    }
                },
            },
        },
        themeId2: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'theme',
                key: 'id'
            },
            validate: {
                exclusiveFactorTheme(value) {
                    if (value !== null && this.factorId2 !== null) {
                        throw new Error('Cannot set both factorId2 and themeId2 at the same time');
                    }
                },
            },
        }
    });

    DefaultFlashNew.associate = function(models) {
        DefaultFlashNew.belongsTo(models.factor, {
            foreignKey: 'factorId1',
            as: 'factor1',
            onDelete: 'CASCADE'
        });
        DefaultFlashNew.belongsTo(models.factor, {
            foreignKey: 'factorId2',
            as: 'factor2',
            onDelete: 'CASCADE'
        });
        DefaultFlashNew.belongsTo(models.theme, {
            foreignKey: 'themeId1',
            as: 'theme1',
            onDelete: 'CASCADE'
        });
        DefaultFlashNew.belongsTo(models.theme, {
            foreignKey: 'themeId2',
            as: 'theme2',
            onDelete: 'CASCADE'
        });
    };

    return DefaultFlashNew;
};
