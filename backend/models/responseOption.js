
module.exports = (sequelize, DataTypes) => {
  const ResponseOption = sequelize.define('responseOption', {
    value: DataTypes.INTEGER, // Ordre d'affichage
    label: DataTypes.STRING,
    description: DataTypes.STRING,
    polarizedValue: {
      type: DataTypes.INTEGER,
      validate: {
        min: -1,
        max: 1
      }
    }
  }, {
    timestamps: false,
  });
  ResponseOption.associate = function (models) {
    ResponseOption.belongsTo(models.responseOptionSet, {
      onDelete: "CASCADE",
    });
  };
  return ResponseOption;
};
