module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    username: { type: DataTypes.STRING, unique: true, allowNull: false },
    email: { type: DataTypes.STRING, unique: true, allowNull: false },
    token: DataTypes.STRING,
    points: DataTypes.INTEGER,
    sex: {
      type: DataTypes.ENUM('f', 'm')
    },
    activatePopup: DataTypes.BOOLEAN,
    role: DataTypes.INTEGER,
    activationToken: {type: DataTypes.INTEGER, allowNull: true, unique: true, },
    activated: {type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false, },
    azure: {type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false, },
  }, {
    timestamps: false,
  });
  User.associate = function (models) {
    User.belongsTo(models.enterprise, {
      onDelete: "CASCADE",
    });
    User.belongsToMany(models.group, {
      through: "userGroup",
      foreignKey: "userId",
      onDelete: 'CASCADE',
    });
  };
  return User;
};
