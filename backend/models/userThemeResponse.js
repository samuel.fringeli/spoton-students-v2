
module.exports = (sequelize, DataTypes, QueryTypes) => {
  const UserThemeResponse = sequelize.define('userThemeResponse', {
    date: {
      type: DataTypes.DATEONLY,
      defaultValue: DataTypes.NOW,
    },
    datetime: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    period: {
      type: DataTypes.ENUM('morning', 'afternoon', 'day')
    },
  }, {
    timestamps: false,
    indexes: [
      {
        unique: true,
        fields: ['date', 'userId', 'themeId', 'period'],
      },
    ]
  });

  UserThemeResponse.associate = function (models) {
    UserThemeResponse.belongsTo(models.responseOption, {
      onDelete: "CASCADE",
    });
    UserThemeResponse.belongsTo(models.user, {
      onDelete: "CASCADE",
    });
    UserThemeResponse.belongsTo(models.theme, {
      onDelete: "CASCADE",
    });
    UserThemeResponse.belongsTo(models.factor, {
      onDelete: "CASCADE",
    });
    UserThemeResponse.belongsTo(models.room);

  };

  function checkIfThemeIsSelectedbyEnterprise(values, options) {
    return sequelize.models.user.findOne({
      where: {
        id: values.userId
      }
    }).then((user) => sequelize.models.theme.count({
        where: {
          id: values.themeId
        },
        include: [{
          model: sequelize.models.factor,
          required: true,
          include: [{
            model: sequelize.models.factorEnterprise,
            required: true,
            where: {
              enterpriseId: user.enterpriseId
            }
          }]
        }]
      })
    ).then((count) => {
      if(count == 0) {
        throw "Cannot vote for this factor because the theme isn't selected !";
      } else {
        return (values, options);
      }
    });
  }

  UserThemeResponse.beforeCreate((model, options) => checkIfThemeIsSelectedbyEnterprise(model, options));

  UserThemeResponse.beforeUpdate((model, options) => checkIfThemeIsSelectedbyEnterprise(model, options));

  UserThemeResponse.beforeUpsert((values, options) => checkIfThemeIsSelectedbyEnterprise(values, options));

  return UserThemeResponse;
};
