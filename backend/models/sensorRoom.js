"use strict";
module.exports = (sequelize, DataTypes) => {
  const SensorRoom = sequelize.define(
    "sensorRoom",
    {
      // primary key on roomId & sensorId: it is enough, as each sensorType of the same physical
      // sensor has a different sensorId -> in one room, we can have many sensors with same sensorType
      // (e.g. 3 sensors for the temperature)
      roomId: DataTypes.INTEGER,
      sensorId: DataTypes.INTEGER,
      sensorType: DataTypes.ENUM('temperature', 'humidity', 'luminosity', 'motion', 'noise', 'pressure', 'CO2')
    },
    {
      timestamps: false,
    }
  );
  SensorRoom.associate = function (models) {
    SensorRoom.belongsTo(models.room, {
      onDelete: "CASCADE",
    });
  };
  return SensorRoom;
};
