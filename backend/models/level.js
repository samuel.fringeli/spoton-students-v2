module.exports = (sequelize, DataTypes) => {
  const Level = sequelize.define('level', {
    name: DataTypes.STRING,
    fromNbr: DataTypes.INTEGER
  }, {
    timestamps: false,
  });
  Level.associate = function (models) {
  };
  return Level;
};
