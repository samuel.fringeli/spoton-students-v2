
module.exports = (sequelize, DataTypes) => {
  const Room = sequelize.define('room', {
    name: DataTypes.STRING,
  }, {
    timestamps: false,
  });
  Room.associate = function (models) {
    Room.belongsTo(models.enterprise, {
      onDelete: "CASCADE",
    });
  };
  return Room;
};
