'use strict';
module.exports = (sequelize, DataTypes) => {
  const FactorEnterprise = sequelize.define('factorEnterprise', {
    enterpriseId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    factorId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    }
  }, {
    updatedAt: false
  });

  FactorEnterprise.removeAttribute('id');
  
  FactorEnterprise.associate = function(models) {
    // associations can be defined here
    FactorEnterprise.belongsTo(models.factor, {
      onDelete: 'CASCADE',
      foreignKey: 'factorId'
    });

    FactorEnterprise.belongsTo(models.enterprise, {
      onDelete: 'CASCADE',
      foreignKey: 'enterpriseId'
    });
  };

  return FactorEnterprise;
};