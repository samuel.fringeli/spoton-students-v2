'use strict';
module.exports = (sequelize, DataTypes) => {
  const Group = sequelize.define('group', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
  }, {
    timestamps: false
  });

  Group.associate = function (models) {
    Group.belongsTo(models.enterprise, {
      onDelete: 'CASCADE',
    });
    Group.belongsToMany(models.user, {
      through: 'userGroup',
      foreignKey: 'groupId',
      onDelete: 'CASCADE',
    });
  };
  return Group;
};