'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserGroup = sequelize.define('userGroup', {
    status: DataTypes.ENUM('pending', 'accepted'),
    role: DataTypes.ENUM('admin', 'member'),
    groupId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
  }, {
    timestamps: false
  });
  UserGroup.associate = function (models) {
    UserGroup.belongsTo(models.user, {
      onDelete: 'CASCADE',
    });
    UserGroup.belongsTo(models.group, {
      onDelete: 'CASCADE',
    });
  };
  return UserGroup;
};