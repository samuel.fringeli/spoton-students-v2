
module.exports = (sequelize, DataTypes) => {
  const Theme = sequelize.define('theme', {
    name: DataTypes.STRING,
    icon: DataTypes.STRING,
    description: DataTypes.STRING,
    question: DataTypes.STRING,
    questionFactor: DataTypes.STRING,
  }, {
    timestamps: false,
  });
  
  Theme.associate = (models) => {
    Theme.hasMany(models.factor);
    Theme.belongsTo(models.responseOptionSet, {
      onDelete: "CASCADE",
    });
  };

  return Theme;
};
