module.exports = (sequelize, DataTypes) => {
  const Alarm = sequelize.define(
    "alarm",
    {
      messageJSON: {
        allowNull: false,
        type: DataTypes.JSON,
      },
      notificationStatus: {
        // informé par le notification handler
        allowNull: false,
        type: DataTypes.ENUM("received", "sent", "not_sent"),
        defaultValue: "not_sent",
      },
      chatMessageStatus: {
        // informé par le notification handler
        allowNull: false,
        type: DataTypes.ENUM("received", "sent", "not_sent"),
        defaultValue: "not_sent",
      },
      state: {
        allowNull: true,
        type: DataTypes.ENUM("read", "false_alarm", "acted"), // acted = on a pris une décision
        validate: {
          stateDateMandatory(value) {
            if (
              value &&
              (value.toLowerCase() === "acted" ||
                value.toLowerCase() === "false_alarm") &&
              !this.stateDate
            ) {
              throw new Error('When state is "acted", stateDate is mandatory.');
            }
          },
          stateMessageMandatory(value) {
            if (
              value &&
              value.toLowerCase() === "acted" &&
              !this.stateMessage
            ) {
              throw new Error(
                'When state is "acted", stateMessage is mandatory.'
              );
            }
          },
        },
      },
      stateDate: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      stateMessage: {
        allowNull: true,
        type: DataTypes.STRING,
        validate: {
          correctStateCheck(value) {
            if (
              value &&
              value !== "" &&
              !(this.state === "false_alarm" || this.state === "acted")
            ) {
              throw new Error(
                "You can't change a stateMessage when the state is not acted or false_alarm"
              );
            }
          },
        },
      },
    },
    {
      timestamps: true,
    }
  );

  Alarm.associate = (models) => {
    Alarm.belongsTo(models.rule, {
      foreignKey: {
        name: "ruleId",
        allowNull: true,
        type: DataTypes.INTEGER,
        references: {
          model: "rule",
          key: "id",
        },
      },
      onDelete: "CASCADE",
    });
  };

  return Alarm;
};
