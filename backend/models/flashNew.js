'use strict';

module.exports = (sequelize, DataTypes) => {
    const FlashNew = sequelize.define('flashNew', {
        analyseType: {
            type: DataTypes.ENUM('correlation', 'clustering'),
            allowNull: false,
            validate: {
                isCorrelation(value) {
                    if (value === 'correlation') {
                        if (!this.period2 || (!this.factorId2 || !this.themeId2) || (!this.userId2 || !this.groupId2)) {
                            throw new Error('All fields concerning period2 must be filled when analyseType is "correlation".');
                        }
                    }
                },
            },
        },
        title: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        frequency: {
            type: DataTypes.ENUM('daily', 'weekly', 'monthly'),
            allowNull: false
        },
        period1: {
            type: DataTypes.ENUM('daily', 'weekly', 'monthly'),
            allowNull: false
        },
        factorId1: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'factor',
                key: 'id'
            },
            validate: {
                exclusiveFactorTheme(value) {
                    if (value === null && this.themeId1 === null) {
                        throw new Error('One of factorId1 or themeId1 must be set');
                    }
                    if (value !== null && this.themeId1 !== null) {
                        throw new Error('Cannot set both factorId1 and themeId1 at the same time');
                    }
                }
            }
        },
        themeId1: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'theme',
                key: 'id'
            },
            validate: {
                exclusiveFactorTheme(value) {
                    if (value === null && this.factorId1 === null) {
                        throw new Error('One of themeId1 or factorId1 must be set');
                    }
                    if (value !== null && this.factorId1 !== null) {
                        throw new Error('Cannot set both factorId1 and themeId1 at the same time');
                    }
                }
            }
        },
        userId1: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'user',
                key: 'id'
            },
            validate: {
                exclusiveUserGroup(value) {
                    if (value === null && this.groupId1 === null) {
                        throw new Error('One of userId1 or groupId1 must be set');
                    }
                    if (value !== null && this.groupId1 !== null) {
                        throw new Error('Cannot set both userId1 and groupId1 at the same time');
                    }
                }
            }
        },
        groupId1: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'userGroup',
                key: 'id'
            },
            validate: {
                exclusiveUserGroup(value) {
                    if (value === null && this.userId1 === null) {
                        throw new Error('One of groupId1 or userId1 must be set');
                    }
                    if (value !== null && this.userId1 !== null) {
                        throw new Error('Cannot set both groupId1 and userId1 at the same time');
                    }
                }
            }
        },
        period2: {
            type: DataTypes.ENUM('daily', 'weekly', 'monthly'),
            allowNull: true
        },
        factorId2: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'factor',
                key: 'id'
            },
            validate: {
                exclusiveFactorTheme(value) {
                    if (value !== null && this.themeId2 !== null) {
                        throw new Error('Cannot set both factorId2 and themeId2 at the same time');
                    }
                }
            }
        },
        themeId2: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'theme',
                key: 'id'
            },
            validate: {
                exclusiveFactorTheme(value) {
                    if (value !== null && this.factorId2 !== null) {
                        throw new Error('Cannot set both factorId2 and themeId2 at the same time');
                    }
                }
            }
        },
        userId2: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'user',
                key: 'id'
            },
            validate: {
                exclusiveUserGroup(value) {
                    if (value !== null && this.groupId2 !== null) {
                        throw new Error('Cannot set both userId2 and groupId2 at the same time');
                    }
                }
            }
        },
        groupId2: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'userGroup',
                key: 'id'
            },
            validate: {
                exclusiveUserGroup(value) {
                    if (value !== null && this.userId2 !== null) {
                        throw new Error('Cannot set both groupId2 and userId2 at the same time');
                    }
                }
            }
        },
        payload: {
            type: DataTypes.JSON
        },
        defaultFlashNewId: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'defaultFlashNew',
                key: 'id'
            }
        }
    });

    FlashNew.associate = function(models) {
        FlashNew.belongsTo(models.defaultFlashNew, {
            foreignKey: 'defaultFlashNewId',
            as: 'defaultFlashNew',
            onDelete: 'CASCADE'
        });

        FlashNew.belongsTo(models.factor, {
            foreignKey: 'factorId1',
            as: 'factor1',
            onDelete: 'CASCADE'
        });
        FlashNew.belongsTo(models.factor, {
            foreignKey: 'factorId2',
            as: 'factor2',
            onDelete: 'CASCADE'
        });

        FlashNew.belongsTo(models.theme, {
            foreignKey: 'themeId1',
            as: 'theme1',
            onDelete: 'CASCADE'
        });
        FlashNew.belongsTo(models.theme, {
            foreignKey: 'themeId2',
            as: 'theme2',
            onDelete: 'CASCADE'
        });

        FlashNew.belongsTo(models.user, {
            foreignKey: 'userId1',
            as: 'user1',
            onDelete: 'CASCADE'
        });
        FlashNew.belongsTo(models.user, {
            foreignKey: 'userId2',
            as: 'user2',
            onDelete: 'CASCADE'
        });

        FlashNew.belongsTo(models.userGroup, {
            foreignKey: 'groupId1',
            as: 'group1',
            onDelete: 'CASCADE'
        });
        FlashNew.belongsTo(models.userGroup, {
            foreignKey: 'groupId2',
            as: 'group2',
            onDelete: 'CASCADE'
        });
    };

    return FlashNew;
};
