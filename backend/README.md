# SooZ back-end

The backend is launched automatically with ``docker-compose`` (cf. main README).

## References
- [Relations and associations between classes]( https://sequelize.readthedocs.io/en/2.0/docs/associations/#belongs-to-many-associations )
- [Sequelize Migrations—Setting Up Associations](https://codeburst.io/sequelize-migrations-setting-up-associations-985d29b61ee7)
- [REST API with Sequelize and Express.js](https://medium.com/infocentric/setup-a-rest-api-with-sequelize-and-express-js-fae06d08c0a7)
- [Sequelize](http://docs.sequelizejs.com/manual/)
- [Fixtures reference](https://www.npmjs.com/package/sequelize-fixtures)
- [Terms Definitions and application](https://www.duringthedrive.com/2017/05/06/models-migrations-sequelize-node/)
- [Kitematic for local db](https://kitematic.com/)
- [Workbench for db GUI](https://www.mysql.com/fr/products/workbench/)
