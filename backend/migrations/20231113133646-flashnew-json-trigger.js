'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.sequelize.query(`
      CREATE TRIGGER trigger_populate_payload
      BEFORE INSERT ON flashNew
      FOR EACH ROW
      BEGIN
        IF NEW.analyseType = 'correlation' THEN
          SET NEW.payload = JSON_OBJECT(
            'start_date1', '',
            'end_date1', '',
            'factor_theme_type1', IF(NEW.factorId1 IS NOT NULL, 'factor', 'theme'),
            'factor_theme_id1', COALESCE(NEW.factorId1, NEW.themeId1),
            'user_group_type1', IF(NEW.userId1 IS NOT NULL, 'user', 'group'),
            'user_group_id1', COALESCE(NEW.userId1, NEW.groupId1),
            'start_date2', '',
            'end_date2', '',
            'factor_theme_type2', IF(NEW.factorId2 IS NOT NULL, 'factor', 'theme'),
            'factor_theme_id2', COALESCE(NEW.factorId2, NEW.themeId2),
            'user_group_type2', IF(NEW.userId2 IS NOT NULL, 'user', 'group'),
            'user_group_id2', COALESCE(NEW.userId2, NEW.groupId2),
            'visualize', false
          );
        ELSEIF NEW.analyseType = 'clustering' THEN
          SET NEW.payload = JSON_OBJECT(
            'start_date1', '',
            'end_date1', '',
            'factor_theme_type1', IF(NEW.factorId1 IS NOT NULL, 'factor', 'theme'),
            'factor_theme_id1', COALESCE(NEW.factorId1, NEW.themeId1),
            'user_group_type1', IF(NEW.userId1 IS NOT NULL, 'user', 'group'),
            'user_group_id1', COALESCE(NEW.userId1, NEW.groupId1)
          );
        END IF;
      END;
    `);
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.sequelize.query(`
      DROP TRIGGER IF EXISTS trigger_populate_payload;
    `);
    }
};
