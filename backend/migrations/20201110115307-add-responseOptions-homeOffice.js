'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("responseOption", [
      {
        value: 1,
        label: 'Stressé',
        responseOptionSetId: 8,
        description: '',
      },
      {
        value: 2,
        label: 'Ennuyé',
        responseOptionSetId: 8,
        description: '',
      },
      {
        value: 3,
        label: 'Satisfait',
        responseOptionSetId: 8,
        description: '',
      },
      {
        value: 4,
        label: 'Heureux',
        responseOptionSetId: 8,
        description: '',
      },
    ])

  },

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('responseOption', {responseOptionSetId: 8,}, {})
};
