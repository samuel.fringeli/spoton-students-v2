'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // addConstraint cannot be used in a transaction, because mysql does not allow rollbacks on alter table actions
    // https://dev.mysql.com/doc/refman/8.0/en/cannot-roll-back.html
    return  queryInterface.addConstraint("theme",{
      type: "FOREIGN KEY",
      fields: ["responseOptionSetId"],
      name: "theme_ibfk_1", // explicit constraint name (for down function) but should be the same as the default name
      defaultValue: 1,
      references: {
        table: "responseOptionSet",
        field: "id",
      },
      onDelete: "CASCADE",
    });
  },
  async down (queryInterface, Sequelize) {
    await queryInterface.removeConstraint("theme", "theme_ibfk_1")
    // mysql create automatically an index on FK, so we need to delete it too
    await queryInterface.removeIndex("theme", "theme_ibfk_1");
  }
};
