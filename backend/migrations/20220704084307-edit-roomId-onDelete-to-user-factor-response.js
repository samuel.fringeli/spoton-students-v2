'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint("userFactorResponse", "userFactorResponse_ibfk_4");
    await queryInterface.addConstraint("userFactorResponse",{
      type: "FOREIGN KEY",
      fields: ["roomId"],
      name: "userFactorResponse_ibfk_4",
      references: {
        table: "room",
        field: "id",
      },
      onDelete: "SET NULL", // SET DEFAULT raises an error, but we do not know why
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint("userFactorResponse", "userFactorResponse_ibfk_4");
    await queryInterface.addConstraint("userFactorResponse",{
      type: "FOREIGN KEY",
      fields: ["roomId"],
      name: "userFactorResponse_ibfk_4",
      references: {
        table: "room",
        field: "id",
      },
      // without onDelete: SET NULL
    });
  }
};
