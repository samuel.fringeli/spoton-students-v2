'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint("group", "group_ibfk_1");
    await queryInterface.addConstraint("group",{
      type: "FOREIGN KEY",
      fields: ["enterpriseId"],
      name: "group_ibfk_1",
      references: {
        table: "enterprise",
        field: "id",
      },
      onDelete: "CASCADE",
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint("group", "group_ibfk_1");
    await queryInterface.addConstraint("group",{
      type: "FOREIGN KEY",
      fields: ["enterpriseId"],
      name: "group_ibfk_1",
      references: {
        table: "enterprise",
        field: "id",
      },
      // without onDelete: CASCADE
    });
  }
};
