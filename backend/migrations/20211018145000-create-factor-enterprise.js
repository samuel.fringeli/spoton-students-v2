'use strict';
const {enterprise, factor} = require("../models");

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable('factorEnterprise', {
        enterpriseId: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          allowNull: false,
          references: {
            model: 'enterprise',
            key: 'id',
            onDelete: 'CASCADE'
          }
        },
        factorId: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          allowNull: false,
          // `onDelete: 'CASCADE'` should be here
          references: {
            model: 'factor',
            key: 'id',
            // !!! this is the wrong location to put the onDelete instruction.
            // It is updated in the migration 20220403110111-update-fk-factor-enterprise.js
            onDelete: 'CASCADE'
          }
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }, { transaction })
      const enterpriseIds = await enterprise.findAll({attributes: ["id"]}).map(enterprise => enterprise.dataValues.id)
      const factorIds = await factor.findAll({attributes: ["id"]}).map(factor => factor.dataValues.id)
      const records = []

      for (let i = 0; i < enterpriseIds.length; i++) {
        for (let j = 0; j < factorIds.length; j++) {
          records.push({
            enterpriseId: enterpriseIds[i],
            factorId: factorIds[j],
            createdAt: Sequelize.fn('NOW')
          })
        }
      }
      await queryInterface.bulkInsert('factorEnterprise', records, { transaction })

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }

  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('factorEnterprise')
};