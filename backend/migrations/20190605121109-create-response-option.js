'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable('responseOption', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        value: {
          allowNull: false,
          type: Sequelize.INTEGER,
        },
        label: {
          allowNull: false,
          type: Sequelize.STRING,
        },
        description: {
          allowNull: true,
          type: Sequelize.STRING,
        },
        responseOptionSetId: {
          allowNull: false,
          type: Sequelize.INTEGER,
          onDelete: 'CASCADE',
          references: {
            model: 'responseOptionSet',
            key: 'id',
          },
        },
        polarizedValue: {
          allowNull: true,
          type: Sequelize.INTEGER,
        },
      }, { transaction })
      await queryInterface.bulkInsert('responseOption', [
        {
          value: 1,
          label: 'Stressé',
          responseOptionSetId: 1,
          description: '',
        },
        {
          value: 2,
          label: 'Ennuyé',
          responseOptionSetId: 1,
          description: '',
        },
        {
          value: 3,
          label: 'Satisfait',
          responseOptionSetId: 1,
          description: '',
        },
        {
          value: 4,
          label: 'Heureux',
          responseOptionSetId: 1,
          description: '',
        },
        {
          value: 1,
          label: 'Excédé',
          responseOptionSetId: 2,
          description: '',
        },
        {
          value: 2,
          label: 'Frustré',
          responseOptionSetId: 2,
          description: '',
        },
        {
          value: 3,
          label: 'Satisfait',
          responseOptionSetId: 2,
          description: '',
        },
        {
          value: 4,
          label: 'Heureux',
          responseOptionSetId: 2,
          description: '',
        },
        {
          value: 1,
          label: 'Super mal',
          responseOptionSetId: 7,
          description: '',
        },
        {
          value: 2,
          label: 'Mal',
          responseOptionSetId: 7,
          description: '',
        },
        {
          value: 3,
          label: 'Bien',
          responseOptionSetId: 7,
          description: '',
        },
        {
          value: 4,
          label: 'Super bien',
          responseOptionSetId: 7,
          description: '',
        },
        {
          value: 1,
          label: 'Trop froide',
          responseOptionSetId: 3,
          description: '',
        },
        {
          value: 2,
          label: 'Satisfaisante froide',
          responseOptionSetId: 3,
          description: '',
        },
        {
          value: 3,
          label: 'Optimale',
          responseOptionSetId: 3,
          description: ''
        },
        {
          value: 4,
          label: 'Satisfaisante chaude',
          responseOptionSetId: 3,
          description: '',
        },
        {
          value: 5,
          label: 'Trop chaude',
          responseOptionSetId: 3,
          description: '',
        },
        {
          value: 1,
          label: 'Irrespirable',
          responseOptionSetId: 4,
          description: '',
        },
        {
          value: 2,
          label: 'Satisfaisante',
          responseOptionSetId: 4,
          description: '',
        },
        {
          value: 3,
          label: 'Optimale',
          responseOptionSetId: 4,
          description: '',
        },
        {
          value: 1,
          label: 'Trop silencieuse',
          responseOptionSetId: 5,
          description: '',
        },
        {
          value: 2,
          label: 'Satisfaisante silencieuse',
          responseOptionSetId: 5,
          description: '',
        },
        {
          value: 3,
          label: 'Optimale',
          responseOptionSetId: 5,
          description: '',
        },
        {
          value: 4,
          label: 'Satisfaisante bruyante',
          responseOptionSetId: 5,
          description: '',
        },
        {
          value: 5,
          label: 'Trop bruyante',
          responseOptionSetId: 5,
          description: '',
        },
        {
          value: 1,
          label: 'Trop faible',
          responseOptionSetId: 6,
          description: '',
        },
        {
          value: 2,
          label: 'Satisfaisante faible',
          responseOptionSetId: 6,
          description: '',
        },
        {
          value: 3,
          label: 'Optimale',
          responseOptionSetId: 6,
          description: '',
        },
        {
          value: 4,
          label: 'Satisfaisante intense',
          responseOptionSetId: 6,
          description: '',
        },
        {
          value: 5,
          label: 'Trop intense',
          responseOptionSetId: 6,
          description: '',
        },
      ], { transaction })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  down: (queryInterface) => queryInterface.dropTable('responseOption'),
};


