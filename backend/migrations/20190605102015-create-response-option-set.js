'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        const transaction = await queryInterface.sequelize.transaction();
        try {
            await queryInterface.createTable('responseOptionSet', {
                id: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER,
                },
            }, { transaction })
            await queryInterface.bulkInsert('responseOptionSet', [
                {
                    // work stressed/bored/satisfied/happy
                    id: 1
                },
                {
                    // social overwhelmed/frustrated/satisfied/happy
                    id: 2
                },
                {
                    // temperature
                    id: 3
                },
                {
                    // smell
                    id: 4
                },
                {
                    // sound level
                    id: 5
                },
                {
                    // luminosity
                    id: 6
                },
                {
                    // general well being very bad/bad/good/very good
                    id: 7
                }
            ], { transaction })
            /**
             * Create themes (which have a foreign key constraint)
             */
            await queryInterface.bulkInsert('theme', [
                {
                    name: 'Bien-être',
                    icon: 'Bien Etre icon',
                    description: 'Bien-être description',
                    question: "Comment te sens-tu aujourd'hui?",
                    responseOptionSetId: 7,
                    questionFactor: ''
                },
                {
                    name: 'Travail',
                    icon: 'emoji_emotions',
                    description: 'Ce qu\'on te demande de faire face à ce que tu penses être capable de faire et tes attentes de résultat',
                    question: 'Quel est ton ressenti lié à ton travail ?',
                    responseOptionSetId: 1,
                    questionFactor: 'Quel facteur a le plus influencé ton ressenti quant au thème du travail ?'
                },
                {
                    name: 'Social',
                    icon: 'work',
                    description: 'Le rapport que tu as avec les autres face à tes attentes.',
                    question: 'Quel est ton ressenti lié à tes relations sociales ?',
                    responseOptionSetId: 2,
                    questionFactor: 'Quel facteur a le plus influencé ton ressenti quant au thème des relations sociales ?'
                },
                {
                    name: 'Environnement physique',
                    icon: 'device_hub',
                    description: 'Les facteurs physiques (température, odeur, bruit, luminosité) influançant ton bien-être.',
                    question: 'Quel est ton ressenti lié à ton environnement ?',
                    responseOptionSetId: 2,
                    questionFactor: 'Quel facteur a le plus influencé ton ressenti quant au thème de l\'environnement ?'
                },
            ], { transaction })
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw err;
        }
    },
    async down(queryInterface) {
        const transaction = await queryInterface.sequelize.transaction();
        try {
            await queryInterface.dropTable('responseOptionSet', { transaction })
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw err;
        }
    },
};
