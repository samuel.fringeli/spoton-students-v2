'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // groupId
    await queryInterface.removeConstraint("userGroup", "userGroup_ibfk_1");
    await queryInterface.addConstraint("userGroup",{
      type: "FOREIGN KEY",
      fields: ["groupId"],
      name: "userGroup_ibfk_1",
      references: {
        table: "group",
        field: "id",
      },
      onDelete: "CASCADE",
    });
    // userId
    await queryInterface.removeConstraint("userGroup", "userGroup_ibfk_2");
    await queryInterface.addConstraint("userGroup",{
      type: "FOREIGN KEY",
      fields: ["userId"],
      name: "userGroup_ibfk_2",
      references: {
        table: "user",
        field: "id",
      },
      onDelete: "CASCADE",
    });
  },
  down: async (queryInterface, Sequelize) => {
    // groupId
    await queryInterface.removeConstraint("userGroup", "userGroup_ibfk_1");
    await queryInterface.addConstraint("userGroup",{
      type: "FOREIGN KEY",
      fields: ["groupId"],
      name: "userGroup_ibfk_1",
      references: {
        table: "group",
        field: "id",
      },
      // without onDelete: CASCADE
    });
    // userId
    await queryInterface.removeConstraint("userGroup", "userGroup_ibfk_2");
    await queryInterface.addConstraint("userGroup",{
      type: "FOREIGN KEY",
      fields: ["userId"],
      name: "userGroup_ibfk_2",
      references: {
        table: "user",
        field: "id",
      },
      // without onDelete: CASCADE
    });
  }
};
