'use strict';

const { Op } = require("sequelize");


module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("room", [
      {
        name: 'Salle 1',
        enterpriseId: 4,
      },
      {
        name: 'Salle 2',
        enterpriseId: 4,
      },
      {
        name: 'Salle 3',
        enterpriseId: 4,
      },
      {
        name: 'Salle 4',
        enterpriseId: 4,
      },
      {
        name: 'Salle 5',
        enterpriseId: 4,
      },
    ])
  },

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('room', { enterpriseId: 4, name: { [Op.in]: ["Salle 1", "Salle 2", "Salle 3", "Salle 4", "Salle 5",] } }, {})
};
