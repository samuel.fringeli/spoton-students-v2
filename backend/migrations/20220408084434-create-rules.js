'use strict';

let models = require("../models/index.js");

module.exports = {
  async up (queryInterface, Sequelize) {
    // put rule attribues in raw there if you want to modify the model later in an other migration
    await queryInterface.createTable(models.rule.tableName, models.rule.rawAttributes);
  },

  async down (queryInterface, Sequelize) {
    // put alarm attribues in raw there if you want to modify the model later in an other migration
    await queryInterface.dropTable('rule');
  }
};
