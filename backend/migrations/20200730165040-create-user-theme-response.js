module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('userThemeResponse', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    date: {
      allowNull: false,
      type: Sequelize.DATEONLY
    },
    datetime: {
      allowNull: false,
      type: Sequelize.DATE
    },
    period: {
      allowNull: false,
      type: Sequelize.ENUM('morning', 'afternoon', 'day')
    },
    userId: {
      allowNull: false,
      type: Sequelize.INTEGER,
      onDelete: 'CASCADE',
      references: {
        model: 'user',
        key: 'id',
      },
    },
    factorId: {
      allowNull: true,
      type: Sequelize.INTEGER,
      onDelete: 'CASCADE',
      references: {
        model: 'factor',
        key: 'id',
      },
    },
    responseOptionId: {
      allowNull: false,
      type: Sequelize.INTEGER,
      onDelete: 'CASCADE',
      references: {
        model: 'responseOption',
        key: 'id',
      },
    },
    roomId: {
      allowNull: true,
      type: Sequelize.INTEGER,
      references: {
        model: 'room',
        key: 'id',
      },
    },
    themeId: {
      allowNull: false,
      type: Sequelize.INTEGER,
      onDelete: 'CASCADE',
      references: {
        model: 'theme',
        key: 'id',
      },
    },
  }, {
    uniqueKeys: {
      uniqueVotePerPeriodPerDay: {
        fields: ['date', 'userId', 'themeId', 'period'],
      },
    }
  }),

  down: (queryInterface, Sequelize) => queryInterface.dropTable('userThemeResponse'),
};

