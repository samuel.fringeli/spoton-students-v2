/**
 * The seeding of the theme table is handled in the migration 
 * of the response option sets because of the foreign key constraint
 */

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('theme', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    name: {
      allowNull: false,
      type: Sequelize.STRING,
      unique: true,
    },
    description: {
      allowNull: true,
      type: Sequelize.STRING,
    },
    icon: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    question: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    responseOptionSetId: {
      allowNull: false,
      type: Sequelize.INTEGER,
      // !!! FK is missing here (updated in migration 20220523061852-add-responseOptionSetId-fk-in-theme.js)
    },
    questionFactor: {
      allowNull: false,
      type: Sequelize.STRING,
    }
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('theme'),
};
