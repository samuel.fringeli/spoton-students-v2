'use strict';

const {enterprise} = require("../models")


module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      const rosasId = (await enterprise.findOne({where: {name: "ROSAS"}, attributes: ["id"], raw: true, transaction: transaction})).id
      const raiffeisenId = (await enterprise.findOne({where: {name: "Raiffeisen Sarine-Ouest"}, attributes: ["id"], raw: true, transaction: transaction})).id

      await queryInterface.bulkInsert('room', [
          {
            name: "201",
            enterpriseId: rosasId
          },
          {
            name: "202",
            enterpriseId: rosasId
          },
          {
            name: "205",
            enterpriseId: rosasId
          },
          {
            name: "206",
            enterpriseId: rosasId
          },
          {
            name: "207",
            enterpriseId: rosasId
          },
          {
            name: "Guichet",
            enterpriseId: raiffeisenId
          },
          {
            name: "Back office",
            enterpriseId: raiffeisenId
          },
          {
            name: "Conseillers",
            enterpriseId: raiffeisenId
          },
          {
            name: "Directeur",
            enterpriseId: raiffeisenId
          },
          {
            name: "Credit",
            enterpriseId: raiffeisenId
          },
        ],
        {transaction});
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.bulkDelete('room', {
        name: ["201", "202", "205", "206", "207", "Guichet", "Back office", "Conseillers", "Directeur", "Credit"]
      }, {transaction});

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
