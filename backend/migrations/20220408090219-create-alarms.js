'use strict';

let models = require("../models/index.js");

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable(models.alarm.tableName, models.alarm.rawAttributes);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('alarm');
  }
};
