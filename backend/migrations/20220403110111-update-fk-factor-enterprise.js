'use strict';

const {theme, factor, responseOption, responseOptionSet} = require("../models");
module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeConstraint("factorEnterprise", "factorEnterprise_ibfk_2", {transaction: transaction})
      await queryInterface.addConstraint("factorEnterprise", {
        fields: ["factorId"],
        name: "factorEnterprise_ibfk_2",
        type: "FOREIGN KEY",
        references: {
          table: 'factor',
          field: 'id'
        },
        onDelete: 'cascade',
      })

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeConstraint("factorEnterprise", "factorEnterprise_ibfk_2", {transaction: transaction})
      await queryInterface.addConstraint("factorEnterprise", {
        fields: ["factorId"],
        name: "factorEnterprise_ibfk_2",
        type: "FOREIGN KEY",
        references: {
          table: 'factor',
          field: 'id',
          onDelete: 'cascade',
        },
      })

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
