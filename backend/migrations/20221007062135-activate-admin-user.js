'use strict';

const {user} = require("../models");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      // we need to find roomId
      const admin_user = await user.findOne({
        where: {
          email: "admin@sooz.tic.heia-fr.ch"
        },
        attributes: ["id"],
        raw: true,
        transaction
      });
      if (admin_user) {
        await queryInterface.bulkUpdate("user", { activated: true }, { id: admin_user.id }, { transaction });
      }
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      // we need to find roomId
      const admin_user = await user.findOne({
        where: {
          email: "admin@sooz.tic.heia-fr.ch"
        },
        attributes: ["id"],
        raw: true,
        transaction
      });
      if (admin_user) {
        await queryInterface.bulkUpdate("user", { activated: false }, { id: admin_user.id }, { transaction });
      }
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
