'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('userGroup', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      status: {
        allowNull: false,
        type: Sequelize.ENUM('pending', 'accepted'),
        defaultValue: 'pending',
      },
      role: {
        allowNull: false,
        type: Sequelize.ENUM('admin', 'member'),
        defaultValue: 'member',
      },
      groupId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        // `onDelete: 'CASCADE'` should be here
        references: {
          model: 'group',
          key: 'id',
          // !!! this is the wrong location to put the onDelete instruction
          onDelete: 'CASCADE'
        },
      },
      userId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        // `onDelete: 'CASCADE'` should be here
        references: {
          model: 'user',
          key: 'id',
          // !!! this is the wrong location to put the onDelete instruction
          onDelete: 'CASCADE'
        },
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('userGroup');
  }
};
