'use strict';

const {enterprise} = require("../models");
module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      const heiaId = (await enterprise.findOne({where: {name: "HEIA"}, attributes: ["id"], raw: true, transaction: transaction})).id

      await queryInterface.bulkInsert('room', [
          {
            name: "C00.10",
            enterpriseId: heiaId
          },
          {
            name: "C10.17",
            enterpriseId: heiaId
          },
        ],
        {transaction});
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      const heiaId = (await enterprise.findOne({where: {name: "HEIA"}, attributes: ["id"], raw: true, transaction: transaction})).id
      await queryInterface.bulkDelete('room', {
        name: ["C00.10", "C10.17"],
        enterpriseId: heiaId,
      }, {transaction});

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
