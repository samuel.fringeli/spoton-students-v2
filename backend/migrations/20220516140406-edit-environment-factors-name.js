'use strict';

const {factor, theme, responseOption, responseOptionSet, enterprise} = require("../models")

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {

      await factor.update({name: "Lumière"},
        {
          where: {
            name: "Luminosité",
          },
          transaction: transaction
        })
      await factor.update({name: "Bruit"},
        {
          where: {
            name: "Ambiance sonore",
          }, transaction: transaction
        })
      await factor.update({name: "Air"},
        {
          where: {
            name: "Odeur",
          },
          transaction: transaction
        })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await factor.update({name: "Luminosité"},
        {
          where: {
            name: "Lumière",
          },
          transaction: transaction
        })
      await factor.update({name: "Ambiance sonore"},
        {
          where: {
            name: "Bruit",
          }, transaction: transaction
        })
      await factor.update({name: "Odeur"},
        {
          where: {
            name: "Air",
          },
          transaction: transaction
        })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
