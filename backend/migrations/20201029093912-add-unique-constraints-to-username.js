'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('user', {
      fields: ['username'],
      type: 'unique',
      name: 'unique_username'
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('user', 'unique_username')
  }
};
