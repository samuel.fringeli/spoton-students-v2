'use strict';

// DISCLAIMER: this migration can only be ran if the fixtures have been applied and at least 4 rooms have been created.
//             the id of other sensors is given on gitlab https://gitlab.forge.hefr.ch/sandy.ingram/sooze/-/issues/57#note_19670

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('sensorRoom', [{
      roomId: 1,
      sensorId: 13321,
      sensorType: 'temperature'
    }, {
      roomId: 2,
      sensorId: 13321,
      sensorType: 'temperature'
    }, {
      roomId: 3,
      sensorId: 13321,
      sensorType: 'temperature'
    }, {
      roomId: 4,
      sensorId: 13321,
      sensorType: 'temperature'
    },])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('sensorRoom', { roomId: [1, 2, 3, 4], sensorId: 13321 })
  }
};
