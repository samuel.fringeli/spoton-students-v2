'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.describeTable('user').then(tableDefinition => {
      if (!tableDefinition['activationToken']){
        return queryInterface.addColumn("user", "activationToken", {
          type: Sequelize.STRING,
          allowNull: true,
          unique: true // unique constraint does not consider NULL values, so it is possible to use both constraints
        })
      } else {
        return Promise.resolve(true);
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("user", "activationToken");
  }
};
