'use strict';

const { responseOption } = require("../models");
const polarizedValues = {
  // set 1
  1: -1,
  2: -1,
  3: 1,
  4: 1,

  // set 7
  9: -1,
  10: -1,
  11: 1,
  12: 1,

  // set 3
  13: -1,
  14: -1,
  15: 1,
  16: -1,
  17: -1,

  // set 4
  18: -1,
  19: 1,
  20: 1,

  // set 5
  21: -1,
  22: -1,
  23: 1,
  24: -1,
  25: -1,

  // set 6
  26: -1,
  27: -1,
  28: 1,
  29: -1,
  30: -1,
};

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      const ids = (await responseOption.findAll({
        attributes: ["id"],
        raw: true,
        transaction
      })).map(e => e.id);

      for (let id of ids) {
        await queryInterface.bulkUpdate("responseOption", { polarizedValue: polarizedValues[id] }, { id }, { transaction });
      }
      transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      const ids = (await responseOption.findAll({
        attributes: ["id"],
        raw: true,
        transaction
      })).map(e => e.id);

      for (let id of ids) {
        await queryInterface.bulkUpdate("responseOption", { polarizedValue: null }, { id }, { transaction });
      }
      transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
