module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('user', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    firstName: {
      allowNull: true,
      type: Sequelize.STRING,
    },
    lastName: {
      allowNull: true,
      type: Sequelize.STRING,
    },
    username: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    email: {
      allowNull: true,
      type: Sequelize.STRING,
    },
    token: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    points: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    activatePopup: {
      allowNull: false,
      type: Sequelize.BOOLEAN,
    },
    sex: {
      allowNull: false,
      type: Sequelize.ENUM('f', 'm')
    },
    role: {
      allowNull: false,
      type: Sequelize.INTEGER,
      defaultValue: 1,
    },
    enterpriseId: {
      allowNull: false,
      type: Sequelize.INTEGER,
      onDelete: 'CASCADE',
      references: {
        model: 'enterprise',
        key: 'id',
      },
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('user'),
};
