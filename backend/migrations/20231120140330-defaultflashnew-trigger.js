'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(`
      CREATE TRIGGER trigger_on_defaultFlashNew_insert 
      AFTER INSERT ON defaultFlashNew 
      FOR EACH ROW 
      BEGIN
          DECLARE done INT DEFAULT FALSE;
          DECLARE g_id INT;
          DECLARE cursor_userGroups CURSOR FOR 
              SELECT id 
              FROM userGroup 
              WHERE status = 'accepted'; -- Assuming you only want to include 'accepted' groups, modify as needed.
          DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
      
          OPEN cursor_userGroups;
      
          userGroups_loop: LOOP
              FETCH cursor_userGroups INTO g_id;
              IF done THEN
                  LEAVE userGroups_loop;
              END IF;
              
              INSERT INTO flashNew (analyseType, title, frequency, period1, factorId1, themeId1, userId1, groupId1, period2, factorId2, themeId2, userId2, groupId2, createdAt, updatedAt) 
              VALUES (NEW.analyseType, NEW.title, NEW.frequency, NEW.period1, NEW.factorId1, NEW.themeId1, NULL, g_id, NEW.period2, NEW.factorId2, NEW.themeId2, NULL, g_id, NOW(), NOW());
          END LOOP;

          CLOSE cursor_userGroups;
      END;
    `);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(`
      DROP TRIGGER IF EXISTS trigger_on_defaultFlashNew_insert;
    `);
  }
};
