'use strict';

// !!! This is safe because at this point in time, the table alarm is empty.
const models = require("../models/index.js");

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.dropTable('alarm');
    await queryInterface.createTable(models.alarm.tableName, models.alarm.rawAttributes);
  },

  async down (queryInterface, Sequelize) {
    // We do nothing as the new version of the table just adds one column.
  }
};
