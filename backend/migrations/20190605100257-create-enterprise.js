'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable('enterprise', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING,
        },
      }, { transaction })
      await queryInterface.bulkInsert('enterprise', [
        {
          name: "HEIA"
        },
        {
          name: "Logitech"
        },
        {
          name: "La Mobilière"
        },
        {
          name: "Raiffeisen Sarine-Ouest"
        },
        {
          name: "ROSAS"
        },
      ], { transaction })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('enterprise')
};
