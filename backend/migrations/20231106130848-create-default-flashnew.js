'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('defaultFlashNew', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      analyseType: {
        allowNull: false,
        type: Sequelize.ENUM('correlation', 'clustering')
      },
      title: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      frequency: {
        allowNull: false,
        type: Sequelize.ENUM('daily', 'weekly', 'monthly')
      },
      period1: {
        allowNull: false,
        type: Sequelize.ENUM('daily', 'weekly', 'monthly')
      },
      factorId1: {
        allowNull: true,
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'factor',
          key: 'id'
        }
      },
      themeId1: {
        allowNull: true,
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'theme',
          key: 'id'
        }
      },
      userGroupType1: {
        allowNull: false,
        type: Sequelize.ENUM('user', 'group')
      },
      period2: {
        allowNull: true,
        type: Sequelize.ENUM('daily', 'weekly', 'monthly')
      },
      factorId2: {
        allowNull: true,
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'factor',
          key: 'id'
        }
      },
      themeId2: {
        allowNull: true,
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'theme',
          key: 'id'
        }
      },
      userGroupType2: {
        allowNull: false,
        type: Sequelize.ENUM('user', 'group')
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('defaultFlashNew');
  }
};
