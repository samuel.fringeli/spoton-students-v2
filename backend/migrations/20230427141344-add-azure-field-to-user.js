'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.describeTable('user').then(tableDefinition => {
          if (!tableDefinition['azure']){
              return queryInterface.addColumn(
                  'user',
                  'azure',
                  {
                      type: Sequelize.BOOLEAN,
                      allowNull: false,
                      defaultValue: false,
                  });
          } else {
              return Promise.resolve(true);
          }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
        'user',
        'azure'
    );
  }
};
