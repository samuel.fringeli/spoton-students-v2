
module.exports = {
  // unused
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('measuredValue', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      date: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      value: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      roomId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'room',
          key: 'id',
        },
      },
      themeId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'theme',
          key: 'id',
        },
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('measuredValue');
  }
};
