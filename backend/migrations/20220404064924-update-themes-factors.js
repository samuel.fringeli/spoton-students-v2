'use strict';

const {theme, factor, responseOptionSet, responseOption, enterprise} = require("../models");

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      //1. update theme names
      await theme.update({name: "Sensation"}, {where: {name: "Environnement physique"}, transaction: transaction})
      //2. clean responseOptionSet, ids etc. --> to be kept: 1, 7, 3, 4, 5,
      const responseOptionSetIdToDelete = [2, 8]
      await factor.update({responseOptionSetId: 1}, {where: {responseOptionSetId: responseOptionSetIdToDelete}, transaction: transaction})
      await theme.update({responseOptionSetId: 1}, {where: {responseOptionSetId: responseOptionSetIdToDelete}, transaction: transaction})
      await responseOption.destroy({where: {responseOptionSetId: responseOptionSetIdToDelete}, transaction: transaction})
      await responseOptionSet.destroy({where: {id: responseOptionSetIdToDelete}, transaction: transaction})
      //3. add themes Personnel & Environnement
      const newThemes = [
        {
          name: "Personnel",
          icon: "dummy_icon_name",
          question: "Question Personnel",
          responseOptionSetId: 1,
          questionFactor: "Quel facteur a le plus influencé ton ressenti quant au thème du ressenti personnel ?"
        },
        {
          name: "Environnement",
          icon: "dummy_icon_name",
          question: "Question Environnement",
          responseOptionSetId: 1,
          questionFactor: "Quel facteur a le plus influencé ton ressenti quant au thème de l'environnement ?"
        },
      ]
      await theme.bulkCreate(newThemes, { transaction: transaction })

      //4. delete factors, except Sensation
      await factor.destroy({where: {themeId: [2, 3, 5]}, transaction: transaction})

      //5. insert factors & factorEnterprise
      const personalId = await theme.findOne({where: {name: "Personnel"}, attributes: ["id"], raw: true, transaction: transaction})
      const environmentId = await theme.findOne({where: {name: "Environnement"}, attributes: ["id"], raw: true, transaction: transaction})
      await factor.bulkCreate([
        {name: 'Ma tâche', themeId: 2, responseOptionSetId: 1},
        {name: 'Mon résultat', themeId: 2, responseOptionSetId: 1},
        {name: 'Ma charge', themeId: 2, responseOptionSetId: 1},
        {name: 'Feed-back', themeId: 2, responseOptionSetId: 1},
        {name: 'Autonomie', themeId: 2, responseOptionSetId: 1},
        {name: 'Tâche', themeId: 2, responseOptionSetId: 1},
        {name: 'Résultat', themeId: 2, responseOptionSetId: 1},
        {name: 'Collaboration', themeId: 3, responseOptionSetId: 1},
        {name: 'Écoute', themeId: 3, responseOptionSetId: 1},
        {name: 'Ambiance', themeId: 3, responseOptionSetId: 1},
        {name: 'Intégration', themeId: 3, responseOptionSetId: 1},
        {name: 'Responsable', themeId: 3, responseOptionSetId: 1},
        {name: 'Collègues', themeId: 3, responseOptionSetId: 1},
        {name: 'Subordonnés', themeId: 3, responseOptionSetId: 1},
        {name: 'Organisation', themeId: 5, responseOptionSetId: 1},
        {name: 'Distraction', themeId: 5, responseOptionSetId: 1},
        {name: 'Infrastructure', themeId: 5, responseOptionSetId: 1},
        {name: 'IT', themeId: 5, responseOptionSetId: 1},
        {name: 'Équilibre', themeId: 5, responseOptionSetId: 1},
        {name: 'Énergie', themeId: personalId.id, responseOptionSetId: 1},
        {name: 'Motivation', themeId: personalId.id, responseOptionSetId: 1},
        {name: 'Stress', themeId: personalId.id, responseOptionSetId: 1},
        {name: 'Ordre', themeId: environmentId.id, responseOptionSetId: 1},
        {name: 'Mouvement', themeId: environmentId.id, responseOptionSetId: 1},
        {name: 'Outils', themeId: environmentId.id, responseOptionSetId: 1},
      ], { transaction: transaction })

      const enterpriseIds = await enterprise.findAll({attributes: ["id"]}).map(enterprise => enterprise.dataValues.id)
      const factorIds = await factor.findAll({
        where: {
          themeId: [2, 3, 5, personalId.id, environmentId.id]
        },
        attributes: ["id"],
        transaction: transaction
      }).map(factor => factor.dataValues.id)
      const records = []

      for (let i = 0; i < enterpriseIds.length; i++) {
        for (let j = 0; j < factorIds.length; j++) {
          records.push({
            enterpriseId: enterpriseIds[i],
            factorId: factorIds[j],
            createdAt: Sequelize.fn('NOW')
          })
        }
      }
      await queryInterface.bulkInsert('factorEnterprise', records, { transaction })

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  down: (queryInterface, Sequelize) => {
    return Promise.resolve({
      then: function(resolve) {
        console.warn("Cannot revert migration 20220404064924-update-themes-factors because it's irreversible")
        resolve();
      }
    })
  }
};
