'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable('level', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.DataTypes.INTEGER
        },
        name: {
          type: Sequelize.STRING
        },
        fromNbr: {
          type: Sequelize.INTEGER
        }
      }, { transaction });
      await queryInterface.bulkInsert('level', [{
        name: 'Opposum',
        fromNbr: 50,
      },
      {
        name: 'Suricat',
        fromNbr: 100,
      },
      {
        name: 'Buffalo',
        fromNbr: 150,
      },
      {
        name: 'Marmotte',
        fromNbr: 200,
      },
      {
        name: 'Aigle Royal',
        fromNbr: 250,
      },
      {
        name: 'Hamster',
        fromNbr: 300,
      },
      ], { transaction })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('level');
  }
};
