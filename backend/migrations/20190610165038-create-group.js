'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('group', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      enterpriseId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        // `onDelete: 'CASCADE'` is missing here
        references: {
          model: 'enterprise',
          key: 'id',
        },
      },

    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('group');
  }
};