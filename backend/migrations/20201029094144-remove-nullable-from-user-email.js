'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('user', 'email', {
      allowNull: false,
      type: Sequelize.STRING,
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('user', 'email', {
      allowNull: true,
      type: Sequelize.STRING,
    })
  }
};
