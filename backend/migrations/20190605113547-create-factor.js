'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable('factor', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING
        },
        themeId: {
          allowNull: false,
          type: Sequelize.INTEGER,
          onDelete: 'CASCADE',
          references: {
            model: 'theme',
            key: 'id',
          },
        },
        responseOptionSetId: {
          allowNull: false,
          type: Sequelize.INTEGER,
          onDelete: 'CASCADE',
          references: {
            model: 'responseOptionSet',
            key: 'id',
          },
        },
      }, { transaction });
      await queryInterface.bulkInsert('factor', [
        {
          name: 'Bien-être en général',
          themeId: 1,
          responseOptionSetId: 7
        },
        {
          name: 'Difficulté de la tâche par rapport à tes compétences',
          themeId: 2,
          responseOptionSetId: 1
        },
        {
          name: 'Résultat par rapport à tes attentes',
          themeId: 2,
          responseOptionSetId: 1
        },
        {
          name: 'Soutien de ton leader',
          themeId: 3,
          responseOptionSetId: 2
        },
        {
          name: 'Feed-back de ton leader',
          themeId: 3,
          responseOptionSetId: 2
        },
        {
          name: 'Autonomie par rapport à ton leader',
          themeId: 3,
          responseOptionSetId: 2
        },
        {
          name: 'Les échanges avec tes collègues',
          themeId: 3,
          responseOptionSetId: 2
        },
        {
          name: 'Les échanges avec tes subordonnés',
          themeId: 3,
          responseOptionSetId: 2
        },
        {
          name: 'Température',
          themeId: 4,
          responseOptionSetId: 3
        },
        {
          name: 'Odeur',
          themeId: 4,
          responseOptionSetId: 4
        },
        {
          name: 'Ambiance sonore',
          themeId: 4,
          responseOptionSetId: 5
        },
        {
          name: 'Luminosité',
          themeId: 4,
          responseOptionSetId: 6
        },
      ], { transaction })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('factor');
  }
};
