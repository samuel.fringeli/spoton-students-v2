'use strict';

const { query } = require("express");

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.bulkUpdate('responseOption', { label: 'Pas du tout satisfait' }, { label: "Stressé" }, { transaction })
      await queryInterface.bulkUpdate('responseOption', { label: 'Pas du tout satisfait' }, { label: "Excédé" }, { transaction })
      await queryInterface.bulkUpdate('responseOption', { label: 'Pas satisfait' }, { label: "Ennuyé" }, { transaction })
      await queryInterface.bulkUpdate('responseOption', { label: 'Pas satisfait' }, { label: "Frustré" }, { transaction })
      await queryInterface.bulkUpdate('responseOption', { label: 'Tout à fait satisfait' }, { label: "Heureux" }, { transaction })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  async down(queryInterface, Sequelize) {
    // not possible
    console.warn("Can not revert migration 20210205134335-update-response-labels.js because it's irriversible...")
  }
};
