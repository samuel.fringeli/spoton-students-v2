"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("sensorRoom", {
      roomId: {
        primaryKey: true,
        // `onDelete: 'CASCADE'` should be here
        references: {
          model: "room",
          key: "id",
          // !!! this is the wrong location to put the onDelete instruction
          onDelete: "CASCADE",
        },
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      sensorId: {
        primaryKey: true,
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      sensorType: {
        type: Sequelize.ENUM('temperature', 'humidity', 'luminosity', 'motion', 'noise', 'pressure', 'co2'),
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("sensorRoom");
  },
};
