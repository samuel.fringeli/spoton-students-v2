module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('userFactorResponse', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    date: {
      allowNull: false,
      type: Sequelize.DATEONLY
    },
    datetime: {
      allowNull: false,
      type: Sequelize.DATE
    },
    period: {
      allowNull: false,
      type: Sequelize.ENUM('morning', 'afternoon', 'day')
    },
    userId: {
      allowNull: false,
      type: Sequelize.INTEGER,
      onDelete: 'CASCADE',
      references: {
        model: 'user',
        key: 'id',
      },
    },
    factorId: {
      allowNull: false,
      type: Sequelize.INTEGER,
      onDelete: 'CASCADE',
      references: {
        model: 'factor',
        key: 'id',
      },
    },
    responseOptionId: {
      allowNull: false,
      type: Sequelize.INTEGER,
      onDelete: 'CASCADE',
      references: {
        model: 'responseOption',
        key: 'id',
      },
    },
    roomId: {
      allowNull: true,
      type: Sequelize.INTEGER,
      references: {
        model: 'room',
        key: 'id',
      },
    },
    isFromGuided: {
      defaultValue: false,
      type: Sequelize.BOOLEAN
    },
  }),

  down: (queryInterface, Sequelize) => queryInterface.dropTable('userFactorResponse'),
};

