'use strict';

const { query } = require('express');
const { Op } = require('sequelize')
const {
  enterprise, room
} = require("../models");

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      // get the ids of the enterprises
      const enterpriseIds = await enterprise.findAll({
        where: {
          name: {
            [Op.or]: ["La Mobilière", "Logitech"]
          }
        },
        attributes: ["id"]
      }).map(enterprise => enterprise.dataValues.id)
      // get associated rooms
      const roomIds = await room.findAll({
        where: {
          enterpriseId: {
            [Op.or]: enterpriseIds
          }
        },
        attributes: ["id"]
      }).map(room => room.dataValues.id)

      await queryInterface.bulkDelete('sensorRoom', { roomId: roomIds })

      await queryInterface.bulkDelete('enterprise', {
        id: enterpriseIds
      }, { transaction })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  down: (queryInterface, Sequelize) => queryInterface.bulkInsert('enterprise', [
    {
      name: "Logitech"
    },
    {
      name: "La Mobilière"
    },
  ])
};
