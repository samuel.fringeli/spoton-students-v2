'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("responseOptionSet", [
      {
        // homeOffice stressed/bored/satisfied/happy
        id: 8
      },
    ])

  },

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('responseOptionSet', {id: 8}, {})
};
