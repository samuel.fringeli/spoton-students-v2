'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('user', {
      fields: ['email'],
      type: 'unique',
      name: 'unique_email'
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('user', 'unique_email')
  }
};
