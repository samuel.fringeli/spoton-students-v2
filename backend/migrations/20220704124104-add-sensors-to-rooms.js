'use strict';

const {room} = require("../models");
const sensors = [
  // ROSAS
  {roomName: "201", sensorId: 13895, sensorType: "co2"},
  {roomName: "201", sensorId: 13894, sensorType: "temperature"},
  {roomName: "201", sensorId: 13854, sensorType: "luminosity"},
  // noise ?!

  {roomName: "202", sensorId: 13899, sensorType: "co2"},
  {roomName: "202", sensorId: 13898, sensorType: "temperature"},
  {roomName: "202", sensorId: 13857, sensorType: "luminosity"},
  // noise ?!

  {roomName: "205", sensorId: 13909, sensorType: "co2"},
  {roomName: "205", sensorId: 13908, sensorType: "temperature"},
  {roomName: "205", sensorId: 13849, sensorType: "luminosity"},
  // noise ?!

  {roomName: "206", sensorId: 13902, sensorType: "co2"},
  {roomName: "206", sensorId: 13901, sensorType: "temperature"},
  {roomName: "206", sensorId: 13843, sensorType: "luminosity"},
  {roomName: "206", sensorId: 13904, sensorType: "noise"},

  {roomName: "207", sensorId: 13888, sensorType: "co2"},
  {roomName: "207", sensorId: 13887, sensorType: "temperature"},
  {roomName: "207", sensorId: 13838, sensorType: "luminosity"},
  {roomName: "207", sensorId: 13890, sensorType: "noise"},

  // RAIFFEISEN
  //{roomName: "Guichet", sensorId: 13979, sensorType: "co2"},
  {roomName: "Guichet", sensorId: 13994, sensorType: "temperature"},
  //{roomName: "Guichet", sensorId: 13925, sensorType: "luminosity"},
  {roomName: "Guichet", sensorId: 13992, sensorType: "noise"},

  //{roomName: "Back office", sensorId: 13995, sensorType: "co2"},
  {roomName: "Back office", sensorId: 14010, sensorType: "temperature"},
  //{roomName: "Back office", sensorId: 13942, sensorType: "luminosity"},
  {roomName: "Back office", sensorId: 14008, sensorType: "noise"},

  //{roomName: "Conseillers", sensorId: 13982, sensorType: "co2"},
  {roomName: "Conseillers", sensorId: 13984, sensorType: "temperature"},
  //{roomName: "Conseillers", sensorId: 13955, sensorType: "luminosity"},
  // noise ?!

  //{roomName: "Directeur", sensorId: 13986, sensorType: "co2"},
  {roomName: "Directeur", sensorId: 13988, sensorType: "temperature"},
  //{roomName: "Directeur", sensorId: 13964, sensorType: "luminosity"},
  // noise ?!

  //{roomName: "Credit", sensorId: 13998, sensorType: "co2"},
  {roomName: "Credit", sensorId: 14000, sensorType: "temperature"},
  //{roomName: "Credit", sensorId: 13977, sensorType: "luminosity"},
  // noise ?!
]

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      // we need to find roomId
      for (const sensor of sensors) {
        sensor.roomId = (await room.findOne({where: {name: sensor.roomName},
          attributes: ["id"],
          raw: true,
          transaction: transaction
        })).id
        delete sensor.roomName;
      }
      console.log(sensors)
      await queryInterface.bulkInsert('sensorRoom', sensors, {transaction});
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      for (const sensor of sensors) {
        sensor.roomId = (await room.findOne({where: {name: sensor.roomName},
          attributes: ["id"],
          raw: true,
          transaction: transaction
        })).id;
        delete sensor.roomName;
      }

      await queryInterface.bulkDelete('sensorRoom', {
        sensorId: sensors.map(sensor => sensor.sensorId)
      }, {transaction});

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
