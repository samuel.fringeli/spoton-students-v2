'use strict';

const {user} = require("../models");

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      // Note that the addColumn instruction will be committed by mysql, even if sequelize makes a rollback
      // on the actual transaction. This is the reason why no transaction is specified here.
      await queryInterface.addColumn("user", "activated", {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      })

      // We set all existing accounts as already activated
      await user.update({ activated: true }, { where: { activated: false }, transaction: transaction })

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("user", "activated");
  }
};
