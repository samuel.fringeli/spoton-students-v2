'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('flashNew', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      analyseType: {
        type: Sequelize.ENUM('correlation', 'clustering'),
        allowNull: false
      },
      title: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      frequency: {
        type: Sequelize.ENUM('daily', 'weekly', 'monthly'),
        allowNull: false
      },
      period1: {
        type: Sequelize.ENUM('daily', 'weekly', 'monthly'),
        allowNull: false
      },
      factorId1: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'factor',
          key: 'id'
        }
      },
      themeId1: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'theme',
          key: 'id'
        }
      },
      userId1: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'user',
          key: 'id'
        }
      },
      groupId1: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'userGroup',
          key: 'id'
        }
      },
      period2: {
        type: Sequelize.ENUM('daily', 'weekly', 'monthly'),
        allowNull: true
      },
      factorId2: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'factor',
          key: 'id'
        }
      },
      themeId2: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'theme',
          key: 'id'
        }
      },
      userId2: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'user',
          key: 'id'
        }
      },
      groupId2: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'userGroup',
          key: 'id'
        }
      },
      payload: {
        type: Sequelize.JSON
      },
      defaultFlashNewId: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'defaultFlashNew',
          key: 'id'
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('flashNew');
  }
};
