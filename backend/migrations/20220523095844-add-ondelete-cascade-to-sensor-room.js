'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint("sensorRoom", "sensorRoom_ibfk_1");
    await queryInterface.addConstraint("sensorRoom",{
      type: "FOREIGN KEY",
      fields: ["roomId"],
      name: "sensorRoom_ibfk_1",
      references: {
        table: "room",
        field: "id",
      },
      onDelete: "CASCADE",
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint("sensorRoom", "sensorRoom_ibfk_1");
    await queryInterface.addConstraint("sensorRoom",{
      type: "FOREIGN KEY",
      fields: ["roomId"],
      name: "sensorRoom_ibfk_1",
      references: {
        table: "room",
        field: "id",
      },
      // without onDelete: CASCADE
    });
  }
};
