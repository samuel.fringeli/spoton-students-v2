'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(`
      CREATE TRIGGER trigger_on_new_group 
      AFTER INSERT ON userGroup 
      FOR EACH ROW 
      BEGIN
          DECLARE done INT DEFAULT FALSE;
          DECLARE d_id INT;
          DECLARE d_analyseType ENUM('correlation','clustering');
          DECLARE d_title TEXT;
          DECLARE d_frequency ENUM('daily','weekly','monthly');
          DECLARE d_period1 ENUM('daily','weekly','monthly');
          DECLARE d_factorId1 INT;
          DECLARE d_themeId1 INT;
          DECLARE d_period2 ENUM('daily','weekly','monthly');
          DECLARE d_factorId2 INT;
          DECLARE d_themeId2 INT;
          DECLARE cursor_defaultFlashNew CURSOR FOR 
              SELECT id, analyseType, title, frequency, period1, factorId1, themeId1, period2, factorId2, themeId2 
              FROM defaultFlashNew 
              WHERE userGroupType1 = 'group' AND userGroupType2 = 'group';
          DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
      
          OPEN cursor_defaultFlashNew;
      
          read_loop: LOOP
              FETCH cursor_defaultFlashNew INTO d_id, d_analyseType, d_title, d_frequency, d_period1, d_factorId1, d_themeId1, d_period2, d_factorId2, d_themeId2;
              IF done THEN
                  LEAVE read_loop;
              END IF;
              
              INSERT INTO flashNew (analyseType, title, frequency, period1, factorId1, themeId1, groupId1, period2, factorId2, themeId2, groupId2, createdAt, updatedAt) 
              VALUES (d_analyseType, d_title, d_frequency, d_period1, d_factorId1, d_themeId1, NEW.groupId, d_period2, d_factorId2, d_themeId2, NEW.groupId, NOW(), NOW());
          END LOOP;
      
        CLOSE cursor_defaultFlashNew;
      END;
    `);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(`
      DROP TRIGGER IF EXISTS trigger_on_new_group;
    `);
  }
};
