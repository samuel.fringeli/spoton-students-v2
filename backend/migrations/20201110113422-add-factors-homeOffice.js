'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("factor", [
      {
        name: 'Organisation de ton emploi du temps',
        themeId: 5,
        responseOptionSetId: 8
      },
      {
        name: 'Stratégie institutionnelle de la collaboration à distance',
        themeId: 5,
        responseOptionSetId: 8
      },
      {
        name: 'Niveau de distraction',
        themeId: 5,
        responseOptionSetId: 8
      },
      {
        name: 'Adéquation de ta place de travail',
        themeId: 5,
        responseOptionSetId: 8
      },
      {
        name: 'Performance des moyens informatiques',
        themeId: 5,
        responseOptionSetId: 8
      },
      {
        name: 'Équilibre entre travail et vie privée',
        themeId: 5,
        responseOptionSetId: 8
      },
    ])

  },

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('factor', {themeId: 5, responseOptionSetId: 8}, {})
};
