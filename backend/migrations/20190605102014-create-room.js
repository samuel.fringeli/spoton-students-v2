
module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable('room', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING,
        },
        enterpriseId: {
          allowNull: false,
          type: Sequelize.INTEGER,
          onDelete: 'CASCADE',
          references: {
            model: 'enterprise',
            key: 'id',
          },
        },
      }, {
        uniqueKeys: {
          uniqueRoomNamePerEnterprise: {
            fields: ['name', 'enterpriseId',],
          },
        },
      }, { transaction })
      await queryInterface.bulkInsert('room', [
        {
          name: "Bureau 1",
          enterpriseId: 1
        },
        {
          name: "Bureau 2",
          enterpriseId: 2
        },
        {
          name: "Bureau 3",
          enterpriseId: 3
        },
        {
          name: "Bureau 4",
          enterpriseId: 4
        },
      ], { transaction })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }

  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('room'),
};
