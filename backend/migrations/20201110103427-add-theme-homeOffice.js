'use strict';

// to be sure we use twice the same name
const homeOfficeName = "Télétravail";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("theme", [{
      name: homeOfficeName,
      icon: "teletravail_icon",
      description: "",
      question: "Quel est ton ressenti lié à ton travail à distance ?",
      responseOptionSetId: 8,
      questionFactor: "Quel facteur a le plus influencé ton ressenti quant au thème du télétravail ?",
    }
    ])

  },

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('theme', {name: homeOfficeName, responseOptionSetId: 8}, {})
};
