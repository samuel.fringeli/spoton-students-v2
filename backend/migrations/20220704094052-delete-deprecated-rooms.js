'use strict';

const {enterprise} = require("../models")


module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.bulkDelete('room', {
        name: ["Bureau 1", "Bureau 4", "Salle 1", "Salle 2", "Salle 3", "Salle 4", "Salle 5"]
        }, {transaction});
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      const heiaId = (await enterprise.findOne({where: {name: "HEIA"}, attributes: ["id"], raw: true, transaction: transaction})).id
      const raiffeisenId = (await enterprise.findOne({where: {name: "Raiffeisen Sarine-Ouest"}, attributes: ["id"], raw: true, transaction: transaction})).id

      await queryInterface.bulkInsert('room', [
          {
            name: "Bureau 1",
            enterpriseId: heiaId
          },
          {
            name: "Bureau 4",
            enterpriseId: raiffeisenId
          },
          {
            name: "Salle 1",
            enterpriseId: raiffeisenId
          },
          {
            name: "Salle 2",
            enterpriseId: raiffeisenId
          },
          {
            name: "Salle 3",
            enterpriseId: raiffeisenId
          },
          {
            name: "Salle 4",
            enterpriseId: raiffeisenId
          },
          {
            name: "Salle 5",
            enterpriseId: raiffeisenId
          },
        ],
        {transaction});
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }

  },
};
