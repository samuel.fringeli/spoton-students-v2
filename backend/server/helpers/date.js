/**
 * @file Helper for Date
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

/**
 * Get today date
 * @return {String} today date
 */
function getToday() {
  return new Date().toISOString().slice(0, 10);
}
function getCurrentTimestamp() {
  return new Date()
}
function checkDateFormat(date) {
  const newDate = Date.parse(date)
  return !isNaN(newDate) && new Date(newDate).toISOString().slice(0, 10) === date
}

function getDatesInBetween(date1, date2) {
  let date = new Date(date1),
    end = new Date(date2);
  // necessary to manage summer/winter time
  date.setHours(12,0,0,0)
  end.setHours(12,0,0,0)

  // make sure that "date" is in the past of "end"
  if (date > end) {
    let tmp = end;
    end = date;
    date = tmp;
  }

  let allDates = [toISODate(date1)];
  date.setDate(date.getDate() + 1);
  while (date.getTime() <= end.getTime()) {
    if (!isWeekend(date)) {
      allDates.push(toISODate(date));
    }
    date.setDate(date.getDate() + 1);
  }
  return allDates;
}

function isWeekend(date) {
  return date.getDay() === 0 || date.getDay() === 6; // sunday or saturday
}

function toISODate(date) {
  return new Date(date).toISOString().slice(0, 10);
}

module.exports = {
  getToday,
  getCurrentTimestamp,
  checkDateFormat,
  getDatesInBetween,
}
