/**
 * @file to verify the content of a variable
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

/**
 * This function verify that all elements of a variable or that the variable itself exist and is not null, undefined or empty
 * @param {Object} variable - with items to verify
 * @return {Boolean} false if variable is null, undefined or empty
 */
function variableExists(variable) {
  if (typeof variable === 'object') {
    for (let i = 0; i < variable.length; i++) {
      if ((typeof variable[i] === 'undefined' || variable[i] === null || variable[i] === '')) {
        return false;
      }
    }
    return true;
  }
  return !(typeof variable === 'undefined' || variable === null || variable === '');
}
module.exports = variableExists;
