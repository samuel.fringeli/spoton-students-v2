/**
 * @file Helper for Sending Push Notifications and Messages
 * @author Léonard Noth <leonard.noth@master.hes-so.ch>
 * @date 23.08.2022
 * @version 1.0
 */

const {subscription} = require("../../models");
const webpush = require("web-push");

// load the environment file
// require('dotenv').config()


webpush.setGCMAPIKey(process.env.FCM_API_KEY);
let vapidSubjectURL = process.env.NODE_ENV === "production" ? `${process.env.APP_BASE_URL}:8080` : `https://${process.env.APP_BASE_URL}:8080`;
webpush.setVapidDetails(
  vapidSubjectURL, // subject (is used by the push api host to contact the app owner (us) in case of any problems
  process.env.PUBLIC_VAPID_KEY_BASE64, // publicKey
  process.env.PRIVATE_VAPID_KEY // privateKey
);

function generate_unique_id(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() *
      charactersLength));
  }
  return result;
}

function sendPushNotification(subscriptionObject, subscriptionId, notificationContent, userId, alarmId, requireInteraction, actions) {
  const temp_options = {
    title: "Sooze",
    body: notificationContent,
    icon: `${process.env.APP_BASE_URL}/img/icons/icon-512x512.png`,
    data: {'id': alarmId, 'userId': userId},
    vibrate: [200, 100, 200],
    requireInteraction: requireInteraction,
    tag: 'push_notification_' + generate_unique_id(8),
  };
  const options = {
    ...temp_options,
    ...(requireInteraction) && {actions: actions}
  }
  return webpush.sendNotification(
    subscriptionObject.subscription,
    Buffer.from(JSON.stringify(options), "utf8"),
    {
      TTL: 60 * 60 * 24
    }
  ).then(
    () => {
      console.log("Push notification was send successfully")
      return 200;
    },
    (error) => {
      if (Number(error.statusCode) === 410 || Number(error.statusCode) === 404) {
        subscription.destroy({
          where: {
            id: subscriptionId,
          },
        }).catch((e) => {
          console.warn(e);
          return 410;
        });

        // set alarm status to error
      } else {
        console.log("WebPush Notification error 1:", error);
        return 500;
      }
    }
  ).catch((error) => {
    console.warn("WebPush Notification error 2:", error);
    return 500;
  });
}

function sendPushMessage(subscriptionObject, subscriptionId, messageContent, userId, hasAlarmAttached) {
  const options = {
    title: "Sooze",
    body: messageContent,
    icon: `${process.env.APP_BASE_URL}/img/icons/icon-512x512.png`,
    data: {userId: userId},
    vibrate: [200, 100, 200],
    requireInteraction: false,
    tag: hasAlarmAttached ? 'push_alarm_msg_' + generate_unique_id(8) : 'push_message_' + generate_unique_id(8),
  };

  return webpush.sendNotification(
    subscriptionObject.subscription,
    Buffer.from(JSON.stringify(options), "utf8"),
    {
      TTL: 60 * 60 * 24
    }
  ).then(
    () => {
      console.log("Push message was send successfully")
      return 200;
    },
    (error) => {
      if (Number(error.statusCode) === 410 || Number(error.statusCode) === 404) {
        subscription.destroy({
          where: {
            id: subscriptionId,
          },
        }).catch((e) => {
          console.warn(e);
          return 410;
        });

        // set alarm status to error
      } else {
        console.log("WebPush Message error 1:", error);
        return 500;
      }
    }
  ).catch((error) => {
    console.warn("WebPush Message error 2:", error);
    return 500;
  });
}

function sendPushMessageAndNotification(params) {
  return new Promise(resolve => {
    subscription.findAll({
      attributes: ["id", "subscription", "userId"],
      where: {userId: params.userId},
      raw: true,
      nest: true,
    }).then(async (results) => {
      if (results && results.length !== 0) {
        let notificationResult = 0;
        let messageResult = 0;
        if (params.shouldSendNotification) {
          notificationResult = await sendPushNotification(results[0], results[0].id, params.notificationContent, params.userId, params.alarmId, params.notificationRequireInteraction, params.actions);
        }
        if (params.shouldSendMessage) {
          messageResult = await sendPushMessage(results[0],  results[0].id, params.messageContent, params.userId, params.hasAlarmAttached);
        }
        resolve([notificationResult, messageResult])
      } else {
        console.log("No result in DB");
        resolve(500);
      }
    }).catch(error => {
        console.log(error);
      resolve(500);
    });
  });
}

module.exports = {
  sendPushMessageAndNotification
}
