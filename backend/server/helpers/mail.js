/**
 * @file Helper to send various types of emails
 * @author Uchendu Nwachukwu <Uchendu.Nwachukwu@edu.hefr.ch>
 * @date 07.09.2020
 * @version 2.0
 */

const nodemailer = require("nodemailer");
const Mailgen = require("mailgen");

EMAIL_ADDRESS='noreply-spoton@hefr.ch'
EMAIL_USER='asdf'
EMAIL_PASSWORD='asdf'
EMAIL_HOST='smtp.hefr.ch'
EMAIL_HOST_PORT=25

const {
  APP_BASE_URL
} = process.env;

const transporter = nodemailer.createTransport({
  host: EMAIL_HOST,
  port: EMAIL_HOST_PORT,
  auth: {
    user: EMAIL_USER,
    pass: EMAIL_PASSWORD,
  },
});

const MailGenerator = new Mailgen({
  theme: "default",
  product: {
    name: "SpotOn",
    link: APP_BASE_URL,
  },
});

const userHasEmailConfigured = (user) => {
  return user.email != "";
};

module.exports = {
  sendGroupInvitationMail(user, group) {
    if (!userHasEmailConfigured(user)) return;

    const content = {
      body: {
        greeting: "Bonjour",
        signature: "Avec nos meilleures salutations",
        name: user.firstName,
        intro: `Tu as reçu une nouvelle invitation du groupe ${group.name}.`,
        action: {
          instructions:
            "Pour accepter ou refuser l'invitation, appuie sur le bouton ci-dessous:",
          button: {
            color: "#FF7756",
            text: "Réponds à l'invitation",
            link: `https://${APP_BASE_URL}/groups`,
          },
        },
      },
    };

    let mail = MailGenerator.generate(content);

    let message = {
      from: EMAIL_ADDRESS,
      to: user.email,
      subject: `Invitation du groupe ${group.name}`,
      html: mail,
    };

    return transporter.sendMail(message);
  },
  sendAccountActivation(user) {
    if (!userHasEmailConfigured(user)) return;

    const content = {
      body: {
        greeting: "Bonjour",
        signature: "Avec nos meilleures salutations",
        name: user.firstName,
        intro: `Un compte SpotOn a été créé avec ton adresse email ${user.email}.`,
        action: {
          instructions:
            "Pour activer ton compte, clique sur le bouton ci-dessous:",
          button: {
            color: "#FF7756",
            text: "Activer mon compte",
            link: `https://${APP_BASE_URL}/users/${user.email}/activate/${user.activationToken}`,
          },
        },
        outro: "Si tu n'es pas à l'origine de la création de ce compte SpotOn, il te suffit d'ignorer cet email."
      },
    };

    let mail = MailGenerator.generate(content);

    let message = {
      from: EMAIL_ADDRESS,
      to: user.email,
      subject: `Activation de ton compte SpotOn ${user.username}, ${user.email}`,
      html: mail,
    };

    return transporter.sendMail(message);
  },
};
