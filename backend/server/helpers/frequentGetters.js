/**
 * @file Assembly of frequently used getters either as middleware or async functions
 * @author Uchendu Nwachukwu <uchendu.nwachukwu@edu.hefr.ch>
 * @date 02.10.2020
 * @version 2.0
 */

const {
    theme,
    factor,
    userGroup
} = require('../../models');
const requestStatus = require("../types/requestStatus");

/**
 * Custom definition of a valid element return by a database call
 * @param {*} entity to be validated
 */
const isValidEntity = (entity) => {
    return entity !== null
}

/** 
* Middleware: 
* Fetches the theme and adds it to the request object.
* If the theme does not exist, it returns a 404 error
*/
const getThemeByPk = (req, res, next) => {
    theme.findByPk(req.params.themeId).then(currentTheme => {
        if (!(isValidEntity(currentTheme))) {
            res.sendStatus(404)
        }
        else {
            req.theme = currentTheme
            return next()
        }
    }).catch(error => {
        console.warn(error)
        res.sendStatus(500)
    })
}

/** 
* Middleware: 
* Fetches the factor and adds it to the request object.
* If the factor does not exist, it returns a 404 error
*/
const getFactorByPk = (req, res, next) => {
    factor.findByPk(req.params.factorId).then(currentFactor => {
        if (!(isValidEntity(currentFactor))) {
            res.sendStatus(404)
        }
        else {
            req.factor = currentFactor
            return next()
        }
    }).catch(error => {
        console.warn(error)
        res.sendStatus(500)
    })
}

/**
 * Async function:
 * Get all responseOptions based on the object which must have a belongsTo relation with a responseOptionSet
 */
const getResponseOptionsForEntity = async (currentFactor) => {
    return currentFactor.getResponseOptionSet().then(currentResponseOptionSet =>
        currentResponseOptionSet.getResponseOptions()
    )
}

/**
 * Async function:
 * Get the role of a user in a particular group. If the user is not accepted in the group, null is returned
 */
const getRoleInGroup = async (userId, groupId) => {
    const membership = await userGroup.findOne({
        where: {
            status: requestStatus.ACCEPTED,
            userId,
            groupId,
        },
    });
    if (membership) {
        return membership.role
    } else {
        return null
    }
}

module.exports = {
    getThemeByPk,
    getFactorByPk,
    getResponseOptionsForEntity,
    getRoleInGroup
}