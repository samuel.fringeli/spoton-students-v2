/**
 * @file Role of the application
 * @note Device is currently not used
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

module.exports = {
  Root: 9,
  Admin: 8,
  Device: 2,
  User: 1,
  Guest: 0,
};
