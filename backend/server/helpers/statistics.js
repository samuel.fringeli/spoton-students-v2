const {
  userFactorResponse,
  responseOption,
  factor,
  userGroup,
} = require('../../models');
const Sequelize = require('sequelize');
const {
  Op,
} = Sequelize;
const moment = require('moment');
const requestStatus = require('../types/requestStatus');

const simulated_votes_db_answer = require('./fake_votes.js');


// For the simulated votes, we don't really care about the real factor neither response Id.
// We just want to simulate positive and negative votes on a virtual theme that has 3 factors.
// To simulate properly according to the db, a negative vote will have a
// responseOptionId of 1 and a positive vote will have a responseOptionId of 3.
let should_simulate_votes = false;


// Currently only counting the number of open days as being the weekdays
const getNumberOfOpenDays = (start_date, end_date) => {
  const startDate = moment(start_date, 'YYYY-MM-DD');
  const endDate = moment(end_date, 'YYYY-MM-DD');
  let openDays = 0;

  while (startDate.isBefore(endDate) || startDate.isSame(endDate)) {
    if (startDate.isoWeekday() !== 6 && startDate.isoWeekday() !== 7) {
      openDays++;
    }
    startDate.add(1, 'day');
  }
  return openDays;
};

const turnVoteValueIntoPositiveOrNegative = (responseOptionId) => {
  // If it's in this list it means it's a negative vote
  return [1, 2, 9, 10, 13, 17, 18, 21, 25, 26, 30].includes(responseOptionId) ? -1 : 1;
};

const calculateFactorPonderation = (userVotes) => {
  for (let user in userVotes) {
    for (let key in userVotes[user]) {
      if (!['votes', 'voting_rate', 'number_of_votes_on_theme'].includes(key)) {
        // It means it's a factor
        userVotes[user][key]['ponderation'] = (
            userVotes[user][key]['positive_votes'] + userVotes[user][key]['negative_votes']
        ) / userVotes[user]['number_of_votes_on_theme'];
      }
    }
  }

  return userVotes;
};

// Get the number of votes for each user
const getAllFactorsForTheme = (themeId) => {
  return factor
      .findAll({
        attributes: ['id', 'name'],
        where: {
          themeId: themeId,
        },
        raw: true,
      });
};

const calculateFactorSatisfaction = (userVotes) => {

  for (let user in userVotes) {
    for (let key in userVotes[user]) {
      if (!['votes', 'voting_rate', 'number_of_votes_on_theme'].includes(key)) {
        // It means it's a factor
        if (userVotes[user][key]['positive_votes'] === 0 && userVotes[user][key]['negative_votes'] === 0) {
          userVotes[user][key]['satisfaction'] = 'Pas de vote';
        } else {
          userVotes[user][key]['satisfaction'] = userVotes[user][key]['positive_votes'] /
              (userVotes[user][key]['positive_votes'] + userVotes[user][key]['negative_votes']);
        }
      }
    }
  }
  return userVotes;

};

const calculateThemeSatisfaction = (userVotes) => {
  for (let user in userVotes) {
    let user_theme_satisfaction = 0;
    for (let key in userVotes[user]) {
      if (!['voting_rate', 'number_of_votes_on_theme'].includes(key)) {
        // It means it's a factor
        if (userVotes[user][key]['satisfaction'] !== 'Pas de vote') {
          user_theme_satisfaction += userVotes[user][key]['satisfaction'] * userVotes[user][key]['ponderation'];
        }
      }
    }
    userVotes[user]['theme_satisfaction'] = user_theme_satisfaction;
  }
  return userVotes;
};

const reformatVotes = (userVotes, start_date, end_date, all_theme_factors, themeId) => {
  let reformatted_user_votes = {};
  userVotes.forEach((user) => {
    if (user['userId'] in reformatted_user_votes) {
      // It means we already saw once the user
      if (user['factorId'] in reformatted_user_votes[user['userId']]) {
        // It means we already saw once the factor
        turnVoteValueIntoPositiveOrNegative(user['responseOptionId']) === -1 ?
            reformatted_user_votes[user['userId']][user['factorId']]['negative_votes'] += 1 :
            reformatted_user_votes[user['userId']][user['factorId']]['positive_votes'] += 1;
      } else {
        // It means we haven't seen the factor yet
        reformatted_user_votes[user['userId']][user['factorId']] = {
          'positive_votes': turnVoteValueIntoPositiveOrNegative(user['responseOptionId']) === -1 ? 0 : 1,
          'negative_votes': turnVoteValueIntoPositiveOrNegative(user['responseOptionId']) === -1 ? 1 : 0,
        };
      }
      reformatted_user_votes[user['userId']]['votes'].push(user);
    } else {
      // It means we haven't seen the user yet
      reformatted_user_votes[user['userId']] = {};
      reformatted_user_votes[user['userId']][user['factorId']] = {
        'positive_votes': turnVoteValueIntoPositiveOrNegative(user['responseOptionId']) === -1 ? 0 : 1,
        'negative_votes': turnVoteValueIntoPositiveOrNegative(user['responseOptionId']) === -1 ? 1 : 0,
      };
      reformatted_user_votes[user['userId']]['votes'] = [user];

    }
  });

  for (let prop in reformatted_user_votes) {
    all_theme_factors.forEach((factor) => {
      if (!(factor in reformatted_user_votes[prop])) {
        reformatted_user_votes[prop][factor] = {
          'positive_votes': 0,
          'negative_votes': 0,
        };
      }
    });
    reformatted_user_votes[prop]['number_of_votes_on_theme'] = reformatted_user_votes[prop]['votes'].length;
    // TODO replace '1' by a constant
    reformatted_user_votes[prop]['voting_rate'] = (themeId === '1' ? reformatted_user_votes[prop]['number_of_votes_on_theme'] /
        (getNumberOfOpenDays(start_date, end_date)) : reformatted_user_votes[prop]['number_of_votes_on_theme'] /
        (getNumberOfOpenDays(start_date, end_date) * 2));
  }
  reformatted_user_votes = calculateFactorPonderation(reformatted_user_votes);
  reformatted_user_votes = calculateFactorSatisfaction(reformatted_user_votes);

  return reformatted_user_votes;

};


const calculateVotesForUsers = async (usersId, themeId, date) => {
  let votes;
  if (should_simulate_votes) {
    let factorsIds = [1, 2, 3]; // For generated votes, the factors ids are always 1, 2 and 3
    let temp_votes = [];
    simulated_votes_db_answer.forEach((vote) => {
      if (typeof usersId !== 'number' ? usersId.includes(vote['userId']) : usersId === vote['userId']) {
        if (
            moment(vote['date'], 'YYYY-MM-DD').isBetween(
                moment(date['dateFrom'], 'YYYY-MM-DD'),
                moment(date['dateTo'], 'YYYY-MM-DD')) ||
            moment(vote['date'], 'YYYY-MM-DD').isSame(moment(date['dateFrom'], 'YYYY-MM-DD')) ||
            moment(vote['date'], 'YYYY-MM-DD').isSame(moment(date['dateTo'], 'YYYY-MM-DD'))) {
          temp_votes.push(vote);
        }
      }
    });
    votes = {
      'votes': temp_votes,
      'dateFrom': date['dateFrom'],
      'dateTo': date['dateTo'],
      'all_theme_factors': factorsIds,
    };
  } else {
    let factors = await getAllFactorsForTheme(themeId);
    let factorsIds = [];
    factors.forEach((factor) => {
      factorsIds.push(factor['id']);
    });
    // TODO: replace 1 by a constant
    const period = (themeId === '1') ? {[Op.eq]: 'day'} : {[Op.not]: 'day'};
    votes = {
      'votes': await userFactorResponse.findAll({
        where: {
          userId: usersId, // Works with a single array or an array of userIds
          period: period,
          date: {
            [Op.gte]: date['dateFrom'],
            [Op.lte]: date['dateTo'],
          },
          factorId: factorsIds,
        },
        order: [
          ['datetime', 'DESC'],
        ],
        attributes: [
          'date',
          'datetime',
          'period',
          'userId',
          'factorId',
          'responseOptionId',
          'roomId',
          [Sequelize.col('responseOption.value'), 'value'],
        ],
        include: [{
          model: responseOption,
          attributes: [],
          required: true,
        }],
        raw: true,
      }),
      'dateFrom': date['dateFrom'],
      'dateTo': date['dateTo'],
      'theme': themeId,
      'all_theme_factors': factorsIds,
    };
  }

  let reformatted_voting_periods = reformatVotes(
      votes['votes'],
      votes['dateFrom'],
      votes['dateTo'],
      votes['all_theme_factors'],
      votes['theme'],
  );


  let filtered_output_user_votes = {};
  for (const key in reformatted_voting_periods) {
    const {
      votes,
      ... remainingAttributes
    } = reformatted_voting_periods[key];
    filtered_output_user_votes[key] = remainingAttributes;
  }
  filtered_output_user_votes = calculateThemeSatisfaction(filtered_output_user_votes);

  return filtered_output_user_votes;
};

const calculateStandardDeviationGroupMemberVoting = (group_member_votes) => {
  for (let user in group_member_votes['groupMembersVotes']) {
    for (let key in group_member_votes['groupMembersVotes'][user]) {
      if (key === 'number_of_votes_on_theme') {
        group_member_votes['averageValueCalculation']['theme']['standardDeviation'] += Math.pow(
            Math.abs(
                group_member_votes['groupMembersVotes'][user][key] -
                group_member_votes['averageValueCalculation']['theme']['average']),
            2,
        );
      } else if (key !== 'voting_rate' && key !== 'theme_satisfaction') {
        group_member_votes['averageValueCalculation'][key]['standardDeviation'] += Math.pow(
            Math.abs((
                group_member_votes['groupMembersVotes'][user][key]['positive_votes'] +
                group_member_votes['groupMembersVotes'][user][key]['negative_votes']
            ) - group_member_votes['averageValueCalculation'][key]['average']),
            2,
        );
      }
    }
  }

  for (let key in group_member_votes['averageValueCalculation']) {
    group_member_votes['averageValueCalculation'][key]['standardDeviation'] =
        Math.sqrt(group_member_votes['averageValueCalculation'][key]['standardDeviation'] /
            (Object.keys(group_member_votes['groupMembersVotes']).length - 1));
  }
  return group_member_votes;
};

const calculateAverageGroupMemberVotingNumber = (group_member_votes) => {
  group_member_votes['averageValueCalculation'] = {};
  for (let user in group_member_votes['groupMembersVotes']) {
    for (let key in group_member_votes['groupMembersVotes'][user]) {
      if (key === 'number_of_votes_on_theme') {
        if (!('theme' in group_member_votes['averageValueCalculation'])) {
          // If we don't already added the theme to the averageValueCalculation object
          group_member_votes['averageValueCalculation']['theme'] = {
            'average': group_member_votes['groupMembersVotes'][user][key],
            'standardDeviation': 0,
          };
        } else {
          group_member_votes['averageValueCalculation']['theme']['average'] +=
              group_member_votes['groupMembersVotes'][user][key];
        }
      } else if (key !== 'voting_rate' && key !== 'theme_satisfaction') {
        // It means its a factor
        if (!(key in group_member_votes['averageValueCalculation'])) {
          // If we don't already added the factor to the averageValueCalculation object
          group_member_votes['averageValueCalculation'][key] = {
            'average': group_member_votes['groupMembersVotes'][user][key]['positive_votes'] +
                group_member_votes['groupMembersVotes'][user][key]['negative_votes'],
            'standardDeviation': 0,
          };
        } else {
          group_member_votes['averageValueCalculation'][key]['average'] +=
              group_member_votes['groupMembersVotes'][user][key]['positive_votes'] +
              group_member_votes['groupMembersVotes'][user][key]['negative_votes'];
        }
      }
    }
  }

  for (let key in group_member_votes['averageValueCalculation']) {
    group_member_votes['averageValueCalculation'][key]['average'] =
        group_member_votes['averageValueCalculation'][key]['average'] /
        Object.keys(group_member_votes['groupMembersVotes']).length;
  }

  group_member_votes = calculateStandardDeviationGroupMemberVoting(group_member_votes);
  return group_member_votes;
};

const checkIfUserIsWithinLimit = (group_member_votes) => {
  for (let user in group_member_votes['groupMembersVotes']) {
    group_member_votes['groupMembersVotes'][user]['within_limit'] = {};
    for (let key in group_member_votes['groupMembersVotes'][user]) {
      if (key !== 'voting_rate' && key !== 'theme_satisfaction' && key !== 'number_of_votes_on_theme' && key !== 'within_limit') {
        const lower_limit = group_member_votes['averageValueCalculation'][key]['average'] - 2 *
            group_member_votes['averageValueCalculation'][key]['standardDeviation'];
        const upper_limit = group_member_votes['averageValueCalculation'][key]['average'] + 2 *
            group_member_votes['averageValueCalculation'][key]['standardDeviation'];

        if (group_member_votes['groupMembersVotes'][user][key]['positive_votes'] === 0 &&
            group_member_votes['groupMembersVotes'][user][key]['negative_votes'] === 0) {
          group_member_votes['groupMembersVotes'][user]['within_limit'][key] = 'Pas de vote';
        } else {
          const number_of_votes = (group_member_votes['groupMembersVotes'][user][key]['positive_votes'] +
              group_member_votes['groupMembersVotes'][user][key]['negative_votes']);
          group_member_votes['groupMembersVotes'][user]['within_limit'][key] = lower_limit <= number_of_votes && number_of_votes <= upper_limit;
        }
      }
    }
  }

  return group_member_votes;
};

const calculateUserGroupPonderation = (group_member_votes) => {
  const coefficient_smaller_impact_for_outlier = 1 / 4;
  const coefficient_non_voting_users = 0;
  // Store the default coefficient for each factor
  const number_of_voting_user_per_factor = {};
  const number_of_outliers_per_factor = {};
  const number_of_user_within_limit_per_factor = {};

  const adjusted_coefficient = {};

  for (let user in group_member_votes['groupMembersVotes']) {
    for (let key in group_member_votes['groupMembersVotes'][user]) {
      if (key !== 'voting_rate' && key !== 'theme_satisfaction' && key !== 'number_of_votes_on_theme' && key !== 'within_limit') {
        // It means its a factor
        if (!(key in number_of_voting_user_per_factor)) {
          number_of_voting_user_per_factor[key] =
              group_member_votes['groupMembersVotes'][user]['within_limit'][key] === 'Pas de vote' ? 0 : 1;
          number_of_outliers_per_factor[key] =
              group_member_votes['groupMembersVotes'][user]['within_limit'][key] ? 0 : 1;
          number_of_user_within_limit_per_factor[key] =
              group_member_votes['groupMembersVotes'][user]['within_limit'][key] ? 1 : 0;
        } else {
          number_of_voting_user_per_factor[key] +=
              group_member_votes['groupMembersVotes'][user]['within_limit'][key] === 'Pas de vote' ? 0 : 1;
          number_of_outliers_per_factor[key] +=
              group_member_votes['groupMembersVotes'][user]['within_limit'][key] ? 0 : 1;
          number_of_user_within_limit_per_factor[key] +=
              group_member_votes['groupMembersVotes'][user]['within_limit'][key] ? 1 : 0;
        }
      }
    }
  }

  for (let key in number_of_voting_user_per_factor) {
    let number_of_voting_users_for_factor = number_of_voting_user_per_factor[key];
    let default_factor_voting_user_coefficient = 1 / number_of_voting_users_for_factor;
    adjusted_coefficient[key] = {
      'coefficient_for_outliers': default_factor_voting_user_coefficient * coefficient_smaller_impact_for_outlier,
      'coefficient_for_non_voting_users': coefficient_non_voting_users,
      'coefficient_for_in_limit_users': default_factor_voting_user_coefficient +
          (default_factor_voting_user_coefficient * (1 - coefficient_smaller_impact_for_outlier) *
              (number_of_outliers_per_factor[key] / number_of_user_within_limit_per_factor[key])),
    };
  }

  for (let user in group_member_votes['groupMembersVotes']) {
    group_member_votes['groupMembersVotes'][user]['groupPonderation'] = {};
    for (let key in group_member_votes['groupMembersVotes'][user]) {
      if (
          key !== 'voting_rate' &&
          key !== 'theme_satisfaction' &&
          key !== 'number_of_votes_on_theme' &&
          key !== 'within_limit' &&
          key !== 'groupPonderation'
      ) {
        // It means its a factor
        if (group_member_votes['groupMembersVotes'][user]['within_limit'][key] === 'Pas de vote') {
          group_member_votes['groupMembersVotes'][user]['groupPonderation'][key] =
              adjusted_coefficient[key]['coefficient_for_non_voting_users'];
        } else if (group_member_votes['groupMembersVotes'][user]['within_limit'][key]) {
          group_member_votes['groupMembersVotes'][user]['groupPonderation'][key] =
              adjusted_coefficient[key]['coefficient_for_in_limit_users'];
        } else {
          group_member_votes['groupMembersVotes'][user]['groupPonderation'][key] =
              adjusted_coefficient[key]['coefficient_for_outliers'];
        }
      }
    }
  }

  return group_member_votes;

};

const calculateGroupFactorSatisfaction = (group_member_votes) => {
  let group_factor_satisfaction = {};
  for (let user in group_member_votes['groupMembersVotes']) {
    for (let key in group_member_votes['groupMembersVotes'][user]) {
      if (
          key !== 'voting_rate' &&
          key !== 'theme_satisfaction' &&
          key !== 'number_of_votes_on_theme' &&
          key !== 'within_limit' &&
          key !== 'groupPonderation'
      ) {
        // It means its a factor
        if (!(key in group_factor_satisfaction)) {
          group_factor_satisfaction[key] = 0;
        }

        if (group_member_votes['groupMembersVotes'][user][key]['satisfaction'] !== 'Pas de vote') {
          group_factor_satisfaction[key] += group_member_votes['groupMembersVotes'][user][key]['satisfaction'] *
              group_member_votes['groupMembersVotes'][user]['groupPonderation'][key];
        }
      }
    }
  }

  group_member_votes['group_factor_satisfaction'] = group_factor_satisfaction;

  return group_member_votes;

};

const calculateGroupThemeSatisfaction = (group_member_votes) => {
  let group_theme_satisfaction = 0;
  let factor_ponderation = {};
  let number_total_vote_on_theme = 0;
  for (let user in group_member_votes['groupMembersVotes']) {
    for (let key in group_member_votes['groupMembersVotes'][user]) {
      if (
          key !== 'voting_rate' &&
          key !== 'theme_satisfaction' &&
          key !== 'within_limit' &&
          key !== 'groupPonderation'
      ) {
        if (key === 'number_of_votes_on_theme') {
          number_total_vote_on_theme += group_member_votes['groupMembersVotes'][user][key];
        }
        // It means its a factor
        if (!(key in factor_ponderation)) {
          factor_ponderation[key] = 0;
        }
        factor_ponderation[key] +=
            group_member_votes['groupMembersVotes'][user][key]['positive_votes'] +
            group_member_votes['groupMembersVotes'][user][key]['negative_votes'];
      }
    }
  }

  for (let key in factor_ponderation) {
    factor_ponderation[key] = factor_ponderation[key] / number_total_vote_on_theme;
  }

  for (let key in group_member_votes['group_factor_satisfaction']) {
    group_theme_satisfaction += group_member_votes['group_factor_satisfaction'][key] * factor_ponderation[key];
  }

  group_member_votes['group_theme_satisfaction'] = group_theme_satisfaction;

  return group_member_votes;

};

const calculateGroupVotingRate = (group_member_votes, start_date, end_date, themeId) => {
  // TODO replace '1' by a constant
  const max_amount_of_votes = (themeId === '1' ? getNumberOfOpenDays(start_date, end_date) *
      Object.keys(group_member_votes['groupMembersVotes']).length : getNumberOfOpenDays(start_date, end_date) * 2 *
      Object.keys(group_member_votes['groupMembersVotes']).length);
  let total_amount_of_votes = 0;
  for (let user in group_member_votes['groupMembersVotes']) {
    total_amount_of_votes += group_member_votes['groupMembersVotes'][user]['number_of_votes_on_theme'];
  }
  group_member_votes['theme_voting_rate'] = total_amount_of_votes / max_amount_of_votes;

  return group_member_votes;

};


const getUsersVotes = async (
    res,
    usersId,
    themeId,
    date,
    shouldReturnThemeLevelStatistics,
    shouldReturnFactorLevelStatistics,
) => {
  if (should_simulate_votes) {
    usersId = [1];
  }
  let filtered_output_user_votes = await calculateVotesForUsers(usersId, themeId, date);
  let return_map = {};
  if (filtered_output_user_votes !== undefined && Object.keys(filtered_output_user_votes).length > 0) {
    filtered_output_user_votes = filtered_output_user_votes[Object.keys(filtered_output_user_votes)[0]];
    console.log(filtered_output_user_votes);


    if (shouldReturnThemeLevelStatistics === 'true') {
      return_map['theme_level_statistics'] = {
        'theme_satisfaction': filtered_output_user_votes['theme_satisfaction'],
        'voting_rate': filtered_output_user_votes['voting_rate'],
      };
    }

    if (shouldReturnFactorLevelStatistics === 'true') {
      return_map['factor_level_statistics'] = {};
      for (let key of Object.keys(filtered_output_user_votes)) {
        console.log(key);
        if (
            key !== 'theme_satisfaction' &&
            key !== 'voting_rate' &&
            key !== 'votes' &&
            key !== 'number_of_votes_on_theme') {
          return_map['factor_level_statistics'][key] = filtered_output_user_votes[key];
        }
      }
    }
  }
  res.json(return_map);
};

const getGroupVotes = async (
    res,
    groupId,
    themeId,
    dates,
    shouldReturnThemeLevelStatistics,
    shouldReturnFactorLevelStatistics,
) => {

  let usersId = [];
  if (should_simulate_votes) {
    usersId = [1, 2, 3, 4, 5, 6, 7];
  } else {
    let temp_usersId = await userGroup
        .findAll({
          where: {
            groupId: groupId,
            status: requestStatus.ACCEPTED,
          },
          raw: true,
        });
    temp_usersId.forEach((user) => {
      usersId.push(user['userId']);

    });
  }
  console.log('usersId: ', usersId);
  let filtered_output_user_votes = await calculateVotesForUsers(usersId, themeId, dates);

  let processing_map = {};
  processing_map['groupMembersVotes'] = filtered_output_user_votes;
  processing_map = calculateAverageGroupMemberVotingNumber(processing_map);
  processing_map = checkIfUserIsWithinLimit(processing_map);
  processing_map = calculateUserGroupPonderation(processing_map);
  processing_map = calculateGroupFactorSatisfaction(processing_map);
  processing_map = calculateGroupThemeSatisfaction(processing_map);
  processing_map = calculateGroupVotingRate(processing_map, dates['dateFrom'], dates['dateTo'], themeId);
  let return_map = {};

  if (shouldReturnThemeLevelStatistics === 'true') {
    return_map['theme_level_statistics'] = {
      'theme_satisfaction': processing_map['group_theme_satisfaction'],
      'voting_rate': processing_map['theme_voting_rate'],
    };
  }

  const has_voted = JSON.parse(JSON.stringify(processing_map['group_factor_satisfaction']));
  Object.keys(has_voted).forEach(key => {
    has_voted[key] = 0;
  });

  Object.keys(processing_map['groupMembersVotes']).forEach(user => {
    Object.keys(processing_map['groupMembersVotes'][user]).forEach(factor => {
      if (factor !== 'number_of_votes_on_theme' &&
          factor !== 'voting_rate' &&
          factor !== 'theme_satisfaction' &&
          factor !== 'within_limit' &&
          factor !== 'groupPonderation') {
        has_voted[factor] = has_voted[factor] + processing_map['groupMembersVotes'][user][factor]['positive_votes'] + processing_map['groupMembersVotes'][user][factor]['negative_votes'];
      }
    });
  });

  console.log(has_voted);
  if (shouldReturnFactorLevelStatistics === 'true') {
    return_map['factor_level_statistics'] = {
      'group_satisfaction': processing_map['group_factor_satisfaction'],
      'has_anyone_voted_on_factors': has_voted,
      'nb_of_users_in_group': usersId.length,
    };
  }
  res.json(return_map);
};

module.exports = {
  getUsersVotes,
  getGroupVotes,
};