/**
 * @file dictionary for feminine masculine conversion
 * @todo Implement this functionality directly in the database
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

module.exports = [
  {
    "m": "Ennuyé",
    "f": "Ennuyée"
  },
  {
    "m": "Excédé",
    "f": "Excédée"
  },
  {
    "m": "Frustré",
    "f": "Frustrée"
  },
  {
    "m": "Heureux",
    "f": "Heureuse"
  },
  {
    "m": "Satisfait",
    "f": "Satisfaite"
  },
  {
    "m": "Stressé",
    "f": "Stressée"
  },
]
