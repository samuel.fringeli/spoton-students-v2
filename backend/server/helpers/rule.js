const {rule} = require('../../models');
const default_ruleset = require('./default_ruleset');

module.exports = async function createRule(ruleType, entityType) {
  // periodicbacity, operator, participationRate, votingRate, voteType, rate, messagePrefix, name, factorId, groupId, personId

    try {
      let n = 1
      for (let ruleset of default_ruleset[ruleType]) {
        let createdRule = ruleset;
        if (ruleType === 'group') {
          createdRule.groupId = entityType.id;
          createdRule.name = `Règle par défaut ${ n++ } pour le groupe ${ entityType.name }`
        } else if (ruleType === 'user') {
          createdRule.personId = entityType.id;
          createdRule.name = `Règle par défaut ${ n++ } pour moi`
        }
        await rule.create(createdRule);
      }
    } catch (err) {
      console.log(err);
      return 500;
    }
    return 200;
};