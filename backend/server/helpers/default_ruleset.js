const default_ruleset = {
  'user': [
    {
      periodicity: 'daily',
      operator: '<',
      participationRate: 100,
      votingRate: 66,
      voteType: 'positive',
      rate: 66,
      messagePrefix: '',
      name: null,
      factorId: 20, // travail > ma tâche
      groupId: null,
      personId: 1,
    },
  ],
  'group': [
    {
      periodicity: 'daily',
      operator: '<',
      participationRate: 80,
      votingRate: 66,
      voteType: 'positive',
      rate: 66,
      messagePrefix: '',
      name: null,
      factorId: 20, // travail > ma tâche
      groupId: 1,
      personId: null,
    },
  ],
};

module.exports = default_ruleset