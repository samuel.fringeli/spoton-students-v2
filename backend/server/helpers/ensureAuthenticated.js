/**
 * @file Security file
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

const {
  user
} = require('../../models');
const Role = require('../helpers/role');

/**
 * This function make sure a user have the required Role and is logged in before to execute a function.
 * @name ensureAuthenticated
 * @param {Role} role - Minimal Role needed to authorize the action
 * @function
 * @return user informations, error 403, 401 or error 500
 */
module.exports = function ensureAuthenticated(role = Role.Guest) {
  return (req, res, next) => {
    if (req.session && req.session.token && req.session.username) {
      user.findOne({
        where: {
          token: req.session.token,
          username: req.session.username,
        },
      })
        .then((result) => {
          if (result) {
            req.user = {
              userPoints: result.points,
              id: result.id,
              enterpriseId: result.enterpriseId,
              role: result.role,
            };

            if (req.user.role < role) {
              return res.status(403).end();
            }
            return next();
          }
          return res.status(401).end();
        })
        .catch((error) => {
          console.warn(error);
          return res.status(500).end();
        });
    } else if (role === Role.Guest) {
      return next();
    } else {
      return res.status(401).end();
    }
  };
};
