const simulated_votes_db_answer = [
  // ---------- Monday Morning ----------
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T07:00:00.000Z",
    "period": "morning",
    "userId": 1,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T07:00:00.000Z",
    "period": "morning",
    "userId": 2,
    "factorId": 2,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 2
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T07:00:00.000Z",
    "period": "morning",
    "userId": 3,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T07:00:00.000Z",
    "period": "morning",
    "userId": 4,
    "factorId": 1,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T07:00:00.000Z",
    "period": "morning",
    "userId": 5,
    "factorId": 2,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T07:00:00.000Z",
    "period": "morning",
    "userId": 6,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T07:00:00.000Z",
    "period": "morning",
    "userId": 7,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  // ---------- Monday Afternoon ----------
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T14:00:00.000Z",
    "period": "afternoon",
    "userId": 1,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T14:00:00.000Z",
    "period": "afternoon",
    "userId": 2,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T14:00:00.000Z",
    "period": "afternoon",
    "userId": 3,
    "factorId": 1,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T14:00:00.000Z",
    "period": "afternoon",
    "userId": 4,
    "factorId": 2,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 2
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T14:00:00.000Z",
    "period": "afternoon",
    "userId": 5,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-13",
    "datetime": "2023-03-13T14:00:00.000Z",
    "period": "afternoon",
    "userId": 6,
    "factorId": 1,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  // ---------------------------------------------------
  // ---------- Tuesday Morning ----------
  {
    "date": "2023-03-14",
    "datetime": "2023-03-14T07:00:00.000Z",
    "period": "morning",
    "userId": 1,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  // ---------- Tuesday Afternoon -----
  {
    "date": "2023-03-14",
    "datetime": "2023-03-14T14:00:00.000Z",
    "period": "afternoon",
    "userId": 1,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-14",
    "datetime": "2023-03-14T14:00:00.000Z",
    "period": "afternoon",
    "userId": 2,
    "factorId": 1,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-14",
    "datetime": "2023-03-14T14:00:00.000Z",
    "period": "afternoon",
    "userId": 3,
    "factorId": 2,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-14",
    "datetime": "2023-03-14T14:00:00.000Z",
    "period": "afternoon",
    "userId": 4,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-14",
    "datetime": "2023-03-14T14:00:00.000Z",
    "period": "afternoon",
    "userId": 5,
    "factorId": 1,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 2
  },
  {
    "date": "2023-03-14",
    "datetime": "2023-03-14T14:00:00.000Z",
    "period": "afternoon",
    "userId": 6,
    "factorId": 2,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  // ---------------------------------------------------
  // ---------- Wednesday Morning ----------
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T07:00:00.000Z",
    "period": "morning",
    "userId": 1,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T07:00:00.000Z",
    "period": "morning",
    "userId": 2,
    "factorId": 2,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T07:00:00.000Z",
    "period": "morning",
    "userId": 3,
    "factorId": 1,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T07:00:00.000Z",
    "period": "morning",
    "userId": 4,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T07:00:00.000Z",
    "period": "morning",
    "userId": 5,
    "factorId": 2,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T07:00:00.000Z",
    "period": "morning",
    "userId": 6,
    "factorId": 1,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T07:00:00.000Z",
    "period": "morning",
    "userId": 7,
    "factorId": 2,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  // ---------- Wednesday Afternoon ----------
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T14:00:00.000Z",
    "period": "afternoon",
    "userId": 1,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T14:00:00.000Z",
    "period": "afternoon",
    "userId": 2,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T14:00:00.000Z",
    "period": "afternoon",
    "userId": 3,
    "factorId": 2,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T14:00:00.000Z",
    "period": "afternoon",
    "userId": 4,
    "factorId": 1,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T14:00:00.000Z",
    "period": "afternoon",
    "userId": 5,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-15",
    "datetime": "2023-03-15T14:00:00.000Z",
    "period": "afternoon",
    "userId": 6,
    "factorId": 2,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  // ---------------------------------------------------
  // ---------- Thursday Morning ----------
  {
    "date": "2023-03-16",
    "datetime": "2023-03-16T07:00:00.000Z",
    "period": "morning",
    "userId": 1,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-16",
    "datetime": "2023-03-16T07:00:00.000Z",
    "period": "morning",
    "userId": 2,
    "factorId": 1,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-16",
    "datetime": "2023-03-16T07:00:00.000Z",
    "period": "morning",
    "userId": 3,
    "factorId": 2,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-16",
    "datetime": "2023-03-16T07:00:00.000Z",
    "period": "morning",
    "userId": 4,
    "factorId": 2,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-16",
    "datetime": "2023-03-16T07:00:00.000Z",
    "period": "morning",
    "userId": 5,
    "factorId": 1,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-16",
    "datetime": "2023-03-16T07:00:00.000Z",
    "period": "morning",
    "userId": 6,
    "factorId": 2,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  // ---------- Thursday Afternoon ----------

  {
    "date": "2023-03-16",
    "datetime": "2023-03-16T14:00:00.000Z",
    "period": "afternoon",
    "userId": 1,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-16",
    "datetime": "2023-03-16T14:00:00.000Z",
    "period": "afternoon",
    "userId": 7,
    "factorId": 1,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },

  // ---------------------------------------------------
  // ---------- Friday Morning ----------
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T07:00:00.000Z",
    "period": "morning",
    "userId": 1,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T07:00:00.000Z",
    "period": "morning",
    "userId": 2,
    "factorId": 2,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T07:00:00.000Z",
    "period": "morning",
    "userId": 3,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T07:00:00.000Z",
    "period": "morning",
    "userId": 4,
    "factorId": 1,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T07:00:00.000Z",
    "period": "morning",
    "userId": 5,
    "factorId": 2,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T07:00:00.000Z",
    "period": "morning",
    "userId": 6,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  // ---------- Friday Afternoon ----------
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T14:00:00.000Z",
    "period": "afternoon",
    "userId": 1,
    "factorId": 3,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T14:00:00.000Z",
    "period": "afternoon",
    "userId": 2,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T14:00:00.000Z",
    "period": "afternoon",
    "userId": 3,
    "factorId": 1,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T14:00:00.000Z",
    "period": "afternoon",
    "userId": 4,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 3
  },
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T14:00:00.000Z",
    "period": "afternoon",
    "userId": 5,
    "factorId": 3,
    "responseOptionId": 3,
    "roomId": 20,
    "value": 4
  },
  {
    "date": "2023-03-17",
    "datetime": "2023-03-17T14:00:00.000Z",
    "period": "afternoon",
    "userId": 6,
    "factorId": 1,
    "responseOptionId": 1,
    "roomId": 20,
    "value": 1
  },

];

module.exports = simulated_votes_db_answer;