/**
 * @file Azure AD helper file
 * @author Vincent Audergon <vincent.audergon@edu.hefr.ch>
 * @date 27.04.2023
 * @version 1.0
 */

const jwt = require('jsonwebtoken');
const passport = require('passport');
const AzureAdOAuth2Strategy = require('passport-azure-ad-oauth2').Strategy;

passport.use(new AzureAdOAuth2Strategy({
    clientID: process.env.AZURE_AD_CLIENT_ID,
    clientSecret: process.env.AZURE_AD_CLIENT_SECRET,
    callbackURL: process.env.AZURE_AD_CALLBACK_URL,
    resource: process.env.AZURE_AD_RESOURCE_ID,
}, (accessToken, refresh_token, params, profile, done) => {
        done(null, jwt.decode(params.id_token));
    })
);

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});


/**
 * This function is used to login with Azure AD
 * @param req the request object (express)
 * @param res the response object (express)
 * @param next the next function (express)
 */
const authenticate = function(req, res, next) {
    req.session.callbackURL = req.query.callbackURL;
    passport.authenticate('azure_ad_oauth2')(req, res, next);
}

/**
 * This function is used to handle the callback from Azure AD
 * @param req the request object (express)
 * @param res the response object (express)
 * @param next the next function (express)
 */
const authenticateCallback = function(req, res, next) {
    passport.authenticate('azure_ad_oauth2', { failureRedirect: '/failed' })(req, res, next);
}

module.exports = {
    authenticate,
    authenticateCallback,
}