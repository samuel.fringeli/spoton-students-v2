/**
 * @file file to convert word implemented in the dictionary file
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

const dictionary = require('./dictionary.js');

/**
 * Convert the masculine word to feminine if the logged in user is a girl with the help of the "dictionary.js" file
 * @param {Object} word - The object to parse that will be returned to the client
 * @param {string} sex - The sex of the user.
 * @return {Object} Object with the words converted
 */
function convert(responseOptions, sex) {
  // make a deep copy to return at the end and prevent side-effects
  const responseOptionsCopy = JSON.parse(JSON.stringify(responseOptions))
  if (sex === 'f') {
    dictionary.forEach(word => {
      responseOptionsCopy.forEach(responseOption => {
        // if the masculine word is matching, replace it with its feminin counterpart
        if (word.m === responseOption.label) {
          responseOption.label = word.f
        }
      })
    })
  }
  return responseOptionsCopy;
}

module.exports = convert;
