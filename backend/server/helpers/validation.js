/**
 * @file Assembly of validation functions
 * @author Uchendu Nwachukwu <uchendu.nwachukwu@edu.hefr.ch>
 * @date 02.10.2020
 * @version 2.0
 */

const { getResponseOptionsForEntity, getRoleInGroup } = require('./frequentGetters')
const groupRole = require("../types/groupRole");
const { group } = require('../../models');

/**
 * Middlerware:
 * Ensures that the user is an admin in the group concerned, else returns an error 403 to the user
 * @param {int} req.params.groupId
 */
const ensureUserIsAdminInGroup = async (req, res, next) => {
    const userRole = await getRoleInGroup(req.user.id, req.params.groupId);
    try {
        if (userRole === groupRole.ADMIN) {
            next()
        } else {
            res.sendStatus(403)
        }
    } catch (err) {
        res.sendStatus(500)
    }
}

/**
 * Middleware:
 * Ensures that a group with this id exists in the enterprise of the user, else returns an error 404 to the user
 * @param {int} req.params.groupId
 */
const ensureGroupExistsInEnterprise = async (req, res, next) => {
    const foundGroup = await group.count({
        where: {
            id: req.params.groupId,
            enterpriseId: req.user.enterpriseId
        }
    })
    if (foundGroup > 0) {
        next()
    } else {
        res.sendStatus(404)
    }
}

/**
 * Returns true if the responseOptionId is valid for the correspondingEntity
 * @param {int} responseOptionId 
 * @param {*} correspondingEntity 
 */
const isResponseToEntity = async (responseOptionId, correspondingEntity) => {
    const validResponseOptionsToEntity = await getResponseOptionsForEntity(correspondingEntity)
    validResponseOptionsToEntity
    const matchedResponse = validResponseOptionsToEntity.filter(validResponseOption => validResponseOption.id === responseOptionId)
    return matchedResponse.length > 0
}

module.exports = {
    isResponseToEntity,
    ensureUserIsAdminInGroup,
    ensureGroupExistsInEnterprise
}