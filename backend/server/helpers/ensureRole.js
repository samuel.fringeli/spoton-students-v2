/**
 * @file Security file
 * @author Uchendu Nwachukwu <uchendu.nwachukwu@hefr.ch
 * @date 06.01.2021
 * @version 2.0
 */

const {
  user
} = require('../../models');
const Role = require('./role');

/**
 * This function make sure a user have the required Role and is logged in
 * @name ensureRole
 * @param {Role} role - Minimal Role needed to authorize the action
 * @function
 * @return boolean
 */
module.exports = async function ensureRole(userId, role = Role.Guest) {
  return user.findOne({
    where: {
      id: userId
    },
  })
    .then((result) => {
      if (result) {
        return result.role >= role
      }
      return false
    })
    .catch((error) => {
      console.warn(error);
    });
};
