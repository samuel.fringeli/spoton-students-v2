/**
 * @file Responses related calls to the database
 * @author Uchendu Nwachukwu <uchendu.nwachukwu@edu.hefr.ch>
 * @date 02.10.2020
 * @version 2.0
 */

const express = require('express');

const router = express.Router({ mergeParams: true });
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const Role = require('../helpers/role');
const {
  factor,
  userFactorResponse,
  sequelize
} = require('../../models');
const variableExists = require("../helpers/variable");
const { getFactorByPk } = require('../helpers/frequentGetters');
const { isResponseToEntity } = require('../helpers/validation')
const { getToday, getCurrentTimestamp, checkDateFormat} = require('../helpers/date')

/**
 * @swagger
 * /themes/{themeId}/factors/{factorId}/responses:
 *   get:
 *     tags:
 *       - Responses
 *     description: Get the responses of the user for today to this factor and for the given period
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: themeId
 *         description: Id of a theme
 *         in: path
 *         required: true
 *         type: integer
 *       - name: factorId
 *         description: Id of a factor
 *         in: path
 *         required: true
 *         type: integer
 *       - name: period
 *         description: Period for which the answer should be searched
 *         in: query
 *         required: true
 *         schema:
 *           type: string
 *           enum:
 *             - morning
 *             - afternoon
 *             - day
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error 
 */
router.get('/', ensureAuthenticated(Role.User), getFactorByPk, async (req, res) => {
  const responseFound = await userFactorResponse.findOne({
    where: {
      date: getToday(),
      factorId: req.params.factorId,
      userId: req.user.id,
      period: req.query.period,
    }
  })
  res.json(responseFound)
})

/**
 * @swagger
 * /themes/{themeId}/factors/{factorId}/responses:
 *   put:
 *     tags:
 *       - Responses
 *     description: Upsert the current response of the user for this factor. Key on the columns (userId, factorId, date, period). If date is not specified, it is set to today.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: themeId
 *         description: Id of a theme
 *         in: path
 *         required: true
 *         type: integer
 *       - name: factorId
 *         description: Id of a factor
 *         in: path
 *         required: true
 *         type: integer
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             responseOptionId:
 *               type: integer
 *             roomId:
 *               type: integer
 *             period:
 *               type: string
 *               enum:
 *                 - morning
 *                 - afternoon
 *                 - day
 *
 *             date:
 *               type: string
 *         required:
 *           - responseOptionId
 *           - roomId
 *           - period
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.put('/', ensureAuthenticated(Role.User), getFactorByPk, async (req, res) => {
  // minimal validation
  const validResponse = await isResponseToEntity(req.body.responseOptionId, req.factor)
  if (!validResponse) {
    res.sendStatus(400);
    return;
  }

  /**************************************
   * TODO: add validation that it's a factor associated to that theme
   */
  const getFactor = await factor.findOne({
    where: {
      id: req.params.factorId
    }
  });

  if(getFactor.themeId != req.params.themeId) {
    res.sendStatus(400);
    return;
  }

  // upsert the currentResponse as userFactorResponse
  const currentResponse = {
      datetime: getCurrentTimestamp(),
      userId: req.user.id,
      factorId: req.params.factorId,
      ...req.body,
    }

    if (!variableExists(req.body.date)) {
      currentResponse.date = getToday();
    } else if (!checkDateFormat(req.body.date)){
      res.sendStatus(400)
      return
    }

  try {
    // check if a vote exist for this theme, date, period and userId
    let record = await userFactorResponse.findOne({
      where: {
        // is forming a unique key (combined with the condition on include) which is checked with a create hook on userFactorResponse model
        date: currentResponse.date,
        userId: currentResponse.userId,
        period: currentResponse.period,
      },
      include: {
        attributes: ["themeId"],
        model: factor,
        where: {
          themeId: req.factor.themeId
        },
        required: true,
      }
    });

    if (record) {
      await record.update(currentResponse)
    } else {
      record = await userFactorResponse.create(currentResponse)
    }
    res.status(200).json(record);
  } catch (err) {
    console.warn(err);
    res.sendStatus(500);
  }
});

/**
 * @swagger
 * /themes/{themeId}/factors/{factorId}/responses/{responseId}:
 *   delete:
 *     tags:
 *       - Responses
 *     description: Delete the given response
 *     parameters:
 *       - name: themeId
 *         description: Id of a theme
 *         in: path
 *         required: true
 *         type: integer
 *       - name: factorId
 *         description: Id of a factor
 *         in: path
 *         required: true
 *         type: integer
 *       - name: responseId
 *         description: Id of a response
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error 
 */
router.delete('/:responseId', ensureAuthenticated(Role.User), getFactorByPk, async (req, res) => {
  // minimal validation
  const valid =
    req.params.factorId !== undefined &&
    req.params.themeId !== undefined &&
    req.params.responseId !== undefined;
  if (!valid) {
    res.sendStatus(400)
    return
  }

  try {
    // we need to find the row to delete
    let record = await userFactorResponse.findOne({
      where: {
        factorId: req.params.factorId,
        id: req.params.responseId
      }
    });
    await record.destroy();

    res.sendStatus(200);
  } catch (err) {
    console.warn(err);
    res.sendStatus(500);
  }
});

module.exports = router;
