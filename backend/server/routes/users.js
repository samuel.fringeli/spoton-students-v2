/**
 * @file User related calls to the database
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

const express = require('express');
const router = express.Router();
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const Role = require('../helpers/role');
const Mail = require('../helpers/mail');
const {user, enterprise, group} = require('../../models');
const passwordHash = require('password-hash');
const {rnd} = require('../helpers/randomToken');
const createRule = require('../helpers/rule');
const {where} = require("sequelize");
const azureAd = require("../helpers/azure_ad");


/**
 * @swagger
 * /users:
 *   post:
 *     tags:
 *       - Users
 *     description: Create a user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             firstName:
 *               type: string
 *             lastName:
 *               type: string
 *             sex:
 *               type: string
 *               enum:
 *                 - m
 *                 - f
 *             username:
 *               type: string
 *             email:
 *               type: string
 *             enterpriseId:
 *               type: integer
 *             token:
 *               type: string
 *             activatePopup:
 *               type: boolean
 *             points:
 *               type: integer
 *         required:
 *           - username
 *           - email
 *           - enterpriseId
 *           - token
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.post('/', (req, res) => {
  req.body.points = 0;
  let token = req.body.token;
  let hashedPassword = passwordHash.generate(token);
  req.body.token = hashedPassword;
  // we check username and email (unique keys) to give a feedback to the user if an error occurs
  user.findOne({
    where: {
      username: req.body.username,
    },
    raw: true,
    nest: true,
  }).then((resultUsername) => {
    if (!resultUsername) {
      user.findOne({
        where: {
          email: req.body.email,
        },
        raw: true,
        nest: true,
      }).then((resultEmail) => {
        if (!resultEmail) {
          user.findAll({
            attributes: ['activationToken'],
            raw: true,
            nest: true,
          }).then((result) => {
            // create activation token
            let activationToken;
            do {
              activationToken = rnd(8);
            } while (result.find(e => e.activationToken === activationToken) !== undefined); // find returns undefined if there is no match
            req.body.activationToken = activationToken;
            user.create(req.body)
                .then((user) => {
                  try {
                    console.log(process.env.NODE_ENV);
                    // TODO: Change that to be able to send email locally
                    if (process.env.NODE_ENV !== 'development') {
                      // send email to activate account
                      Mail.sendAccountActivation(user);
                      res.json(user);
                    } else {
                      user.update({activated: true}, {
                        where: {
                          email: req.body.email,
                        },
                      }).then(async resolve => {
                        const status = await createRule('user', user);
                        if (status === 500) {
                          res.status(500).send(user);
                        } else if (status === 200) {
                          res.status(200).send(user);
                        }
                      });
                    }
                  } catch (error) {
                    res.json(user);
                    res.status(500).send('Le compte a été créé mais le mail de confirmation n\'a pas pu être envoyé. Veuillez SVP contacter les administrateurs.');
                    console.warn(error);
                  }
                }).catch((error) => {
              res.status(500).send('Le compte n\'a pas pu être créé !');
              console.warn(error);
            });
          });

        } else {
          res.status(500).send('Cet email est déjà utilisé !');
        }
      });
    } else {
      res.status(500).send('Ce nom d\'utilisateur est déjà utilisé !');
    }
  });
});

/**
 * @swagger
 * /users:
 *   put:
 *     tags:
 *       - Users
 *     description: Update the connected user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         description: To change the password, provide the old token and a new token. If the password is changed, the session of the user will be invalidated since it's based on his old password and he is forced to reconnect.
 *         schema:
 *           type: object
 *           properties:
 *             firstName:
 *               type: string
 *             lastName:
 *               type: string
 *             sex:
 *               type: string
 *               enum:
 *                 - m
 *                 - f
 *             username:
 *               type: string
 *             email:
 *               type: string
 *             enterpriseId:
 *               type: integer
 *             oldToken:
 *               type: string
 *             token:
 *               type: string
 *             activatePopup:
 *               type: boolean
 *         required:
 *           - firstName
 *           - lastName
 *           - username
 *           - email
 *           - enterpriseId
 *           - activatePopup
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.put('/', ensureAuthenticated(Role.User), (req, res) => {
  var {oldToken, enterprise, ... values} = req.body;
  if (typeof oldToken !== 'undefined') {
    user.findOne({
      attributes: ['token'],
      where: {
        username: req.body.username,
      },
      raw: true,
      nest: true,
    }).then((result) => {
      if (!passwordHash.verify(oldToken, result.token)) {
        //Si l'ancien token n'est pas identique au mot de passe de
        //la DB hashé alors on retourne une erreur 403
        res.status(403).send();
        return;
      }
    });
    //Si le mot de passe est valide, on hash le nouveau mot de passe
    //et on le mettra dans la requête UPDATE
    let hashedPassword = passwordHash.generate(req.body.token);
    values.token = hashedPassword;
  }

  // update session variable if sent any
  if (req.body.sex) {
    req.session.sex = req.body.sex;
  }

  user.update(values, {
    where: {
      id: req.user.id,
    },
  }).then(() => {
    res.status(200).send();
  })
      .catch((error) => {
        res.status(500).send();
        console.warn(error);
      });
});


/**
 * @swagger
 * /users/login:
 *   post:
 *     tags:
 *       - Users
 *     description: Login a user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         description: Use a username or email as identifier and the password in plain-text.
 *         schema:
 *           type: object
 *           properties:
 *             user:
 *               type: object
 *               properties:
 *                 identifier:
 *                   type: string
 *                 token:
 *                   type: string
 *         required:
 *           - user
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.post('/login', (req, res) => {
  var password = req.body.user.token;
  var identifier = req.body.user.identifier;

  // check if a username or email is used as identifier
  var identifierClause;
  if (identifier.includes('@')) {
    identifierClause = {email: identifier};
  } else {
    identifierClause = {username: identifier};
  }
  // make sure the user is a non-azure user
  identifierClause = {...identifierClause, azure: false};
  user.findOne({
    attributes: ['token', 'id'],
    where: identifierClause,
    raw: true,
    nest: true,
  }).then((result) => {
    if (passwordHash.verify(password, result.token)) {
      user.findOne({
        where: {... identifierClause, ... {token: result.token, activated: true}},
        include: [
          {model: enterprise},
          {
            model: group,
            as: 'groups',
          },
        ],
        nest: true,
      }).then((result2) => {
        if (result2) {
          let token = result2.token;
          let username = result2.username;
          let sex = result2.sex;
          const {... user} = result2;
          req.session.username = username;
          req.session.token = token;
          req.session.sex = sex;
          res.json(result2);
        } else {
          res.status(401).send();
        }
      })
          .catch((error) => {
            res.status(500).send();
            console.warn(error);
          });
    } else {
      res.status(401).send();
    }
  })
      .catch((error) => {
        res.status(401).send();
        console.warn(error);
      });
});

/**
 * @swagger
 * /users/login/azure:
 *   get:
 *     tags:
 *       - Users
 *     parameters: [callbackURL]
 *     description: Login a user with MS Azure
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/login/azure", azureAd.authenticate);

/**
 * @swagger
 * /users/login/azure/callback:
 *    get:
 *      tags:
 *        - Users
 *      description: Callback for Azure AD authentication
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Successful
 *        401:
 *          description: Unauthorized (authentication missing)
 *        403:
 *          description: Forbidden (not enough privileges)
 *        500:
 *          description: Server-side processing error
 */
router.get("/login/azure/callback", azureAd.authenticateCallback, (req, res) => {
  // Connection with Azure AD successful
  // check if user exists in database
  user.findOne({where: {username: req.session.passport.user.upn.split("@")[0]}}).then((result) => {
    const redirectURL = req.session.callbackURL ? req.session.callbackURL : '/';
    req.session.callbackURL = null;
    if (result) {
      // if yes, login and redirect to home page
      req.session.username = result.username;
      req.session.token = result.token;
      req.session.sex = result.sex;

      res.redirect(redirectURL);
    } else {
      // if not, redirect to signup page
      res.redirect(redirectURL + '/signup');
    }
  });
});

/**
 * @swagger
 * /users/login/azure/info:
 *    get:
 *      tags:
 *        - Users
 *      description: Get information about the azure user (after /login/azure)
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Successful
 *        401:
 *          description: Unauthorized (authentication missing)
 *        403:
 *          description: Forbidden (not enough privileges)
 *        500:
 *          description: Server-side processing error
 */
router.get("/login/azure/info", (req, res) => {
    // Connection with Azure AD successful
    if (!req.session.passport) {
      res.status(401).send();
      return;
    }
    res.json(req.session.passport.user);
});

/**
 * @swagger
 * /users/me:
 *    get:
 *      tags:
 *        - Users
 *      description: Get information about the connected user
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Successful
 *        401:
 *          description: Unauthorized (authentication missing)
 *        403:
 *          description: Forbidden (not enough privileges)
 *        500:
 *          description: Server-side processing error
 */
router.get("/me", (req, res) => {
  if (!req.session.username) {
    res.status(401).send();
    return;
  }
  user.findOne({where: {username: req.session.username}}).then((result) => {
    if (result) {
      res.json(result);
    } else {
        res.status(401).send();
    }
  });
});

/**
 * @swagger
 * /users/logout:
 *   post:
 *     tags:
 *       - Users
 *     description: Logout the connected user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       500:
 *         description: Server-side processing error
 */
router.post('/logout', (req, res) => {
  req.session = null;
  req.sessionOptions.maxAge = 0;
  res.end();
});


/**
 * @swagger
 * /users/activate:
 *   post:
 *     tags:
 *       - Users
 *     description: Activate account
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       500:
 *         description: Server-side processing error
 */
router.put('/activate', (req, res) => {
  const userEmail = req.body.userEmail;
  const activationToken = req.body.activationToken;
  user.findOne({
    attributes: ['id', 'activated'],
    where: {
      email: userEmail,
      activationToken: activationToken,
    },
    raw: true,
    nest: true,
  }).then((result) => {
    if (result) {
      if (result.activated) {
        res.status(500).send('Ce compte est déjà activé');
      } else {
        user.update({activated: true}, {
          where: {
            id: result.id,
            email: userEmail,
            activationToken: activationToken,
          },
        }).then(() => {
          res.status(200).send();
        }).catch((error) => {
          res.status(500).send();
          console.warn(error);
        });
      }
    } else {
      res.status(500).send('Erreur dans le lien d\'activation');
    }
  }).catch((error) => {
    res.status(500).send();
    console.warn(error);
  });


});

module.exports = router;
