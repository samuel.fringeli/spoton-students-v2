const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');
require('aws-sdk/lib/maintenance_mode_message').suppress = true;

const s3 = new AWS.S3({
    accessKeyId: process.env.S3_USERNAME,
    secretAccessKey: process.env.S3_PASSWORD,
    endpoint: 'https://minio1.isc.heia-fr.ch:9008',
    s3ForcePathStyle: true,
    signatureVersion: 'v4'
});

/**
 * @swagger
 * /pdf/{filename}:
 *   get:
 *     tags:
 *       - PDF
 *     summary: Get a file from S3 bucket
 *     description: >
 *       Retrieves a specific file from the S3 bucket based on the provided filename.
 *       The file is streamed directly to the client.
 *     parameters:
 *       - in: path
 *         name: filename
 *         required: true
 *         schema:
 *           type: string
 *         description: The name of the file to retrieve. Append '.pdf' if no extension is provided.
 *     produces:
 *       - application/pdf
 *     responses:
 *       200:
 *         description: Successful retrieval of the file
 *       404:
 *         description: File not found in S3 bucket
 *       500:
 *         description: Server-side error or issue with fetching the file
 */

router.get('/:filename', (req, res) => {
    const { filename } = req.params;
    const s3Params = {
        Bucket: 'spoton-pdf',
        Key: !filename.includes('.') ? filename + '.pdf' : filename,
    };

    // Check if the file exists in S3 bucket
    s3.headObject(s3Params, function(err, metadata) {
        if (err && err.code === 'NotFound') {
            res.status(404).send('File not found');
        } else {
            // Stream the file from S3
            const fileStream = s3.getObject(s3Params).createReadStream();
            res.setHeader('Content-Type', 'application/pdf');
            res.setHeader('Content-Disposition', `inline; filename="${filename}"`);
            fileStream.pipe(res);
        }
    });
});

module.exports = router;
