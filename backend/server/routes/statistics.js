/**
 * @file Calls to the database for the statistics
 * @author Yael Iseli <yael.iseli@edu.hefr.ch>
 * @date 11.08.2020
 * @version 3.0
 */
const express = require("express");
const Sequelize = require("sequelize");

const router = express.Router();
const ensureAuthenticated = require("../helpers/ensureAuthenticated");
const ensureRole = require("../helpers/ensureRole");
const Role = require("../helpers/role");
const {
  userThemeResponse,
  userFactorResponse,
  responseOption,
  factor,
  userGroup,
  theme,
  user,
  enterprise,
} = require("../../models");
const requestStatus = require("../types/requestStatus");

const { Op, fn } = Sequelize;
const variableExists = require("../helpers/variable");
const {getUsersVotes, getGroupVotes} = require("../helpers/statistics");

/**
 * @swagger
 * /stats/lastVote:
 *   get:
 *     tags:
 *       - Statistics
 *     description: Returns last vote
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/lastVote", ensureAuthenticated(Role.User), (req, res) => {
  userFactorResponse
    .findAll({
      where: {
        userId: req.user.id,
      },
      order: [["datetime", "DESC"]],
      limit: 1,
      attributes: [
        "date",
        "period",
        "datetime",
        "responseOptionId",
        "roomId",
        [Sequelize.col("factor.themeId"), "themeId"],
        [Sequelize.col("factor.id"), "factorId"],
        /*[Sequelize.col("user.enterprise.id"), "enterprise"],*/
      ],
      include: [
        {
          model: factor,
          attributes: [],
        },
        {
          model: user,
          attributes: ["enterpriseId"],
          include: [{
            model: enterprise
          }]
        },
      ],
    })
    .then((result) => {
      res.json(result[0]); // only return a single object as we limit the query beforehand
    })
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * @swagger
 * /stats/responseCountForFactor:
 *   get:
 *     tags:
 *       - Statistics
 *     description: Returns the number of responses per responseId for one factor
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: factorId
 *         description: Id of a factor
 *         in: query
 *         required: true
 *       - name: dateFrom
 *         description: Start date
 *         in: query
 *         required: true
 *       - name: dateTo
 *         description: End date
 *         in: query
 *         required: true
 *       - name: roomId
 *         description: Id of a room
 *         in: query
 *         required: false
 *       - name: groupId
 *         description: Id of a group. Might as well by an array of groupd id's
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get(
  "/responseCountForFactor",
  ensureAuthenticated(Role.User),
  async (req, res) => {
    if (
      !variableExists([
        req.query.factorId,
        req.query.dateFrom,
        req.query.dateTo,
      ])
    ) {
      res.status(400).send();
    } else {
      const factorIdArray = req.query.factorId.split(",");
      let options = {
        attributes: [
          "responseOptionId",
          [fn("COUNT", Sequelize.col("responseOptionId")), "counterOfResponse"],
        ],
        where: {
          date: { [Op.gte]: req.query.dateFrom, [Op.lte]: req.query.dateTo },
          factorId: factorIdArray ? factorIdArray : req.query.factorId,
        },
        order: ["responseOptionId"],
        group: ["responseOptionId"],
        includeIgnoreAttributes: false,
      };

      if (req.query.roomId) { // can be one or more values
        options.where.roomId = req.query.roomId.split(",")
      }
      return manageDataRequest(
        req.user.id,
        req.query,
        userFactorResponse,
        options,
        res
      );
    }
  }
);

/**
 * @swagger
 * /stats/days:
 *   get:
 *     tags:
 *       - Statistics
 *     description: Returns statistics by day (and by period or not)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: factorId
 *         description: Id of a factor. Might as well be an array of factor id's
 *         in: path
 *         required: true
 *         type: integer
 *       - name: dateFrom
 *         description: Start date
 *         in: query
 *         required: true
 *       - name: dateTo
 *         description: End date
 *         in: query
 *         required: true
 *       - name: period
 *         description: True if the stats distinguish the periods, otherwise false
 *         in: query
 *         required: true
 *       - name: roomId
 *         description: Id of a room
 *         in: query
 *         required: false
 *       - name: groupId
 *         description: Id of a group. Might as well by an array of groupd id's
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/days", ensureAuthenticated(Role.User), (req, res) => {
  if (
    !variableExists([
      req.query.factorId,
      req.query.dateFrom,
      req.query.dateTo,
      req.query.period
    ])
  ) {
    res.status(400).send();
  } else {
    const factorIdArray = req.query.factorId.split(",");
    let options = {
      attributes: [
        [fn("ROUND", fn("AVG", Sequelize.col("value"))), "value"],
        "date",
      ],
      where: {
        date: { [Op.gte]: req.query.dateFrom, [Op.lte]: req.query.dateTo },
        factorId: factorIdArray ? factorIdArray : req.query.factorId,
      },
      include: [
        {
          model: responseOption,
          required: true
        },
      ],
      order: [["date", "ASC"]],
      group: ["date"],
      includeIgnoreAttributes: false,
    };

    if (req.query.roomId) { // can be one or more values
      options.where.roomId = req.query.roomId.split(",")
    }
    if (!req.query.groupId && !req.query.enterpriseId) {
      options.attributes.push("datetime");
      options.order.push(["datetime", "ASC"]);
      options.group.push("datetime");
    }

    if (JSON.parse(req.query.period)) {
      options.attributes.push("period");
      options.order.push(["period", "ASC"]);
      options.group.push("period");
    }
    return manageDataRequest(
      req.user.id,
      req.query,
      userFactorResponse,
      options,
      res
    );
  }
});

/**
 * @swagger
 * /stats/months:
 *   get:
 *     tags:
 *       - Statistics
 *     description: Returns statistics by month (and by period)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: factorId
 *         description: Id of a factor. Might as well be an array of factor id's
 *         in: path
 *         required: true
 *         type: integer
 *       - name: dateFrom
 *         description: Start date
 *         in: query
 *         required: true
 *       - name: dateTo
 *         description: End date
 *         in: query
 *         required: true
 *       - name: roomId
 *         description: Id of a room
 *         in: query
 *         required: false
 *       - name: groupId
 *         description: Id of a group. Might as well by an array of groupd id's
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/months", ensureAuthenticated(Role.User), (req, res) => {
  if (
    !variableExists([
      req.query.factorId,
      req.query.dateFrom,
      req.query.dateTo,
      req.query.period,
    ])
  ) {
    res.status(400).send();
  } else {
    const factorIdArray = req.query.factorId.split(",");
    let options = {
      where: {
        date: { [Op.gte]: req.query.dateFrom, [Op.lte]: req.query.dateTo },
        factorId: factorIdArray ? factorIdArray : req.query.factorId,
      },
      attributes: [
        [Sequelize.literal("ROUND(AVG(value))"), "average"],
        [Sequelize.literal("MONTH(date)"), "month"],
        [Sequelize.literal("YEAR(date)"), "year"],
        "period",
      ],
      include: [
        {
          model: responseOption,
          required: true,
        },
      ],
      group: [
        fn("MONTH", Sequelize.col("date")),
        fn("YEAR", Sequelize.col("date")),
        "period",
      ],
      order: [
        [fn("YEAR", Sequelize.col("date")), "ASC"],
        [fn("MONTH", Sequelize.col("date")), "ASC"],
        ["period", "ASC"],
      ],
      includeIgnoreAttributes: false,
    };
    if (req.query.roomId) { // can be one or more values
      options.where.roomId = req.query.roomId.split(",")
    }
    return manageDataRequest(
      req.user.id,
      req.query,
      userFactorResponse,
      options,
      res
    );
  }
});

/**
 * @swagger
 * /stats/seasons:
 *   get:
 *     tags:
 *       - Statistics
 *     description: Returns statistics by season
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: factorId
 *         description: Id of a factor. Might as well be an array of factor id's
 *         in: path
 *         required: true
 *         type: integer
 *       - name: dateFrom
 *         description: Start date of the season
 *         in: query
 *         required: true
 *       - name: dateTo
 *         description: End date of the season
 *         in: query
 *         required: true
 *       - name: roomId
 *         description: Id of a room
 *         in: query
 *         required: false
 *       - name: groupId
 *         description: Id of a group. Might as well by an array of groupd id's
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/seasons", ensureAuthenticated(Role.User), (req, res) => {
  if (
    !variableExists([req.query.factorId, req.query.dateFrom, req.query.dateTo])
  ) {
    res.status(400).send();
  } else {
    const factorIdArray = req.query.factorId.split(",");
    let options = {
      where: {
        date: { [Op.gte]: req.query.dateFrom, [Op.lte]: req.query.dateTo },
        factorId: factorIdArray ? factorIdArray : req.query.factorId,
      },
      include: [
        {
          model: responseOption,
          required: true,
        },
      ],
      group: ["period"],
      order: ["period"],
      attributes: [
        [Sequelize.literal("ROUND(AVG(value))"), "average"],
        "period",
      ],
      includeIgnoreAttributes: false,
    };
    if (req.query.roomId) { // can be one or more values
      options.where.roomId = req.query.roomId.split(",")
    }
    return manageDataRequest(
      req.user.id,
      req.query,
      userFactorResponse,
      options,
      res
    );
  }
});

/**
 * @swagger
 * /stats/themes/sortedByNbVotes:
 *   get:
 *     tags:
 *       - Statistics
 *     description: Returns the themes ordered by the number of votes (descending)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: dateFrom
 *         description: Start date of the season
 *         in: query
 *         required: true
 *       - name: dateTo
 *         description: End date of the season
 *         in: query
 *         required: true
 *       - name: roomId
 *         description: Id of a room
 *         in: query
 *         required: false
 *       - name: groupId
 *         description: Id of a group
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get(
  "/themes/sortedByNbVotes",
  ensureAuthenticated(Role.User),
  (req, res) => {
    if (!variableExists([req.query.dateFrom, req.query.dateTo])) {
      res.status(400).send();
    } else {
      let options = {
        attributes: [[Sequelize.literal("COUNT(themeId)"), "numberOfVotes"]],
        where: {
          date: { [Op.gte]: req.query.dateFrom, [Op.lte]: req.query.dateTo },
        },
        include: [
          {
            model: theme,
            attributes: ["name"],
            required: true,
          },
        ],
        group: ["themeId"],
        order: [[fn("COUNT", Sequelize.col("themeId")), "DESC"]],
      };

      return manageDataRequest(
        req.user.id,
        req.query,
        userThemeResponse,
        options,
        res
      );
    }
  }
);

/**
 * @swagger
 * /stats/themes/sortedByFeeling:
 *   get:
 *     tags:
 *       - Statistics
 *     description: Returns themes ordered by the average of the responses (i.e. satisfaction)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: dateFrom
 *         description: Start date of the season
 *         in: query
 *         required: true
 *       - name: dateTo
 *         description: End date of the season
 *         in: query
 *         required: true
 *       - name: roomId
 *         description: Id of a room
 *         in: query
 *         required: false
 *       - name: groupId
 *         description: Id of a group
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get(
  "/themes/sortedByFeeling",
  ensureAuthenticated(Role.User),
  (req, res) => {
    if (!variableExists([req.query.dateFrom, req.query.dateTo])) {
      res.status(400).send();
    } else {
      let options = {
        attributes: [
          [fn("AVG", Sequelize.col("responseOption.value")), "average"],
          [Sequelize.literal("theme.name"), "name"],
        ],
        where: {
          date: { [Op.gte]: req.query.dateFrom, [Op.lte]: req.query.dateTo },
        },
        include: [
          {
            model: theme,
            attributes: [],
            required: true,
          },
          {
            model: responseOption,
            attributes: [],
            required: true,
          },
        ],
        group: ["themeId"],
        order: [[fn("AVG", Sequelize.col("responseOption.value")), "ASC"]],
      };
      return manageDataRequest(
        req.user.id,
        req.query,
        userThemeResponse,
        options,
        res
      );
    }
  }
);


/**
 * @swagger
 * /stats/theme_factor_satisfaction:
 *   post:
 *     tags:
 *       - Statistics
 *     description: Returns statistics by day (and by period or not)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: themeId
 *         description: Id of a theme.
 *         in: query
 *         required: true
 *         type: integer
 *       - name: requested_dates
 *         description: Object containing a dateFrom and a dateTo
 *         in: body
 *         required: true
 *         type: object
 *         properties:
 *           dateFrom:
 *             type: string
 *             example: "2023-03-13"
 *           dateTo:
 *             type: string
 *             example: "2023-03-17"
 *       - name: groupId
 *         description: Id of a group
 *         in: query
 *         required: false
 *       - name: shouldReturnThemeLevelStatistics
 *         description: Boolean indicating if the statistics should be returned at the theme level
 *         in: query
 *         required: true
 *       - name: shouldReturnFactorLevelStatistics
 *         description: Boolean indicating if the statistics should be returned at the factor level
 *         in: query
 *         required: true
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */


router.post( "/theme_factor_satisfaction", ensureAuthenticated(Role.User), (req, res) => {
  let parsed_json_date = req.body;
  console.log("parsed_json_date: " + JSON.stringify(parsed_json_date));


  if (req.query.groupId) {
    getGroupVotes(
      res,
      req.query.groupId,
      req.query.themeId,
      parsed_json_date,
      req.query.shouldReturnThemeLevelStatistics,
      req.query.shouldReturnFactorLevelStatistics
    )
  } else {
    getUsersVotes(
      res,
      [req.user.id],
      req.query.themeId,
      parsed_json_date,
      req.query.shouldReturnThemeLevelStatistics,
      req.query.shouldReturnFactorLevelStatistics,
    )
  }
});


async function manageDataRequest(userId, query, model, options, res) {
  groupId = query.groupId
  enterpriseId = query.enterpriseId
  if (variableExists(groupId) && groupId) {
    const groupIdArray = groupId.split(",");
    userGroup
      .findAll({
        where: {
          groupId: groupIdArray ? groupIdArray : groupId,
          status: requestStatus.ACCEPTED,
        },
      })
      .then((memberships) => {
        const ids = memberships.map((membership) => membership.userId);
        options.where.userId = ids;
        return findAllEntries(model, options, res);
      })
      .catch((error) => {
        res.status(500).send();
        console.warn(error);
      });
  } else if (variableExists(enterpriseId) && enterpriseId) {
    if (await ensureRole(userId, Role.Admin)) {
      user.findAll({
        where: {
          enterpriseId
        }
      }).then(users => {
        const ids = users.map((user) => user.id);
        options.where.userId = ids;
        return findAllEntries(model, options, res);
      })
    } else {
      res.sendStatus(403)
    }

  }
  else {
    options.where.userId = [userId];
    return findAllEntries(model, options, res);
  }
}

function findAllEntries(model, options, res) {
  model
    .findAll(options)
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
}

module.exports = router;
