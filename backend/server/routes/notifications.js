const express = require("express");

const router = express.Router();
const cron = require("node-cron");
const webpush = require("web-push");
const ensureAuthenticated = require("../helpers/ensureAuthenticated");
let {sendPushMessageAndNotification} = require('../helpers/push')

const {alarm, subscription} = require("../../models");


webpush.setGCMAPIKey(process.env.FCM_API_KEY);
let vapidSubjectURL = process.env.NODE_ENV === "production" ? `${process.env.APP_BASE_URL}:8080` : `https://${process.env.APP_BASE_URL}:8080`;
webpush.setVapidDetails(
  vapidSubjectURL, // subject (is used by the push api host to contact the app owner (us) in case of any problems
  process.env.PUBLIC_VAPID_KEY_BASE64, // publicKey
  process.env.PRIVATE_VAPID_KEY // privateKey
);

/**
 * @swagger
 * /notifications/subscription:
 *   post:
 *     tags:
 *       - Push_subscription
 *     description: Save in the database a Push subscription for a client
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         description: userId, subscription, date, it's all done by the front-end
 *         schema:
 *           type: object
 *           properties:
 *             user:
 *               type: int
 *               properties:
 *                 id:
 *                   type: int
 *             subscription:
 *               type: object
 *             date:
 *               type: datetime
 *         required:
 *           - userId, subscription, datetime
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
// Subscribe to web push service
router.post("/subscription", ensureAuthenticated(), (req, res) => {
    subscription.findAll({
      attributes: ["id", "subscription", "userId"],
      where: {userId: req.user.id},
      raw: true,
      nest: true,
    })
      .then((results) => {
        if (results && results.length !== 0) {
          subscription.destroy({
            where: {
              userId: req.user.id
            },
          }).then(() => {
            res.status(200).send()
            subscription.create({
              subscription: req.body,
              userId: req.user.id,
            }).then((result) => res.json(result)).catch((error) => {
              res.status(500).send();
              console.warn(error);
            });
          }).catch((error) => {
            res.status(500).send();
            console.warn(error);
          });
        } else {
          subscription.create({
            subscription: req.body,
            userId: req.user.id,
          }).then((result) => res.json(result)).catch((error) => {
            res.status(500).send();
            console.warn(error);
          });
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  }
);

/**
 * @swagger
 * /notifications/unsubscription:
 *   post:
 *     tags:
 *       - Push_subscription
 *     description: Destroy in the database a Push subscription for a client
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         description: userId
 *         schema:
 *           type: object
 *           properties:
 *             user:
 *               type: int
 *               properties:
 *                 id:
 *                   type: int
 *         required:
 *           - userId
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
// Unsubscribe to web push service
router.post("/unsubscription", ensureAuthenticated(), (req, res) => {
    subscription.findAll({
      attributes: ["id", "subscription", "userId"],
      where: {userId: req.user.id},
      raw: true,
      nest: true,
    }).then((results) => {
      if (results && results.length !== 0) {
        subscription.destroy({
          where: {
            userId: req.user.id
          },
        }).then(() => {
          res.status(200).send()
        }).catch((error) => {
          res.status(500).send();
          console.warn(error);
        });
      }
    }).catch((error) => {
      console.warn(error);
    });
  }
);

/**
 * @swagger
 * /notifications/sendMessageAndNotification:
 *   post:
 *     tags:
 *       - Push_Notifications_Messages
 *     description: Send a Push message straight to the Chatbot on the specified userId app.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         description: Set all of the variables.
 *         schema:
 *           type: object
 *           properties:
 *             user:
 *               type: object
 *               properties:
 *                 id:
 *                   type: int,
 *             hasAlarmAttached:
 *               type: boolean
 *             shouldSendNotification:
 *               type: boolean
 *             notificationContent:
 *               type: string
 *             notificationRequireInteraction:
 *               type: boolean
 *             actions:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  action:
 *                    type: string
 *                  title:
 *                   type: string
 *             alarmId:
 *               type: int
 *             shouldSendMessage:
 *               type: boolean
 *             messageContent:
 *               type: string
 *         required:
 *           - user, hasAlarmAttached, shouldSendNotification, notificationContent, notificationRequireInteraction, shouldSendMessage, messageContent
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.post("/sendMessageAndNotification", (req, res) => {
  let params = {
    userId: req.body.user.id,
    hasAlarmAttached: req.body.hasAlarmAttached,
    shouldSendNotification: req.body.shouldSendNotification,
    notificationContent: req.body.notificationContent,
    notificationRequireInteraction: req.body.notificationRequireInteraction,
    actions: req.body.actions,
    alarmId: req.body.alarmId,
    shouldSendMessage: req.body.shouldSendMessage,
    messageContent: req.body.messageContent,
  }
  sendPushMessageAndNotification(params).then(r => res.json(r));
});

/**
 * @swagger
 * /notifications/updatestatus:
 *   post:
 *     tags:
 *       - Push_Notifications_Messages
 *     description: Update the status of an alarm in the DB from send to received.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         description: alarm's id
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: int
 *             newStatus:
 *               type: string
 *         required:
 *           - userId, id
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.post("/updatestatus", ensureAuthenticated(), (req, res) => {
  console.log(req.body.id);
  alarm.findAll({
    attributes: ["id", "notificationStatus"],
    where: {id: req.body.id},
    raw: true,
    nest: true,
  }).then((results) => {
    console.log(req.body);
    if (results && results.length !== 0) {
      alarm.update(
        {notificationStatus: req.body.newStatus === "read" ? "received" : req.body.newStatus},
        {where: {id: req.body.id}}
      ).then(() => {
        res.status(200).send()
      }).catch((error) => {
        res.status(500).send();
        console.warn(error);
      });
    } else {
      res.status(500).send();
    }
  }).catch((error) => {
    console.warn(error);
  });
});

/**
 * Get answer options to an alarm
 * @name get
 * @function
 * @return answer options
 */
router.get("/get-answer-options", ensureAuthenticated(), (req, res) => {
  //const options = ['C\'est une fausse alerte', 'Je vais prendre des mesures', 'J\'ai déjà entamé des mesures'];
  const optionsAPI = alarm.rawAttributes.state.values;
  res.send(optionsAPI);
});


// Send push notification every week day at 16:30 (the docker container has a timeshift of +1h00).
// As a consequence, to send message on weekdays at 16:30 CET, the cron job should be scheduled to 30 15 * * 1-5
cron.schedule(
  // "* * * * *",
  "30 16 * * 1-5",
  () => {
    let params = {
      userId: null,
      hasAlarmAttached: false,
      shouldSendNotification: true,
      notificationContent: "Comment s'est passée votre journée ?",
      notificationRequireInteraction: true,
      actions: [
        {
          action: "vote",
          title: "Répondre",
        },
      ],
      alarmId: -1,
      shouldSendMessage: false,
      messageContent: "",
    }
    subscription.findAll({
      attributes: ["id", "subscription", "userId"],
      raw: true,
      nest: true,
    }).then((result) => {
      if (result.length !== 0) {
        result.forEach((sub) => {
          params.userId = sub.userId;
          sendPushMessageAndNotification(params).then(_ => console.log("Notifications sent"));
        });

      }
    }).catch((error) => {
      console.warn(error);
    });
  },
  {
    scheduled: true,
    timezone: "Europe/Berlin",
  }
);

module.exports = router;
