/**
 * @file OneShot related calls to the database
 * @author Uchendu Nwachukwu <uchendu.nwachukwu@edu.hefr.ch>
 * @date 02.10.2020
 * @version 2.0
 */

const express = require('express');
const uuid = require("uuid");
const {Op} = require('sequelize');

const router = express.Router({mergeParams: true});
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const Role = require('../helpers/role');
const {
  theme,
  factor,
  factorEnterprise,
  responseOptionSet,
  responseOption,
  userFactorResponse,
  user,
  userGroup
} = require('../../models');
const {getToday} = require('../helpers/date');
const variableExists = require("../helpers/variable");

// THIS IS CLEARLY A SHORTCUT FUNCTION AND INTENDED FOR VERY SPECIFIC USAGE


/**
 * @swagger
 * /oneshot:
 *   get:
 *     tags:
 *       - ResponseOptions
 *     description: Gets the whole survey catalogue (themes, factors, responseOptions) of an enterprise
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get('/', ensureAuthenticated(Role.User), async (req, res) => {
  const data = await theme.findAll({
    order: [
      ["id", "ASC"],
      [factor, "id", "ASC"],
      [factor, responseOptionSet, responseOption, "id", "ASC"], // sorts the nested include...
      [responseOptionSet, responseOption, "id", "ASC"] // sorts the nested include...
    ],
    include: [
      {
        model: factor,
        required: true,
        attributes: {exclude: ['responseOptionSetId']},
        include: [
          {
            model: factorEnterprise,
            attributes: ["createdAt"],
            required: true,
            where: {
              enterpriseId: req.user.enterpriseId
            }
          },
          {
            model: responseOptionSet,
            include: [
              {
                model: responseOption
              },
            ]
          }
        ]
      },
      {
        model: responseOptionSet,
        include: [
          {
            model: responseOption
          },
        ]
      }
    ]
  })

  res.send(data)
});


// Should maybe be moved to the responses file
/**
 * @swagger
 * /oneshot/responses:
 *   get:
 *     tags:
 *       - Responses
 *     description: Get all responses of the user for today
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.get('/responses', ensureAuthenticated(Role.User), async (req, res) => {
  const data = await userFactorResponse.findAll({
    order: [
      ["id", "ASC"]
    ],
    where: {date: getToday(), userId: req.user.id},
    include: [
      {
        model: factor,
        attributes: ["themeId"]
      }
    ]
  });
  res.send(data);
});


/**
 * @swagger
 * /oneshot/allresponses/since/{since}/to/{to}:
 *   get:
 *     tags:
 *       - Responses
 *     description: Get all responses of the current user between a specific date range.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: since
 *         description: Start date (YYYY-MM-DD)
 *         in: path
 *         required: true
 *         type: string
 *       - name: to
 *         description: End date (YYYY-MM-DD)
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.get('/allresponses/since/:since/to/:to', ensureAuthenticated(Role.User), async (req, res) => {
  const {since, to} = req.params

  userFactorResponse.findAll({
    order: [
      ["id", "ASC"]
    ],
    where: {
      date: {
        [Op.between]: [new Date(since), new Date(to)]
      },
      userId: req.user.id
    },
    include: [
      {
        model: factor,
        attributes: ["themeId"]
      }
    ]
  }).then(data => {
    res.send(data);
  }).catch(err => {
    console.log(err)
    res.status(500).send('An error occurred while requesting all responses')
  })
});


/**
 * @swagger
 * /oneshot/allresponses/since/{since}/to/{to}/group/{groupId}:
 *   get:
 *     tags:
 *       - Responses
 *     description: Get all responses of the group between a specific date range. Must be an admin user to do this req.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: since
 *         description: Start date (YYYY-MM-DD)
 *         in: path
 *         required: true
 *         type: string
 *       - name: to
 *         description: End date (YYYY-MM-DD)
 *         in: path
 *         required: true
 *         type: string
 *       - name: groupId
 *         description: group id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.get('/allresponses/since/:since/to/:to/group/:groupId', ensureAuthenticated(Role.Admin), async (req, res) => {
  const {since, to, groupId} = req.params

  const allUserGroups = await userGroup.findAll({
    attributes: ["userId", "status", "role"],
    where: {
      groupId
    },
    include: [
      {
        model: user,
        attributes: ["firstName", "lastName", "userName", "email"],
      },
    ]
  });
  const userIds = allUserGroups.map(g => g.userId);

  userFactorResponse.findAll({
    order: [
      ["id", "ASC"]
    ],
    where: {
      date: {
        [Op.between]: [new Date(since), new Date(to)]
      },
      userId: userIds
    },
    include: [
      {
        model: factor,
        attributes: ["themeId"]
      }
    ]
  }).then(data => {
    const anonymousUserIds = {};
    for (const userId of userIds) anonymousUserIds['user_' + userId] = uuid.v4();
    const anonymousData = data.map((r) => {
      let rcopy = JSON.parse(JSON.stringify(r));
      rcopy.userId = anonymousUserIds['user_' + r.userId];
      return rcopy;
    });
    res.send(anonymousData);
  }).catch(err => {
    console.log(err)
    res.status(500).send('An error occured while requesting all responses')
  })
});


/**
 * @swagger
 * /oneshot/allresponses:
 *   get:
 *     tags:
 *       - Responses
 *     description: Get all responses corresponding to the dates, factorId, groupId/personId.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: openDays
 *         description: List of open days (date type)
 *         in: query
 *         required: true
 *       - name: factorId
 *         description: Id of factor
 *         in: query
 *         required: true
 *       - name: groupId
 *         description: Id of group. Either groupId or personId must be defined.
 *         in: query
 *         required: false
 *       - name: personId
 *         description: Id of person. Either groupId or personId must be defined.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.get('/allresponses', ensureAuthenticated(Role.Admin), async (req, res) => {
  if (!variableExists([req.query.openDays, req.query.factorId, req.query.userId])) {
    res.status(400).send();
    return
  }

  userFactorResponse.findAll({
    order: [
      ["id", "ASC"]
    ],
    where: {
      date: req.query.openDays,
      factorId: req.query.factorId,
      userId: req.query.userId
    },
  }).then(data => {
    res.send(data);
  }).catch(err => {
    console.log(err)
    res.status(500).send('An error occurred while requesting all responses')
  })
});


module.exports = router;
