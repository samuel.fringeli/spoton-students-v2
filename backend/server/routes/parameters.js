/**
 * @file Parameter related calls to the database
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

const express = require('express');

const router = express.Router();
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const Role = require('../helpers/role');
const {
  Parameter,
  Ressenti,
  Effect
} = require('../../models');

/**
 * Get all parameters
 * @name get
 * @function
 * @return parameters or error 500
 */
router.get('/', ensureAuthenticated(Role.User), (req, res) => {
  Parameter.findAll()
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * Get a specific parameter
 * @name get
 * @function
 * @param {id} id of the parameter
 * @return parameter or error 500
 */
router.get('/:id', ensureAuthenticated(Role.User), (req, res) => {
  Parameter.findOne({
      include: [{
        model: Ressenti,
      }, {
        model: Effect,
      }],
      where: {
        id: Number(req.params.id),
      },
    })
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * Create a parameters
 * @name post
 * @function
 * @param {Object} data for the parameters creation
 * @return parameters created or error 500
 */
router.post('/', ensureAuthenticated(Role.Admin), (req, res) => {
  Parameter.create(req.body)
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * Update a parameter
 * @name put
 * @function
 * @param {id} id of the parameter to update
 * @param {Object} data for the parameter update
 * @return status 200 or error 500
 */
router.put('/:id', ensureAuthenticated(Role.Admin), (req, res) => {
  Parameter.update(req.body, {
      where: {
        id: req.params.id,
      },
    })
    .then(() => res.status(200).send())
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * Delete a parameter
 * @name delete
 * @function
 * @param {id} id of the parameter to delete
 * @return status 200 or error 500
 */
router.delete('/:id', ensureAuthenticated(Role.Admin), (req, res) => {
  Parameter.destroy({
      where: {
        id: req.params.id,
      },
    })
    .then(() => res.status(200).send())
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

module.exports = router;
