/**
 * @file Group related calls to the database
 * @author Roland Salloum <roland.salloum00@outlook.com>
 * @date 31.07.2020
 */

const express = require('express');
const router = express.Router();
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const Role = require('../helpers/role');
const {
  group,
  user,
  userGroup,
} = require('../../models');
const requestStatus = require('../types/requestStatus');
const groupRole = require('../types/groupRole');
const {ensureUserIsAdminInGroup, ensureGroupExistsInEnterprise} = require('../helpers/validation');
const createRule = require('../helpers/rule');

const throwError = (res, error) => {
  res.sendStatus(500);
  console.warn(error);
};

/**
 * @swagger
 * /groups:
 *   get:
 *     tags:
 *       - Groups
 *     description: Returns all groups of a user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get('/', ensureAuthenticated(Role.User), async (req, res) => {
  try {
    let userGroups = await userGroup.findAll({
      attributes: {exclude: ['groupId']},
      where: {
        userId: req.user.id,
      },
      include: [
        {
          model: group,
          attributes: ['id', 'name', 'description', 'enterpriseId'],
        },
      ],
    });
    res.json(userGroups);
  } catch (err) {
    throwError(res, err);
  }
});

/**
 * @swagger
 * /groups:
 *   post:
 *     tags:
 *       - Groups
 *     description: Create a group in the enterprise of the user
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *             description:
 *               type: string
 *         required:
 *           - name
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.post('/', ensureAuthenticated(Role.User), async (req, res) => {
  try {
    await user.findOne({
      where: {
        username: req.session.username,
      },
    }).then(async (fullUser) => {
      await group.create({
        name: req.body.name,
        description: req.body.description,
        enterpriseId: fullUser.enterpriseId,
      }).then(async (group) => {
        const userGroupPayload = {
          status: requestStatus.ACCEPTED,
          role: groupRole.ADMIN,
          groupId: group.id,
          userId: fullUser.id,
        };
        await userGroup.create(userGroupPayload);
        const status = await createRule('group', group);
        if (status === 500) {
          res.status(500).send({
            group,
            ... userGroupPayload,
          });
        } else if (status === 200) {
          res.status(200).send({
            group,
            ... userGroupPayload,
          });
        }
      });
    });
  } catch (err) {
    throwError(res, err);
  }
});

/**
 * @swagger
 * /groups/{groupId}:
 *   put:
 *     tags:
 *       - Groups
 *     description: Update a group (requires the user to be admin of the group)
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: groupId
 *         description: Group's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *             description:
 *               type: string
 *         required:
 *           - name
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.put('/:groupId', ensureAuthenticated(Role.User), ensureGroupExistsInEnterprise, ensureUserIsAdminInGroup, async (req, res) => {
  try {
    await group.update(req.body, {
      where: {
        id: Number(req.params.groupId),
      },
    }).then(() => {
      res.send({
        ... req.body,
        id: Number(req.params.groupId),
      });
    });
  } catch (err) {
    throwError(res, err);
  }
});

/**
 * @swagger
 * /groups/{groupId}:
 *   delete:
 *     tags:
 *       - Groups
 *     description: Delete a group (requires the user to be admin of the group)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: groupId
 *         description: Group's id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.delete('/:groupId', ensureAuthenticated(Role.User), ensureGroupExistsInEnterprise, ensureUserIsAdminInGroup, async (req, res) => {
  try {
    userGroup.destroy({
      where: {
        groupId: Number(req.params.groupId),
      },
    }).then(() => {
      group.destroy({
        where: {
          id: Number(req.params.groupId),
        },
      })
          .then(() => res.sendStatus(200));
    });
  } catch (err) {
    throwError(res, err);
  }
});

module.exports = router;
