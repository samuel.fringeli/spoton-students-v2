/**
 * @file Room related calls to the database
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

const express = require("express");

const router = express.Router();
const ensureAuthenticated = require("../helpers/ensureAuthenticated");
const Role = require("../helpers/role");
const { room, sensorRoom } = require("../../models");

/**
 * Get all rooms
 * @name get
 * @function
 * @return rooms or error 500
 */
router.get("/", ensureAuthenticated(Role.User), (req, res) => {
  room
    .findAll({
      attributes: ["id", "name"],
      where: {
        enterpriseId:
          req.body.enterpriseId && req.user.role >= Role.Root
            ? req.body.enterpriseId
            : req.user.enterpriseId,
      },
      order: ["name"],
    })
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * Get a specific room
 * @name get
 * @function
 * @param {id} id of the room
 * @return user or error 500
 */
router.get("/:id", ensureAuthenticated(Role.User), (req, res) => {
  room
    .findOne({
      attributes: ["id", "name"],
      where: {
        id: Number(req.params.id),
      },
    })
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * Create a room
 * @name post
 * @function
 * @param {Object} data for the room creation
 * @return room created or error 500
 */
router.post("/", ensureAuthenticated(Role.Admin), (req, res) => {
  room
    .create({
      ...req.body,
      enterpriseId:
        req.body.enterpriseId && req.user.role >= Role.Root
          ? req.body.enterpriseId
          : req.user.enterpriseId,
    })
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * Update a room
 * @name put
 * @function
 * @param {id} id of the room to update
 * @param {Object} data for the room update
 * @return status 200 or error 500
 */
router.put("/:id", ensureAuthenticated(Role.Admin), (req, res) => {
  room
    .update(req.body, {
      where: {
        id: req.params.id,
      },
    })
    .then(() => res.status(200).send())
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * Delete a room
 * @name delete
 * @function
 * @param {id} id of the room to delete
 * @return status 200 or error 500
 */
router.delete("/:id", ensureAuthenticated(Role.Admin), (req, res) => {
  room
    .destroy({
      where: {
        id: req.params.id,
      },
    })
    .then(() => res.status(200).send())
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * Get all objectIds for one room
 * @name get
 * @function
 * @return objectIds or error 500
 */
router.get("/:id/sensors", ensureAuthenticated(Role.User), (req, res) => {
  sensorRoom
    .findAll({
      attributes: ["roomId", "sensorId", "sensorType"],
      where: {
        roomId: req.params.id,
        sensorType: req.query.sensorType
      },
      limit: 1,
    })
    .then((result) => res.json(result[0])) // first element as we asked one
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

module.exports = router;
