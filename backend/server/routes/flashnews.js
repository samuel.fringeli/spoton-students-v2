const express = require('express');
const ensureAuthenticated = require("../helpers/ensureAuthenticated");
const Role = require("../helpers/role");
const {pdf, userGroup, group} = require("../../models");
const router = express.Router();

function capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function groupByMonth(items) {
    const grouped = {};

    items.forEach(item => {
        const date = new Date(item.createdAt);
        const monthYear = capitalizeFirstLetter(date.toLocaleString('fr-FR', { month: 'long', year: 'numeric' }));

        if (!grouped[monthYear]) {
            grouped[monthYear] = [];
        }

        grouped[monthYear].push(item.url);
    });

    return Object.keys(grouped).map(key => ({
        title: key,
        pdfs: grouped[key]
    }));
}

/**
 * @swagger
 * /flashnews/{groupId}:
 *   get:
 *     tags:
 *       - FlashNews
 *     summary: Get PDFs for a specific group
 *     description: >
 *       Retrieves a list of PDFs associated with a given group ID, if the user
 *       is part of that group. Requires user authentication.
 *     parameters:
 *       - in: path
 *         name: groupId
 *         required: true
 *         schema:
 *           type: string
 *         description: The ID of the group to retrieve PDFs for
 *       - in: query
 *         name: formatTitles
 *         required: false
 *         schema:
 *           type: boolean
 *         description: If set to true, the titles of the PDFs will be formatted. Defaults to false.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Bad Request (invalid group ID)
 *       401:
 *         description: Unauthorized (authentication missing or invalid)
 *       403:
 *         description: Forbidden (user not part of the group)
 *       500:
 *         description: Server-side processing error
 */

router.get('/:groupId', ensureAuthenticated(Role.User), async (req, res) => {
    try {
        let userGroups = await userGroup.findAll({
            where: {
                userId: req.user.id,
                groupId: req.params.groupId
            },
            include: [
                {
                    model: group,
                    attributes: ['id', 'name', 'description', 'enterpriseId'],
                },
            ],
        });
        if (userGroups.length > 0) {
            let pdfs = await pdf.findAll({
                where: {
                    groupId: req.params.groupId
                }
            });
            if (req.query.formatTitles === 'true') {
                res.json(groupByMonth(pdfs));
            } else {
                res.json(pdfs);
            }
        } else {
            res.json([]);
        }
    } catch (err) {
        throwError(res, err);
    }
});

module.exports = router;
