/**
 * @file ResponseOptions related calls to the database
 * @author Uchendu Nwachukwu <uchendu.nwachukwu@edu.hefr.ch>
 * @date 02.10.2020
 * @version 2.0
 */

const express = require('express');

const router = express.Router({ mergeParams: true });
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const Role = require('../helpers/role');

const { getFactorByPk, getResponseOptionsForEntity } = require('../helpers/frequentGetters');

/**
 * @swagger
 * /themes/{themeId}/factors/{factorId}/responseOptions:
 *   get:
 *     tags:
 *       - ResponseOptions
 *     description: Get all possible responses for a particular factor
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: themeId
 *         description: Id of a theme
 *         in: path
 *         required: true
 *         type: integer
 *       - name: factorId
 *         description: Id of a factor
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error 
 */
router.get('/', ensureAuthenticated(Role.User), getFactorByPk, async (req, res) => {
  try {
    const responseOptions = await getResponseOptionsForEntity(req.factor)
    res.json(responseOptions)
  } catch (err) {
    console.warn(err)
    res.sendStatus(500)
  }
});

module.exports = router;
