/**
 * @file Theme related calls to the database
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

const express = require('express');
const shortid = require('shortid');
const fs = require('fs');

const router = express.Router();
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const { isResponseToEntity } = require('../helpers/validation');
const convert = require('../helpers/wordconverter');
const Role = require('../helpers/role');
const { getToday, getCurrentTimestamp } = require("../helpers/date");
const { getResponseOptionsForEntity } = require('../helpers/frequentGetters')
const {
  theme,
  userThemeResponse,
  user,
  factor,
  responseSet,
  response,
} = require('../../models');

/**
 * @swagger
 * /themes:
 *   get:
 *     tags:
 *       - Themes
 *     description: Get all themes (not specific to an enterprise)
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error 
 */
router.get('/', ensureAuthenticated(Role.User), (req, res) => {
  theme.findAll()
    .then((result) => {
      res.json(result)
    })
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});


/**
 * @swagger
 * /themes/responses:
 *   get:
 *     tags:
 *       - Responses
 *     description: Get responses of today to all themes for the user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error 
 */
router.get('/responses', ensureAuthenticated(Role.User), async (req, res) => {
  // This method is placed here to prevent matching with the wildcard /:themeId, which would provoke an error
  try {
    const responsesFound = await userThemeResponse.findAll({
      where: {
        date: getToday(),
        userId: req.user.id,
      }
    })
    res.json(responsesFound)
  } catch (error) {
    res.status(500).send();
    console.warn(error);
  };
});

/**
 * @swagger
 * /themes/{themeId}:
 *   get:
 *     tags:
 *       - Themes
 *     description: Get a specifc theme
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: themeId
 *         description: Id of a theme
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error 
 */
router.get('/:themeId', ensureAuthenticated(Role.User), (req, res) => {
  theme.findByPk(req.params.themeId)
    .then((result) => {
      if (result === null) {
        res.sendStatus(404)
      } else {
        res.json(result)
      }
    })
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * @swagger
 * /themes/{themeId}/responseOptions:
 *   get:
 *     tags:
 *       - ResponseOptions
 *     description: Get the responseOptions to the question of a theme
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: themeId
 *         description: Id of a theme
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error 
 */
router.get('/:themeId/responseOptions', ensureAuthenticated(Role.User), async (req, res) => {
  try {
    // minimal validation
    const currentTheme = await theme.findByPk(req.params.themeId)
    if (currentTheme === null) {
      res.sendStatus(404)
      return
    }
    const currentResponseOptions = await getResponseOptionsForEntity(currentTheme)
    res.json(convert(currentResponseOptions, req.session.sex))
  } catch (error) {
    res.status(500).send();
    console.warn(error);
  };
});

/**
 * @swagger
 * /themes/{themeId}/responses:
 *   get:
 *     tags:
 *       - Responses
 *     description: Get todays response of the user for given theme (only used for the environment category question)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: themeId
 *         description: Id of a theme
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error 
 */
router.get('/:themeId/responses', ensureAuthenticated(Role.User), async (req, res) => {
  try {
    const responseFound = await userThemeResponse.findAll({
      where: {
        date: getToday(),
        userId: req.user.id,
        themeId: req.params.themeId
      }
    })
    res.json(responseFound)
  } catch (error) {
    res.status(500).send();
    console.warn(error);
  };
});

/**
 * @swagger
 * /themes/{themeId}/responses:
 *   put:
 *     tags:
 *       - Responses
 *     description: Upsert the current response of the user. Key on the columns (userId, themeId, period, date)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: themeId
 *         description: Id of a theme
 *         in: path
 *         required: true
 *         type: integer
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             factorId:
 *               type: integer
 *             responseOptionId:
 *               type: integer
 *             roomId:
 *               type: integer
 *             period:
 *               type: string
 *               enum:
 *                 - morning
 *                 - afternoon
 *                 - day
 *         required:
 *           - period
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error 
 */
router.put('/:themeId/responses', ensureAuthenticated(Role.User), async (req, res) => {
  try {
    // minimal validation
    const currentTheme = await theme.findByPk(req.params.themeId)
    if (currentTheme === null) {
      res.sendStatus(404)
      return
    }
    const valid = await isResponseToEntity(req.body.responseOptionId, currentTheme)
    if (!valid) {
      res.sendStatus(400)
      return
    }

    const currentResponse = {
      date: getToday(),
      datetime: getCurrentTimestamp(),
      userId: req.user.id,
      themeId: req.params.themeId,
      ...req.body
    }
    
    await userThemeResponse.upsert(currentResponse);

    res.sendStatus(200);
    augmentPointsOfUser(req.user.id, req.user.userPoints);
  } catch (error) {
    res.status(500).send();
    console.warn(error);
  }
});

const augmentPointsOfUser = (userId, currentUserPoints) => {
  // with steps of 25, the level climbs when filling out the survey as foreseen
  const newUserPoints = currentUserPoints + 25;
  user.update({ points: newUserPoints }, { where: { id: userId } })
}

/**
 * Create a theme
 * @todo Images upload are currently not used
 * @name post
 * @function
 * @param {Object} data for the theme creation
 * @return theme created or error 500
 */
router.post('/', ensureAuthenticated(Role.Admin), (req, res) => {
  if (!req.files) {
    res.status(500).send();
  } else {
    const file = req.files.icon;
    if (!/^image\/svg/.test(file.mimetype)) {
      res.status(500).send();
    } else {
      const icon = `${shortid.generate()}.svg`;
      file.mv(`/uploads/${icon}`);

      theme.create({
        icon,
        ...req.body,
      })
        .then((result) => res.json(result))
        .catch((error) => {
          res.status(500).send();
          console.warn(error);
        });
    }
  }
});

/**
 * Update a theme
 * @todo Images upload are currently not used
 * @name put
 * @function
 * @param {Object} data for the theme update
 * @return error 500 if necessary
 */
router.put('/:themeId', ensureAuthenticated(Role.Admin), (req, res) => {
  const {
    id,
    ...theme
  } = req.body;

  if (req.files) {
    const file = req.files.icon;
    if (!/^image\/svg/.test(file.mimetype)) {
      res.status(500).send();
    } else {
      theme.icon = `${shortid.generate()}.svg`;
      file.mv(`/uploads/${theme.icon}`);

      theme.findOne({
        attributes: ['icon'],
        where: {
          id: Number(req.params.themeId),
        },
      })
        .then((result) => {
          fs.unlinkSync(`/uploads/${result.icon}`);
        });
    }
  }

  /**
   * Update a theme
   * @name put
   * @function
   * @param {id} id of the theme to update
   * @param {Object} data for the theme update
   * @return status 200 or error 500
   */
  theme.update(theme, {
    where: {
      id: req.params.themeId,
    },
  })
    .then(() => res.status(200).send())
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * Delete a theme
 * @name delete
 * @todo Icon upload is currently not used
 * @function
 * @param {id} id of the theme to delete
 * @return status 200 or error 500
 */
router.delete('/:themeId', ensureAuthenticated(Role.Admin), (req, res) => {
  theme.findOne({
    attributes: ['icon'],
    where: {
      id: Number(req.params.themeId),
    },
  })
    .then((result) => {
      theme.destroy({
        where: {
          id: req.params.themeId,
        },
      })
        .then(() => {
          fs.unlinkSync(`/uploads/${result.icon}`);
          res.status(200).send();
        })
        .catch((error) => {
          res.status(500).send();
          console.warn(error);
        });
    })
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

module.exports = router;
