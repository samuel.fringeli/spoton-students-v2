/**
 * @file API call for factors related query
 * @author Uchendu Nwachukwu <uchendu.nwachukwu@edu.hefr.ch>
 * @date 02.10.2020
 * @version 2.1
 */

const express = require('express');
const Sequelize = require('sequelize');

const router = express.Router({ mergeParams: true });
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const Role = require('../helpers/role');
const {
  factor,
  theme,
} = require('../../models');

const { getThemeByPk } = require('../helpers/frequentGetters')

/**
 * @swagger
 * /themes/{themeId}/factors:
 *   get:
 *     tags:
 *       - Factors
 *     description: Get all factors for a theme (not specific to an enterprise)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: themeId
 *         description: Id of a theme
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error 
 */
router.get('/', ensureAuthenticated(Role.User), getThemeByPk, (req, res) => {
  req.theme.getFactors().then(factors => {
    res.json(factors)
  })
});


module.exports = router;
