/**
 * @file Cron related routes
 * @author Léonard Noth <leonard.noth@master.hes-so.ch>
 * @date 22.07.2020
 * @version 2.0
 */

const express = require("express");
const util = require('util');
const cron = require("node-cron");
const fetch = require('node-fetch');
const router = express.Router();
let Netmask = require('netmask').Netmask

const exec = util.promisify(require('child_process').exec);
let cronsRunning = []

function checkIfIPIsLocal(requestIP) {
  const localIPs = [new Netmask('10.0.0.0/8'), new Netmask('172.16.0.0/12'), new Netmask('192.168.0.0/16')]
  let returnResult = false
  for (let localIPMask of localIPs) {
    if (localIPMask.contains(requestIP)) {
      returnResult = true;
    }
  }
  return returnResult;
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

async function runBashCommand(bashCommand) {
  try {
    const { stdout, _ } = await exec(bashCommand);
    console.log('stdout:', stdout);
    // console.log('stderr:', stderr);
    return true;
  } catch (err) {
    // console.error(err);
    return false;
  }
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

// Init the crons

const list_of_crons = [
  {
    'periodicity': 'daily',
    'cron_expression': '10 01 * * 1-5',
  },
  {
    'periodicity': 'weekly',
    'cron_expression': '10 01 * * 7',
  },
  {
    'periodicity': 'monthly',
    'cron_expression': '10 01 1 * *',
  }
]

for (const cron_to_create of list_of_crons) {
  let new_cron = cron.schedule(
    cron_to_create['cron_expression'],
    () => {
      // Make post request to python script
      fetch('http://rule_scheduler:5000/run_rule_checker', {
        method: 'POST',
        body: JSON.stringify({
          'username' : 'admin',
          'password' : 's00z_Admin',
          'periodicity' : cron_to_create['periodicity']
        }),
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(json => console.log(json))
        .catch (err => console.log(err));
    },
    {
      scheduled: true,
      timezone: "Europe/Berlin",
    }
  );

  cronsRunning.push({
    'id': uuidv4(),
    'name': capitalizeFirstLetter(cron_to_create['periodicity']) + " rule checker",
    'description': "This cron will automatically check the rules " + cron_to_create['periodicity'],
    'cron': new_cron
  });
}


let flash_news_cron = cron.schedule(
    '10 01 * * 7',
    () => {
      // Make post request to python script
      // TODO: Change to kilian service url
      fetch('http://kilian_service:5000/', {
        method: 'GET',
        body: JSON.stringify({}),
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
          .then(res => res.json())
          .then(json => console.log(json))
          .catch (err => console.log(err));
    },
    {
      scheduled: true,
      timezone: "Europe/Berlin",
    }
);


/**
 * @swagger
 * /crons:
 *   post:
 *     tags:
 *       - Crons
 *     description: Create a new cron
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             cronName:
 *               type: string
 *             cronDescription:
 *               type: string
 *             cronScheduleExpression:
 *               type: string
 *             cronBashCommand:
 *               type: string
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.post("/", (req, res) => {
  const clientIP = (req.headers['x-forwarded-for'] || req.socket.remoteAddress).replace(/^.*:/, '');
  if (checkIfIPIsLocal(clientIP)) {
    let new_cron = cron.schedule(
      req.body.cronScheduleExpression,
      () => {
        runBashCommand(req.body.cronBashCommand)
      },
      {
        scheduled: true,
        timezone: "Europe/Berlin",
      }
    );
    cronsRunning.push({
      'id': uuidv4(),
      'name': req.body.cronName,
      'description': req.body.cronDescription,
      'cron': new_cron
    });
    res.status(200).send('The cron was created');

  } else {
    res.status(401).send('This route is only available from local network');
  }
});


/**
 * @swagger
 * /crons/{cronId}:
 *   delete:
 *     tags:
 *       - Crons
 *     description: Stop and delete one cron
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: cronId
 *         description: Id of a cron
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.delete("/:id", (req, res) => {
  const clientIP = (req.headers['x-forwarded-for'] || req.socket.remoteAddress).replace(/^.*:/, '');
  if (checkIfIPIsLocal(clientIP)) {
    let cronToDelete = cronsRunning.filter(cron => cron.id === req.params.id)[0]
    cronToDelete['cron'].stop();
    cronsRunning = cronsRunning.filter(cron => cron.id !== req.params.id)
    res.status(200).send('The cron was stopped');
  } else {
    res.status(401).send('This route is only available from local network');
  }
});

/**
 * @swagger
 * /crons:
 *   get:
 *     tags:
 *       - Crons
 *     description: Get all the crons
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get('/', (req, res) => {
  const clientIP = (req.headers['x-forwarded-for'] || req.socket.remoteAddress).replace(/^.*:/, '');
  if (checkIfIPIsLocal(clientIP)) {
    res.send({
      crons : cronsRunning.map(item => ({'id': item['id'], 'name': item['name'], 'description': item['description']}))
    });
  } else {
    res.status(401).send('This route is only available from local network');
  }
});


module.exports = router;
