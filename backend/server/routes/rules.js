/**
 * @file Rules related calls to the database
 * @author Samuel Fringeli <samuel.fringeli@hefr.ch>
 * @date 11.04.2022
 * @version 1.0
 */

const express = require('express');

const router = express.Router({mergeParams: true});
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const Role = require('../helpers/role');
const {rule, alarm, user, group} = require('../../models');
const {getToday} = require('../helpers/date');

/**
 * @swagger
 * /rules:
 *   get:
 *     tags:
 *       - Rules
 *     description: Get all rules
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get('/', ensureAuthenticated(Role.User), async (req, res) => {
  const userRules = await rule.findAll({
    order: [['id', 'ASC']], include: [{
      model: user, attributes: ['firstName', 'lastName', 'username', 'email'], required: true, where: {id: req.user.id},
    }],
  });
  const groupRulesUnfiltered = (await rule.findAll({
    order: [['id', 'ASC']], include: [{
      model: group, required: true, include: [{
        model: user, attributes: ['firstName', 'lastName', 'username', 'email'],
      }],
    }],
  }));
  const groupRules = groupRulesUnfiltered.filter(r => r.group.users.some(({userGroup}) => userGroup.role === 'admin' && userGroup.userId === parseInt(req.user.id)));

  const relatedRules = JSON.parse(JSON.stringify(userRules.concat(groupRules)));
  const relatedAlarms = new Map(await alarm.findAll({
    where: {ruleId: relatedRules.map(rule => rule.id)}, attributes: ['ruleId'],
  }).map(a => [a.ruleId]));

  res.send(relatedRules.map(rule => ({
    ... rule, hasLinkedAlarms: relatedAlarms.has(rule.id),
  })));
});

/**
 * @swagger
 * /rules:
 *   post:
 *     tags:
 *       - Rules
 *     description: Create a new rule
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         description: Fields for a rule.
 *         schema:
 *           type: object
 *           properties:
 *             periodicity:
 *               type: string
 *               enum:
 *                 - daily
 *                 - weekly
 *                 - monthly
 *             operator:
 *               type: string
 *               enum:
 *                 - <
 *                 - >
 *             participationRate:
 *               type: integer
 *             votingRate:
 *               type: integer
 *             voteType:
 *               type: string
 *               enum:
 *                 - positive
 *                 - negative
 *                 - neutral
 *             rate:
 *               type: integer
 *             messagePrefix:
 *               type: string
 *             name:
 *               type: string
 *             factorId:
 *               type: integer
 *             groupId:
 *               type: integer
 *             personId:
 *               type: integer
 *         required:
 *           - user
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.post('/', ensureAuthenticated(Role.User), async (req, res) => {
  if (!req.body.groupId && req.body.personId !== req.user.id) {
    res.status(500).send('Can not create a rule for an other user.');
  }
  try {
    const createdRule = await rule.create({
      ... req.body,
    });
    res.send({
      rule: createdRule,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send('An error occured while creating a rule.');
  }
});

/**
 * @swagger
 * /rules/{ruleId}:
 *   put:
 *     tags:
 *       - Rules
 *     description: Archives a rule
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: ruleId
 *         description: RuleId id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.put('/:id', ensureAuthenticated(Role.User), (req, res) => {
  // todo : verify if user can archive rule
  rule.update({archived: true}, {
    where: {
      id: req.params.id,
    },
  })
      .then(() => res.status(200).send())
      .catch((error) => {
        console.log(error);
        res.status(500).send();
      });
});

/**
 * @swagger
 * /rules/{ruleId}:
 *   delete:
 *     tags:
 *       - Rules
 *     description: Permanently delete an archived rule
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: ruleId
 *         description: RuleId id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.delete('/:id', ensureAuthenticated(Role.User), (req, res) => {
  // todo : verify if user can delete rule
  rule.destroy({
    where: {
      id: req.params.id,
    },
  })
      .then(() => res.status(200).send())
      .catch((error) => {
        console.log(error);
        res.status(500).send();
      });
});

module.exports = router;
