/**
 * @file Group Member related calls to the database
 * @author Uchendu Nwachukwu <uchendu.nwachukwu@edu.hefr.ch>
 * @date 10.10.2020
 */

const express = require("express");
const router = express.Router({ mergeParams: true });
const ensureAuthenticated = require("../helpers/ensureAuthenticated");
const Role = require("../helpers/role");
const {
  group,
  user,
  userGroup,
  sequelize,
} = require("../../models");
const { Op } = require("sequelize");
const requestStatus = require("../types/requestStatus");
const groupRole = require("../types/groupRole");
const mail = require("../helpers/mail");
const { ensureUserIsAdminInGroup, ensureGroupExistsInEnterprise } = require("../helpers/validation")
const { getRoleInGroup } = require("../helpers/frequentGetters")
const {sendPushMessageAndNotification} = require("../helpers/push");

const throwError = (res, error) => {
  res.sendStatus(500);
  console.warn(error);
};

/**
 * Add middleware to the router that ensures that the user is authenticated
 */
router.use(ensureAuthenticated(Role.User))
/**
 * Add middleware to the router that ensures that groupId in path exists and is from users enterprise
 */
router.use(ensureGroupExistsInEnterprise)

/**
 * @swagger
 * /groups/{groupId}/invitableUsers:
 *   get:
 *     tags:
 *       - Group Members
 *     description: Return users that can be invited to a group
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: groupId
 *         description: Group's id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.get("/invitableUsers", ensureUserIsAdminInGroup, async (req, res) => {
  // returns a list of users in the same enterprise of the current user, or an empty list -> No current user exist in his enterprise
  try {
    const invitableUsers = await getInvitableUsers(req);
    return res.json(invitableUsers);
  } catch (err) {
    throwError(res, err);
  }
}
);

/**
 * @swagger
 * /groups/{groupId}/invite:
 *   post:
 *     tags:
 *       - Group Members
 *     description: Invite users to a specified group
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: groupId
 *         description: Group's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             userIds:
 *               type: array
 *               items:
 *                 type: integer
 *         required:
 *           - userIds
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.post("/invite", ensureUserIsAdminInGroup, async (req, res) => {
  try {
    // body validation
    if (!req.body.userIds) {
      return res.sendStatus(400);
    }
    const valid = await allSubmittedUserIdsExistInEnterprise(req.body.userIds, req.user.enterpriseId)
    if (!valid) {
      return res.sendStatus(404);
    }

    const createdInvitations = await createInvitationForUsers(req);
    const invitedUsersIds = createdInvitations.map(
      (invitation) => invitation.userId
    );
    // no need to await the result of notification sending
    group.findOne({
      where: {
        id: req.params.groupId,
      },
    }).then((group) => {
      sendPushNotificationToInvitedUsers(invitedUsersIds, group);
      sendEmailNotificationToInvitedUsers(invitedUsersIds, group);
    });

    return res.json(createdInvitations);
  } catch (err) {
    throwError(res, err);
  }
}
);

/**
 * @swagger
 * /groups/{groupId}/invitation:
 *   put:
 *     tags:
 *       - Group Members
 *     description: Accept/decline an invitation to join a group
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: groupId
 *         description: Group's id associated to the invitation
 *         in: path
 *         required: true
 *         type: integer
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             status:
 *               type: string
 *               enum:
 *                 - accepted
 *                 - declined
 *         required:
 *           - status
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.put("/invitation", async (req, res) => {
  try {
    // validate that there is actually a pending invitation waiting
    const valid = await hasPendingInvitationForGroup(req.user.id, req.params.groupId)
    if (!valid) {
      return res.sendStatus(404)
    }
    // body validation
    if (req.body.status === requestStatus.PENDING) {
      return res.sendStatus(404)
    }


    if (req.body.status === requestStatus.ACCEPTED) {
      await acceptInvitation(req);
    } else {
      await declineAndDeleteInvitation(req);
    }

    return res.sendStatus(200);
  } catch (err) {
    throwError(res, err);
  }
}
);

/**
 * @swagger
 * /groups/{groupId}/members:
 *   get:
 *     tags:
 *       - Group Members
 *     description: Return members of a group
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: groupId
 *         description: Groups's id
 *         in: path
 *         required: false
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.get("/members", async (req, res) => {
  try {
    const userRole = await getRoleInGroup(req.user.id, req.params.groupId);
    let filters = null;
    // filter users with pending invitations if the user making the request is not an admin
    if (userRole !== groupRole.ADMIN) {
      filters = { status: requestStatus.ACCEPTED }
    }
    const userList = await getGroupMembers(req.params.groupId, filters)
    return res.json(userList);
  } catch (err) {
    throwError(res, err);
  }
}
);

/**
 * @swagger
 * /groups/{groupId}/members/{userId}:
 *   delete:
 *     tags:
 *       - Group Members
 *     description: Remove a user from a group
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: groupId
 *         description: Group's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: userId
 *         description: User that should be removed from the group
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.delete("/members/:userId", async (req, res) => {
  try {
    const usersRoleInGroup = await getRoleInGroup(req.user.id, req.params.groupId);
    if (usersRoleInGroup === null) {
      return res.sendStatus(404)
    }
    // Check privileges and prevent an admin removing himself from a group
    const isAdmin = usersRoleInGroup === groupRole.ADMIN;
    const isConcerned = await isConcernedUserMakingTheRequest(req)
    // Only let requests pass, where the user is either concerned himself or an admin of the group (XOR)
    if (!(isAdmin ^ isConcerned)) {
      res.sendStatus(403);
    } else {
      await leaveGroup(req);
      res.sendStatus(200)
    }
  } catch (err) {
    throwError(res, err);
  }
}
);

const leaveGroup = async (req) => {
  userGroup.destroy({
    where: {
      userId: Number(req.params.userId),
      groupId: Number(req.params.groupId),
    },
  });
};


const getGroupMembers = async (groupId, filters = null) => {
  return userGroup.findAll({
    attributes: ["userId", "status", "role"],
    where: {
      groupId,
      ...filters
    },
    include: [
      {
        model: user,
        attributes: ["firstName", "lastName", "userName", "email"],
      },
    ],
  });
};

const createInvitationForUsers = async (req) => {
  const userIds = req.body.userIds;
  const newRecords = userIds.map((userId) => {
    return {
      status: requestStatus.PENDING,
      role: groupRole.MEMBER,
      groupId: req.params.groupId,
      userId: userId,
    };
  });
  return userGroup.bulkCreate(newRecords);
};

const sendEmailNotificationToInvitedUsers = async (invitedUsersIds, group) => {
  // find all users
  user.findAll({
    attributes: ["email", "firstName", "lastName"],
    where: {
      id: invitedUsersIds,
    },
  }).then((users) => {
    // send to each user an email
    users.forEach((user) => {
      mail
        .sendGroupInvitationMail(user, group)
        .catch((error) => console.error(error));
    });
  });
};

const sendPushNotificationToInvitedUsers = async (invitedUsersId, group) => {

  const params = {
    userId: invitedUsersId,
    shouldSendNotification: true,
    notificationContent: `Vous avez reçu une nouvelle invitation du groupe ${group.name}`,
    notificationRequireInteraction: true,
    actions: [
      {
        action: "invitation",
        title: "Répondre",
      },
    ],
    shouldSendMessage: false,
    messageContent: "",
    hasAlarmAttached: false,
    alarmId: 0,
  }
  await sendPushMessageAndNotification(params);
};

const isConcernedUserMakingTheRequest = async (req) => {
  return req.user.id === Number(req.params.userId);
};

// Invitable users are defined as users from the same enterprise, which are currently not in this group.
const getInvitableUsers = async (req) => {
  const usersEnterpriseId = req.user.enterpriseId;
  const groupId = req.params.groupId;
  return await user.findAll({
    attributes: ["id", "firstName", "lastName", "email",],
    where: {
      enterpriseID: usersEnterpriseId,
      id: {
        [Op.notIn]: sequelize.literal(`(
          SELECT userGroup.userId FROM userGroup as userGroup 
          WHERE userGroup.groupId = ${groupId}
        )`),
      },
    },
  });
};

const acceptInvitation = async (req) => {
  await userGroup.update(
    { status: requestStatus.ACCEPTED },
    {
      where: {
        groupId: req.params.groupId,
        userId: req.user.id,
      },
    }
  );
};

const declineAndDeleteInvitation = async (req) => {
  await userGroup.destroy({
    where: {
      groupId: req.params.groupId,
      userId: req.user.id,
    },
  });
};

const allSubmittedUserIdsExistInEnterprise = async (userIds, enterpriseId) => {
  const nbrOfUsersFoundInEnterprise = await user.count({
    where: {
      id: userIds,
      enterpriseId: enterpriseId
    }
  })
  return nbrOfUsersFoundInEnterprise === userIds.length
}

const hasPendingInvitationForGroup = async (userId, groupId) => {
  const nbrOfInvitationWaiting = await userGroup.count({
    where: {
      userId,
      groupId,
      status: requestStatus.PENDING
    }
  })
  return nbrOfInvitationWaiting > 0
}

module.exports = router;
