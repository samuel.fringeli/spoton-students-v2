/**
 * @file Subscriptions related calls to the environment variables
 * @author Léonard Noth <leonard.noth@master.hes-so.ch>
 * @date 11.04.2022
 * @version 1.0
 */

const express = require('express');

const router = express.Router({ mergeParams: true });
const Role = require('../helpers/role');
const ensureAuthenticated = require('../helpers/ensureAuthenticated');


/**
 * @swagger
 * /subscriptions/:
 *   get:
 *     tags:
 *       - Subscriptions
 *     description: Get environment variable for subscription creation
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get('/', ensureAuthenticated(Role.User), async (req, res) => {
  res.send(process.env.PUBLIC_VAPID_KEY_BASE64);
});

module.exports = router;
