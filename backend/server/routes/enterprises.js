/**
 * @file Enterprise related calls to the database
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

const express = require("express");

const router = express.Router();
const ensureAuthenticated = require("../helpers/ensureAuthenticated");
const Role = require("../helpers/role");
const variableExists = require("../helpers/variable");
const {getDatesInBetween} = require("../helpers/date");
const {enterprise, user, room, factorEnterprise, sequelize, factor, theme, group, rule} = require("../../models");
const Sequelize = require("sequelize");
const {THEME_IDS_HIDDEN_IN_SELECTION, DEFAULT_FACTOR_IDS} = require("../helpers/factor");
const Op = Sequelize.Op;

/**
 * @swagger
 * /enterprises:
 *   get:
 *     tags:
 *       - Enterprise
 *     description: Get all enterprises
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/", (req, res) => {
  enterprise.findAll()
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * @swagger
 * /enterprises/members:
 *   get:
 *     tags:
 *       - Enterprise
 *     description: Get all enterprises with their members (groups+users)
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/members", (req, res) => {
  enterprise.findAll({
    attributes: ["id"],
    include: [
      {
        model: user,
        attributes: ["id"],
      },
      {
        model: group,
        attributes: ["id"],
        include: {
          model: user,
          attributes: ["id"],
          through: {
            attributes: []
          },
        }
      }
    ],
  })
    .then((result) => {
      const res1 = []
      for (let i = 0; i < result.length; i++) {
        const users = result[i].users.map(user => user.get("id"))
        const groups = JSON.parse(JSON.stringify(result[i].groups)) // necessary, otherwise changes are not taken into account
        for (let j = 0; j < groups.length; j++) {
          groups[j].users = groups[j].users.map(user => user.id)
        }
        // const groups = result[i].groups.map(group => group.users.map(user => user.get("id")))
        res1.push({id: result[i].id, usersId: users, groupsId: groups})
      }

      //console.log(result[0].users[0])
      /*res3 = result.forEach(enterprise => {
        enterprise.users = )
      })
      console.log(res3)*/

      res.json(res1)
    })
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * @swagger
 * /enterprises/{enterpriseId}:
 *   get:
 *     tags:
 *       - Enterprise
 *     description: Get a specific enterprise
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: enterpriseId
 *         description: Id of the enterprise
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/:id", (req, res) => {
  enterprise.findOne({
    where: {
      id: Number(req.params.id),
    },
  })
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * @swagger
 * /enterprises:
 *   post:
 *     tags:
 *       - Enterprise
 *     description: Create an enterprise (needs to be a root user)
 *     parameters:
 *       - name: body
 *         in: body
 *         description: Name of the enterprise.
 *         schema:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *         required:
 *           - user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.post("/", ensureAuthenticated(Role.Root), (req, res) => {
  sequelize.transaction((t) =>
    enterprise.create(req.body, {
      transaction: t
    })
  ).then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * @swagger
 * /enterprises/{enterpriseId}:
 *   put:
 *     tags:
 *       - Enterprise
 *     description: Update an enterprise (needs to be a root user)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         description: Name of the enterprise.
 *         schema:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *       - name: enterpriseId
 *         description: Id of the enterprise to update
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.put("/:id", ensureAuthenticated(Role.Root), (req, res) => {
  enterprise.update(req.body, {
    where: {
      id: req.params.id,
    },
  })
    .then(() => res.status(200).send())
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * @swagger
 * /enterprises/{enterpriseId}:
 *   delete:
 *     tags:
 *       - Enterprise
 *     description: Delete an enterprise (needs to be a root user)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: enterpriseId
 *         description: Id of an enterprise
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.delete("/:id", ensureAuthenticated(Role.Root), (req, res) => {
  enterprise.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then(() => res.status(200).send())
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * @swagger
 * /enterprises/{enterpriseId}/users:
 *   get:
 *     tags:
 *       - Enterprise
 *     description: Get the users of an enterprise if the user is part of the enterprise
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: enterpriseId
 *         description: Id of an enterprise
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/:id/users", ensureAuthenticated(Role.User), (req, res) => {
  // use non-strict comparision because of different types
  if (req.user.enterpriseId != req.params.id) {
    res.status(403).send();
    return;
  }
  user.findAll({
    attributes: ["id", "firstName", "lastName", "username", "email"],
    where: {
      enterpriseId: req.params.id,
      id: {
        [Op.ne]: req.user.UserId,
      },
    },
  })
    .then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * @swagger
 * /enterprises/{enterpriseId}/rooms:
 *   get:
 *     tags:
 *       - Enterprise
 *     description: Get all rooms for an entreprise (needs to be an admin user)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: enterpriseId
 *         description: Id of an enterprise
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/:id/rooms", ensureAuthenticated(Role.Admin), (req, res) => {
  console.log("****** Start ")
  if (!variableExists(req.params.id)) {
    res.sendStatus(400)
  }
  console.log("****** Validated ")
  room.findAll({
    where: {
      enterpriseId: Number(req.params.id)
    }
  }).then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});


/**
 * @swagger
 * /enterprises/{enterpriseId}/factors:
 *   get:
 *     tags:
 *       - Enterprise
 *     description: Get all factors with selected factors of an enterprise
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: enterpriseId
 *         description: Id of an enterprise
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/:id/factors", ensureAuthenticated(Role.Admin), (req, res) => {
  // use non-strict comparision because of different types
  if (req.user.enterpriseId != req.params.id) {
    res.status(403).send();
    return;
  }

  theme.findAll({
    attributes: ["id", "name", "description", "icon", "question", "questionFactor"],
    include: [
      {
        model: factor,
        attributes: ["id", "name"],
        required: true,
        include: [
          {
            model: factorEnterprise,
            attributes: ["createdAt"],
            required: false,
            where: {
              enterpriseId: req.params.id
            }
          }
        ]
      }
    ],
    where: {
      id: {
        [Op.notIn]: THEME_IDS_HIDDEN_IN_SELECTION // Skip the specified themes (such as the "well-being" theme)
      }
    }
  }).then((result) => res.json(result))
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

/**
 * @swagger
 * /enterprises/{enterpriseId}/factors:
 *   put:
 *     tags:
 *       - Enterprise
 *     description: Purge and recreate selected factors of an enterprise
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: enterpriseId
 *         description: Enterprise's id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: body
 *         in: body
 *         schema:
 *           type: array
 *           items:
 *             type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.put("/:id/factors", ensureAuthenticated(Role.Admin), (req, res) => {
  // use non-strict comparision because of different types
  if (req.user.enterpriseId != req.params.id) {
    res.status(403).send();
    return;
  }

  let factorIds = req.body.concat(DEFAULT_FACTOR_IDS);
  let objFactors = factorIds.map((fId) => {
    return {
      enterpriseId: req.params.id,
      factorId: fId
    };
  });

  sequelize.transaction(function (t) {
    return factorEnterprise.destroy({
      where: {
        enterpriseId: req.params.id,
        factorId: {
          [Op.notIn]: factorIds // Ignore the deselection of the specified factors (and default factors)
        }
      },
      transaction: t
    }).then(() => {
      return factorEnterprise.bulkCreate(objFactors, {
        ignoreDuplicates: true,
        transaction: t
      }).then(() => {
        objFactors = [];
        res.sendStatus(200);
      }, err => {
        console.warn(err);
        res.status(500).send();
      })
    }, err => {
      console.warn(err);
      res.status(500).send();
    })
  }).catch(function (err) {
    console.warn(err);
    res.status(500).send();
  });
});

/**
 * @swagger
 * /enterprises/{enterpriseId}/rules:
 *   get:
 *     tags:
 *       - Enterprise
 *     description: Get all rules of enterprise
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: enterpriseId
 *         description: Id of enterprise
 *         in: path
 *         required: true
 *         type: integer
 *       - name: periodicity
 *         description: Periodicity in ['daily', 'weekly', 'monthly']
 *         in: query
 *         required: true
 *       - name: createdBefore
 *         description: Last date at which the rule could be created
 *         in: query
 *         required: true
 *       - name: archived
 *         description: If rule is archived or not
 *         in: query
 *         required: true
 *       - name: groupsId
 *         description: Possible groups ids
 *         in: query
 *         required: false
 *       - name: personsId
 *         description: Possible persons ids
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get('/:enterpriseId/rules', ensureAuthenticated(Role.User), async (req, res) => {
  if (!variableExists([
    // req.query.groupsId,
    // req.query.usersId,
    req.query.periodicity,
    req.query.createdBefore,
    req.query.archived,
  ])) {
    res.status(400).send();
    return
  }

  const userRules = await rule.findAll({
    order: [["id", "ASC"]],
    include: [{
      model: user,
      attributes: [],
      required: true,
      where: {
        id: req.query.usersId || [],
      }
    },
      {
        model: factor,
        required: true,
        attributes: ["themeId"]
      }],
    where: {
      archived: req.query.archived,
      createdAt: {[Op.lte]: req.query.createdBefore}
    }
  });
  const groupRules = await rule.findAll({
    order: [["id", "ASC"]],
    include: [{
      model: group,
      attributes: [],
      required: true,
      where: {
        id: req.query.groupsId || [],
      }
    },
      {
        model: factor,
        required: true,
        attributes: ["themeId"]
      }],
    where: {
      archived: req.query.archived,
      createdAt: {[Op.lte]: req.query.createdBefore}
    }
  });

  res.send(JSON.parse(JSON.stringify(userRules.concat(groupRules))));
});

/**
 * @swagger
 * /enterprises/{enterpriseId}/opendays:
 *   get:
 *     tags:
 *       - Enterprise
 *     description: Get all open days for an entreprise (needs to be an admin user)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: enterpriseId
 *         description: Id of an enterprise
 *         in: path
 *         required: true
 *         type: integer
 *       - name: begin
 *         description: First day to consider
 *         in: query
 *         required: true
 *       - name: end
 *         description: Last day to consider
 *         in: query
 *         required: true
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get("/:id/opendays", ensureAuthenticated(Role.User), (req, res) => {
  if (!variableExists([req.params.id, req.query.begin, req.query.end])) {
    res.sendStatus(400)
    return
  }

  // TODO: should here be linked to the DB, but it is not existing now. So we just return all open days (monday-friday)
  const opendays = getDatesInBetween(req.query.begin, req.query.end)
  res.send(opendays)
});


module.exports = router;
