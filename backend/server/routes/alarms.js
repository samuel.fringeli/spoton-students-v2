/**
 * @file Alarms related calls to the database
 * @author Samuel Fringeli <samuel.fringeli@hefr.ch>
 * @date 11.04.2022
 * @version 1.0
 */

const express = require('express');

const router = express.Router({mergeParamsx: true});
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const Role = require('../helpers/role');
const {rule, alarm, user, group, factor, userGroup} = require('../../models');
const {sendPushMessageAndNotification} = require('../helpers/push');
const {Op} = require('sequelize');

async function getAlarmsRelatedToUser(userId, ruleId = false, stateFilter = 'all') {
  let stateCondition = {};
  if (stateFilter === 'null') {
    stateCondition.state = {[Op.is]: null};
  } else if (stateFilter === 'notNull') {
    stateCondition.state = {[Op.ne]: null};
  }

  const userAlarms = await alarm.findAll({
    order: [['id', 'ASC']],
    where: {
      ... ruleId ? {ruleId: ruleId} : {},
      ... stateCondition,
    },
    include: [
      {
        model: rule,
        required: true,
        include: [
          {
            model: user,
            attributes: ['firstName', 'lastName', 'username', 'email'],
            required: true,
            where: {id: userId},
          },
        ],
      },
    ],
  });

  const groupAlarms = (
      await alarm.findAll({
        order: [['id', 'ASC']],
        where: {
          ... ruleId ? {ruleId: ruleId} : {},
          ... stateCondition,
        },
        include: [
          {
            model: rule,
            required: true,
            include: [
              {
                model: group,
                required: true,
                include: [
                  {
                    model: user,
                    attributes: ['firstName', 'lastName', 'username', 'email'],
                  },
                ],
              },
            ],
          },
        ],
      })
  ).filter((a) =>
      a.rule.group.users.some(
          (u) => u.userGroup.role === 'admin' && u.userGroup.userId === parseInt(userId),
      ),
  );

  console.log(groupAlarms)

  return JSON.parse(JSON.stringify(userAlarms.concat(groupAlarms)));
}

/**
 * @swagger
 * /alarms/all:
 *   get:
 *     tags:
 *       - Alarms
 *     description: Get all alarms from current user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get('/all', ensureAuthenticated(Role.User), async (req, res) => {
  const alarms = await getAlarmsRelatedToUser(req.user.id, false);
  const factorIds = [];
  alarms.forEach(async alarm => {
    if (!factorIds.includes(alarm.rule.factorId)) {
      factorIds.push(alarm.rule.factorId);
    }
  });
  const factors = await factor.findAll({
    raw: true,
    where: {
      id: factorIds,
    },
  });
  alarms.forEach(alarm => {
    const factor = factors.find(factor => {
      return factor.id === alarm.rule.factorId;
    });
    if (factor) alarm.rule.factor = factor.name;
  });
  res.send(alarms);
});


/**
 * @swagger
 * /alarms/{ruleId}:
 *   get:
 *     tags:
 *       - Alarms
 *     description: Get all alarms
 *     parameters:
 *       - name: ruleId
 *         description: ruleId id
 *         in: path
 *         required: true
 *         type: integer
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.get('/:ruleId', ensureAuthenticated(Role.User), async (req, res) => {
  res.send(await getAlarmsRelatedToUser(req.user.id, req.params.ruleId));
});

/**
 * @swagger
 * /alarms:
 *   post:
 *     tags:
 *       - Alarms
 *     description: Create a new alarm
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         description: Fields for an alarm.
 *         schema:
 *           type: object
 *           properties:
 *             rate:
 *               description: The rate for which the alarm was raised (ex= 65% when alarm says 60%)
 *               type: int
 *             votingRate:
 *               description: The voting rate for which the alarm was raised (ex= 65% when alarm says 60%)
 *               type: int
 *             participationRate:
 *               description: The participation rate for which the alarm was raised (ex= 65% when alarm says 60%)
 *               type: int
 *             ruleId:
 *               type: integer
 *             shouldSendNotification:
 *               description: If the system should send a notification to the users
 *               type: boolean
 *             shouldSendMessage:
 *               description: If the system should send a chat message to the users
 *               type: boolean
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error
 */
router.post('/', ensureAuthenticated(Role.User), async (req, res) => {
  try {
    const ruleAttachedToAlarm = await rule.findOne({
      where: {
        id: req.body.ruleId,
      },
    });

    // We build the JSON message attribute
    const messageJsonObject = {
      messagePrefix: ruleAttachedToAlarm.messagePrefix,
      participationRate: req.body.participationRate,
      votingRate: req.body.votingRate,
      periodicity: req.body.periodicity,
      ruleType: req.body.ruleType,
      rate: req.body.rate,
      voteType: ruleAttachedToAlarm.voteType,
      factorId: ruleAttachedToAlarm.factorId,
    };

    // We get all information from the request
    let alarmObject = {
      ... req.body,
      messageJSON: JSON.stringify(messageJsonObject),
    };
    // We remove rate that we don't need in the alarm table
    delete alarmObject.rate;
    delete alarmObject.votingRate;
    delete alarmObject.participationRate;

    const createdAlarm = await alarm.create(alarmObject);

    let listOfUsers = null;
    if (ruleAttachedToAlarm.groupId !== null) {
      const usersFromGroup = await userGroup.findAll({
        where: {
          groupId: ruleAttachedToAlarm.groupId,
          role: 'admin',
          status: 'accepted',
        },
      });
      listOfUsers = usersFromGroup.map(item => item.userId);
    } else {
      listOfUsers = [ruleAttachedToAlarm.personId];
    }

    const factorAttachedToRule = await factor.findOne({
      where: {
        id: ruleAttachedToAlarm.factorId,
      },
    });

    delete messageJsonObject.factorId;
    messageJsonObject.factor = factorAttachedToRule.name;
    messageJsonObject.alarmId = createdAlarm.id;

    let allNotificationStatusCodes = [];

    for (const userId of listOfUsers) {
      const params = {
        userId: userId,
        shouldSendNotification: req.body.shouldSendNotification,
        notificationContent: req.body.notificationMessage,
        notificationRequireInteraction: false,
        shouldSendMessage: req.body.shouldSendMessage,
        messageContent: JSON.stringify(messageJsonObject),
        hasAlarmAttached: true,
        alarmId: createdAlarm.id,
      };
      const statusCode = await sendPushMessageAndNotification(params);
      if (typeof statusCode === 'object') {
        if (statusCode[0] === 200) {
          // Check if the notification was sent properly
          alarm.update(
              {
                notificationStatus: 'sent',
              },
              {
                where: {
                  id: createdAlarm.id,
                },
              },
          );
        }
        if (statusCode[1] === 200) {
          // Check if the message was sent properly
          alarm.update(
              {
                chatMessageStatus: 'sent',
              },
              {
                where: {
                  id: createdAlarm.id,
                },
              },
          );
        }
        if (statusCode.every((val) => val === 200)) {
          allNotificationStatusCodes.push(200);
        } else {
          allNotificationStatusCodes.push(500);
        }
      } else {
        allNotificationStatusCodes.push(statusCode);
      }
    }

    if (allNotificationStatusCodes.every((val) => val === 200)) {
      res.status(allNotificationStatusCodes[0]).send({
        rule: createdAlarm,
      });
    } else {
      res
          .status(500)
          .send('An error occurred while sending the notifications.');
    }
  } catch (err) {
    console.error(err);
    res.status(500).send('An error occurred while creating an alarm.');
  }
});

/**
 * @swagger
 * /alarms/{alarmId}:
 *   put:
 *     tags:
 *       - Alarms
 *     description: Updates an alarm. Empty string means no update.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: alarmId
 *         description: alarmId id
 *         in: path
 *         required: true
 *         type: integer
 *       - name: body
 *         in: body
 *         description: Fields for an alarm.
 *         schema:
 *           type: object
 *           properties:
 *             state:
 *               type: string
 *               enum:
 *                 - read
 *                 - false_alarm
 *                 - acted
 *             stateMessage:
 *               type: string
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.put('/:id', ensureAuthenticated(Role.User), async (req, res) => {
  let updateBody = {
    state: req.body.state,
  };

  if (req.body.stateMessage) updateBody.stateMessage = req.body.stateMessage;
  if (req.body.stateDate) updateBody.stateDate = req.body.stateDate;
  alarm
      .update(updateBody, {
        where: {
          id: req.params.id,
        },
      })
      .then(() => res.status(200).send())
      .catch((error) => {
        console.log(error);
        res.status(500).send();
      });
});

/**
 * @swagger
 * /alarms/{alarmId}:
 *   delete:
 *     tags:
 *       - Alarms
 *     description: Permanently delete an alarm
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: alarmId
 *         description: alarmId id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       404:
 *         description: Entity not found
 *       500:
 *         description: Server-side processing error
 */
router.delete('/:id', ensureAuthenticated(Role.Admin), (req, res) => {
  // todo : verify if user can delete alarm
  alarm
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then(() => res.status(200).send())
      .catch((error) => {
        console.log(error);
        res.status(500).send();
      });
});


module.exports = router;
