/**
 * @file Level related calls to the database
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

const express = require('express');
const Sequelize = require('sequelize');

const router = express.Router();
const ensureAuthenticated = require('../helpers/ensureAuthenticated');
const Role = require('../helpers/role');
const {
  level,
} = require('../../models');

const {
  Op
} = Sequelize;
const getToday = require('../helpers/date');
const variableExists = require('../helpers/variable');

/**
 * @swagger
 * /levels:
 *   get:
 *     tags:
 *       - Levels
 *     description: Return the current level of the user for gamification. The points are increased by 25 with each PUT to /themes/{themeId}/responses.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Successful
 *       401:
 *         description: Unauthorized (authentication missing)
 *       403:
 *         description: Forbidden (not enough privileges)
 *       500:
 *         description: Server-side processing error 
 */
router.get('/', ensureAuthenticated(Role.User), (req, res) => {
  let points = req.user.userPoints;
  level.findOne({
    where: {
      fromNbr: {
        [Op.gte]: points
      }
    },
  })
    .then((result) => {
      if (result == null) {
        result = {
          name: "God"
        };
      }
      res.json(result);
    })
    .catch((error) => {
      res.status(500).send();
      console.warn(error);
    });
});

module.exports = router;
