/**
 * @file Express server root file
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

// load the environment file
require("dotenv").config();


const express = require("express");
const cors = require("cors");
const cookieSession = require("cookie-session");
const fileUpload = require("express-fileupload");
const swaggerUi = require("swagger-ui-express");
const swaggerJSDoc = require("swagger-jsdoc");

const users = require("./routes/users");
const groups = require("./routes/groups");
const groupMembers = require("./routes/groupMembers");
const rooms = require("./routes/rooms");
const enterprises = require("./routes/enterprises");
const themes = require("./routes/themes");
const parameters = require("./routes/parameters");
const levels = require("./routes/levels");
const notifications = require("./routes/notifications");
const stats = require("./routes/statistics");
const factors = require("./routes/factors");
const responses = require("./routes/responses");
const responseOptions = require("./routes/responseOptions");
const oneShot = require("./routes/oneShot");
const rules = require("./routes/rules");
const alarms = require("./routes/alarms");
const subscriptions = require("./routes/subscriptions");
const crons = require("./routes/crons");
const pdfRoutes = require('./routes/pdf');
const flashNewsRoutes = require("./routes/flashnews");

const app = express();
const port = process.env.PORT || 3000;

// express instance parameters and middlewares ------------------------------------------
app.disable("x-powered-by");
app.use(
  cors({
    origin:
      process.env.NODE_ENV === "production" ? process.env.APP_BASE_URL : true,
    credentials: true,
    methods: ["GET", "PUT", "POST", "DELETE"],
    allowedHeaders: ["Content-Type"],
  })
);

app.use(
  cookieSession({
    name: "session",
    keys: [process.env.SESSION_PASSWORD],
    maxAge: 14 * 24 * 60 * 60 * 1000, // 14 days
    httpOnly: true,
    secure: false, // HTTPS managed by Nginx proxy
    signed: true,
    overwrite: true,
  })
);

app.use(express.json());

app.use(
  fileUpload({
    createParentPath: true,
    safeFileNames: true,
    preserveExtension: true,
    abortOnLimit: true,
    limits: {
      fileSize: 1 * 1024 * 1024,
      files: 1,
    },
  })
);

// Routes ------------------------------------------
app.use("/users", users);
app.use("/rooms", rooms);
app.use("/enterprises", enterprises);
app.use("/themes", themes);
app.use("/themes/:themeId/factors", factors)
app.use("/themes/:themeId/factors/:factorId/responseOptions", responseOptions)
app.use("/themes/:themeId/factors/:factorId/responses", responses)
app.use("/parameters", parameters);
app.use("/groups", groups);
app.use("/groups/:groupId/", groupMembers);
app.use("/levels", levels);
app.use("/notifications", notifications);
app.use("/stats", stats);
app.use("/oneshot", oneShot);
app.use("/rules", rules);
app.use("/alarms", alarms);
app.use("/subscriptions", subscriptions);
app.use("/crons", crons);
app.use("/pdf", pdfRoutes);
app.use("/flashnews", flashNewsRoutes);

// Swagger config -------------------------------------
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      version: "4.1.4",
      title: "Sooze API",
      description: "Sooze API Information",
      contact: {
        name: "Sooze Developers",
      },
      servers: ["http://localhost:3000"],
    },
  },
  // ['.routes/*.js']
  apis: ["./routes/*.js"],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);
const swaggerServe = swaggerUi.serve
const swaggerSetup = swaggerUi.setup(swaggerDocs)
app.use("/api-docs", swaggerServe, swaggerSetup);
app.use("/api/api-docs", swaggerServe, swaggerSetup);

// Error 404 ------------------------------------------
app.use((req, res) => {
  res.status(404).send();
});

// App listen ------------------------------------------
app.listen(port, () => {
  console.log(`server running on port: ${port}`);
});
