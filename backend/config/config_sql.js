module.exports = {
	"username": process.env.MYSQL_USER,
	"password": process.env.MYSQL_PASSWORD,
	"database": process.env.MYSQL_DATABASE,
	"host": "database",
	"dialect": "mysql",
	"dialectOptions": {
		"timezone": "Z"
	},
	"timezone": "UTC"
}
