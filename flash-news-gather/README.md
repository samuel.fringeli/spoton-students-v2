# Flash news gather service

This service will retrieve from the "flashNews" table in the database all the flash news related to a given user.

Then, it will remove the flash news that must not be run due to their frequency. 
For example, if a flash news must be run monthly, it will only be run in between the 1st and the 7th of each month.

Both monthly and weekly flash news are only executed on Sundays.

Daily flash news are executed every day except in the weekend.

Finally, it will return the list of flash news that must be run.

## How to use
This service is served in the docker-compose files at the root of the repository.

To request the service, you must send a GET request to the following URL: 
```
http://localhost:6011/flash-news-gather?groupId=XXX
``` 
where XXX is the id of the group for which you want to retrieve the flash news.

It returns an array. If it is empty it means that no flash news must be run for this group.
Otherwise, there is arrays inside with the flash news title and then the correct payload.