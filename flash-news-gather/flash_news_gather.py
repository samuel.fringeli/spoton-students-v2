from mysql.connector.cursor import Error as DBError
from datetime import date, timedelta, datetime
from utils import DbAccess
from flask import jsonify
import json


query_str = ('SELECT id, defaultFlashNewId, analyseType, title, frequency, period1, factorId1, themeId1, groupId1, '
             'userId1, period2, factorId2, themeId2, groupId2, userId2, payload FROM flashNew WHERE groupId1 = %s;')


LOCAL_NO_DATES_CHECK = False


def gather(group_id: int):
    results = get_from_database(group_id)

    today_day = date.today().strftime('%d')
    today_weekday = datetime.today().weekday()

    final_payload = []
    for entry in results:
        if LOCAL_NO_DATES_CHECK:
            pass
        else:
            # If the flash news must be run weekly, and it is not a Sunday, it will not be executed
            if entry[4] == 'weekly' and not (int(today_weekday) == 6):
                continue

            # If the flash news must be run monthly, and it is not a Sunday, it will not be executed.
            # The monthly flash news will be executed only between the 1st and the 7th of a month.
            if (entry[4] == 'monthly' and int(today_day) >= 8) or (entry[4] == 'monthly' and not (int(today_weekday) == 6)):
                continue

            # Checks if today is a Saturday or a Sunday, if yes the daily routines won't be executed
            if entry[4] == 'daily' and int(today_weekday) >= 5:
                continue


        payload = json.loads(entry[15])

        # Checks each row in the json to be sure they are correct
        # factor_theme_type1
        if 'factor_theme_type1' in payload:
            if entry[6] is None and not (payload['factor_theme_type1'] == 'theme'):
                payload['factor_theme_type1'] = 'theme'
            elif entry[7] is None and not (payload['factor_theme_type1'] == 'factor'):
                payload['factor_theme_type1'] = 'factor'
        else:
            payload['factor_theme_type1'] = 'theme' if entry[6] is None else 'factor'

        # factor_theme_id1
        if 'factor_theme_id1' in payload:
            if payload['factor_theme_type1'] == 'factor' and not (payload['factor_theme_id1'] == entry[6]):
                payload['factor_theme_id1'] = entry[6]
            elif payload['factor_theme_type1'] == 'theme' and not (payload['factor_theme_id1'] == entry[7]):
                payload['factor_theme_id1'] = entry[7]
        else:
            payload['factor_theme_id1'] = entry[7] if payload['factor_theme_type1'] == 'theme' else entry[6]

        # user_group_type1
        if 'user_group_type1' in payload:
            if entry[8] is None and not (payload['user_group_type1'] == 'user'):
                payload['user_group_type1'] = 'user'
            elif entry[9] is None and not (payload['user_group_type1'] == 'group'):
                payload['user_group_type1'] = 'group'
        else:
            payload['user_group_type1'] = 'group' if entry[9] is None else 'user'

        # user_group_id1
        if 'user_group_id1' in payload:
            if payload['user_group_type1'] == 'group' and not (payload['user_group_id1'] == entry[8]):
                payload['user_group_id1'] = entry[8]
            elif payload['user_group_type1'] == 'user' and not (payload['user_group_id1'] == entry[9]):
                payload['user_group_id1'] = entry[9]
        else:
            payload['user_group_id1'] = entry[8] if payload['user_group_type1'] == 'group' else entry[9]

        # The following keys must be in the json only if the analysis to do is a correlation
        if entry[2] == 'correlation':
            # factor_theme_type2
            if 'factor_theme_type2' in payload:
                if entry[11] is None and not (payload['factor_theme_type2'] == 'theme'):
                    payload['factor_theme_type2'] = 'theme'
                elif entry[12] is None and not (payload['factor_theme_type2'] == 'factor'):
                    payload['factor_theme_type2'] = 'factor'
            else:
                payload['factor_theme_type2'] = 'theme' if entry[11] is None else 'factor'

            # factor_theme_id2
            if 'factor_theme_id2' in payload:
                if payload['factor_theme_type2'] == 'factor' and not (payload['factor_theme_id2'] == entry[11]):
                    payload['factor_theme_id2'] = entry[11]
                elif payload['factor_theme_type2'] == 'theme' and not (payload['factor_theme_id2'] == entry[12]):
                    payload['factor_theme_id2'] = entry[12]
            else:
                payload['factor_theme_id2'] = entry[12] if payload['factor_theme_type2'] == 'theme' else entry[11]

            # user_group_type2
            if 'user_group_type2' in payload:
                if entry[13] is None and not (payload['user_group_type2'] == 'user'):
                    payload['user_group_type2'] = 'user'
                elif entry[14] is None and not (payload['user_group_type2'] == 'group'):
                    payload['user_group_type2'] = 'group'
            else:
                payload['user_group_type2'] = 'group' if entry[14] is None else 'user'

            # user_group_id2
            if 'user_group_id2' in payload:
                if payload['user_group_type2'] == 'group' and not (payload['user_group_id2'] == entry[13]):
                    payload['user_group_id2'] = entry[13]
                elif payload['user_group_type2'] == 'user' and not (payload['user_group_id2'] == entry[14]):
                    payload['user_group_id2'] = entry[14]
            else:
                payload['user_group_id2'] = entry[13] if payload['user_group_type2'] == 'group' else entry[14]

            # visualize
            payload['visualize'] = False
        else:
            # checks if the keys are present in the json, if yes they are removed
            if 'factor_theme_type2' in payload:
                del payload['factor_theme_type2']
            if 'factor_theme_id2' in payload:
                del payload['factor_theme_id2']
            if 'user_group_type2' in payload:
                del payload['user_group_type2']
            if 'user_group_id2' in payload:
                del payload['user_group_id2']
            if 'visualize' in payload:
                del payload['visualize']

    # Sets start and end dates
        payload['end_date1'] = str(date.today())
        end_date2 = None

        # Handles period1
        match entry[5]:
            case 'daily':
                start_date = date.today() - timedelta(days=1)
                payload['start_date1'] = str(start_date)
                end_date2 = start_date - timedelta(days=1)
            case 'weekly':
                start_date = date.today() - timedelta(days=6)
                payload['start_date1'] = str(start_date)
                end_date2 = start_date - timedelta(days=1)
            case 'monthly':
                start_date = date.today() - timedelta(days=27)
                payload['start_date1'] = str(start_date)
                end_date2 = start_date - timedelta(days=1)

        if entry[2] == 'correlation':
            payload['end_date2'] = str(end_date2)

            # Handles period2
            match entry[10]:
                case 'daily':
                    start_date = end_date2 - timedelta(days=1)
                    payload['start_date2'] = str(start_date)
                case 'weekly':
                    start_date = end_date2 - timedelta(days=6)
                    payload['start_date2'] = str(start_date)
                case 'monthly':
                    start_date = end_date2 - timedelta(days=27)
                    payload['start_date2'] = str(start_date)
        else:
            if 'end_date2' in payload:
                del payload['end_date2']
            if 'start_date2' in payload:
                del payload['start_date2']

        final_payload.append([entry[3], payload])

    return jsonify(final_payload)


def get_from_database(group_id: int):
    try:
        with DbAccess() as db:
            db.cursor.execute(query_str, (group_id,), multi=False)
            results = db.cursor.fetchall()
            db.close()
            return results

    except DBError as e:
        print('An error occurred while using the DB:', str(e))
        return jsonify({'error': 'DBError: ' + str(e)}), 500
