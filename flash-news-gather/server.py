from utils import Config
from flask import Flask, request
from flash_news_gather import gather

app = Flask(__name__)

# Load configuration once when the app starts
Config.parse_from_config()


@app.route('/')
def hello():
    return 'Flash news gather service - up and running!'


@app.route('/flash-news-gather', methods=['GET'])
def gather_api():
    if request.args.get('groupId') is not None:
        return gather(group_id=int(request.args.get('groupId')))
    else:
        return 'Bad Request', 400


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=6011, debug=True)
