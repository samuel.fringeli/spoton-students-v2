import os
from mysql.connector import connect
import toml
from pathlib import Path


base_dir = Path(__file__).parent


class Config(object):
    DEFAULT_CONFIG_PATH = base_dir / 'conf/config.toml'

    # DB config
    db_host: str
    db_port: int
    db_username: str
    db_password: str
    db_database: str

    @classmethod
    def parse_from_config(cls, path: Path = DEFAULT_CONFIG_PATH):
        data = toml.load(path)

        cls.db_host = data['database']['host']
        cls.db_port = data['database']['port']
        cls.db_username = os.environ['MYSQL_USER_NAME']
        cls.db_password = os.environ['MYSQL_ROOT_PASSWORD']
        cls.db_database = os.environ['MYSQL_DATABASE']


class DbAccess(object):
    def __init__(self):
        self.connection = connect(
            host=Config.db_host,
            port=Config.db_port,
            user=os.environ['MYSQL_USER_NAME'],
            password=os.environ['MYSQL_ROOT_PASSWORD'],
            database=os.environ['MYSQL_DATABASE'],
            ssl_disabled=True)
        self.cursor = self.connection.cursor()

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.close()

    def close(self):
        self.cursor.close()
        self.connection.close()
