1. Connect to server via ssh: 
``` 
ssh user@sooz.tic.heia-fr.ch 
```

2. From your home directory, create a temp repo in your home folder on the remote PC and use chmod to allow writing to this temp directory:

```
    mkdir temp
    chmod 755 temp
```
3. switch to root user with   
``` 
    beroot   
```
4. Run your sql query using docker exec and specify the output file (replacer *user* with your username):

```
   docker exec -it sooze_database_1 mysql -u root -p s00z_2_db "COPY (your sql query) TO STDOUT CSV" > /home/net/*user*/temp/output.csv
    
```
5. Copy the file from the remote server directory to your local PC (replace *user* with your username):

```
    scp *user*@sooz.tic.heia-fr.ch:~/temp/output_6.csv ~/personal
```
