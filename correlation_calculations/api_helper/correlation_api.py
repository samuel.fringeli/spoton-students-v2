import uuid
from correlation_analysis import VoteGatherer, get_correlation_peaks, plot_correlation
from .error_handlers import handle_db_error, handle_api_error, handle_value_error, handle_generic_error
from flask import Flask, request, jsonify
from utils import Config, DbAccess, ApiAccess
from requests import HTTPError
from mysql.connector import Error as DBError
import json
import os
import requests
from datetime import datetime, timedelta
import datetime as dt
import sys

import logging

# Configure the logging settings
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

SENTENCE_TOLERANCE = 0.3
WANNA_GROUP_DATES = True # TODO: If we don't want to group the dates we can change it here


# New function for sentence formatting
def is_consecutive(date1, date2):
    """Check if two dates are consecutive, considering weekends."""
    next_day = date1 + timedelta(days=1)
    if next_day.weekday() == 5:  # If next day is Saturday, skip to Monday
        next_day += timedelta(days=2)
    return next_day == date2


def group_dates(date_ranges):
    """Group consecutive date ranges."""
    if not date_ranges:
        return []
    grouped_ranges = []
    original_start, end = date_ranges[0]
    start = original_start

    for i in range(1, len(date_ranges)):
        next_start, next_end = date_ranges[i]
        # Check if the next range starts the day after the current range ends
        if is_consecutive(start, next_start):
            end = next_end  # Extend the current range
            start = next_start
        else:
            grouped_ranges.append((original_start, end))
            original_start = next_start
            start, end = next_start, next_end

    grouped_ranges.append((original_start, end))  # Append the last range
    return grouped_ranges


def parse_french_date(date_str):
    # Define a mapping from French to English month names for parsing
    months_fr_to_en = {
        'janvier': 'January', 'février': 'February', 'mars': 'March',
        'avril': 'April', 'mai': 'May', 'juin': 'June',
        'juillet': 'July', 'août': 'August', 'septembre': 'September',
        'octobre': 'October', 'novembre': 'November', 'décembre': 'December'
    }

    # Split the string to extract the date part and translate the month
    parts = date_str.split(' ')
    parts[2] = months_fr_to_en[parts[2].lower()]  # Translate month

    # Reconstruct the date string in a format that can be parsed by datetime.strptime
    date_str_en = ' '.join(parts[1:4])

    return datetime.strptime(date_str_en, '%d %B %Y')


def date_to_string(date):
    # Assuming 'date' is a datetime.date object
    # Adding a dummy time '00:00:00' and timezone 'GMT'
    date_str = date.strftime('%a, %d %b %Y') + " 00:00:00 GMT"
    return date_str


# End new function


def convert_date(date_str):
    # Mapping English day names to French
    days_en_to_fr = {
        'Mon': 'lundi', 'Tue': 'mardi', 'Wed': 'mercredi',
        'Thu': 'jeudi', 'Fri': 'vendredi', 'Sat': 'samedi', 'Sun': 'dimanche'
    }

    # Mapping English month names to French
    months_en_to_fr = {
        'Jan': 'janvier', 'Feb': 'février', 'Mar': 'mars',
        'Apr': 'avril', 'May': 'mai', 'Jun': 'juin',
        'Jul': 'juillet', 'Aug': 'août', 'Sep': 'septembre',
        'Oct': 'octobre', 'Nov': 'novembre', 'Dec': 'décembre'
    }

    # Parsing the original date string
    original_format = '%a, %d %b %Y %H:%M:%S GMT'
    date_obj = datetime.strptime(date_str, original_format)

    # Constructing the new date string manually
    day_name_fr = days_en_to_fr[date_obj.strftime('%a')]
    month_name_fr = months_en_to_fr[date_obj.strftime('%b')]
    new_date_str = f"{day_name_fr} {date_obj.day} {month_name_fr} {date_obj.year}"

    return new_date_str


def is_first_range_bigger(start_date1, end_date1, start_date2, end_date2):
    """ Compares two date ranges and returns True if the first range is longer or equal to the second. """
    # Parse dates
    start_date1 = datetime.strptime(start_date1, "%Y-%m-%d").date()
    end_date1 = datetime.strptime(end_date1, "%Y-%m-%d").date()
    start_date2 = datetime.strptime(start_date2, "%Y-%m-%d").date()
    end_date2 = datetime.strptime(end_date2, "%Y-%m-%d").date()

    # Calculate duration of each range
    duration1 = (end_date1 - start_date1).days
    duration2 = (end_date2 - start_date2).days

    # Compare durations
    return duration1 >= duration2


def classify_date_range(start_date, end_date):
    """ Classifies the date range as 'daily', 'weekly', or 'monthly'. """
    duration = (end_date - start_date).days
    if duration < 2:
        return 'daily'
    elif 4 <= duration < 10:
        return 'weekly'
    else:
        return 'monthly'


def get_date_combinations(start_date1, end_date1, start_date2, end_date2):
    """ Determines the combinations of date ranges in an ordered format. """
    # Parse dates
    start_date1 = datetime.strptime(start_date1, "%Y-%m-%d").date()
    end_date1 = datetime.strptime(end_date1, "%Y-%m-%d").date()
    start_date2 = datetime.strptime(start_date2, "%Y-%m-%d").date()
    end_date2 = datetime.strptime(end_date2, "%Y-%m-%d").date()

    # Classify each date range
    range1_type = classify_date_range(start_date1, end_date1)
    range2_type = classify_date_range(start_date2, end_date2)

    # Order and combine the results
    return range1_type + '/' + range2_type


def generate_sentences(start_date1, start_date2, end_date1, end_date2, serializable_data, smaller_range, bigger_range):
    # Determine the periods
    if not is_first_range_bigger(start_date1, end_date1, start_date2, end_date2):
        periods = get_date_combinations(start_date1, end_date1, start_date2, end_date2)
    else:
        periods = get_date_combinations(start_date2, end_date2, start_date1, end_date1)

    if periods in ['daily/daily', 'weekly/weekly', 'monthly/monthly']:
        result = serializable_data[0][1]
        dates_from_small_range = get_dates_from_voting_ranges(smaller_range, 0, -1)
        dates_from_bigger_range = get_dates_from_voting_ranges(bigger_range, serializable_data[0][0],
                                                               len(smaller_range) - 1)

        if 1 - SENTENCE_TOLERANCE <= result <= 1 + SENTENCE_TOLERANCE:
            return f"Les périodes du {convert_date(dates_from_small_range[0])} au {convert_date(dates_from_small_range[1])} et du {convert_date(dates_from_bigger_range[0])} au {convert_date(dates_from_bigger_range[1])} sont similaires."

        elif -1 - SENTENCE_TOLERANCE <= result <= -1 + SENTENCE_TOLERANCE:
            return f"Les périodes du {convert_date(dates_from_small_range[0])} au {convert_date(dates_from_small_range[1])} et du {convert_date(dates_from_bigger_range[0])} au {convert_date(dates_from_bigger_range[1])} sont contraires."

        else:
            return f""

    elif periods in ['daily/weekly', 'daily/monthly', 'weekly/monthly']:
        sentences = []
        small_dates = get_dates_from_voting_ranges(smaller_range, 0, -1)
        primary_range = f"du {convert_date(small_dates[0])} au {convert_date(small_dates[1])}"
        similar_ranges = []
        different_ranges = []

        for result in serializable_data:
            dates_from_bigger_range = get_dates_from_voting_ranges(bigger_range, result[0],
                                                                   result[0] + len(smaller_range) - 1)
            range_str = f"du {convert_date(dates_from_bigger_range[0])} au {convert_date(dates_from_bigger_range[1])}"

            if 1 - SENTENCE_TOLERANCE <= result[1] <= 1 + SENTENCE_TOLERANCE:
                similar_ranges.append(range_str)
            elif -1 - SENTENCE_TOLERANCE <= result[1] <= -1 + SENTENCE_TOLERANCE:
                different_ranges.append(range_str)

        similar_dates = []
        different_dates = []
        # Convert and group the similar and different ranges
        i = 0
        for range in similar_ranges:
            if i != 8:
                similar_dates.append(
                    (parse_french_date(range.split("du ")[1].split(' au ')[0]), parse_french_date(range.split(' au ')[1])))
            i += 1

        for range in different_ranges:
            different_dates.append(
                (parse_french_date(range.split("du ")[1].split(' au ')[0]), parse_french_date(range.split(' au ')[1])))

        if WANNA_GROUP_DATES:
            grouped_similar = group_dates([(start.date(), end.date()) for start, end in similar_dates])
            grouped_different = group_dates([(start.date(), end.date()) for start, end in different_dates])
        else:
            grouped_similar = similar_dates
            grouped_different = different_dates


        if grouped_similar:
            if len(grouped_similar) == 1:
                similar_str = f"du {convert_date(date_to_string(grouped_similar[0][0]))} au {convert_date(date_to_string(grouped_similar[0][1]))}"
                sentences.append(f"La période {primary_range} est similaire à la période suivante: {similar_str}. \n\n")
            else:
                similar_str = ";\n- ".join(
                    [f"du {convert_date(date_to_string(start))} au {convert_date(date_to_string(end))}" for start, end
                     in grouped_similar])
                sentences.append(
                    f"La période {primary_range} est similaire aux périodes suivantes :\n- {similar_str}. \n\n")
        if grouped_different:
            if len(grouped_different) == 1:
                different_str = f"du {convert_date(date_to_string(grouped_different[0][0]))} au {convert_date(date_to_string(grouped_different[0][1]))}"
                sentences.append(
                    f"La période {primary_range} est différente à la période suivante: {different_str}. \n\n")
            else:
                different_str = ";\n- ".join(
                    [f"du {convert_date(date_to_string(start))} au {convert_date(date_to_string(end))}" for start, end
                     in grouped_different])
                sentences.append(
                    f"La période {primary_range} est différente des périodes suivantes :\n- {different_str}. \n")

        return " ".join(sentences) if sentences else ""

    # Add any additional logic here if necessary


def get_dates_from_voting_ranges(voting_ranges, first_index, second_index):
    return voting_ranges[first_index][0], voting_ranges[second_index][0]


class CorrelationService:

    @staticmethod
    def get_gather_results(vg, request):
        data1 = vg.gather(request.get('start_date1'), request.get('end_date1'), request.get('factor_theme_type1'),
                          request.get('factor_theme_id1'), request.get('user_group_type1'),
                          request.get('user_group_id1'))
        data2 = vg.gather(request.get('start_date2'), request.get('end_date2'), request.get('factor_theme_type2'),
                          request.get('factor_theme_id2'), request.get('user_group_type2'),
                          request.get('user_group_id2'))
        return data1, data2

    @staticmethod
    def get_gather_results_by_api(data):
        try:
            # Read the API URL from the host environment variable
            api_url = 'http://gather:6002/gather'

            # Send a POST request to the API URL with JSON data
            headers = {'Content-Type': 'application/json'}

            # logger.info("response1  before--->")
            # logger.info("data1  --->"+str(data))

            response = requests.post(api_url, json=data, headers=headers)
            # logger.info("response1  after--->")
            # logger.info("response1.content : %s", response.content )
            response_content = response.content.decode('utf-8')

            if response.status_code == 200:
                # Parse the JSON response
                response_data = json.loads(response.text)
                return response_data
            else:
                print(f"Failed to retrieve data from the API. Status code: {response.status_code}")
                return None

        except Exception as e:
            print(f"An error occurred while making the API request: {str(e)}")

    @staticmethod
    def analyze_api_request():
        try:
            request_data = request.get_json()
            # logger.info("request_data  --->"+str(request_data))

            visualize = request_data.get('visualize')

            if visualize is not None and not isinstance(visualize, bool):
                raise ValueError('Invalid parameter type: "visualize" must be a boolean.')
            with DbAccess() as db, ApiAccess() as api:
                # vg = VoteGatherer(db, api)
                # logger.info("request_data -- analyze_api_request --->")
                # logger.info(f"request_data -- analyze_api_request ---> {request_data}")

                # Print request_data to the console
                gather_results = CorrelationService.get_gather_results_by_api(request_data)
                # logger.info(f"gather_results -- analyze_api_request ---> {gather_results}")

                # Check if the first date range is bigger than the second
                if is_first_range_bigger(request_data.get('start_date1'), request_data.get('end_date1'),
                                         request_data.get('start_date2'), request_data.get('end_date2')):
                    smaller_range = gather_results["data"]["result"][1]
                    bigger_range = gather_results["data"]["result"][0]
                else:
                    smaller_range = gather_results["data"]["result"][0]
                    bigger_range = gather_results["data"]["result"][1]

                # gather_results = CorrelationService.get_gather_results(vg, request_data)
                result = get_correlation_peaks(bigger_range, smaller_range)
                # logger.info(f"get_correlation_peaks result -- analyze_api_request ---> {result}")

                serializable_data = [tuple((int(x), float(y))) for x, y in result]
                file_name = str(uuid.uuid4())

                if request_data.get('visualize'):
                    plot_correlation(bigger_range, smaller_range, f"out/{file_name}.pdf")

                # TODO: WARNING - REMEMBER THAT WE KNOW SKIP ALL THE ODD INDEXES OF THE CORRELATION RESULTS
                # AS THEY MEAN THAT YOU WOULD START THE COMPARISON ON A AFTERNOON AND NOT A MORNING
                # WHICH DOESN'T MAKE SENSE. VALIDATED BY HOUDA

                filtered_serializable_data = [x for i, x in enumerate(serializable_data) if i % 2 == 0]

                sentences = generate_sentences(request_data.get('start_date1'), request_data.get('start_date2'),
                                               request_data.get('end_date1'), request_data.get('end_date2'),
                                               filtered_serializable_data, smaller_range, bigger_range)
                # return jsonify({'data': {'result': serializable_data, 'sentences': sentences, 'filename': file_name, 'plt_link': f"http://127.0.0.1:6001/download/?filename={file_name}"}})
                return jsonify({'sentences': sentences})

        except DBError as e:
            return handle_db_error(e)

        except HTTPError as e:
            return handle_api_error(e)

        except ValueError as e:
            return handle_value_error(e)

        except Exception as e:
            return handle_generic_error(e)
