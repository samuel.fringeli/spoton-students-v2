from flask import request, send_file

def download_plt_api_request():
    filename = request.args.get('filename')
    local_file_path = f'./out/{filename}.pdf'
    
    try:
        return send_file(local_file_path, as_attachment=True)
    except Exception as e:
        return str(e)