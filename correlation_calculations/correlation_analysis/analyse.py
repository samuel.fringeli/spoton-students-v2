import numpy as np
import numpy.typing as npt
import matplotlib.pyplot as plt

from .export_votes import VoteData

import sys

NPFloatArray = npt.NDArray[np.floating]


def flatten_votes(data: VoteData) -> NPFloatArray:
    return np.array([e[2] for e in data])


def find_peaks(data: NPFloatArray, n_maximas: int = 1) -> list[tuple[int, float]]:
    abs_data = np.abs(data)
    max_val = np.max(abs_data)
    is_peak_vals = list(map(lambda e: np.isclose(e, max_val), abs_data))
    peak_args = [e[0] for e in np.argwhere(is_peak_vals)]
    peaks = [(i, float(data[i])) for i in peak_args]

    if n_maximas > 1:
        return peaks + find_peaks(data[np.invert(is_peak_vals)], n_maximas - 1)
    return peaks


def normalized_correlate(a: NPFloatArray, b: NPFloatArray) -> NPFloatArray:
    std_a = np.std(a)
    std_b = np.std(b)

    if np.isclose(std_a, 0) or np.isclose(std_b, 0):
        raise ValueError("Can't calculate correlation on constant signal")

    a = (a - np.mean(a)) / std_a
    b = (b - np.mean(b)) / std_a

    res = np.correlate(a, b) / min(len(a), len(b))
    return res


def get_correlation_peaks(data1: VoteData, data2: VoteData):
    data1 = flatten_votes(data1)
    data2 = flatten_votes(data2)

    res = normalized_correlate(data1, data2)
    return find_peaks(res)


def plot_correlation(data1: VoteData, data2: VoteData, save_path: str) -> None:
    plt.clf()
    data1 = flatten_votes(data1)
    data2 = flatten_votes(data2)

    res = normalized_correlate(data1, data2)
    x1 = np.linspace(0, len(res) - 1, len(res))
    x2 = np.linspace(0, len(data1) - 1, len(data1))
    x3 = np.linspace(0, len(data2) - 1, len(data2))
    line1, = plt.plot(x1, res, 'o-r', label='correlation')
    line2, = plt.plot(x2, data1, 'o-g', label='data1')
    line3, = plt.plot(x3, data2, 'o-b', label='data2')
    plt.legend(handles=[line1, line2, line3])

    if save_path is None:
        plt.show()
    else:
        plt.savefig(save_path)
    plt.clf()
