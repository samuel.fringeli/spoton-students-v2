

from utils import Config
from flask import Flask
from api_helper import download_plt_api_request 
from api_helper import CorrelationService 





app = Flask(__name__)

# Load configuration once when the app starts
Config.parse_from_config()



@app.route('/')
def hello():
    return 'This Compose/Flask demo has been viewed %s time(s) - correlation service'

@app.route('/welcome', methods=['GET'])
def welcome():
    return 'Welcome to correlation service'

@app.route('/download/', methods=['GET'])
def download_plt_api():
    return download_plt_api_request()

@app.route('/analyse', methods=['POST'])
def analyze_api():
    return CorrelationService.analyze_api_request()

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=6001, debug=True)
