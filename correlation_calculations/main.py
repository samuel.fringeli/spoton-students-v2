#!/usr/bin/env python3

from mysql.connector import Error as DBError
from requests import HTTPError
import argparse
from datetime import date
from pathlib import Path

from test_data_generation import generate_test_data, load_json, insert_test_data, delete_test_data
from correlation_analysis import VoteGatherer, get_correlation_peaks, plot_correlation
from utils import Config, DbAccess, ApiAccess


def init_argparser() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Module to do correlation calculations and tests on voting data')

    parser.add_argument('--config', type=str,
                        default=str(Config.DEFAULT_CONFIG_PATH), help='Path to TOML config file')

    subparsers = parser.add_subparsers(
        help='choose a command', required=True, dest='command')

    subparsers.add_parser(
        'import', help='imports the JSON test cases in the DB')

    subparsers.add_parser(
        'delete', help='removes the JSON test cases in the DB')

    generate_parser = subparsers.add_parser(
        'generate', help='generates fake users and vote data')
    generate_parser.add_argument(
        'start_date', type=date.fromisoformat, help='start date (format: YYYY-mm-dd)')
    generate_parser.add_argument(
        'end_date', type=date.fromisoformat, help='end date (format: YYYY-mm-dd)')
    generate_parser.add_argument(
        'theme_id', type=int, help='id of the theme for which the users vote')

    analyze_parser = subparsers.add_parser(
        'analyze', help='analyse vote data from the DB')
    analyze_parser.add_argument(
        '-s', '--save_plot', type=str, help='save plot to PNG file (specify the path)')
    analyze_parser.add_argument(
        '-v', '--visualize', action='store_true', help='visualize plot')
    analyze_parser.add_argument(
        'start_date1', type=date.fromisoformat, help='start date (format: YYYY-mm-dd)')
    analyze_parser.add_argument(
        'end_date1', type=date.fromisoformat, help='end date (format: YYYY-mm-dd)')
    analyze_parser.add_argument('factor_theme_type1', choices=[
                                'factor', 'theme'], help='is the id for a factor or a theme')
    analyze_parser.add_argument(
        'factor_theme_id1', type=int, help='id of factor or theme to analyse')
    analyze_parser.add_argument('user_group_type1', choices=[
                                'user', 'group'], help='is the id for a user or a group')
    analyze_parser.add_argument(
        'user_group_id1', type=int, help='id of user or group to analyse')
    analyze_parser.add_argument(
        'start_date2', type=date.fromisoformat, help='start date (format: YYYY-mm-dd)')
    analyze_parser.add_argument(
        'end_date2', type=date.fromisoformat, help='end date (format: YYYY-mm-dd)')
    analyze_parser.add_argument('factor_theme_type2', choices=[
                                'factor', 'theme'], help='is the id for a factor or a theme')
    analyze_parser.add_argument(
        'factor_theme_id2', type=int, help='id of factor or theme to analyse')
    analyze_parser.add_argument('user_group_type2', choices=[
                                'user', 'group'], help='is the id for a user or a group')
    analyze_parser.add_argument(
        'user_group_id2', type=int, help='id of user or group to analyse')

    return parser.parse_args()


def main():
    args = init_argparser()
    Config.parse_from_config(Path(args.config))
    try:
        with DbAccess() as db:
            with ApiAccess() as api:
                if args.command == 'import':
                    users = load_json(Config.users_path)
                    votes = load_json(Config.votes_path)
                    try:
                        offset = insert_test_data(db, users, votes)
                        print(
                            f'User ID offset to map from the users in the JSON to the DB is {offset} (user 5 in the JSON is now {5 + offset})')
                   
                   
                   
                    
                    except RuntimeError as e:
                        print('An error occured inserting test data:')
                        print(e)
                elif args.command == 'delete':
                    users = load_json(Config.users_path)
                    votes = load_json(Config.votes_path)

                    try:
                        delete_test_data(db, users, votes)
                    except RuntimeError as e:
                        print('An error occured deleting test data:')
                        print(e)
                elif args.command == 'generate':
                    generate_test_data(
                        db, api, args.start_date, args.end_date, args.theme_id)
                elif args.command == 'analyze':
                    vg = VoteGatherer(db, api)
                    data1 = vg.gather(args.start_date1, args.end_date1, args.factor_theme_type1,
                                      args.factor_theme_id1, args.user_group_type1, args.user_group_id1)
                    data2 = vg.gather(args.start_date2, args.end_date2, args.factor_theme_type2,
                                      args.factor_theme_id2, args.user_group_type2, args.user_group_id2)

                    print(get_correlation_peaks(data1, data2))
                    if args.visualize:
                        plot_correlation(data1, data2, args.save_plot)
    except DBError as e:
        print('An error occured while using the DB:')
        print(e)
    except HTTPError as e:
        print('An error occured while using the backend API:')
        print(e)
    except ValueError as e:
        print('Invalid arguments passed:')
        print(e)
    except IOError as e:
        print('An error occured while reading the JSON files:')
        print(e)


if __name__ == '__main__':
    main()
