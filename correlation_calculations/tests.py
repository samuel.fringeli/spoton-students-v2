#!/usr/bin/env python3

from typing import Union
from enum import Enum
import unittest
import json
from datetime import date
from pathlib import Path
import numpy as np

from correlation_analysis import VoteGatherer, VoteGatheringQuery, get_correlation_peaks
from test_data_generation import load_json, insert_test_data, delete_test_data
from utils import Config, DbAccess, ApiAccess


class TestCaseDataType(Enum):
    QUERY = 'query'
    PATTERN = 'pattern'

    @classmethod
    def parse_from_json(cls, type: str):
        if type == 'query':
            return cls.QUERY
        elif type == 'pattern':
            return cls.PATTERN
        else:
            raise ValueError(f'{type} is an invalid Test case data type')


class TestCase(object):

    def __init__(self, data: dict[str, any], user_id_offset: int):
        self.min_threshold = data.get('min_threshold', float('inf'))
        self.max_threshold = data.get('max_threshold', float('-inf'))
        self.peak_locations = data['peak_locations']
        self.name = data['test_name']
        self.description = data.get('description', '')

        def parseData(x: int) -> tuple[TestCaseDataType, Union[VoteGatheringQuery, list[float]]]:
            d = data[f'data_{x}']
            type = TestCaseDataType.parse_from_json(d['type'])

            if type is TestCaseDataType.QUERY:
                start_date = date.fromisoformat(d['start_date'])
                end_date = date.fromisoformat(d['end_date'])
                user_id = int(d['user_id']) + user_id_offset
                if 'factor_id' in d:
                    return type, VoteGatheringQuery(
                        start_date, end_date, 'factor', d['factor_id'], 'user', user_id)
                else:
                    return type, VoteGatheringQuery(
                        start_date, end_date, 'theme', d['theme_id'], 'user', user_id)
            elif type is TestCaseDataType.PATTERN:
                return type, list(map(float, d['pattern']))

        self.data1 = parseData(1)
        self.data2 = parseData(2)

    def gather_data(self, vg: VoteGatherer):
        def gather_one(data: tuple[TestCaseDataType, Union[VoteGatheringQuery, list[float]]]):
            type, content = data
            if type == TestCaseDataType.QUERY:
                return vg.gather_from_query(content)
            elif type == TestCaseDataType.PATTERN:
                return [(None, None, e) for e in content]

        return gather_one(self.data1), gather_one(self.data2)


def load_test_cases(path: Path, user_id_offset: int):
    with open(path) as file:
        return [TestCase(data, user_id_offset) for data in json.loads(file.read())]


class CorrelationCalculationTest(unittest.TestCase):

    def setUp(self):
        Config.parse_from_config()
        self.users = load_json(Config.users_path)
        self.votes = load_json(Config.votes_path)
        db = DbAccess()
        api = ApiAccess()
        self.vg = VoteGatherer(db, api)

        # delete the previous test data if present
        delete_test_data(self.vg.db, self.users, self.votes)
        user_id_offset = insert_test_data(self.vg.db, self.users, self.votes)

        self.test_cases: list[TestCase] = load_test_cases(
            Config.test_cases_path, user_id_offset)

    def test(self):
        for tc in self.test_cases:
            data1, data2 = tc.gather_data(self.vg)

            self.assertNotEqual(
                len(data1), 0, f'votes of query1 not empty for {tc.name}')
            self.assertNotEqual(
                len(data2), 0, f'votes of query2 not empty for {tc.name}')

            peaks = get_correlation_peaks(data1, data2)

            index = 0
            for x, y in peaks:
                if np.less_equal(y, tc.max_threshold) or np.greater_equal(y, tc.min_threshold):
                    self.assertLess(index, len(tc.peak_locations),
                                    f'are there more peaks within the threshold as expected for {tc.name}')
                    self.assertEqual(x, tc.peak_locations[index], f'{tc.name}')
                    index += 1

            self.assertEqual(index, len(tc.peak_locations),
                             f'have all peaks occured as expected for {tc.name}')

    def tearDown(self):
        delete_test_data(self.vg.db, self.users, self.votes)

        self.vg.db.close()
        self.vg.api.close()


if __name__ == '__main__':
    unittest.main()
