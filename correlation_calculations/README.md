# Correlation calculations

The aim of this part of the project is to be able to do correlation calculation on the voting data on the DB.

## How to run

* Go to the config file at `conf/config.toml` and adapt it to your needs. For the DB password for root use the one specified in the .env file
* Install all the necessary packages with `pip install -r requirements.txt` (use conda to install [numpy](https://numpy.org/install/) if pip alone doesn't work)
* To generate users and voting data first adapt the `conf/user_profiles.json` file with the profiles you want to have
* Run `./main.py generate` to generate the fake data (it will be generated in `out/`)
* To import / remove it manually into the DB use `./main.py import` and `./main.py delete` respectively
* To run the unit tests (they insert and delete the testing data automatically) use `./tests.py`
* You can configure the test cases for the unit tests in the `conf/test_cases.json` file
* Use the `./main.py analyze --help` to get information on how to make a query to analyze voting data


### Configure the unit tests

Documented example of the `conf/test_cases.json` file:

```json
[
    {
        // the 2 sources of data:
        // here a query that will gather votes in the db
        "data_1": {
            "type": "query",
            // the user_id are the ones specified in the users.json file (they are mapped to db IDs in the test script)
            "user_id": 15,
            "start_date": "2023-06-01",
            "end_date": "2023-06-08",
            // can either specify factor_id or theme_id
            "theme_id": 7
        },
        // here a pattern with hardcoded values
        "data_2": {
            "type": "pattern",
            "pattern": [1.0, 1.0, 1.0, -1.0, -1.0, -1.0]
        },
        // this represents the test case that there should be no peaks with y values >= 0.7 or <= -0.7
        "min_threshold": 0.7,
        "max_threshold": -0.7,
        "peak_locations": [],
        "test_name": "part-time positive - pattern",
        "description": "is there a sharp change in voting behaviour on part-time positive"
    },
    {
        // ...

        // this represents the test case that there should be peaks at x = 0, 2, 4, ... with y values >= 1
        "min_threshold": 1,
        "peak_locations": [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32],
        "test_name": "part-time positive week - part-time positive"
    },
    // ...
]
```

### Configure the test data generation

Documented example of the `conf/user_profiles.json` file:

```json
{
    // doesn't really matter what is chosen as long as they are valid ids
    "roomId": 20,
    "enterpriseId": 1,
    "profiles": [
        {
            // DB fields
            "firstName": "Charlie",
            "lastName": "Happy then sad - ",
            // usernames have to be unique
            "username": "happy_sad_charlie_",
            "password": "password",
            "points": 0,
            "activatePopup": 1,
            "sex": "f",
            "role": 8,
            "activationToken": null,
            "activated": 1,

            "voting_behaviour": {
                // can be left empty
                "pattern": [1, 1, 1, 1, 1, -1, -1, -1, -1, -1],
                
                // where to place the pattern: start, middle, end, random
                "pattern_placement": "middle",

                // how will it be filled around the pattern -> choice between:
                // * random: fill with random data 
                // * repeat: repeat the pattern over the specified time
                // * no_vote: fill with 0
                // * positive: fill with 1
                // * negative: fill with -1
                "fill_with": "positive"
            },
            // the amount of users that will be created accoring to this profile
            "number_of_occurences": 2
        },
        // ...
    ]
}
```


# API Endpoints
## Analyze API Endpoint

### Description

The `analyze_api` endpoint allows you to perform correlation analysis on data based on the input parameters and generate a downloadable PDF plot if requested.

### Endpoint Details

- **HTTP Method**: POST
- **URL**: `/analyse`

### Request

- **Headers**: 
  - `Content-Type`: `application/json` (Assuming JSON request body)
- **Body**: 
  ```json
  {
    "start_date1": "2023-10-01",
    "end_date1": "2023-10-30",
    "factor_theme_type1": "theme",
    "factor_theme_id1": 6,
    "user_group_type1": "user",
    "user_group_id1": 10,
    "start_date2": "2023-10-08",
    "end_date2": "2023-10-16",
    "factor_theme_type2": "theme",
    "factor_theme_id2": 6,
    "user_group_type2": "user",
    "user_group_id2": 10,
    "visualize": true
  }
  ``` 
### Successful Response

- **Status Code**: 200 OK

**Body:**

```json
{
    "data": {
        "filename": "2023-10-08_2023-10-16_10_plt",
        "plt_link": "http://127.0.0.1:6001/download/?filename=2023-10-08_2023-10-16_10_plt",
        "result": [
            [
                10,
                0.8075365739516148
            ]
        ]
    }
}
```

### Error Response

- **Status Code**: 400 Bad Request

**Body:**

```json
{
  "error": "Invalid parameter type: 'visualize' must be a boolean."
}
```

### Error Response
* To use this endpoint, make a POST request to /analyse with the appropriate request body as shown above. The response will contain the correlation analysis result and a download link for the plot.

## Download PDF Plot API Endpoint

### Description
* The download_plt_api endpoint allows you to download a previously generated PDF plot.

### Endpoint Details
HTTP Method: GET
URL: /download/

### Request

#### Query Parameter:
filename: The name of the file you want to download (without the ".pdf" extension).

### Response

#### Successful Response:
* The PDF plot file for download.

#### Error Response:
* Status Code: 404 Not Found if the specified file does not exist.

### Usage
* To download a PDF plot, make a GET request to /download/ with the filename query parameter specifying the name of the file you want to download.


**ex.:**

```json
 /download/?filename=2023-10-08_2023-10-16_10_plt
```
