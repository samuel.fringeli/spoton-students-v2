import numpy as np
from sklearn.cluster import KMeans

def determine_clusters_for_kmeans(data, max_clusters=10, method="wcss"):
    """
    Determine the optimal number of clusters using the elbow method.
    """
    scores = []  # within-cluster sum of squares

    for i in range(2, max_clusters+1):

        kmeans = KMeans(n_clusters=i, init='k-means++', n_init=10, random_state=42)
        kmeans.fit(data)
        scores.append(kmeans.inertia_)

    gradients = np.gradient(np.gradient(scores))
    print(gradients)
    elbow_point_index = np.argmax(gradients)
    optimal_clusters = elbow_point_index + 2  # Adjust for 0-based indexing
    return optimal_clusters