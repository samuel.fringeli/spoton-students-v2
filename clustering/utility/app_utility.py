import click
import numpy as np
import pandas as pd
import json
from flask import request, send_file
import os

def add_jitter(arr, amount=0.05):
    """
    Adds a small random jitter to array values.
    """
    return arr + (np.random.random(size=len(arr)) - 0.5) * amount


def one_hot_encode(values, num_categories):
    """
    One-hot encode a list of categorical values.
    """
    return np.eye(num_categories)[values]

def one_hot_encode_data(x, y, z):
    """
    One-hot encode the x, y, z categorical values.
    """
    x_encoded = one_hot_encode(np.array(x) + 1, 3)  # Since x has values -1, 0, 1
    y_encoded = one_hot_encode(np.array(y) - 1, 7)  # As theme values range from 1 to 7
    z_encoded = one_hot_encode(np.array(z) - 1, 7)  # As weekdays values range from 1 to 7
    return np.hstack((x_encoded, y_encoded, z_encoded))

def date_to_weekday(date_str):
    """
    Convert a date string to its corresponding weekday number (1=Monday, 5=Friday).
    """
    date_obj = pd.Timestamp(date_str)
    return date_obj.weekday() + 1  # Adjusting for 1=Monday

def get_encode_data(args):
    """
    This function retrieves votes satisfaction,thems,weekday and then one-hot encodes the data.
    Args: args: Input arguments, which are used to retrieve votes.
    Returns:
        x:  satisfaction from the votes.
        y:  thems from the votes.
        z:  weekday from the votes.
        encoded_data: One-hot encoded data.
    """
    from cluster_helper import VoteService
    # get votes
    x, y, z= VoteService.get_votes(body_json=args)

    # encoded votes
    encoded_data = one_hot_encode_data(x, y, z)

    return x, y, z, encoded_data


class Logger:
    """Class description."""
    display = True

    @classmethod
    def write(cls, text: str):
        if Logger.display:
            click.echo(text)


def download_plt():
    file_type = request.args.get('file_type')
    if file_type == "pdf":
        return download_plt_as_pdf()
    else:
        return download_plt_as_html()


        



    
    

def download_plt_as_html():
    filename = request.args.get('filename')
    local_file_path = f'./out/{filename}.HTML'
    try:
        return send_file(local_file_path, as_attachment=True)
    except Exception as e:
        return str(e) 
    



def download_plt_as_pdf():
    filename = request.args.get('filename')
    local_file_path = f'./out/{filename}.pdf'
    try:
        return send_file(local_file_path, as_attachment=True)
    except Exception as e:
        return str(e) 