from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import silhouette_score, calinski_harabasz_score

def determine_clusters_for_agglomerative(data, max_clusters=10):
    """
    Determine the optimal number of clusters for Agglomerative Clustering using the silhouette method.
    """
    silhouette_scores = []
    
    for n_clusters in range(2, max_clusters+1):  # Start from 2 clusters as silhouette can't be computed for 1 cluster
        clusterer = AgglomerativeClustering(n_clusters=n_clusters)
        preds = clusterer.fit_predict(data)

        # Compute the silhouette score
        score = silhouette_score(data, preds)
        # silhouette_scores.append(score)
        silhouette_scores.append(calinski_harabasz_score(data, preds))


    # Get the number of clusters with the maximum silhouette score
    #print(silhouette_scores)
    optimal_clusters = silhouette_scores.index(max(silhouette_scores)) + 2  # +2 because the range starts from 2
    return optimal_clusters