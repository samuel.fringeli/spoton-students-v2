
from mysql.connector import Error as DBError
from flask import Flask
from api_helper import ClusterAPIHelper
from utility import download_plt
app = Flask(__name__)


@app.route('/')
def hello():
    return 'This Compose/Flask demo has been viewed %s time(s) - Clustering service'

@app.route('/welcome', methods=['GET'])
def welcome():
    return 'Welcome to Clustering service'

@app.route('/cluster', methods=['POST'])
def cluster_api():
    return ClusterAPIHelper.cluster_api_request()

@app.route('/most/repeated/cluster', methods=['POST'])
def most_repeated_cluster():
    return ClusterAPIHelper.find_most_repeated_cluster()

@app.route('/download/', methods=['GET'])
def download_plt_by_api():
    return download_plt()

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=6004, debug=True)