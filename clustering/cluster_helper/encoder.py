import numpy as np
import pandas as pd
import json
np.random.seed(42)

class NumpyEncoder(json.JSONEncoder):
    """
    Special json encoder for numpy types
    """
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                            np.int16, np.int32, np.int64, np.uint8,
                            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        return json.JSONEncoder.default(self, obj)