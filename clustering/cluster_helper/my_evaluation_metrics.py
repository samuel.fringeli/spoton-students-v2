from sklearn.metrics import davies_bouldin_score,silhouette_score,calinski_harabasz_score

class MyEvaluationMetrics:

    @staticmethod
    def silhouette_coefficient(data_frame,label):
        """
        Silhouette Coefficient or silhouette score is a metric used to calculate the goodness of a clustering technique. Its value ranges from -1 to 1:
        1: Means clusters are well apart from each other and clearly distinguished.
        0: Means clusters are indifferent, or we can say that the distance between clusters is not significant.
        -1: Means clusters are assigned in the wrong way.
        """
        # print(f'Silhouette Score(n=10): {silhouette_score(data_frame, label)}')
        return silhouette_score(data_frame, label)

    @staticmethod
    def davies_bouldin_score(data_frame,label):
        """
        Compute the Davies-Bouldin score.
        The score is defined as the average similarity measure of each cluster with its most similar cluster, where similarity is the ratio of within-cluster distances to between-cluster distances. Thus, clusters which are farther apart and less dispersed will result in a better score.
        The minimum score is zero, with lower values indicating better clustering.
        """
        # print(f'davies_bouldin_score: {davies_bouldin_score(data_frame, label)}')
        return davies_bouldin_score(data_frame, label)

    @staticmethod
    def calinski_harabasz_score(data_frame,label):
        """
        Compute the Calinski and Harabasz score.
        It is also known as the Variance Ratio Criterion.
        The score is defined as ratio of the sum of between-cluster dispersion and of within-cluster dispersion.
        where a higher Calinski-Harabasz score relates to a model with better defined clusters.
        """
        # print(f'calinski_harabasz_score: {calinski_harabasz_score(data_frame, label)}')
        return calinski_harabasz_score(data_frame, label)  