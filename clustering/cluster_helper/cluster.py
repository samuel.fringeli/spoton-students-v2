import numpy as np
import pandas as pd
from sklearn.cluster import AgglomerativeClustering, KMeans
from utility import determine_clusters_for_agglomerative, determine_clusters_for_kmeans
from plot import plot_data
from cluster_helper import NumpyEncoder,MyEvaluationMetrics
import json
from utility import get_encode_data,Logger
import math

class ClusterService:

    @staticmethod
    def cluster(analyze_args, clustering_method="KMeans", analysis_type="5-point",should_print_interpretations_json=False, should_print_interpretations_string=True,file_name=None,file_type=None):
      
        # This function retrieves Satisfaction Theme,Weekday and encodes  data.
        x, y, z, encoded_data = get_encode_data(analyze_args)

        df = pd.DataFrame({
            'Satisfaction': x,
            'Theme': y, 
            'Weekday': z
        })
        labels = cluster_data(encoded_data, clustering_method)

        Logger.write(f"labels : {labels}")
        Logger.write(f"_________________________________________________________________{ math.ceil(MyEvaluationMetrics.silhouette_coefficient(df,labels))}")
        Logger.write(f"_________________________________________________________________{ math.floor(MyEvaluationMetrics.calinski_harabasz_score(df,labels))}")
        Logger.write(f"_________________________________________________________________{ math.ceil(MyEvaluationMetrics.calinski_harabasz_score(df,labels))}")

        interpretations = analyze_clusters(df, labels, analysis_type)
        if should_print_interpretations_json:
            print_interpretation_json(interpretations)
        if should_print_interpretations_string:
            print_interpretation_string(interpretations, analysis_type)
        plot_data(df, labels, clustering_method,file_name=file_name,file_type=file_type)




    @staticmethod
    def cluster_multiple_times(analyze_args, clustering_method="KMeans", analysis_type="5-point", num_iterations=10, should_print_interpretations_json=False, should_print_interpretations_string=True,file_name=None,file_type=None):
        from collections import Counter

        # This function retrieves Satisfaction Theme, Weekday, and encodes data.
        x, y, z, encoded_data = get_encode_data(analyze_args)

        df = pd.DataFrame({
            'Satisfaction': x,
            'Theme': y,
            'Weekday': z
        })

        # Store the cluster results in a list
        cluster_results = []

        # Evaluation lists
        silhouette_coefficient_list=[]
        davies_bouldin_score_list=[]
        calinski_harabasz_score_list=[]

        for i in range(num_iterations):
            labels = cluster_data(encoded_data, clustering_method)
            cluster_results.append(labels)
            # Evaluation the cluster 
            silhouette_coefficient_list.append(math.floor(MyEvaluationMetrics.silhouette_coefficient(df,labels)))
            davies_bouldin_score_list.append(math.ceil(MyEvaluationMetrics.davies_bouldin_score(df,labels)))
            calinski_harabasz_score_list.append(math.floor(MyEvaluationMetrics.calinski_harabasz_score(df,labels)))

        # Find the most frequent clustering result
        Logger.write(f"labels : {labels}")
        Logger.write(f"cluster_results : {cluster_results}")

        # Count the occurrences of each cluster result
        cluster_counts = Counter(tuple(result) for result in cluster_results)
        Logger.write(f"cluster_counts : {cluster_counts}")

        # Find the most frequent cluster result
        most_frequent_cluster_result = max(cluster_counts, key=cluster_counts.get)
        
        # Convert it back to a numpy array if needed
        most_frequent_cluster_result = np.array(most_frequent_cluster_result)

        Logger.write(f"************** Start Evaluation **************")
        # the best evaluation cluster labels :
        Logger.write(f"the best cluster labels by using silhouette_coefficient evaluation function(Max)")
        mx_sil_cof_index= silhouette_coefficient_list.index(max(silhouette_coefficient_list))
        label_sil_cof = cluster_results[mx_sil_cof_index]
        Logger.write(f"best cluster : {label_sil_cof}")

        Logger.write(f"the best cluster labels by using davies_bouldin_score evaluation function(Min)")
        min_bouldin_score_indexe= davies_bouldin_score_list.index(min(davies_bouldin_score_list))
        label_bouldin_score = cluster_results[min_bouldin_score_indexe]
        Logger.write(f"best cluster : {label_bouldin_score}")

        Logger.write(f"the best cluster labels by using calinski_harabasz_score evaluation function(Max)")
        max_calinski_score= calinski_harabasz_score_list.index(max(calinski_harabasz_score_list))
        label_calinski_score= cluster_results[max_calinski_score]
        Logger.write(f"best cluster : {label_calinski_score}")

        # the plot best evaluation cluster labels :
        if all(x == y == z for x, y, z in zip(label_sil_cof, label_bouldin_score, label_calinski_score)):
          plot_data(df, x, clustering_method,file_name=f"evaluate_{file_name}_silhouette",file_type=file_type)
          plot_data(df, y, clustering_method,file_name=f"evaluate_{file_name}_bouldin",file_type=file_type)
          plot_data(df, z, clustering_method,file_name=f"evaluate_{file_name}_calinski",file_type=file_type)
        Logger.write(f"************** End Evaluation **************")
  

        # Evaluation for most frequent cluster 
        MyEvaluationMetrics.silhouette_coefficient(df,most_frequent_cluster_result)
        Logger.write(f"_________________________________________________________________")
        MyEvaluationMetrics.davies_bouldin_score(df,most_frequent_cluster_result)
        Logger.write(f"_________________________________________________________________")
        MyEvaluationMetrics.calinski_harabasz_score(df,most_frequent_cluster_result)



        interpretations = analyze_clusters(df, most_frequent_cluster_result, analysis_type)

        if should_print_interpretations_json:
            Logger.write(f"__________________________________{should_print_interpretations_json}_______________________________")
            print_interpretation_json(interpretations)
        if should_print_interpretations_string:
            Logger.write(f"__________________________________{should_print_interpretations_string}_______________________________")
            print_interpretation_string(interpretations, analysis_type)

        plot_data(df, most_frequent_cluster_result, clustering_method,file_name=f"{file_name}_most_frequent",file_type=file_type)





def perform_clustering(data, method="KMeans"):
    """
    Perform clustering on the data using the specified method.
    """
    if method == "Agglomerative":
        optimal_clusters = determine_clusters_for_agglomerative(data)
        clusterer = AgglomerativeClustering(n_clusters=optimal_clusters)
    elif method == "KMeans":
        optimal_clusters = determine_clusters_for_kmeans(data)
        clusterer = KMeans(n_clusters=optimal_clusters, init='k-means++', n_init=10, random_state=42)
    else:
        raise ValueError(f"Unsupported clustering method: {method}")

    labels = clusterer.fit_predict(data)
    return labels

def cluster_data(encoded_data, clustering_method):
    labels = perform_clustering(encoded_data, clustering_method)
    return labels

def five_point_summary(df, column_name):
    """
    Calculate the 5-point summary for a given column in the DataFrame.
    """
    min_val = df[column_name].min()
    q1 = df[column_name].quantile(0.25)
    median = df[column_name].median()
    q3 = df[column_name].quantile(0.75)
    max_val = df[column_name].max()

    return {
        'Min': min_val,
        'Q1': q1,
        'Median': median,
        'Q3': q3,
        'Max': max_val
    }

def round_percentages(percentages):
    rounded_percentages = {k: round(v, 1) for k, v in percentages.items()}
    adjustment = 100 - sum(rounded_percentages.values())

    # Adjust the largest value(s) to make the sum 100%
    for key in sorted(rounded_percentages, key=rounded_percentages.get, reverse=True):
        if adjustment > 0:
            rounded_percentages[key] += 0.1
            adjustment -= 0.1
        if adjustment <= 0:
            break

    return rounded_percentages

def percentage_of_data_points(df, column_name):
    """
    Calculate the percentage of data points for each unique value in the given column.
    """
    total_count = len(df)
    value_counts = df[column_name].value_counts()
    percentages = (value_counts / total_count) * 100

    return percentages.to_dict()


def analyze_clusters(df, labels, analysis_type):
    """
    Analyze the clusters and return the results.
    """
    clusters = {}
    for i, label in enumerate(labels):
        if label not in clusters:
            clusters[label] = []
        clusters[label].append({
            'Weekday': df.iloc[i]['Weekday'],
            'Satisfaction': df.iloc[i]['Satisfaction'],
            'Theme': df.iloc[i]['Theme']
        })

    cluster_analysis = {}
    for label, cluster_points in clusters.items():
        cluster_analysis[label] = pd.DataFrame(cluster_points)

    cluster_interpretations = []
    for label, analysis in cluster_analysis.items():
        if analysis_type == "5-point" or  analysis_type == "percentage":
            cluster_interpretations.append(interpret_cluster(label, analysis, analysis_type))
        else:
            raise ValueError(f"Unsupported analysis type: {analysis_type}")


    sorted_interpretations = sorted(cluster_interpretations, key=lambda x: x['cluster_id'])
    return sorted_interpretations

def interpret_cluster(cluster_id, analysis, analysis_type):
    """
    Provide a structured interpretation of the cluster
    """
    # Threshold for satisfaction
    satisfaction_threshold = 0.5

    cluster_analysis = {"cluster_id": cluster_id}

    # Check satisfaction
    satisfaction_spread = analysis['Satisfaction'].std()
    satisfaction_data = {"std": satisfaction_spread, "threshold": satisfaction_threshold}
    if satisfaction_spread < satisfaction_threshold:
        satisfaction_data["important"] = [analysis['Satisfaction'].mean()]
    cluster_analysis["satisfaction"] = satisfaction_data

    # Check theme
    if analysis_type == "5-point":
        theme_data = five_point_summary(analysis, 'Theme')
    elif analysis_type == "percentage":
        theme_data = round_percentages(percentage_of_data_points(analysis, 'Theme'))
    else:
        theme_data = {}
    cluster_analysis["theme"] = theme_data

    # Check weekday
    if analysis_type == "5-point":
        weekday_data = five_point_summary(analysis, 'Weekday')
    elif analysis_type == "percentage":
        weekday_data = round_percentages(percentage_of_data_points(analysis, 'Weekday'))
    else:
        weekday_data = {}
    cluster_analysis["weekday"] = weekday_data

    return cluster_analysis

def print_interpretation_json(interpretations):
    """
    Print the structured JSON interpretation for each cluster.
    """
    for interpretation in interpretations:
        print(json.dumps(interpretation, indent=4, cls=NumpyEncoder))


def print_interpretation_string(cluster_interpretations, analysis_type):
    interpretation_string = f"Our automated analysis has detected {len(cluster_interpretations)} interesting clusters of votes. Here are the results: \n\n"
    for interpretation in cluster_interpretations:
        cluster_id = interpretation["cluster_id"]
        interpretation_string += f"Voting cluster No.{cluster_id + 1}: \n"


        satisfaction_map = {1.0: "satisfied", -1.0: "unsatisfied", 0.0: "abstinent"}
        satisfaction = [satisfaction_map.get(val, "") for val in interpretation["satisfaction"].get("important", [])]
        # check if the val is equal -0.0035048669277109854 will not considered

        weekday_map = {1: "Monday", 2: "Tuesday", 3: "Wednesday", 4: "Thursday", 5: "Friday"}

        if len(satisfaction) != 0:
            interpretation_string += f"Users are predominantly {', '.join(satisfaction)}.\n"
        else:
            interpretation_string += f"We cannot conclude on user satisfaction.\n"

        if analysis_type == "5-point":
            theme_percentages = interpretation['theme']
            weekday_percentages = interpretation['weekday']

            if theme_percentages['Min'] != theme_percentages['Max']:
                interpretation_string += f"Votes range from theme {int(round(theme_percentages['Min']))} to theme {int(round(theme_percentages['Max']))}, with a median at theme {int(round(theme_percentages['Median']))}. Themes are generally concentrated between {int(round(theme_percentages['Q1']))} and {int(round(theme_percentages['Q3']))}.\n"
            else:
                interpretation_string += f"All votes are for the theme {theme_percentages['Min']}.\n"


            if weekday_percentages['Min'] != weekday_percentages['Max']:
                interpretation_string += "Votes range from "
                interpretation_string += f"{weekday_map.get(int(np.floor(weekday_percentages['Min'])), str(weekday_percentages['Min']))} "
                interpretation_string += f"to {weekday_map.get(int(np.floor(weekday_percentages['Max'])), str(weekday_percentages['Max']))}, "
                interpretation_string += f"with a median on {weekday_map.get(int(round(weekday_percentages['Median'])), str(weekday_percentages['Median']))}. "
                interpretation_string += "The days of the week are generally concentrated between "
                interpretation_string += f"{weekday_map.get(int(np.floor(weekday_percentages['Q1'])), str(weekday_percentages['Q1']))} "
                interpretation_string += f"and {weekday_map.get(int(np.floor(weekday_percentages['Q3'])), str(weekday_percentages['Q3']))}.\n"
            else:
                interpretation_string += f"All votes are for {weekday_map.get(int(weekday_percentages['Min']), str(weekday_percentages['Min']))}.\n"


        elif analysis_type == "percentage":
            theme_percentages = interpretation['theme']
            weekday_percentages = interpretation['weekday']

            if theme_percentages:
                if len(theme_percentages) == 1 and list(theme_percentages.values())[0] == 100:
                    interpretation_string += f"For the themes, all votes are for theme  {int(np.round(list(theme_percentages.keys())[0]))}.\n"
                else:
                    interpretation_string += "For the themes, "
                    for theme, percent in theme_percentages.items():
                        interpretation_string += f"theme {int(np.round(theme))} represents {percent}% of the votes, "

                    interpretation_string = interpretation_string.rstrip(", ") + ".\n"

            if weekday_percentages:
                if len(weekday_percentages) == 1 and list(weekday_percentages.values())[0] == 100:
                    interpretation_string += f"For the days of the week, all votes are for {weekday_map[list(weekday_percentages.keys())[0]]}.\n"
                else:
                    interpretation_string += "For the days of the week, "

                    for weekday, percent in weekday_percentages.items():

                        # get round weekday
                        rounded_weekday =round(weekday)

                        if rounded_weekday in weekday_map:
                            interpretation_string += f"On {weekday_map[rounded_weekday]}, it represents {percent}% of the votes, "
                        else:
                            # Handle the case where the key is not in weekday_map
                            interpretation_string += f"Unknown weekday {rounded_weekday} represents {percent}% of the votes, "

                    interpretation_string = interpretation_string.rstrip(", ") + ".\n"

        else:
            raise ValueError(f"Unsupported analysis type: {analysis_type}")

        interpretation_string += "\n\n"

    print(interpretation_string)
