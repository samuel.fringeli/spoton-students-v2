from requests import HTTPError
import json
import requests
from datetime import datetime


class VoteService:


    
    @staticmethod
    def get_votes(body_json):
        """
        Gathers votes through an API request and parses the data for analysis.

        Args:
            self: The instance of the class (usually 'self' in a class method).
            body_json (dict): A JSON object containing vote data.

        Returns:
            x_satisfaction (list): A list of satisfaction scores associated with the votes.
            y_themes (list): A list of themes or categories for the collected votes.
            z_Weekday (list): A list of weekdays corresponding to when the votes were collected.


        This method gathers votes through an API request by calling 'gather_votes' and then parses the JSON response using 'parse_votes_json'. It returns the parsed data as separate lists for satisfaction scores, weekdays, and themes.

        Example usage:
        x_satisfaction, y_themes,  z_Weekday= get_votes(body_json)
        """
        # Gathers votes through an API request.
        json_vote = VoteService._gather_votes(body_json=body_json)


        # Parses JSON data containing vote information.
        x_satisfaction, y_Weekday, z_themes = VoteService._parse_votes_json(json_votes=json_vote,factor_theme_id1=body_json["factor_theme_id1"],factor_theme_id2=body_json["factor_theme_id2"])
        return x_satisfaction, y_Weekday, z_themes

    @staticmethod
    def _gather_votes(body_json):
       
        """
            Gathers votes through an API request.

            Parameters:
            - body_json (dict): A JSON map containing the following key-value pairs:
            - start_date1 (str): Start date for the first query.
            - end_date1 (str): End date for the first query.
            - factor_theme_type1 (str): Factor theme type for the first query.
            - factor_theme_id1 (int): Factor theme ID for the first query.
            - user_group_type1 (str): User group type for the first query.
            - user_group_id1 (int): User group ID for the first query.
            - start_date2 (str): Start date for the second query.
            - end_date2 (str): End date for the second query.
            - factor_theme_type2 (str): Factor theme type for the second query.
            - factor_theme_id2 (int): Factor theme ID for the second query.
            - user_group_type2 (str): User group type for the second query.
            - user_group_id2 (int): User group ID for the second query.
            - visualize (bool): A flag to indicate whether to visualize the data.

            Returns:
            - Json: A list of json votes obtained through the API request. data:result:[votes]
        """

        try:
            # Read the API URL from the host environment variable
            api_url = 'http://gather:6002/gather'

            # Send a POST request to the API URL with JSON data
            headers = {'Content-Type': 'application/json'}
            response = requests.post(api_url, json=body_json,headers=headers)

            if response.status_code == 200:
                # Parse the JSON response
                response_data = json.loads(response.text)
                return response_data
            else:
                print(f"Failed to retrieve data from the API. Status code: {response.status_code}")
                return None

        except Exception as e:
            print(f"An error occurred while making the API request: {str(e)}")


    @staticmethod
    def _parse_votes_json(json_votes,factor_theme_id1,factor_theme_id2):
        """
        Parses JSON data containing vote information.
        Args: json_votes (dict): A JSON dictionary containing vote data.
        Returns:
            x_satisfaction (list): A list of satisfaction values.
            y_Weekday (list): A list of corresponding weekdays for each vote.
            periods (list): A list of time periods for each vote.
        """

        z_Weekday = []
        periods = []
        x_satisfaction = []
        y_themes =[]
        themes_list_lengths ={"theme1_list_length": 0, "theme2_list_length": 0}

        # Loop through the "result" array in the JSON response
        for i, entry in enumerate(json_votes["data"]["result"]):
            if i == 0:
                themes_list_lengths["theme1_list_length"] = len(entry)
            elif i == 1:
                themes_list_lengths["theme2_list_length"] = len(entry)
                

            for vote in entry:
                # Append the first and second values to their respective lists
                z_Weekday.append(VoteService._convert_data_to_weekday(date_string =vote[0]))  # Corrected method name
                periods.append(vote[1])
                x_satisfaction.append(vote[2])


        # get themes  as a list of numbers
        y_themes =VoteService._get_themes(
                      theme_one=factor_theme_id1,
                      themes1_list_lengths=themes_list_lengths ["theme1_list_length"] ,
                      theme_two=factor_theme_id2,
                      theme2_list_length= themes_list_lengths ["theme2_list_length"] )

        return x_satisfaction, z_Weekday, y_themes
    

    @staticmethod
    def _get_themes(theme_one,themes1_list_lengths, theme_two,theme2_list_length):
        """
        Combine two themes into a single list.

        input:
        - theme_one: The first theme to be repeated.
        - themes1_list_lengths: The number of times theme_one should be repeated.
        - theme_two: The second theme to be included.
        - theme2_list_length: The number of times theme_two should be included.

        output:
        A list containing theme_one repeated themes1_list_lengths times,
        followed by theme_two repeated theme2_list_length times.
        """
      
        # Create theme1 and theme2
        theme1 = [theme_one] * themes1_list_lengths
        theme2 = [theme_two] * theme2_list_length
        # Append the elements from theme2 to theme1
        themes = theme1 + theme2


        return themes


    @staticmethod
    def _convert_data_to_weekday(date_string):
        """
        Convert a date string to the corresponding weekday (1=Monday, 5=Friday).
        Args:date_string (str): A date string in the format "Sun, 01 Oct 2023 00:00:00 GMT".
        Returns: int: The corresponding weekday, with Monday being 1 and Sunday being 7.
        """
        date_obj = datetime.strptime(date_string, "%a, %d %b %Y %H:%M:%S %Z")
        weekday = date_obj.weekday() + 1  # Adding 1 to match the desired numbering (1=Monday, 5=Friday)
        return weekday












