# Gather

The aim of this part of the project is to clusturing the votes.


# Run Clustering Container
* First of all, to run the "Clustering" container separately, execute the following command: "docker-compose up clustering." If you prefer to run the "Clustering" container alongside other containers, use the command "docker-compose up."


## How to run
* Run `./main.py cluster` [PARAMETERS] to run cluster the fake data.
* Use the `./main.py cluster --help` to get information on how to make a query to clustering voting data

### there are two way to run gather method:
* first way by run comand line inside gather container terminal  : ./main.py cluster [PARAMETERS]

**example for running clustering by command line.:**

```json
#./main.py cluster -s file_name clustering_method analysis_type should_print_interpretations_json should_print_interpretations_string file_type gather  start_date1 end_date1 factor_theme_type1 factor_theme_id1 user_group_type1 user_group_id1 start_date2 end_date2 factor_theme_type2 factor_theme_id2 user_group_type2 user_group_id2

./main.py cluster -s plt200 Agglomerative 5-point false true pdf gather 2023-10-01 2023-10-30 theme 6 user 10 2023-10-01 2023-10-08 theme 6 user 1
```

**for best_cluster**

```json
#./main.py best_cluster -s file_name clustering_method (KMeans or Agglomerative) analysis_type should_print_interpretations_json should_print_interpretations_string num_iterations file_type gather start_date1 end_date1 factor_theme_type1 factor_theme_id1 user_group_type1 user_group_id1 start_date2 end_date2 factor_theme_type2 factor_theme_id2 user_group_type2 user_group_id2

ex:
./main.py best_cluster -s best_cluster_100_plt KMeans 5-point false true 100 HTML gather 2023-10-01 2023-10-30 theme 6 user 10 2023-10-01 2023-10-08 theme 6 user 1
```





* secound way By using API endpoint method :
**By Make a POST request to http://localhost:6004/cluster with the following JSON payload in the request body and set the Content-Type header to application/json :**

```json
{
   "clustering_method":"Agglomerative",
   "analysis_type":"5-point",
   "should_print_interpretations_json":false,
   "should_print_interpretations_string":true,
   "file_type":"HTML",
    "gather":{
        "start_date1": "2023-10-01",
        "end_date1": "2023-10-30",
        "factor_theme_type1": "theme",
        "factor_theme_id1": 6,
        "user_group_type1": "user",
        "user_group_id1": 10,
        "start_date2": "2023-10-01",
        "end_date2": "2023-10-30",
        "factor_theme_type2": "theme",
        "factor_theme_id2": 6,
        "user_group_type2": "user",
        "user_group_id2": 10,
        "visualize": true
    }
}
```


**for best clusturing : a POST request to http://127.0.0.1:6004/most/repeated/cluster with the following JSON payload in the request body and set the Content-Type header to application/json :**

```json
{
//    "clustering_method":"Agglomerative",
   "clustering_method":"KMeans",
   "analysis_type":"5-point",
   "should_print_interpretations_json":false,
   "should_print_interpretations_string":true,
   "num_iterations":1000,
   "file_type":"pdf",
    "gather":{
        "start_date1": "2023-10-01",
        "end_date1": "2023-10-30",
        "factor_theme_type1": "theme",
        "factor_theme_id1": 6,
        "user_group_type1": "user",
        "user_group_id1": 10,
        "start_date2": "2023-10-01",
        "end_date2": "2023-10-30",
        "factor_theme_type2": "theme",
        "factor_theme_id2": 6,
        "user_group_type2": "user",
        "user_group_id2": 10,
        "visualize": true
    }
}
```

### Successful Response

- **Status Code**: 200 OK

**Body Example:**

```json
{
    "data": {
        "clustering_method": "Agglomerative",
        "filename": "2023-11-07_14-10-08_plot",
        "format": "HTML",
        "plot_link": "http://127.0.0.1:6004/download/?filename=2023-11-07_14-10-08_plot&file_type=HTML"
    }
}
```
```json
{
    "data": {
        "clustering_method": "KMeans",
        "filename": "2023-11-07_14-11-51_plot",
        "format": "pdf",
        "plot_link": "http://127.0.0.1:6004/download/?filename=2023-11-07_14-11-51_plot&file_type=pdf"
    }
}
```

### Error Response

- **Status Code**: 500 Bad Request

**Body Example:**

```json
{
  "error": "Internal Server Error:...."
}
```

