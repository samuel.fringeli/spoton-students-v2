import numpy as np
import plotly.graph_objects as go
from utility import add_jitter
import matplotlib.pyplot as plt
import plotly.io as pio
import os

def create_3d_scatter_plot(df, labels, clustering_method):
    """
    Create a 3D scatter plot with the given data and cluster labels.
    """
    trace = go.Scatter3d(
        x=df['Theme'],
        y=df['Weekday'],
        z=df['Satisfaction'],
        mode='markers',
        marker=dict(
            size=8,
            color=labels,
            opacity=0.7
        ),
        hoverinfo='text',
        text=df.apply(lambda row: f"Method: {clustering_method}<br>Cluster: {labels[row.name]}<br>Theme: {row['Theme']}<br>Satisfaction: {row['Satisfaction']}<br>Weekday: {row['Weekday']}", axis=1)
    )

    layout = go.Layout(
        title=f"3D Scatter Plot with {clustering_method} Clustering",
        scene=dict(
            xaxis_title="Theme",
            yaxis_title="Weekday",
            zaxis_title="Satisfaction",
            aspectmode="manual",
    ),
    width=1500,  # Adjust the width as needed
    height=800,  # Adjust the height as needed
    margin=dict(l=200, r=200, b=200, t=200)  # Adjust margins as needed

    )

    fig = go.Figure(data=trace, layout=layout)
    return fig

def plot_data(df, labels, clustering_method,file_name=None,file_type="HTML"):
    df['Theme'] = add_jitter(df['Theme'], amount=0.1)
    df['Weekday'] = add_jitter(df['Weekday'], amount=0.1)
    df['Satisfaction'] = add_jitter(df['Satisfaction'], amount=0.02)
    fig = create_3d_scatter_plot(df, labels, clustering_method)

        # Check if the "out" directory exists and create it if not
    if not os.path.exists("out"):
        os.makedirs("out")

    # Display the plot or save it as a 3D HTML file
    if file_name is None:
        pio.show(fig)
    else:
        if file_type=="pdf":
           pio.write_image(fig, f'out/{file_name}.pdf', engine='kaleido', scale=1)
        else:
            pio.write_html(fig, f'out/{file_name}.HTML', auto_open=False)


    # Display the plot or save it as a PDF
    # if file_name is None:
    #     pio.show(fig)
    # else:
    #     pio.write_image(fig, f'out/{file_name}.pdf', engine='kaleido', scale=1)
