
from flask import jsonify

def handle_db_error(e):
    print('An error occurred while using the DB:', str(e))
    return jsonify({'error': 'DBError: ' + str(e)}), 500

def handle_api_error(e):
    print('An error occurred while using the backend API:', str(e))
    return jsonify({'error': 'HTTPError: ' + str(e)}), 500

def handle_value_error(e):
    print('Invalid parameter type:', str(e))
    return jsonify({'error': 'Bad Request: ' + str(e)}), 400

def handle_generic_error(e):
    print('An error occurred:', str(e))
    return jsonify({'error': 'Internal Server Error: ' + str(e)}), 500
