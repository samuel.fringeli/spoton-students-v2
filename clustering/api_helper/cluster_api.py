from .error_handlers import handle_db_error, handle_api_error, handle_value_error, handle_generic_error
from requests import HTTPError
from flask import Flask, request, jsonify
from cluster_helper import ClusterService
from datetime import datetime

class ClusterAPIHelper:

    @staticmethod
    def cluster_api_request():
        try:
            timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            file_name=f"{timestamp}_plot"
            json_data = request.get_json()
            file_type=json_data["file_type"]

            ClusterService.cluster(
                analyze_args=json_data["gather"],
                file_type=file_type,
                clustering_method=json_data["clustering_method"] ,
                analysis_type=json_data["analysis_type"] ,
                should_print_interpretations_json=json_data["should_print_interpretations_json"] ,
                should_print_interpretations_string=json_data["should_print_interpretations_string"],
                file_name=file_name
             )
            return jsonify({'data': {'filename': file_name,
                                        'clustering_method':f'{json_data["clustering_method"]}',
                                        'plot_link': f"http://127.0.0.1:6004/download/?filename={file_name}&file_type={file_type}",
                                        'format':file_type}})
                
        except HTTPError as e:
            return handle_api_error(e)
            
        except ValueError as e:
            return handle_value_error(e)
            
        except Exception as e:
            return handle_generic_error(e)
        
    @staticmethod
    def find_most_repeated_cluster():
        try:
            timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            file_name=f"{timestamp}_plot"
            json_data = request.get_json()
            file_type=json_data["file_type"]

            ClusterService.cluster_multiple_times(
                file_type=file_type,
                num_iterations=json_data["num_iterations"],
                analyze_args=json_data["gather"],
                clustering_method=json_data["clustering_method"] ,
                analysis_type=json_data["analysis_type"] ,
                should_print_interpretations_json=json_data["should_print_interpretations_json"] ,
                should_print_interpretations_string=json_data["should_print_interpretations_string"],
                file_name=file_name)
            return jsonify({'data': {   'filename': file_name,
                                        'clustering_method':f'{json_data["clustering_method"]}',
                                        'plot_link': f"http://127.0.0.1:6004/download/?filename={file_name}&file_type={file_type}",
                                        'format':file_type}})
                
        except HTTPError as e:
            return handle_api_error(e)
            
        except ValueError as e:
            return handle_value_error(e)
            
        except Exception as e:
            return handle_generic_error(e)
        



