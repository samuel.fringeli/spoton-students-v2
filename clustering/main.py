#!/usr/bin/env python3
from requests import HTTPError
import argparse
from datetime import date
from pathlib import Path
from cluster_helper import ClusterService
from utility import Logger

def str2bool(v):
    # Custom function to convert a string to a boolean
    return v.lower() in ('true', '1', 'yes', 'y', 't')

def init_argparser() -> argparse.Namespace:
    print("Initializing argument parser")
    parser = argparse.ArgumentParser(
        description='Module to do clustering voting data')
    subparsers = parser.add_subparsers(
        help='choose a command', required=True, dest='command')
    cluster_parser = subparsers.add_parser(
        'cluster', help='clustering for vote')
    cluster_parser.add_argument(
        '-s', '--file_name', type=str, help='save plot to pdf file in "out" folder (specify the file_name)')
    cluster_parser.add_argument(
        'clustering_method', type=str, help='clustering_method (format: Agglomerative or KMeans)')
    cluster_parser.add_argument(
        'analysis_type', type=str, help='analysis_type (format: 5-point or percentage)')
    cluster_parser.add_argument(
        'should_print_interpretations_json', type=str, help='should_print_interpretations_json (format: True or False)')
    cluster_parser.add_argument(
        'should_print_interpretations_string', type=str, help='should_print_interpretations_string (format: True or False)')  
    cluster_parser.add_argument(
        'file_type', 
        type=str, 
        nargs='?',  # Make the argument optional
        default='HTML',  # Set the default value to 'HTML'
        help='file_type (format: pdf or HTML)'
    )
    
    subparser_one = cluster_parser.add_subparsers()
    gather_parser = subparser_one.add_parser('gather', help='gather vote')

    gather_parser.add_argument(
        'start_date1', type=str, help='start date (format: YYYY-mm-dd)')
    gather_parser.add_argument(
        'end_date1', type=str, help='end date (format: YYYY-mm-dd)')
    gather_parser.add_argument('factor_theme_type1', choices=[
                                'factor', 'theme'], help='is the id for a factor or a theme')
    gather_parser.add_argument(
        'factor_theme_id1', type=int, help='id of factor or theme ')
    gather_parser.add_argument('user_group_type1', choices=[
                                'user', 'group'], help='is the id for a user or a group')
    gather_parser.add_argument(
        'user_group_id1', type=int, help='id of user or group ')
    gather_parser.add_argument(
        'start_date2', type=str, help='start date (format: YYYY-mm-dd)')
    gather_parser.add_argument(
        'end_date2', type=str, help='end date (format: YYYY-mm-dd)')
    gather_parser.add_argument('factor_theme_type2', choices=[
                                'factor', 'theme'], help='is the id for a factor or a theme')
    gather_parser.add_argument(
        'factor_theme_id2', type=int, help='id of factor or theme ')
    gather_parser.add_argument('user_group_type2', choices=[
                                'user', 'group'], help='is the id for a user or a group')
    gather_parser.add_argument(
        'user_group_id2', type=int, help='id of user or group ')
    
# Best Cluster
    best_cluster_parser = subparsers.add_parser('best_cluster', help='Best clustering for vote')
    best_cluster_parser.add_argument(
        '-s', '--file_name', type=str, help='save plot to pdf file in "out" folder (specify the file_name)')
    best_cluster_parser.add_argument(
        'clustering_method', type=str, help='clustering_method (format: Agglomerative or KMeans)')
    best_cluster_parser.add_argument(
        'analysis_type', type=str, help='analysis_type (format: 5-point or percentage)')
    best_cluster_parser.add_argument(
        'should_print_interpretations_json', type=str, help='should_print_interpretations_json (format: True or False)')
    best_cluster_parser.add_argument(
        'should_print_interpretations_string', type=str, help='should_print_interpretations_string (format: True or False)')
    best_cluster_parser.add_argument('num_iterations', type=int, nargs='?', help='num_iterations (format: 00)')
    best_cluster_parser.add_argument(
        'file_type', 
        type=str, 
        nargs='?',  # Make the argument optional
        default='HTML',  # Set the default value to 'HTML'
        help='file_type (format: pdf or HTML)'
    )
    subparser_one = best_cluster_parser.add_subparsers()
    gather_parser = subparser_one.add_parser('gather', help='gather vote')
    gather_parser.add_argument(
        'start_date1', type=str, help='start date (format: YYYY-mm-dd)')
    gather_parser.add_argument(
        'end_date1', type=str, help='end date (format: YYYY-mm-dd)')
    gather_parser.add_argument('factor_theme_type1', choices=[
                                'factor', 'theme'], help='is the id for a factor or a theme')
    gather_parser.add_argument(
        'factor_theme_id1', type=int, help='id of factor or theme ')
    gather_parser.add_argument('user_group_type1', choices=[
                                'user', 'group'], help='is the id for a user or a group')
    gather_parser.add_argument(
        'user_group_id1', type=int, help='id of user or group ')
    gather_parser.add_argument(
        'start_date2', type=str, help='start date (format: YYYY-mm-dd)')
    gather_parser.add_argument(
        'end_date2', type=str, help='end date (format: YYYY-mm-dd)')
    gather_parser.add_argument('factor_theme_type2', choices=[
                                'factor', 'theme'], help='is the id for a factor or a theme')
    gather_parser.add_argument(
        'factor_theme_id2', type=int, help='id of factor or theme ')
    gather_parser.add_argument('user_group_type2', choices=[
                                'user', 'group'], help='is the id for a user or a group')
    gather_parser.add_argument(
        'user_group_id2', type=int, help='id of user or group ')
    return parser.parse_args()


def main():
    args = init_argparser()
    gather_args = {
        "start_date1": args.start_date1,
        "end_date1": args.end_date1,
        "factor_theme_type1": args.factor_theme_type1,
        "factor_theme_id1": args.factor_theme_id1,
        "user_group_type1": args.user_group_type1,
        "user_group_id1": args.user_group_id1,
        "start_date2": args.start_date2,
        "end_date2": args.end_date2,
        "factor_theme_type2": args.factor_theme_type2,
        "factor_theme_id2": args.factor_theme_id2,
        "user_group_type2": args.user_group_type2,
        "user_group_id2": args.user_group_id2,
        "visualize": True
    }

    try:
                try:
                    if args.command == 'cluster':
                        ClusterService.cluster( 
                        analyze_args=gather_args,
                        clustering_method=args.clustering_method,
                        analysis_type=args.analysis_type,
                        should_print_interpretations_json=args.should_print_interpretations_json,
                        should_print_interpretations_string=args.should_print_interpretations_string,
                        file_name=f"{args.file_name}_plot")

                    elif args.command == 'best_cluster':
                        ClusterService.cluster_multiple_times( 
                        analyze_args=gather_args,
                        num_iterations=args.num_iterations,
                        clustering_method=args.clustering_method,
                        analysis_type=args.analysis_type,
                        should_print_interpretations_json=args.should_print_interpretations_json,
                        should_print_interpretations_string=args.should_print_interpretations_string,
                        file_name=f"{args.file_name}_plot")
                          
                except RuntimeError as e:
                        print('An error occured clustering')
                        print(e)
                
    except HTTPError as e:
        print('An error occured while using the backend API:')
        print(e)
    except ValueError as e:
        print('Invalid arguments passed:')
        print(e)


if __name__ == '__main__':
    main()