import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { fileURLToPath, URL } from 'node:url'
import vuetify from "vite-plugin-vuetify";

export default defineConfig({
  plugins: [
    vue({
      /*template: {
        compilerOptions: {
          compatConfig: {
            MODE: 3
          }
        }
      }*/
    }),
    vuetify(),
  ],
  resolve: {
    alias: {
      // Using Vue 3 Migration Build prevents Vuetify components from loading (v-card,...) (outputs LegacyComponent error)
      //vue: '@vue/compat',
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
    extensions: [ '.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue' ],
  },
  server: {
    port: 8080,
  }
});