module.exports = {
  "globDirectory": "dist/",
  "globPatterns": [
    "**/*.{css,ico,woff2,woff,eot,ttf,svg,png,html,js,json,txt}"
  ],

  "swSrc": "workbox-sw.js",
  "swDest": "dist/sw.js"
};
