import OneShot from "@/api/OneShot";
import Responses from "@/api/Responses";
import ONE_SHOT_GRID_ICONS from "@/helpers/oneShotGridIcons";
import { THEME_INDICES } from "@/helpers/constants";
import { getToday } from "@/helpers/date"

// TODO: use a unique constant value (merge with constants)
const ENVIRONMENT_THEME_NAME = "Sensation";
const SMELL_FACTOR_NAME = "Air";

const state = {
  data: { // persistent data
    themes: []
  },
  answers: { // volatile data
    selectedTab: null,
    responses: [],
    environmentPartialResponse: []
  }
};

const getters = {
  getSelectedTab: state => state.answers.selectedTab,
  getThemes: state => state.data.themes,
  getResponses: state => state.answers.responses
};

const mutations = {
  setSelectedTab: (state, payload) => {
    state.answers.selectedTab = payload;
  },
  setThemes: (state, payload) => {
    state.data.themes = payload;
  },
  setResponses: (state, payload) => {
    state.answers.responses = payload;
  },
  setEnvironmentPartialResponse: (state, payload) => {
    if (Array.isArray(payload)) {
      // replace all elements
      state.answers.environmentPartialResponse = payload.map(response => {
        return { ...response, date: getToday() }
      })
    } else {
      // add or replace single element
      const enrichedPayload = { ...payload, date: getToday() }
      let index = state.answers.environmentPartialResponse.findIndex(response => response.period === payload.period)
      if (index >= 0) {
        state.answers.environmentPartialResponse[index] = enrichedPayload
      } else {
        state.answers.environmentPartialResponse.push(enrichedPayload)
      }
    }
  },
  upsertResponse: (state, payload) => {
    let index = state.answers.responses.findIndex(response => response.id == payload.id);
    if (index == -1) {
      // not found, we need to insert
      state.answers.responses.push(payload);
    } else {
      // found, we need to update
      state.answers.responses[index] = payload;
    }
  },
  deleteResponse: (state, payload) => {
    let index = state.answers.responses.findIndex(response => response.id == payload.id);
    if (index == -1) {
      // not found, should not happen but nothing to do
    } else {
      state.answers.responses.splice(index, 1);
    }
  }
};

// ----   actions helpers   ----

const prepareThemes = (commit, rootState) => {
  let themes = rootState.theme.data.themes.filter(
    theme => theme.id !== THEME_INDICES.WELLBEING
  );
  // make a deep copy
  themes = JSON.parse(JSON.stringify(themes));

  // add icons to all answers
  themes.forEach(theme => {
    const iconSetHasSubSets = Array.isArray(
      Object.values(ONE_SHOT_GRID_ICONS[theme.name])[0]
    );

    theme.factors.forEach((factor) => {
      // use text instead of icons for smell
      if (factor.name == SMELL_FACTOR_NAME) {
        factor.useTextForResponses = true;
      }

      let iconSet = [];
      if (iconSetHasSubSets) {
        iconSet = ONE_SHOT_GRID_ICONS[theme.name][factor.name];
      } else {
        iconSet = ONE_SHOT_GRID_ICONS[theme.name];
      }

      factor.responseOptionSet.responseOptions.forEach(
        (responseOption, responseOptionIndex) => {
          responseOption.icon = iconSet[responseOptionIndex];
        }
      );
    });

    // put the smell factor at the end of the "environment" theme
    if (theme.name === ENVIRONMENT_THEME_NAME) {
      let smellFactorId = theme.factors.findIndex((factor) => factor.name == SMELL_FACTOR_NAME);
      if(smellFactorId >= 0)
        theme.factors.push(theme.factors.splice(smellFactorId, 1)[0]);
    }
  });
  commit('setThemes', themes);

  return Promise.resolve();
}

const reloadDynamicData = (commit) => {
  return new Promise((resolve, reject) => {
    OneShot.getResponses().then(res => {
      commit('setResponses', res.data);
    }).then(() => {
      // load the partial response for the environment theme
      Responses.getThemeResponse(THEME_INDICES.ENVIRONMENT).then(res => {
        const responseFound = res.data;
        if (responseFound) {
          commit('setEnvironmentPartialResponse', responseFound)
        }
        resolve();
      });
    }).catch(reject);
  });
}

// ---- end actions helpers ----

const actions = {
  reloadData: {
    // global action
    root: true,
    handler: ({ commit }) => {
      return reloadDynamicData(commit);
    }
  },
  themesLoaded: {
    // global action
    root: true,
    handler: ({ commit, rootState }) => {
      return prepareThemes(commit, rootState);
    }
  },
  deleteResponse({ commit }, payload) {
    return new Promise((resolve, reject) => {
      Responses.deleteFactorResponse(
        payload.themeId,
        payload.factorId,
        payload.id
      ).then(() => {
        commit('deleteResponse', {
          id: payload.id
        });
        resolve();
      }).catch(reject);
    })
  },
  upsertResponse({ commit }, payload) {
    return new Promise((resolve, reject) => {
      Responses.putFactorResponse(
        payload.themeId,
        payload.factorId,
        {
          period: payload.period,
          responseOptionId: payload.responseOptionId,
          roomId: payload.roomId,
          isFromGuided: payload.isFromGuided
        }
      ).then((res) => {
        commit('upsertResponse', { ...res.data, factor: payload.factor }); // inject the factor information
        resolve();
      }).catch(reject);
    });
  },
  setEnvironmentPartialResponse({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const commonParams = {
        period: payload.period,
        responseOptionId: payload.responseOptionId,
        roomId: payload.roomId,
      }
      Responses.putThemeResponse(payload.themeId, commonParams).then(() => {
        commit('setEnvironmentPartialResponse', commonParams)
      }).catch(reject)
    })
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
