const state = {
  msalConfig: {
    auth: {
      clientId: '993d5b97-6676-4d91-bc1b-d0b6e4f63e65',
      authority:
        'https://login.microsoftonline.com/common',
      postLogoutRedirectUri : "http://localhost:8080/login",
    },
    cache: {
      cacheLocation: 'localStorage',
    },
  },
  accessToken: '',
  account: undefined,
  isFromTeams: false
};


const mutations = {
  setAccessToken(state, token) {
    state.accessToken = token;
  },
  setAccount(state, account) {
    state.account = account;
  },
  setIsFromTeams(state, value) {
    state.isFromTeams = value;
  }
};


const getters = {
  getMsalConfig: (state) => state.msalConfig,
  getAccount: (state) => state.account,
  getIsFromTeams: (state) => state.isFromTeams
};

export default {
  state,
  mutations,
  getters
};