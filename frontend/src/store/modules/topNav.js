const state = {
  topNav: {
    layout: null, // TODO: set to null
    displayTitle: "", // TODO: set to ""
  },
};

const getters = {
  getTopNav: (state) => state.topNav,
};

const mutations = {
  setTopNav: (state, payload) => {
    state.topNav = payload;
  },
};

const actions = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
