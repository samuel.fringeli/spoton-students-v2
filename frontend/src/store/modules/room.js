import Rooms from '../../api/Rooms';
import OneShot from "../../api/OneShot";

const state = {
  rooms: [],
  currentRoom: undefined,
};

const getters = {
  getRooms: (state) => state.rooms,
  getRoomById: (state) => (payload) => {
    const id = Number(payload);
    return state.rooms.find((value) => value.id === id);
  },
  getCurrentRoom: (state) => state.currentRoom || state.rooms[0],
  getCurrentRoomId: (state) => {
    if (state.currentRoom) {
      return state.currentRoom.id
    } else if (state.rooms[0]) {
      return state.state.rooms[0].id
    } else {
      // this is the case when an enterprise has not yet any rooms assigned
      return null
    }
  }
};

const mutations = {
  setRooms: (state, payload) => {
    state.rooms = payload;
    if (!state.rooms.includes(state.currentRoom)) {
      [state.currentRoom] = state.rooms;
    }
  },
  setCurrentRoom: (state, payload) => {
    state.currentRoom = payload;
  },
  resetRoom: (state) => {
    state.rooms = [];
    state.currentRoom = undefined;
  },
};

const actions = {
  getAllRooms: ({ dispatch, commit }) => Rooms.getAll()
    .then((result) => {
      commit('setRooms', result.data);
    }).then(() => {
      OneShot.getResponses().then(res => {
        const sortedResponses = res.data
          .map((response) => ({datetime: Date.parse(response.datetime), roomId: response.roomId}))
          .filter((response) => response.roomId)
          .reduce((a, b) => b.datetime > a.datetime ? b : a); // most recent is first one
        dispatch("setCurrentRoom", sortedResponses.roomId)
      })
    }),
  setCurrentRoom: ({
    commit,
    getters
  }, payload) => {
    let room = payload;
    if (typeof room === 'number') {
      room = getters.getRoomById(room);
    }
    commit('setCurrentRoom', room);
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
};
