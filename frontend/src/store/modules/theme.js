import Themes from "@/api/Themes";
import OneShot from "@/api/OneShot";
import { THEME_INDICES } from "@/helpers/constants";
import FEELING_ICONS from "@/helpers/feelingIcons";

const DATA_UPDATE_RATE =
  process.env.NODE_ENV === "development" ? 1000 : 24 * 60 * 60 * 1000; // 24h in prod (in milliseconds) or 1000 ms in dev

const state = {
  data: {
    // persistent data
    timestamp: 0, // latest update timestamp
    themes: [],
  },
  parametersCompleted: [],
  lastThemeSelected: "",
};

const getters = {
  getlastThemeSelected: (state) => state.lastThemeSelected,
  getThemes: (state) => state.data.themes,
  getThemeById: (state) => (payload) => {
    const id = Number(payload);
    return state.data.themes.find((value) => value.id === id);
  }
};

const prepareTheme = (rawTheme) => {
  // make a deep copy
  let theme = JSON.parse(JSON.stringify(rawTheme));

  // add icons to all answers
  theme.responseOptionSet.responseOptions.forEach((option, indexOption) => {
    option.icon = FEELING_ICONS[indexOption];
    option.icon = FEELING_ICONS[indexOption];
  });

  return theme;
};

const mutations = {
  setThemes: (state, payload) => {
    state.data.themes = payload;
  },
  setLastThemeSelected: (state, payload) => {
    state.lastThemeSelected = payload;
  },
  resetThemes: (state) => {
    state.parametersCompleted = [];
  },
  setTimestamp: (state, payload) => {
    state.data.timestamp = payload;
  },
};

const reloadStaticData = (commit, state, dispatch, data) => {
  // check if we need to reload data
  if (state.data !== undefined && state.data.timestamp !== undefined) {
    let newTimestamp = Date.now();
    if ((data && data.force) || (newTimestamp - state.data.timestamp > DATA_UPDATE_RATE)) {
      // DATA_UPDATE_RATE is elapsed; we need to update
      return new Promise((resolve, reject) => {
        OneShot.get()
        // let enterpriseId = this.$store.getters.getUser.enterpriseId;
        // OneShot.getByEnterprise(enterpriseId)
          .then((res) => {
            const rawThemes = res.data;
            const themes = [];
            rawThemes.forEach((theme) => {
              if (theme.id !== THEME_INDICES.WELLBEING) {
                themes.push(prepareTheme(theme));
              } else {
                themes.push(theme);
              }
            });
            commit("setThemes", themes);
            commit("setTimestamp", newTimestamp);
            // dispatch to all that themes have finished loading
            dispatch("themesLoaded", null, { root: true });
            resolve();
          })
          .catch(reject);
      });
    }
    // dispatch to all that themes have finished loading
    dispatch("themesLoaded", null, { root: true });
    return Promise.resolve();
  } else {
    return Promise.reject("Invalid store state !");
  }
};

const actions = {
  reloadData: {
    // global action
    root: true,
    handler: ({ dispatch, commit, state }, payload) => {
      // add data {force: true} to force to reload all data, even if DATA_UPDATE_RATE is not expired
      return reloadStaticData(commit, state, dispatch, payload);
    },
  },
  getAllThemes: ({ commit }) => {
    Themes.getAll()
      .then((result) => {
        let themes = result.data;
        commit("setThemes", themes);
      });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
