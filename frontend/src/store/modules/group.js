import Groups from "@/api/Groups";
import FlashNews from '@/api/FlashNews';

const state = {
  // this state attribute is actually rather a "membership" instead of direct group informations
  groups: [],
  // be carefull, currentGroup has not the same structure as the objects in groups[]
  currentGroup: null,
  currentGroupFlashNews: null,
};

const getters = {
  getGroups: (state) => state.groups,
  getGroupById: (state) => (payload) => {
    const id = Number(payload);
    return state.groups.find((value) => value.group.id === id);
  },
  getFlashNewsForGroup: (state) => state.currentGroupFlashNews,
  getCurrentGroup: (state) => state.currentGroup,
};

const mutations = {
  setGroups: (state, payload) => {
    state.groups = payload;
  },
  setCurrentGroup: (state, payload) => {
    state.currentGroup = payload;
  },
  addGroup: (state, groupWithDetails) => {
    state.groups.push(groupWithDetails);
  },
  setCurrentFlashNews: (state, payload) => {
    state.currentGroupFlashNews = payload;
  },
  updateMembership: (state, membership) => {
    let oldGroupIndex = state.groups.findIndex(groupWithDetails => groupWithDetails.group.id == membership.group.id);
    state.groups[oldGroupIndex] = {
      ...state.groups[oldGroupIndex],
      ...membership,
    }
  },
  updateGroup: (state, group) => {
    let oldGroupIndex = state.groups.findIndex(groupWithDetails => groupWithDetails.group.id == group.id);
    state.groups[oldGroupIndex].group = {
      ...state.groups[oldGroupIndex].group,
      ...group,
    }
  },
  removeGroup: (state, groupId) => {
    const updatedGroups = state.groups.filter((groupDetails) => {
      return groupDetails.group.id != groupId;
    });
    state.groups = updatedGroups;
  },
  resetGroups: (state) => {
    state.groups = [];
    state.currentGroup = null;
  },
};

const reloadDynamicData = (commit) => {
  return new Promise((resolve, reject) => {
    Groups.getAll().then((result) => {
      commit("setGroups", result.data);
      resolve();
    }).catch(reject);
  });
}

const actions = {
  reloadData: {
    // global action
    root: true,
    handler: ({ commit }) => {
      return reloadDynamicData(commit);
    }
  },
  getFlashNews: async ({commit}, groupId) => {
    await FlashNews.getFlashNews(groupId).then((res) => {
      commit("setCurrentFlashNews", res.data)
    });
  },
  addGroup: ({ commit }, group) => {
    Groups.post(group).then((res) => {
      commit("addGroup", res.data);
    });
  },
  updateGroup: ({ commit }, group) => {
    Groups.put(group.id, {
      name: group.name,
    }).then((res) => {
      commit("updateGroup", res.data);
    })
  },
  removeGroup: ({ commit }, group) => {
    Groups.delete(group.id).then(() => {
      commit("removeGroup", group.id);
    })
  },
  setCurrentGroup: ({ commit, getters }, payload) => {
    let group = payload;
    if (typeof group === "number") {
      group = getters.getGroupById(payload);
    }
    commit("setCurrentGroup", group);
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
