import Alarms from '@/api/Alarms';

const state = {
  alarms: [],
  allAlarms: [],
};

const getters = {
  getAlarms: state => state.alarms,
  getUnHandledAlarms: state => state.allAlarms.filter(a => !a.state),
  getHandledAlarms: state => state.allAlarms.filter(a => a.state),
};

const mutations = {
  setAlarms: (state, payload) => {
    state.alarms = payload;
  },
  setAllAlarms: (state, payload) => {
    state.allAlarms = payload;
  },
  deleteAlarm: (state, payload) => {
    let index = state.alarms.findIndex(a => a.id === payload.id);
    if (index !== -1) {
      state.alarms.splice(index, 1);
    }
  },
  updateOrInsertAlarmInAllCurrentUserAlarms: (state, payload) => {
    let index = state.allAlarms.findIndex(a => a.id === payload.alarmId);
    if (index !== -1) state.allAlarms[index] = payload;
    else state.allAlarms.push(payload);
  },
};

const reloadDynamicData = (commit) => {
  return new Promise((resolve, reject) => {
    if (window.location.href.includes('?ruleId')) {
      const [, ruleId] = window.location.search.replace('?', '').split('=');
      Alarms.getAll(ruleId).then(res => {
        commit('setAlarms', res.data);
        resolve();
      }).catch(reject);
    }
  });
};


const actions = {
  reloadData: {
    // global action
    root: true,
    handler: ({commit}) => {
      return reloadDynamicData(commit);
    },
  },
  reloadAllAlarms({commit}) {
    return new Promise((resolve, reject) => {
      Alarms.getAllAlarmsForCurrentUser().then(res => {
        commit('setAllAlarms', res.data);
        commit('setAlarms', res.data)
        resolve();
      }).catch(reject);
    });
  },
  deleteAlarm({commit}, payload) {
    return new Promise((resolve, reject) => {
      Alarms.delete(
          payload.id,
      ).then(() => {
        commit('deleteRule', {
          id: payload.id,
        });
        resolve();
      }).catch(reject);
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
