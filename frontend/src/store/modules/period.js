import { defaultPeriod } from "@/helpers/periods"

const state = {
  selectedPeriod: "",
};

const getters = {
  getSelectedPeriod: (state) => {
    if (state.selectedPeriod) {
      return state.selectedPeriod
    } else {
      return defaultPeriod()
    }
  }
};

const mutations = {
  setSelectedPeriod(state, newSelectedPeriod) {
    state.selectedPeriod = newSelectedPeriod;
  }
};

const actions = {
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
