import Users from '../../api/Users';
import {
  Role,
} from '@/helpers/role';

const guestUser = {
  role: Role.Guest,
};

const state = {
  user: guestUser,
};

const getters = {
  getUser: (state) => state.user,
  isUserConnected: (state) => state.user.role !== Role.Guest,
  getUserGroups: state => state.user.groups,
};

const mutations = {
  setUser: (state, payload) => {
    state.user = payload;
  },
  resetUser: (state) => {
    state.user = guestUser;
  },
};

const actions = {
  login: ({
      commit
    }, payload) => Users.login(payload)
    .then((result) => {
      commit('setUser', result.data);
    }),
  me: ({
    commit
    }, payload) => Users.me(payload)
    .then((result) => {
      if (result.data) {
        commit('setUser', result.data);
      }
    }),
  logout: ({
      commit
    }) => Users.logout()
    .then(() => {
      commit('resetUser');
    }),
  updateUser: ({
      commit
    }, payload) => Users.put(payload)
    .then(() => {
      commit('setUser', payload);
    }),
};


export default {
  state,
  actions,
  mutations,
  getters,
};
