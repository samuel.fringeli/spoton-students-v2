const DELAY_DURATION = 1000; // in [ms]. How many seconds the user needs to click/touch a button before the infobulle is diplayed 

const state = {
  infobulleTimeout: false,
};

const getters = {
  getInfobulleTimeout: (state) => state.infobulleTimeout
}

const mutations = {
  setInfobulleTimeout: (state, timeout) => {
    state.infobulleTimeout = timeout
  },
  clearInfobulleTimeout: (state) => {
    clearTimeout(state.infobulleTimeout)
    state.infobulleTimeout = false;
  }
};

const actions = {
  triggerInfobulleTimeout: ({ commit, getters, rootGetters }, payload) => {
    // check if valid a payload is sent
    if (payload === null || payload === "") {
      console.warn("No description for this popup")
      return;
    }
    if (rootGetters.getUser.activatePopup && !getters.getInfobulleTimeout) {
      const timeout = setTimeout(() => {
        commit("setAlert", {
          help: true,
          value: payload
        });
      }, DELAY_DURATION);
      commit("setInfobulleTimeout", timeout)
    }
  },
  cancleInfobulleTimeout: ({ commit }) => {
    commit("clearInfobulleTimeout")
  }
}

export default {
  state,
  getters,
  mutations,
  actions
};
