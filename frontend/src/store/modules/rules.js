import Rules from "@/api/Rules";

const state = {
  rules: [],
};

const getters = {
  getRules: state => state.rules
};

const mutations = {
  setRules: (state, payload) => {
    state.rules = payload;
  },
  deleteRule: (state, payload) => {
    let index = state.rules.findIndex(r => r.id === payload.id);
    if (index !== -1) {
      state.rules.splice(index, 1);
    }
  }
};

const reloadDynamicData = (commit) => {
  return new Promise((resolve, reject) => {
    Rules.getAll().then(res => {
      commit('setRules', res.data);
      resolve();
    }).catch(reject);
  });
}

const actions = {
  reloadData: {
    // global action
    root: true,
    handler: ({ commit }) => {
      return reloadDynamicData(commit);
    }
  },
  deleteRule({ commit }, payload) {
    return new Promise((resolve, reject) => {
      Rules.delete(
        payload.id
      ).then(() => {
        commit('deleteRule', {
          id: payload.id
        });
        resolve();
      }).catch(reject);
    })
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
