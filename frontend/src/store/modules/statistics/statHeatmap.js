import dayjs from 'dayjs';
import { DATE_SEASONS, DAYS, DAYS_ACRONYMS, ENVIRONMENT_INDICES_ALL } from '@/helpers/constants';
import { addDay } from '@/helpers/date';

const colorUndefined = '#ffffff';

const state = {
  parameters: [],
  dates: [], // only way to be able to have date values within option formatters
  grouping: undefined,
  sensorValues: [],
  envValues: {
    [ENVIRONMENT_INDICES_ALL.TEMPERATURE.id]: false,
    [ENVIRONMENT_INDICES_ALL.SOUND.id]: false,
    [ENVIRONMENT_INDICES_ALL.LUMINOSITY.id]: false,
    [ENVIRONMENT_INDICES_ALL.ODOR.id]: false,
  },
  options: {
    grid: {
      borderColor: '#ececec',
      position: 'front',
      xaxis: {
        lines: {
          show: true,
        },
      },
      yaxis: {
        lines: {
          show: true,
        },
      },
    },
    legend: {
      position: 'bottom',
      showForSingleSeries: true,
    },
    chart: {
      type: 'heatmap',
      toolbar: {
        show: false,
      },
      // when setting height to "auto", any commit with setChartHeight cannot change variable type
      // from string to integer, therefore had to set default height to integer
      height: 90,
      width: '100%',
      animations: {
        enabled: false,
      },
    },
    colors: [colorUndefined],
    plotOptions: {
      heatmap: {
        enableShades: false,
        colorScale: {
          ranges: [],
        },
      },
    },
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      type: 'category',
      categories: [],
      tooltip: {
        enabled: false,
      },
    },
    tooltip: {
      onDatasetHover: {
        highlightDataSeries: true,
      },
      x: {
        show: true,
        formatter: (y, {series, seriesIndex, dataPointIndex}) => {
          let begin, end;

          switch (state.grouping) {
            case 4:
            case 0:
              return (
                  DAYS[DAYS_ACRONYMS.indexOf(y)].toUpperCase() +
                  ' - ' +
                  addDay(state.dates[0], dataPointIndex)
              );
            case 1:
              return (
                  DAYS[DAYS_ACRONYMS.indexOf(y)].toUpperCase() +
                  ' - ' +
                  addDay(
                      state.dates[0],
                      (series.length - seriesIndex - 1) * DAYS.length +
                      dataPointIndex,
                  )
              );
            case 2:
              begin = state.dates[0].slice(0, 4);
              end = state.dates[1].slice(0, 4);
              if (begin !== end)
                begin = dayjs(state.dates[0]).add(dataPointIndex, 'month').year();
              return y.toUpperCase() + ' ' + begin;
            case 3:
              begin = DATE_SEASONS[dataPointIndex].data[0].slice(0, 4);
              end = DATE_SEASONS[dataPointIndex].data[1].slice(0, 4);
              return (
                  y.toUpperCase() + ' ' + begin + (begin === end ? '' : '-' + end)
              );
          }
        },
      },
      y: {
        formatter: function (y, {seriesIndex, dataPointIndex}) {
          let label = 'Pas de vote';

          state.parameters.forEach((e) => {
            if (e.value === y) {
              label = e.label;
              if (state.sensorValues.length) {
                if (state.envValues[ENVIRONMENT_INDICES_ALL.TEMPERATURE.id]) {
                  label += ': ' + (state.sensorValues[seriesIndex][dataPointIndex] ?
                      state.sensorValues[seriesIndex][dataPointIndex] + ' °C'
                      : 'valeur inconnue');
                } else if (state.envValues[ENVIRONMENT_INDICES_ALL.SOUND.id]) {
                  label += ': ' + (state.sensorValues[seriesIndex][dataPointIndex] ?
                      state.sensorValues[seriesIndex][dataPointIndex] // + ""
                      : 'valeur inconnue');
                } else if (state.envValues[ENVIRONMENT_INDICES_ALL.LUMINOSITY.id]) {
                  label += ': ' + (state.sensorValues[seriesIndex][dataPointIndex] ?
                      state.sensorValues[seriesIndex][dataPointIndex] + ' lx'
                      : 'valeur inconnue');
                } else if (state.envValues[ENVIRONMENT_INDICES_ALL.ODOR.id]) {
                  label += ': ' + (state.sensorValues[seriesIndex][dataPointIndex] ?
                      state.sensorValues[seriesIndex][dataPointIndex] + ' ppm'
                      : 'valeur inconnue');
                }
              }
            }
          });
          return label;
        },
        title: {
          formatter: (y) => (y !== '' ? y.toUpperCase() + ' -' : y),
        },
      },
    },
  },
};

const getters = {
  getHeatmapOptions: (state) => state.options,
};

const mutations = {
  setParameters: (state, payload) => {
    state.parameters = [... payload];
  },
  setDates: (state, payload) => {
    state.dates = [... payload];
  },
  setGrouping: (state, payload) => {
    state.grouping = payload;
  },
  setEnvValue: (state, payload) => {
    if (payload.value) {
      // only one value can be true, so we disable the other ones
      for (const key in state.envValues) {
        state.envValues[key] = false;
      }
    }
    state.envValues[payload.key] = payload.value;
  },
  setSensorValues: (state, payload) => {
    state.sensorValues = [... payload];
  },
  setChartHeight: (state, payload) => {
    state.options.chart.height = payload;
  },
  setCategories: (state, payload) => {
    state.options.xaxis.categories = payload;
  },
  setColorRanges: (state, payload) => {
    state.options.plotOptions.heatmap.colorScale.ranges = payload;
  },
};

const actions = {
  setParameters: ({commit}, payload) => {
    commit('setParameters', payload);
  },
  setGrouping: ({commit}, payload) => {
    commit('setGrouping', payload);
  },
  setEnvValue: ({commit}, payload) => {
    commit('setEnvValue', payload);
  },
  setSensorValues: ({commit}, payload) => {
    commit('setSensorValues', payload);
  },
  setChartHeight: ({commit}, payload) => {
    commit('setChartHeight', payload);
  },
  setCategories: ({commit}, payload) => {
    commit('setCategories', payload);
  },
  setColorRanges: ({commit}, payload) => {
    commit('setColorRanges', payload);
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
