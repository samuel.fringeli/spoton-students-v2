import Statistics from '../../../api/Statistics';
import { NO_ROOM_INDICE, TIME_RANGES } from '@/helpers/constants';
import Enterprises from '../../../api/Enterprises';

const state = {
  isLoading: true,
  room: undefined,
  roomId: undefined,
  theme: undefined,
  factor: undefined,
  dates: [],
  range: undefined,
  enterprise: undefined,
  enterpriseRooms: undefined,
  group: undefined,
  individual: true,
};

const getters = {
  isLoading: (state) => state.isLoading,
  room: (state, getters, rootState, rootGetters) => state.room || rootGetters.getRooms[0],
  roomId: (state, getters) => getters.room.id,
  theme: (state) => state.theme,
  factor: (state) => state.factor,
  // theme: (state, getters, rootState, rootGetters) => state.theme || rootGetters['theme/getThemes'][0],
  // factor: (state, getters) => state.factor ||  getters.theme.factors[0],
  dates: (state) => state.dates,
  range: (state) => state.range,
  enterprise: (state) => state.enterprise,
  enterpriseRooms: (state) => state.enterpriseRooms,
  getRoomsForCurrentEnterprise: (state, getters) => {
    const rooms = getters.enterpriseRooms || [];
    if (rooms.length === 0) {
      rooms.unshift({
        id: NO_ROOM_INDICE,
        name: 'Quelconque',
      });
    }
    return rooms;
  },
  group: (state) => state.group,
  individual: (state) => state.individual,
  getGrouping: (state) => state.range.grouping,
};

const mutations = {
  setLoading: (state, payload) => {
    state.isLoading = payload;
  },
  setRoom: (state, payload) => {
    state.room = payload;
  },
  setTheme: (state, payload) => {
    state.theme = payload;
  },
  setFactor: (state, payload) => {
    state.factor = payload;
  },
  setDates: (state, payload) => {
    state.dates = payload;
  },
  setRange: (state, payload) => {
    state.range = payload;
  },
  setEnterprise: (state, payload) => {
    state.enterprise = payload;
  },
  setEnterpriseRooms: (state, payload) => {
    state.enterpriseRooms = payload;
  },
  setGroup: (state, payload) => {
    state.group = payload;
  },
  setIndividual: (state, payload) => {
    state.individual = payload;
  },
};

const actions = {
  setRoom: ({commit}, payload) => {
    commit('setRoom', payload);
  },
  setTheme: ({commit}, payload) => {
    commit('setTheme', payload);
    commit('setFactor', payload.factors[0]);
  },
  setFactor: ({commit}, payload) => {
    commit('setFactor', payload);
  },
  initStats: ({dispatch, commit, rootGetters}, payload) => {
    commit('setLoading', true);
    commit('setRange', TIME_RANGES[0]);
    commit('statheatmap/setGrouping', TIME_RANGES[0].grouping, {root: true});
    Statistics.getLastVote() // default is a vote not linked to a group
        .then((res) => {
          const vote = res.data || {};

          const themes = rootGetters['theme/getThemes'];
          const theme = themes.find(
              (theme) => theme.id === vote.themeId,
          ) || themes[0];
          commit('setTheme', theme);

          const factor = theme.factors.find(
              (factor) => factor.id === vote.factorId,
          ) || theme.factors[0];
          commit('setFactor', factor);

          let room = vote.roomId || rootGetters.getCurrentRoom;
          if (typeof room === 'number') {
            room = rootGetters.getRoomById(room);
          }
          commit('setRoom', room);
          if (payload.isAdminView) {
            dispatch('setEnterprise', vote.user.enterprise);
          }
        })
        .then(() => commit('setLoading', false))
        .catch((error) => console.error(error));
  },
  setDates: ({commit}, payload) => {
    commit('setDates', payload);
    commit('statheatmap/setDates', payload, {root: true});
  },
  setRange: ({commit}, payload) => {
    commit('setRange', payload);
    commit('statheatmap/setGrouping', payload.grouping, {root: true});
  },
  setEnterprise: ({commit, getters}, payload) => {
    commit('setEnterprise', payload);

    Enterprises.getRooms(payload.id).then((res) => {
      let rooms = res.data;
      rooms.unshift({
        id: NO_ROOM_INDICE,
        name: 'Quelconque',
      });
      commit('setEnterpriseRooms', rooms);

      if (rooms.filter((room) => room.id === getters.roomId).length === 0) {
        commit('setRoom', rooms[0]);
      }
    });
  },
  setGroup: ({commit}, payload) => {
    commit('setGroup', payload);
  },
  setIndividual: ({commit}, payload) => {
    commit('setIndividual', payload);
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
