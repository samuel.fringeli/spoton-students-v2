import Factors from '/frontend/src/api/Factors';
import Statistics from '/frontend/src/api/Statistics';


const state = {
  chartThemeLabels: [],
  satisfactionStats: [],
  totalPossibleVotes: null,
  nbVotes: [],
  positiveVotes: [],
  negativeVotes: [],
};

const getters = {
  getChartThemeLabels: (state) => state.chartThemeLabels,
  getSatisfactionStats: (state) => state.satisfactionStats,
  getTotalPossibleVotes: (state) => state.totalPossibleVotes,
  getNumVotes: (state) => state.nbVotes,
  getPositiveVotes: (state) => state.positiveVotes,
  getNegativeVotes: (state) => state.negativeVotes,
};

const mutations = {
  setChartThemeLabels: (state, payload) => {
    state.chartThemeLabels = payload;
  },
  setSatisfactionStats: (state, payload) => {
    state.satisfactionStats = payload;
  },
  setTotalPossibleVotes: (state, payload) => {
    state.totalPossibleVotes = payload;
  },
  setNbVotes: (state, payload) => {
    state.nbVotes = payload;
  },
  setPositiveVotes: (state, payload) => {
    state.positiveVotes = payload;
  },
  setNegativeVotes: (state, payload) => {
    state.negativeVotes = payload;
  },
};

const actions = {
  getAllFactors: ({commit}, payload) => {
    return Factors.getByThemeId(payload)
        .then((result) => {
          const factors = {};
          result.data.forEach(obj => factors[obj.name] = obj.id);
          commit('setChartThemeLabels', factors);
        })
        .catch((error) => console.error(error));
  },
  getSatisfaction: ({commit}, payload) => {
    if (payload.isGroup) {
      return Statistics.getThemeFactorSatisfaction({
            'themeId': payload.themeId,
            'groupId': payload.groupId,
            'shouldReturnThemeLevelStatistics': true,
            'shouldReturnFactorLevelStatistics': true,
          },
          {
            'dateFrom': payload.dateFrom,
            'dateTo': payload.dateTo,
          },
      )
          .then((result) => {
            let stats = result.data;
            commit('setSatisfactionStats', stats);
          });
    } else if (!payload.isGroup)
      return Statistics.getThemeFactorSatisfaction({
            'themeId': payload.themeId,
            'shouldReturnThemeLevelStatistics': true,
            'shouldReturnFactorLevelStatistics': true,
          },
          {
            'dateFrom': payload.dateFrom,
            'dateTo': payload.dateTo,
          },
      )
          .then((result) => {
            let stats = result.data;
            commit('setSatisfactionStats', stats);
          });
  },
};

export default {
  state, mutations, getters, actions,
};