const state = {
  alert: {
    type: null,
    help: null,
    value: null,
  },
  rootState: null,
};

const getters = {
  getAlert: (state) => state.alert,
};

const mutations = {
  setAlert: (state, payload) => {
    state.alert = payload;
    $snackbar.add({
      type: payload.type,
      background: (payload.type === 'success' ? '#4749A0' : payload.background),
      text: payload.value,
    });
  },
  resetAlert: (state) => {
    state.alert = {
      type: null,
      help: null,
      value: null,
    };
  },
};

const actions = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
