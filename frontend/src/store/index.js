import VuexPersistence from 'vuex-persist'
import alert from "./modules/alert";
import user from "./modules/user";
import room from "./modules/room";
import theme from "./modules/theme";
import group from "./modules/group";
import topNav from "./modules/topNav";
import oneshotGrid from "./modules/oneshotgrid";
import infobulle from "./modules/infobulle";
import period from "./modules/period";
import sso from "./modules/sso"
import statChart from '@/store/modules/statistics/statChart';
import statparameter from "./modules/statistics/statparameter"
import statheatmap from "./modules/statistics/statHeatmap"
import rules from "./modules/rules";
import alarms from "./modules/alarms";
import { createStore } from "vuex";

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  reducer: (state) => ({ user: state.user, data: state.theme.data, room: state.room, alarms: state.alarms }), //only save navigation module
})

const store = createStore({
  strict: process.env.NODE_ENV === "development",
  modules: {
    alert,
    user,
    room,
    theme,
    group,
    infobulle,
    topNav,
    oneshotGrid,
    period,
    sso,
    statChart,
    statparameter,
    statheatmap,
    rules,
    alarms
  },
  plugins: [vuexLocal.plugin]
});

export function resetState() {
  store.commit("resetAlert");
  store.commit("resetRoom");
  store.commit("theme/resetThemes");
  store.commit("group/resetGroups");
}

export default store;
