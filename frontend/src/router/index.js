import { createRouter, createWebHistory } from 'vue-router';
import store from '@/store/index';
import { Role } from '@/helpers/role';
import Guided from '@/views/Guided';
import Login from '@/views/Login.vue';
import SignUp from '@/views/SignUp';
import Profil from '@/views/Profil';
import VoteOneShot from '@/views/VoteOneShot.vue';
import Vote from '@/views/Vote.vue';
import Theme from '@/views/Theme.vue';
import End from '@/views/End.vue';
import About from '@/views/About.vue';
import Factor from '@/views/Factor.vue';
import Groups from '@/views/Groups.vue';
import Rules from '@/views/Rules.vue';
import AddRule from '@/views/AddRule.vue';
import Statistics from '@/views/Statistics';
import Settings from '@/views/Settings';
import Enterprise from '@/views/Enterprise';
import EnterpriseThemes from '@/views/EnterpriseThemes';
import AlarmManager from '@/views/AlarmManager.vue';
import AlarmDetails from '@/views/AlarmDetails';
import AccountActivation from '@/views/AccountActivation';
import Alarms from '@/views/Alarms.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/guided",
      name: "guided",
      component: Guided,
      meta: { requiresRole: Role.User },
    },
    {
      /* TODO: for now home points to the guidedview,
      we can decide later whether or not to have another home/landing page
      (perhaps not requiring sign up)
      */
      path: "/home",
      name: "home",
      redirect: () => {
        return { path: '/oneshot' }
      },
      meta: { requiresRole: Role.User },
    },
    {
      path: "/signup",
      name: "signup",
      component: SignUp,
      props: true
    },
    {
      path: "/login",
      name: "login",
      component: Login,
    },
    {
      path: '/users/:userEmail/activate/:activationToken',
      name: 'accountActivation',
      component: AccountActivation,
    },
    {
      path: "/settings",
      name: "settings",
      component: Settings,
      meta: { requiresRole: Role.User },
    },
    {
      path: "/profil",
      name: "profil",
      component: Profil,
      meta: { requiresRole: Role.User },
    },
    {
      path: "/oneshot",
      name: "oneshot",
      component: VoteOneShot,
      meta: { requiresRole: Role.User },
    },
    {
      path: '/theme',
      name: 'theme',
      component: Theme,
      meta: { requiresRole: Role.User },
    },
    {
      path: '/theme/:themeId/vote',
      name: 'vote',
      component: Vote,
      meta: { requiresRole: Role.User },
    },
    {
      path: '/theme/:themeId/vote/:responseOptionId',
      name: 'facteurinfluence',
      component: Factor,
      meta: { requiresRole: Role.User },
    },
    {
      path: '/theme/:themeId/vote/:responseOptionId/end',
      name: 'end',
      component: End,
      meta: { requiresRole: Role.User },
    },
    {
      path: "/about",
      name: "about",
      component: About,
      meta: { requiresRole: Role.User },
    },
    {
      path: '/stat/',
      name: 'statistics',
      component: Statistics,
      meta: {
        title: 'Statistiques',
        requiresRole: Role.User,
      },
    },
    {
      path: '/adminstatistics/',
      name: 'adminstatistics',
      component: Statistics,
      meta: {
        title: 'Admin Statistiques',
        requiresRole: Role.Admin,
      },
      props: {
        isAdminView: true
      }
    },
    {
      path: "/groups",
      name: "groups",
      component: Groups,
      meta: {
        title: "Groups",
        requiresRole: Role.User,
      },
    },
    {
      path: "/rules",
      name: "rules",
      component: Rules,
      meta: {
        title: "Règles",
        requiresRole: Role.User,
      },
    },
    {
      path: "/add_rule",
      name: "add_rule",
      component: AddRule,
      meta: {
        title: "Nouvelle règle",
        requiresRole: Role.User,
      },
    },
    {
      path: "/alarms_manager",
      name: "alarms_manager",
      component: AlarmManager,
      meta: {
        title: "Alarmes",
        requiresRole: Role.User,
      },
    },
    {
      path: "/alarm_details",
      name: "alarm_details",
      component: AlarmDetails,
      meta: {
        title: "Détails de l'alarme",
        requiresRole: Role.User,
      },
    },
    {
      path: "/alarms",
      name: "alarms",
      component: Alarms,
      meta: {
        title: "Mes alarmes",
        requiresRole: Role.User,
      },
    },
    {
      path: "/enterprise",
      name: "enterprise",
      component: Enterprise,
      meta: { requiresRole: Role.Admin }
    },
    {
      path: "/enterprise/themes",
      name: "enterpriseThemes",
      component: EnterpriseThemes,
      meta: { requiresRole: Role.Admin }
    },
   // otherwise redirect to Guided
    {
      path: "/:pathMatch(.*)*",
      redirect: { name: "home" },
      meta: { requiresRole: Role.User },
    },
  ],

  scrollBehavior () {
    return {left: 0, top: 0};
  },
});


/**
 * Called before each access to the links from the router.js file and test the access rules
 * @name beforeEach
 * @function
 * @param {Router}    to - link to
 * @param {Router}    from - link from
 * @param {Function} next - called to resolve the hook
 */
router.beforeEach((to, from, next) => {
  //Test if the user is connected
  const { isUserConnected, getUser } = store.getters;
  //redirect to login
  if (
    to.matched.some((record) => record.meta.requiresRole) &&
    !isUserConnected
  ) {
    // TODO: here
    next({
      name: 'login',
      query: { redirect: to.fullPath },

    });
    //redirect to homepage if not authorized
  } else if (
    to.matched.some((record) => record.meta.requiresRole > getUser.role)
  ) {
    next({
      name: "home",
    });
  } else if (
    to.matched.some((record) => !record.meta.requiresRole) &&
    isUserConnected
  ) {
    next({
      name: "home",
    });
  }
  next();
});



export default router;