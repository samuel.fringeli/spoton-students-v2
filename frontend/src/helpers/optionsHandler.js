import Options from "@/api/Options";

export function getOptions() {
  return Options.get().catch(error => {
        console.error(error)
    });
}
