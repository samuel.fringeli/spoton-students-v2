import {MOBILE_WIDTH} from "@/helpers/constants";

export function isMobile() {
  return window.innerWidth < MOBILE_WIDTH;
}
