const FEELING_ICONS = [
  "mdi-emoticon-frown-outline", // stressed
  "mdi-emoticon-confused-outline", // annoyed
  "mdi-emoticon-happy-outline", // satisfied
  "mdi-emoticon-excited-outline", // happy
];

export default FEELING_ICONS;
