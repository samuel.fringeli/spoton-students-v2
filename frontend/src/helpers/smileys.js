import { THEME_INDICES } from '@/helpers/constants'

/**
 * Return true for the themes where the tooltip should be displayed.
 * 
 * This includes currently the factor for "smell".
 * 
 * @param {int} themeId 
 */
export function showTooltipForTheme(themeId) {
  return themeId === THEME_INDICES.ENVIRONMENT
}
