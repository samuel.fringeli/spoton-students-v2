export const Role = {
  Root: 9,
  Admin: 8,
  Device: 2,
  User: 1,
  Guest: 0,
};
