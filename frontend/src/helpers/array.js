export function getZeroFilledArray(size) {
  return Array.from(Array(size), () => 0)
}

export function getFilledArray(size, number) {
  return Array.from(Array(size), () => number)
}

// Checks if an array contains an object based on a given attribute
export function arrayContainsObjectWithAttribute(baseArray, object, attribute) {
  const filteredArray = baseArray.filter(
    (currentElement) =>
      currentElement[attribute] === object[attribute]
  );
  return filteredArray.length >= 1
}