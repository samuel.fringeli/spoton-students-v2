/**
 * This helper handles the notification subscriptions and unsubscriptions
 */

import Subscription from "@/api/Subscription";
import Subscriptions from "../api/Subscriptions";

// Internal helper function to convert a BASE64 encoded string to UINT8
function urlBase64ToUint8Array(base64String) {
  const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding).replace(/-/g, "+").replace(/_/g, "/");

  const rawData = atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }

  return outputArray;
}

function askBrowserPermission() {
  return new Promise(function (resolve, reject) {
    if ("Notification" in window) {
      const permissionResult = Notification.requestPermission(function (result) {
        resolve(result);
      });

      if (permissionResult) {
        console.log(permissionResult);
        permissionResult.then(resolve, reject);
      }
    } else {
      throw new Error("This browser does not support push notifications");
    }
  });
}

// Create a subscription and store it on the server
function createSubscription(registration) {
  Subscriptions.getEnv().then(res => {

    const applicationServerKey = urlBase64ToUint8Array(res.data);
    const options = {
      applicationServerKey,
      userVisibleOnly: true,
    };
    registration.pushManager.subscribe(options).then((newSubscription) => {
      Subscription.post(newSubscription).catch(error => {
        unsubscribeUser()
        console.error(error)
      });
    });
  });

}

export function subscribeUser() {
  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.ready
      .then(function (registration) {
        askBrowserPermission()
          .then((result) => {
            if (result === "granted") {
              // Check if user has already subscription or not
              registration.pushManager.getSubscription().then((sub) => {
                // TODO: HANDLE ON SAFARI
                if (sub === null) {
                  createSubscription(registration);
                }
              });
            } else {
              // Notify user that his preference setting does not correspond to this decision
              // Option to change preference
            }
          })
          .catch((error) => {
            console.error(error);
          });
      })
      .catch(function (err) {
        console.error("Service Worker registration failed: ", err);
      });
  }
}

export function unsubscribeUser() {
  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.ready
      .then(function (registration) {
        registration.pushManager.getSubscription().then((sub) => {
          if (sub !== null) {
            sub.unsubscribe().then((result) => {
              if (!result) {
                console.error("Encountered a problem unsubscribing the user");
              }
            });
          } else {
            // Nothing to do no subscription was found
          }
        });
      })
      .catch(function (err) {
        console.error("Service Worker registration failed: ", err);
      });
  }
}
