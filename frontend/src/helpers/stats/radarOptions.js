export const radarOptions = {
  chart: {
    height: 350,
    type: 'radar',
    toolbar: {
      show: false
    },
    animations: {
      enabled: false
    }
  },
  title: {
    text: '',
    align: 'center',
  },
  xaxis: {
    categories: []
  },
  yaxis: {
    tickAmount: 5,
    labels: {
      formatter: function (val, i) {
        if (i === 0 || i === 5) {
          return Math.round(val)
        } else {
          return ''
        }
      }
    }
  },
  colors: ["#FF7756"]
}