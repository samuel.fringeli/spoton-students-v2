import {MOBILE_WIDTH} from "@/helpers/constants";

export const pieOptions = {
  legend: {
    position: 'right',
  },
  responsive: [{
    breakpoint: MOBILE_WIDTH,
    options: {
      legend: {
        position: 'bottom',
        show: true,
        horizontalAlign: 'center',
      },
    },

  }],
  theme: {
    monochrome: {
      enabled: true,
      color: '#255aee',
      shadeTo: 'light',
      shadeIntensity: 0.8
    }
  }
}