import { PERIODS } from "@/helpers/constants"

// array of the currently available periods
export const getAvailablePeriods = () => {
  const availablePeriods = [PERIODS.MORNING]
  // if it's morning, we won't show the afternoon
  let currentHour = new Date().getHours();
  if (currentHour >= 12) {
    availablePeriods.push(PERIODS.AFTERNOON);
  }
  return availablePeriods
}

// returns the current period based on the current time
export const defaultPeriod = () => {
  let currentHour = new Date().getHours();
  if (currentHour < 12) {
    return PERIODS.MORNING
  } else {
    return PERIODS.AFTERNOON
  }
}