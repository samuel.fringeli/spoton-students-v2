import {
  getBeginEnd,
  getBeginEndForSixMonths,
  getSeasons,
  getToday,
} from "@/helpers/date";

// obsolete variables! They are replaced in the PERIODS object (Uchendu, 03.10.2020)
export const MORNING = "matin";
export const AFTERNOON = "après-midi";

// Sunday -> Saturday : index 0 -> 6
export const DAYS = [
  "dimanche",
  "lundi",
  "mardi",
  "mercredi",
  "jeudi",
  "vendredi",
  "samedi",
];
export const DAYS_ACRONYMS = ["DI", "LU", "MA", "ME", "JE", "VE", "SA"];

export const MOBILE_WIDTH = 960;

const today = getToday();

export const SEASONS = ["hiver", "printemps", "été", "automne"];

export const DATE_SEASONS = getSeasons(); //getPastSeasons()

// groupings = ["halfday", "day", "month", "season"]
// grouping entry corresponds to index of the list groupings
export const TIME_RANGES = [
  {
    text: "Les 7 derniers jours",
    grouping: 0,
    dates: getBeginEnd(today, 7),
  },
  {
    text: "Les 10 derniers jours",
    grouping: 0,
    dates: getBeginEnd(today, 10),
  },
  {
    text: "Les 6 derniers mois",
    grouping: 2,
    dates: getBeginEndForSixMonths(),
  },
  {
    text: "La dernière année",
    grouping: 3,
    dates: [
      DATE_SEASONS[0].data[0],
      DATE_SEASONS[DATE_SEASONS.length - 1].data[1],
    ],
  },
  {
    text: "Choisir un intervalle",
    grouping: 4,
  },
];

// map the themes to a fixed index
export const THEME_INDICES = {
  WELLBEING: 1,
  WORK: 2,
  SOCIAL: 3,
  ENVIRONMENT: 7,
  TELEWORK: 5,
};

// TODO: replace all occurrences of ENVIRONMENT_INDICES with ENVIRONMENT_INDICES_ALL
export const ENVIRONMENT_INDICES = {
  TEMPERATURE: 10,
  ODOR: 11,
  SOUND: 12,
  LUMINOSITY: 13,
};

export const ENVIRONMENT_INDICES_ALL = {
  TEMPERATURE: {
    id: 10,
    sensorType: "temperature",
  },
  ODOR:  {
    id: 11,
    sensorType: "co2",
  },
  SOUND:  {
    id: 12,
    sensorType: "noise",
  },
  LUMINOSITY:  {
    id: 13,
    sensorType: "luminosity",
  },
};

export const SPECIAL_GROUP_INDICES = {
  INDIVIDUAL: 0,
  ALL_GROUPS: -1
}

export const NO_ROOM_INDICE = -1

// enum for the available periods 
export const PERIODS = {
  MORNING: {
    KEY: "morning",
    DISPLAY_TEXT: "matin",
    DISPLAY_FULL_TEXT: "ce matin",
  },
  AFTERNOON: {
    KEY: "afternoon",
    DISPLAY_TEXT: "après-midi",
    DISPLAY_FULL_TEXT: "cet après-midi",
  },
  DAY: {
    KEY: "day",
    DISPLAY_TEXT: "journée",
    DISPLAY_FULL_TEXT: "cette journée",
  },
};

// magic number for indices
export const ID_NOT_SET = -1;
