import store from '../store/index'

export function handleError(error) {
  console.error(error);
  store.commit('setAlert', {
    type: 'error',
    value: error.message,
  });
}
