const ONE_SHOT_GRID_ICONS = {
  // This object uses the name of the themes as keys
  Travail: [
    "mdi-emoticon-frown-outline", // stressed
    "mdi-emoticon-confused-outline", // annoyed
    "mdi-emoticon-happy-outline", // satisfied
    "mdi-emoticon-excited-outline", // happy
  ],
  Social: [
    "mdi-emoticon-frown-outline", // stressed
    "mdi-emoticon-confused-outline", // annoyed
    "mdi-emoticon-happy-outline", // satisfied
    "mdi-emoticon-excited-outline", // happy
  ],
  "Sensation": {
    "Température": [
      // temperature
      "mdi-snowflake", // cold
      "mdi-thermometer-low", // a bit cold
      "mdi-thermometer", // perfect
      "mdi-thermometer-high", // a bit hot
      "mdi-white-balance-sunny", // hot
    ],
    "Air": [
      // air
      "mdi-biohazard", // unbreathable
      "mdi-spray-bottle", // okay
      "mdi-air-filter", // good
    ],
    "Bruit": [
      // sound
      "mdi-ear-hearing-off", // too quiet
      "mdi-volume-low", // a bit quiet
      "mdi-volume-medium", // perfect
      "mdi-volume-high", // a bit noisy
      "mdi-ear-hearing", // noisy
    ],
    "Lumière": [
      // lighting
      "mdi-brightness-3", // too dark
      "mdi-lightbulb-off-outline", // dark
      "mdi-lightbulb-outline", // perfect
      "mdi-lightbulb-on-outline", // bright
      "mdi-brightness-5", // too bright
    ],
  },
  Télétravail: [
    "mdi-emoticon-frown-outline", // stressed
    "mdi-emoticon-confused-outline", // annoyed
    "mdi-emoticon-happy-outline", // satisfied
    "mdi-emoticon-excited-outline", // happy
  ],
  Personnel: [
    "mdi-emoticon-frown-outline", // stressed
    "mdi-emoticon-confused-outline", // annoyed
    "mdi-emoticon-happy-outline", // satisfied
    "mdi-emoticon-excited-outline", // happy
  ],
  Environnement: [
    "mdi-emoticon-frown-outline", // stressed
    "mdi-emoticon-confused-outline", // annoyed
    "mdi-emoticon-happy-outline", // satisfied
    "mdi-emoticon-excited-outline", // happy
  ],
}

export default ONE_SHOT_GRID_ICONS;