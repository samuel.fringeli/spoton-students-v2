import dayjs from "dayjs";
import fr from "dayjs/locale/fr-ch";
import { DAYS_ACRONYMS } from "@/helpers/constants";

// all functions exclude saturdays' and sundays' dates
export function getToday() {
  return new Date().toISOString().slice(0, 10);
}

// Receives two dates and return an array of months between the first and the last date
export function getMonthsBetweenDates(date1, date2) {
  const startDate = dayjs(date1);
  const endDate = dayjs(date2);
  const months = [];
  let currentDate = startDate.startOf('month');
  while (currentDate.isBefore(endDate) || currentDate.isSame(endDate, 'month')) {
    const month = currentDate.format('MMMM YYYY').charAt(0).toUpperCase() + currentDate.format('MMMM YYYY').slice(1);
    months.push(month);
    currentDate = currentDate.add(1, 'month');
  }
  return months;
}
export function getDatesInBetween(date1, date2) {
  let date = new Date(date1),
    end = new Date(date2);
  // necessary to manage summer/winter time
  date.setHours(12,0,0,0)
  end.setHours(12,0,0,0)

  // make sure that "date" is in the past of "end"
  if (date > end) {
    let tmp = end;
    end = date;
    date = tmp;
  }

  let allDates = [getDateOnly(date1)];
  date.setDate(date.getDate() + 1);
  while (date.getTime() <= end.getTime()) {
    if (!isWeekend(date)) {
      allDates.push(getDateOnly(date));
    }
    date.setDate(date.getDate() + 1);
  }
  return allDates;
}

// Without weekend days
export function getDatesBefore(endDate, nbDates) {
  let dates = [getDateOnly(endDate)];
  let date = new Date(endDate);

  let i = 1;
  while (i < nbDates) {
    date.setDate(date.getDate() - 1);
    if (!isWeekend(date)) {
      dates.push(getDateOnly(date));
      i++;
    }
  }
  return dates.reverse();
}
export function getDatesBeforeWithWeekends(endDate, nbDates) {
  let dates = [getDateOnly(endDate)];
  let date = new Date(endDate);

  let i = 1;
  while (i < nbDates) {
    date.setDate(date.getDate() - 1);
    dates.push(getDateOnly(date));
    i++;
  }
  return dates.reverse();
}

export function getBeginEnd(endDate, nbDates) {
  let dates = getDatesBefore(endDate, nbDates);
  return [dates[0], dates[dates.length - 1]];
}

export function isWeekend(date) {
  return date.getDay() === 0 || date.getDay() === 6; // sunday or saturday
}

export function getDateOnly(date) {
  return new Date(date).toISOString().slice(0, 10);
}
export function getDateWithoutYear(date) {
  return date.substring(5, date.length);
}

export function getDayOfDates(dates) {
  let days = [];
  dates.forEach((date) => days.push(DAYS_ACRONYMS[new Date(date).getDay()]));
  return days.length === 1 ? days[0] : days;
}

// if month not given, it takes automatically the past month
// month from 0 to 11 (january to december)
export function getDatesOfMonth(month) {
  let begin = dayjs();
  let end = dayjs();

  if (month) {
    begin = begin.set("month", month).startOf("month").format("YYYY-MM-DD");
    end = end.set("month", month).endOf("month").format("YYYY-MM-DD");
  } else {
    begin = begin.subtract(1, "month").startOf("month").format("YYYY-MM-DD");
    end = end.subtract(1, "month").endOf("month").format("YYYY-MM-DD");
  }
  return getDatesInBetween(begin, end);
}

// dates are supposed to be in ascendant order
// month from 0 to 11 (january to december)
export function getMonthNames(monthIds) {
  if (monthIds.length <= 0) return [];
  const months = [];
  for (let i = 0; i < monthIds.length; i++) {
    months.push(dayjs().month(monthIds[i]).locale(fr).format("MMMM"));
  }
  return months;
}

export function getBeginEndForSixMonths() {
  let begin = dayjs()
    .subtract(5, "month")
    .startOf("month")
    .format("YYYY-MM-DD");
  let end = dayjs().endOf("month").format("YYYY-MM-DD");
  return [begin, end];
}

export function getMonthsInBetween(begin, end) {
  let start = dayjs(begin).month(),
    final = dayjs(end).month();
  const months = [];
  for (let i = start; i !== final; i = (i + 1) % 12) {
    months.push(i)
  }
  months.push(final)
  return months;
}

export function getBeginEndLastYear() {
  let begin = dayjs()
    .month(0)
    .subtract(1, "year")
    .startOf("month")
    .format("YYYY-MM-DD");
  let end = dayjs()
    .month(11)
    .subtract(1, "year")
    .endOf("month")
    .format("YYYY-MM-DD");
  return [begin, end];
}

export function addDayWithWeekends(date, nbDays) {
  let output = dayjs(date).add(nbDays, "day");
  if (
    (nbDays < 7 && dayjs(date).day() > output.day()) ||
    output.day() === 0 ||
    output.day() === 6
  )
    output = output.add(2, "day");
  return output.format("YYYY-MM-DD");
}

export function addDay(date, nbDays) {
  let i = 0;
  let output = dayjs(date);
  while (i < nbDays) {
    output = output.add(1, "day");
    if (output.day() !== 0 && output.day() !== 6) {
      i++;
    }
  }
  return output.format("YYYY-MM-DD");
}

export function getLastMonthDates() {
  let today = getToday();
  let begin = dayjs(today)
    .subtract(1, "month")
    .add(1, "day")
    .format("YYYY-MM-DD");
  return getDatesInBetween(begin, today);
}

// Based on code: https://stackoverflow.com/a/54501026
export function getSeasons() {
  const md = (month, day) => ({ month, day });
  const toMd = (date) => md(date.month(), date.date());
  const before = (md1, md2) =>
    md1.month < md2.month || (md1.month === md2.month && md1.day < md2.day);
  const equals = (md1, md2) => md1.month === md2.month && md1.day === md2.day;
  const after = (md1, md2) => !before(md1, md2);
  const between = (mdX, mdLow, mdHigh) =>
    (before(mdX, mdHigh) && after(mdX, mdLow)) ||
    equals(mdX, mdLow) ||
    equals(mdX, mdHigh);

  const today = dayjs();
  const seasons = [
    {
      key: 0,
      name: "hiver",
      ranges: [md(11, 21), md(11, 31), md(0, 1), md(2, 20)],
    },
    {
      key: 1,
      name: "printemps",
      ranges: [md(2, 21), md(5, 20)],
    },
    {
      key: 2,
      name: "été",
      ranges: [md(5, 21), md(8, 22)],
    },
    {
      key: 3,
      name: "automne",
      ranges: [md(8, 23), md(11, 20)],
    },
  ];
  const seasonsSize = seasons.length;

  function isBetween(date, ranges) {
    // ranges.length is even
    for (let i = 0; i <= ranges.length / 2; i += 2) {
      // return true only if he finds one positive
      if (between(date, ranges[i], ranges[i + 1])) {
        return true;
      }
    }
    return false;
  }
  function season2(date, seasons) {
    return seasons.find((season) => isBetween(toMd(date), season.ranges));
  }

  let result = Array(seasonsSize);
  let actual = season2(today, seasons);
  result[seasonsSize - 1] = actual;

  for (let i = 0; i < seasonsSize; i++) {
    let date = seasons[(1 + i + actual.key) % seasonsSize];
    let range = date.ranges;
    let start = dayjs(today).date(range[0].day).month(range[0].month);
    let end = dayjs(today)
      .date(range[range.length - 1].day)
      .month(range[range.length - 1].month);
    if (i !== seasonsSize - 1) {
      if (start.isAfter(today)) start = start.subtract(1, "year");
      if (end.isAfter(today)) end = end.subtract(1, "year");
    } else {
      if (start.isAfter(today)) start = start.subtract(1, "year");
      if (end.isBefore(today)) end = end.add(1, "year");
    }

    result[i] = {
      index: date.key,
      data: [start.format("YYYY-MM-DD"), end.format("YYYY-MM-DD")],
    };
  }
  return result;
}
