import { md } from 'vuetify/iconsets/md'
import { mdi, aliases } from 'vuetify/iconsets/mdi'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'

export const vuetify = createVuetify({
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: {
      md,
      mdi
    }
  },
  theme: {
    themes: {
      light: {
        colors: {
          primary: '#FF7756',
          secondary: '#4749A0',
          accent: '#DA502E',
          greyed: '#F2F2F2',
          error: '#b71c1c',
        }
      }
    }
  }
});