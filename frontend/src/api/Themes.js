/**
 * @file API call for Themes related query
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

import Repository from './repository';

const resource = '/themes';
export default {

  /**
   * Get all themes
   * @name getAll
   * @function
   * @return themes with all parameters and responses or error 500
   */
  getAll() {
    return Repository.get(`${resource}`);
  },

  /**
   * Get a theme by id
   * @name get
   * @function
   * @param {int} id of the theme to get
   */
  get(id) {
    return Repository.get(`${resource}/${id}`)
  },

  /**
   * Create a theme
   * @todo Images upload are currently not used
   * @name post
   * @function
   * @param {payload} data for the theme creation
   * @return theme created or error 500
   */
  post(payload) {
    return Repository.post(`${resource}`, payload, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },

  /**
   * Update a theme
   * @todo Images upload are currently not used
   * @name put
   * @function
   * @param {id} id of the theme to update
   * @param {payload} data for the theme update
   * @return error 500 if necessary
   */
  put(id, payload) {
    return Repository.put(`${resource}/${id}`, payload, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },

  /**
   * Delete a theme
   * @name delete
   * @todo Icon upload is currently not used
   * @function
   * @param {id} id of the theme to delete
   * @return status 200 or error 500
   */
  delete(id) {
    return Repository.delete(`${resource}/${id}`);
  },
};
