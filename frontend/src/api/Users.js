/**
 * @file API call for User related query
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

import Repository from './repository';

const resource = '/users';
export default {

  /**
   * Login function
   * @name login
   * @function
   * @param {Object} user data
   * @return user if ok, error 401 if bad login or error 500
   */
  login(user) {
    return Repository.post(`${resource}/login`, {
      user
    });
  },

  /**
   * Get the signup info from azure
   * @function
   * @name azureInfo
   */
  azureInfo() {
    return Repository.get(`${resource}/login/azure/info`);
  },

  /**
   * Get the logged user info
   * @name me
   * @function
   * @return user if ok, error 401 if not logged or error 500
   */
  me() {
    return Repository.get(`${resource}/me`);
  },


  /**
   * Logout the connected user
   * @name logout
   * @function
   * @return none
   */
  logout() {
    return Repository.post(`${resource}/logout`);
  },

  /**
   * Create a user
   * @name post
   * @function
   * @param {payload} data for the user creation
   * @return user created or error 500
   */
  post(payload) {
    return Repository.post(`${resource}`, payload);
  },

  /**
   * Update the connected user
   * @name put
   * @function
   * @param {payload} data for the user update
   * @return user updated or error 500
   */
  put(payload) {
    return Repository.put(`${resource}`, payload);
  },
  /**
   * Activate user account
   * @name activate
   * @function
   * @param {payload} data for the user activation
   * @return user activation or error 500
   */
  activate(payload) {
    return Repository.put(`${resource}/activate`, payload);
  },
};
