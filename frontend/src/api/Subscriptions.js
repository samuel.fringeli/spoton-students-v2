/**
 * @file API call for Subscriptions related query
 * @author Léonard Noth <leonard.noth@master.hes-so.ch>
 * @date 22.07.2020
 * @version 2.0
 */

import Repository from "./repository";

const resource = "/subscriptions/";
export default {
  /**
   * Get env var subscriptions
   * @name getEnv
   * @function
   * @return rooms or error 500
   */

  getEnv() {
    return Repository.get(`${resource}`);
  }
};
