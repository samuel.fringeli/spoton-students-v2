/**
 * @file API call for flashnews related query
 * @author Audrey Delabays
 * @date 27.11.2023
 * @version 1.0
 */

import Repository from "./repository";

const resource = "/flashnews";
export default {

  /**
   * Get flashnews for a group
   * @name flashNews
   * @function
   * @param {groupId} id of the group for which the members should be listed
   * @return status 200 or error 500
   */
  getFlashNews(groupId) {
    return Repository.get(`${resource}/${groupId}?formatTitles=true`);
  },
}