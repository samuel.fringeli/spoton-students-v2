/**
 * @file Axios instance for API call. This file is to be imported to create new call to the API.
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

import axios from "axios";
import store from "../store/index";

/**
 * @constant
 * @type {String}
 * @warning Hardcoded URL
 */
const baseURL = process.env.NODE_ENV === 'production' ? `https://${window.location.host}/api` : 'http://localhost:3000';
axios.defaults.withCredentials = true;

/**
 * @constant
 * @type {Axios}
 */
const httpClient = axios.create({
  baseURL,
  headers: {
    "Content-Type": "application/json",
  },
  withCredentials: true,
});

/**
 * Add a response interceptor that log the user out if the response from the server is a 401 error
 */
httpClient.interceptors.response.use(undefined, (error) => {
  if (!error.status) {
    // network error
    const urlEnd = error.request.responseURL.split("/").pop();
    if (error.request.responseURL && urlEnd !== "login" && urlEnd !== "rooms" && urlEnd !== "me" && urlEnd !== "info") {
      const payload = {
        type: "error",
        value:
          "Tes actions ne peuvent pas être transmises au serveur. Vérifie ta connexion internet et réessaye.",
      };
      store.commit("setAlert", payload);
    }
  }

  if (error.response.status === 401) {
    store.commit("resetUser");
  }
  return Promise.reject(error);
});

export default httpClient;
