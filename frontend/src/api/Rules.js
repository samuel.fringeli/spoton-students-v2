/**
 * @file API call for Rules related query
 * @author Samuel Fringeli <samuel.fringeli@hefr.ch>
 * @date 25.04.2022
 * @version 1.0
 */

import Repository from "./repository";

const resource = "/rules";
export default {
  /**
   * Get all rules
   * @name getAll
   * @function
   * @return rules or error 500
   */
  getAll() {
    return Repository.get(`${resource}`);
  },

  /**
   * Create a rule
   * @name post
   * @function
   * @param {payload} data for the rule creation
   * @return rule created or error 500
   */
  post(payload) {
    return Repository.post(`${resource}`, payload);
  },

  /**
   * Archive a rule
   * @name put
   * @function
   * @param {id} id of the rule to archive
   * @return status 200 or error 500
   */
  archive(id) {
    return Repository.put(`${resource}/${id}`);
  },

  /**
   * Delete an archived rule
   * @name delete
   * @function
   * @param {id} id of the rule to delete
   * @return status 200 or error 500
   */
  delete(id) {
    return Repository.delete(`${resource}/${id}`);
  }
};
