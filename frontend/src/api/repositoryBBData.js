/**
 * @file Axios instance for API call to BBData. This file is to be imported to create new call to the API.
 * @author Yael Iseli <Yael.Iseli@edu.hefr.ch>
 * @date 30.10.2020
 * @version 3.0
 */

import axios from "axios";
import store from "../store/index";

/**
 * @constant
 * @type {String}
 * @warning Hardcoded URL
 */
const baseURL = "https://bbdata-admin.smartlivinglab.ch/api/objects/";
axios.defaults.withCredentials = true; // TODO: useful ???

/**
 * @constant
 * @type {Axios}
 */
const httpClientBBData = axios.create({
  baseURL,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
    bbuser: "102",
    bbtoken: "7bedaa7c5478d884bd72bb33f848e361",
  },
  withCredentials: false,
});

/**
 * Add a response interceptor that log the user out if the response from the server is a 401 error
 */
httpClientBBData.interceptors.response.use(undefined, (error) => {
  if (!error.status) {
    // network error
    const payload = {
      type: "error",
      value:
        "L'accès aux données physiques n'est pas disponible. Vérifie ta connexion internet et réessaye.",
    };
    store.commit("setAlert", payload);
    console.error(error);
  }
  return Promise.reject(error);
});

export default httpClientBBData;
