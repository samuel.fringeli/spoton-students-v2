/**
 * @file API call for Factors related query
 * @author Yael Iseli <yael.iseli@edu.hefr.ch>
 * @date 03.08.2020
 * @version 2.1
 */

import Repository from './repository';

const parentResource = '/themes'
const resource = 'factors';

export default {
  /**
   * Get Factors by Theme ID
   * @name getByThemeId
   * @function
   * @return Factors or error 500
   */
  getByThemeId(themeId) {
    return Repository.get(`${parentResource}/${themeId}/${resource}`);
  },

};
