/**
 * @file API call for ResponseOptions related query
 * @author Uchendu Nwachukwu <uchendu.nwachukwu@edu.hefr.ch>
 * @date 02.10.2020
 * @version 2.1
 */

import Repository from './repository';

const grandParentResource = '/themes'
const parentResource = 'factors';
const resource = 'responseOptions';

export default {
  /**
   * Get possible responses to a factor by themeId and factorId
   */
  getByThemeIdAndFactorId(themeId, factorId) {
    return Repository.get(`${grandParentResource}/${themeId}/${parentResource}/${factorId}/${resource}`);
  },

  /**
  * Get possible responses to a theme by themeId
  */
  getByThemeId(themeId) {
    return Repository.get(`${grandParentResource}/${themeId}/${resource}`);
  },

};
