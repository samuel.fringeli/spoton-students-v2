/**
 * @file API call for Level related query
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

import Repository from './repository';

const resource = '/levels';
export default {
  /**
   * Get the current level of the logged-in user
   * @name getNiveau
   * @function
   * @return Level
   */
  get() {
    return Repository.get(`${resource}`);
  },
};
