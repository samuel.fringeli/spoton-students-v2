/**
 * @file API call for Alarms related query
 * @author Samuel Fringeli <samuel.fringeli@hefr.ch>
 * @date 02.05.2022
 * @version 1.0
 */

import Repository from './repository';

const resource = '/alarms';
export default {
  /**
   * Get all alarms for a rule
   * @name getAll
   * @function
   * @param {ruleId} ID of the rule
   * @return rules or error 500
   */
  getAll(ruleId) {
    return Repository.get(`${resource}/${ruleId}`);
  },
  /**
   * Get all unhandled alarms for a rule
   * @name getAllUnhandled
   * @function
   * @return alarms or error 500
   */
  getAllUnhandled() {
    return Repository.get(`${resource}/unhandled}`);
  },

  /**
   * Get all handled alarms for a rule
   * @name getAllHandled
   * @function
   * @return alarms or error 500
   */
  getAllHandled() {
    return Repository.get(`${resource}/handled}`);
  },

  /**
   * Get all handled alarms for a rule
   * @name getAllAlarmsFromCurrentUser
   * @function
   * @return alarms or error 500
   */
  getAllAlarmsForCurrentUser() {
    return Repository.get(`${resource}/all`);
  },
  /**
   * Create an alarm
   * @name post
   * @function
   * @param {payload} data for the alarm creation
   * @return rule created or error 500
   */
  post(payload) {
    return Repository.post(`${resource}`, payload);
  },

  /**
   * Update an alarm
   * @name put
   * @function
   * @param {payload} data for the alarm update
   * @return rule created or error 500
   */
  put(payload) {
    return Repository.put(`${resource}/${payload.id}`, payload);
  },
};
