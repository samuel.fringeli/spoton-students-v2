/**
 * @file API call for Rooms related query
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

import Repository from "./repository";
import { appendParamsToUrl } from "@/helpers/url";

const resource = "/rooms";
export default {
  /**
   * Get all rooms
   * @name getAll
   * @function
   * @return rooms or error 500
   */
  getAll() {
    return Repository.get(`${resource}`);
  },

  /**
   * Create a room
   * @name post
   * @function
   * @param {payload} data for the room creation
   * @return room created or error 500
   */
  post(payload) {
    return Repository.post(`${resource}`, payload);
  },

  /**
   * Update a room
   * @name put
   * @function
   * @param {payload} data with the id for the room update
   * @return status 200 or error 500
   */
  put(payload) {
    const { id, ...room } = payload;
    return Repository.put(`${resource}/${id}`, room);
  },

  /**
   * Delete a room
   * @name delete
   * @function
   * @param {id} id of the room to delete
   * @return status 200 or error 500
   */
  delete(id) {
    return Repository.delete(`${resource}/${id}`);
  },

  getSensor(roomId, sensorType) {
    return Repository.get(
      appendParamsToUrl(`${resource}/${roomId}/sensors`, sensorType)
    );
  },
};
