/**
 * @file API call for Subscription related query
 * @author Uchendu Nwachukwu <Uchendu.Nwachukwu@edu.hefr.ch>
 * @date 20.08.2020
 * @version 2.0
 */

import Repository from "./repository";

const resource = "/notifications/subscription";
export default {
  /**
   * Store the notification subscription of a user
   * @name post
   * @function
   * @param {payload} data of the subscription
   * @return confirmation or error 500
   */
  post(payload) {
    return Repository.post(`${resource}`, payload);
  },
};
