/**
 * @file API call for oneShot related query
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

import Repository from './repository';

const resource = '/oneshot';
export default {
  /**
   * Get all options to display on the OneShotGrids
   */
  get() {
    return Repository.get(`${resource}`);
  },
  /**
   * Get all options to display on the OneShotGrids for an enterprise
   */
   getByEnterprise(enterpriseId) {
    return Repository.get(`${resource}/${enterpriseId}`);
  },
  /**
   * Get all responses to display on the oneshot grid
   */
  getResponses() {
    return Repository.get(`${resource}/responses`);
  }
};
