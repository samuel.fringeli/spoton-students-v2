/**
 * @file API call for influencingfactors related query
 * @author Yael Iseli <yael.iseli@edu.hefr.ch>
 * @date 03.08.2020
 * @version 2.1
 */

import Repository from './repository';

const resource = '/influencingfactors';
export default {
  /**
   * Get all InfluencingFactor
   * @name getAll
   * @function
   * @return InfluencingFactor or error 500
   */
  getAll() {
    return Repository.get(`${resource}`);
  },

};
