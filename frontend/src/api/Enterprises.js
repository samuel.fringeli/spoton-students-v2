/**
 * @file API call for Enterprises related query
 * @author Anthony cherbuin <Anthony.Cherbuin@edu.hefr.ch>
 * @date 22.07.2020
 * @version 2.0
 */

import Repository from './repository';

const resource = '/enterprises';
export default {
  /**
   * Get all enterprises
   * @name getAll
   * @function
   * @return all enterprises
   */
  getAll() {
    return Repository.get(`${resource}`);
  },

  /**
   * Get all rooms
   * @name getRooms
   * @function
   * @return rooms or error 500
   */
  getRooms(enterpriseId) {
    return Repository.get(`${resource}/${enterpriseId}/rooms`)
  },

  
  /**
   * Get all the themes and factors (with an indication of the selection status of each) in a specific enterprise
   * @name getFactorsInEnterprise
   * @function
   * @param {number} enterpriseId enterprise's id
   * @return selected factor's array
   */
   getFactorsInEnterprise(enterpriseId) {
    return Repository.get(`${resource}/${enterpriseId}/factors`);
  },

  /**
   * Update the selection of factors for an enterprise
   * @name putFactorsInEnterprise
   * @function
   * @param {number} enterpriseId enterprise's id
   * @param {payload} data factor's array
   * @return status 200 or error 500
   */
  putFactorsInEnterprise(enterpriseId, data) {
    return Repository.put(`${resource}/${enterpriseId}/factors`, data);
  }
};
