/**
 * @file API call for Responses related query
 * @author Uchendu Nwachukwu <uchendu.nwachukwu@edu.hefr.ch>
 * @date 02.10.2020
 * @version 2.1
 */

import Repository from './repository';

const grandParentResource = '/themes'
const parentResource = 'factors';
const resource = 'responses';

export default {
  /**
   * Get all responses of today
   */
  getAllThemeResponses() {
    return Repository.get(`${grandParentResource}/${resource}`);
  },

  /**
   * Create or update a response to a theme
   */
  putThemeResponse(themeId, payload) {
    return Repository.put(`${grandParentResource}/${themeId}/${resource}`, payload);
  },

  /**
   * Get the response of the user to a theme
   */
  getThemeResponse(themeId, queryParams = null) {
    let queryParamsString = ""
    if (queryParams !== null) {
      const params = new URLSearchParams(queryParams);
      queryParamsString = "?" + params.toString();
    }
    return Repository.get(`${grandParentResource}/${themeId}/${resource}${queryParamsString}`);
  },

  /**
   * Create or update a response to a factor
   */
  putFactorResponse(themeId, factorId, payload) {
    return Repository.put(`${grandParentResource}/${themeId}/${parentResource}/${factorId}/${resource}`, payload);
  },

  /**
   * Delete a response to a factor
   */
  deleteFactorResponse(themeId, factorId, responseId) {
    return Repository.delete(`${grandParentResource}/${themeId}/${parentResource}/${factorId}/${resource}/${responseId}`);
  },

  /**
   * Get the response of the user to a factor
   */
  getFactorResponse(themeId, factorId, queryParams = null) {
    let queryParamsString = ""
    if (queryParams !== null) {
      const params = new URLSearchParams(queryParams);
      queryParamsString = "?" + params.toString();
    }
    return Repository.get(`${grandParentResource}/${themeId}/${parentResource}/${factorId}/${resource}${queryParamsString}`);
  },
};
