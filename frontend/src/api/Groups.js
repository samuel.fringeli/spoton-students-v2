/**
 * @file API call for Groups related query
 * @author Uchendu NWachukwu <Uchendu.Nwachukwu@edu.hefr.ch>
 * @date 24.08.2020
 * @version 2.0
 */

import Repository from "./repository";

const resource = "/groups";
export default {
  /**
   * Get all groups for the connected user
   * @name getAll
   * @function
   * @return groups with all parameters and responses or error 500
   */
  getAll() {
    return Repository.get(`${resource}`);
  },

  /**
   * Create a group
   * @name post
   * @function
   * @param {payload} data for the group creation
   * @return group created or error 500
   */
  post(payload) {
    return Repository.post(`${resource}`, payload);
  },

  /**
   * Update a group
   * @name put
   * @function
   * @param {groupId} id of the group to update
   * @param {payload} data for the group update
   * @return error 500 if necessary
   */
  put(groupId, payload) {
    return Repository.put(`${resource}/${groupId}`, payload);
  },

  /**
   * Delete a group
   * @name delete
   * @function
   * @param {id} id of the group to delete
   * @return status 200 or error 500
   */
  delete(groupId) {
    return Repository.delete(`${resource}/${groupId}`);
  },

  /**
   * Invite a user to group
   * @name invite
   * @function
   * @param {groupId} id of the group to which the user is invited
   * @param {usersIds} the ids of the users that shall be invited
   * @return status 200 or error 500
   */
  invite(groupId, usersIds) {
    return Repository.post(`${resource}/${groupId}/invite`, usersIds);
  },

  /**
   * List all users that can be invited to the group
   * @name invitableUsers
   * @function
   * @param {int} id of the group
   * @return status 200 or error 500
   */
  invitableUsers(groupId) {
    return Repository.get(`${resource}/${groupId}/invitableUsers`);
  },

  /**
   * Accept or decline an invitation (depending on the payload)
   * @name put
   * @function
   * @param {groupId} groupId for which the invitation status shall be changed
   * @param {payload} status object with new status of the invitation (accepted/declined)
   * @return ???
   */
  answerToInvitation(groupId, payload) {
    return Repository.put(`${resource}/${groupId}/invitation`, payload);
  },

  /**
   * List all members of a group
   * @name members
   * @function
   * @param {groupId} id of the group for which the members should be listed
   * @return status 200 or error 500
   */
  members(groupId) {
    return Repository.get(`${resource}/${groupId}/members`);
  },

  /**
   * Leave a group
   * @name leave
   * @function
   * @param {groupId} id of the group from which a user should be removed
   * @param {userId} id of the user which should be removed
   * @return status 200 or error 403, 404 or 500
   */
  leave(groupId, userId) {
    return Repository.delete(`${resource}/${groupId}/members/${userId}`);
  },
};
