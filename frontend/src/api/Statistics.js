/**
 * @file API call for statistics related query
 * @author Yael Iseli <yael.iseli@edu.hefr.ch>
 * @date 11.08.2020
 * @version 3.0
 */

import Repository from './repository';
import { appendParamsToUrl } from '@/helpers/url';
import BBDATARepository from './repositoryBBData';

const resource = '/stats';

export default {
  getLastVote() {
    return Repository.get(`${resource}/lastVote`);
  },
  getStatsDays(params) {
    return Repository.get(appendParamsToUrl(`${resource}/days`, params));
  },
  getStatsMonths(params) {
    return Repository.get(appendParamsToUrl(`${resource}/months`, params));
  },
  getStatsSeasons(params) {
    return Repository.get(appendParamsToUrl(`${resource}/seasons`, params));
  },
  getResponseCountForFactor(params) {
    return Repository.get(appendParamsToUrl(`${resource}/responseCountForFactor`, params));
  },
  getTemperature(params, sensorId) {
    /*let date = new Date(params.from)
    date.setMonth(date.getMonth()-1)
    params.from = date.toISOString()*/
    return BBDATARepository.get(appendParamsToUrl(`${sensorId}/values`, params));
  },
  getSensorValues(params, sensorId) {
    /*let date = new Date(params.from)
    date.setMonth(date.getMonth()-1)
    params.from = date.toISOString()*/
    return BBDATARepository.get(appendParamsToUrl(`${sensorId}/values`, params));
  },
  getThemesSortedByNbVotes(params) {
    return Repository.get(appendParamsToUrl(`${resource}/themes/sortedByNbVotes`, params));
  },
  getThemesSortedByFeeling(params) {
    return Repository.get(appendParamsToUrl(`${resource}/themes/sortedByFeeling`, params));
  },
  getThemeFactorSatisfaction(params, payload) {
    return Repository.post(appendParamsToUrl(`${resource}/theme_factor_satisfaction`, params), payload);
  },
};
