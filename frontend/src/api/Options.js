import Repository from "./repository";

const resource = "/notifications/get-answer-options";
export default {
  /**
   * Get the options the user can use when he gets an alerte
   * @name post
   * @function
   * @param {payload} data of the subscription
   * @return confirmation or error 500
   */
  get(payload) {
    return Repository.get(`${resource}`, payload);
  },
};
