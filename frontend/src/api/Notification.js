import Repository from "./repository";

const resource = "/notifications";
export default {
  /**
   * Update notification status to send -> received
   * @name post
   * @function
   * @param {payload} value
   * @return confirmation or error 500
   */
  post(payload) {
    return Repository.post(`${resource}/updatestatus`, payload);
  },
};
