import { createApp } from 'vue';
import App from "./App.vue";
import router from './router';
import store from "./store";
import { vuetify } from "./plugins/vuetify.js"
import './stylesheets/settings.scss'
import VueApexCharts from "vue3-apexcharts";

import { SnackbarService } from "vue3-snackbar";
import "vue3-snackbar/styles";

//import VueFusionChartsComponent from 'vue-fusioncharts/component';
import VueFusionCharts from "vue-fusioncharts";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import Widgets from "fusioncharts/fusioncharts.widgets";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";


const app = createApp(App)

app.use(store)
app.use(router)
app.use(vuetify)
app.use(VueApexCharts)
app.use(VueFusionCharts, FusionCharts, Charts, Widgets, FusionTheme);
app.use(SnackbarService);

app.mount('#app')