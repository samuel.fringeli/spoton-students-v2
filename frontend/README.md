# SooZ frontend

The frontend is launched automatically with ``docker-compose`` (cf. main README).

## References
- [Vuejs-cli](https://cli.vuejs.org/guide/deployment.html#gitlab-pages)
- [axios](https://github.com/axios/axios)
- [vuex store](https://vuex.vuejs.org/guide/)
- [vue-router](https://router.vuejs.org/)
- [material-design list of icons](https://cdn.materialdesignicons.com/1.1.34/)
