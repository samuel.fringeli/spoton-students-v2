/* eslint-disable */
importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
);

// const PushMsgEvent = new CustomEvent('pushMsgEvent', { detail: null });
const APP_BASE_URL = location.origin;
workbox.core.skipWaiting();
workbox.core.clientsClaim();

self.__WB_MANIFEST;

// Stylesheet caching
workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "file-cache",
  })
);

// Image caching
workbox.routing.registerRoute(
  /\.(?:png|jpg|jpeg|svg|gif)$/,
  new workbox.strategies.CacheFirst({
    cacheName: "image-cache",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50, // cache only 50 images
        maxAgeSeconds: 30 * 24 * 60 * 60, // 30 days
      }),
    ],
  })
);

// Listen to push event
self.addEventListener("push", (event) => {
  if (event.data) {
    console.log(JSON.parse(event.data.text()).tag.toString())
    if(JSON.parse(event.data.text()).tag.toString().includes('push_message')){
      self.clients.matchAll().then(clients => {
        clients.forEach(client => {
          client.postMessage(JSON.stringify(event.data.text()));
        });
      });
    }
    if(JSON.parse(event.data.text()).tag.toString().includes('push_notification')){
      const { title, ...options } = JSON.parse(event.data.text());
      self.registration.showNotification(title, options).then(r => console.log("popup ok"));
    }
    if(JSON.parse(event.data.text()).tag.toString().includes('push_alarm_msg')){
      self.clients.matchAll().then(clients => {
        clients.forEach(client => {
          // this part is a bit shady but basically we can only trigger message event so we need to manually add a tag
          // to the message to know whenever it's a chat message (so no buttons) or an alerte message (buttons)
          let newData ="alerte_msg".concat('',JSON.stringify(event.data.text()));
          client.postMessage(newData);
        });
      });
    }
  }
});


// Notification action
self.addEventListener("notificationclick", (e) => {
  const { notification, action } = e;
  if( notification.data.id){
    let req = {
      id: notification.data.id,
      newStatus: 'read',
    };

		const fetchUrl = /localhost:8080/.test(location.origin) ?
			'http://localhost:3000/notifications/updatestatus' :
			location.origin + '/api/notifications/updatestatus';
    fetch(fetchUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(req),
    }).catch(error => {
      console.error(error)
    });

    notification.close();

    if (action === "vote") {
      clients.openWindow(`${APP_BASE_URL}`);
    }
    else if (action === "invitation") {
      clients.openWindow(`${APP_BASE_URL}/groups`);
    }
  } else{
    notification.close();
  }
});

workbox.precaching.precacheAndRoute([]);

