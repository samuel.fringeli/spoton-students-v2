/* eslint-disable */
importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
);

// Load all ENVERYWHERE enviroment variables
// importScripts('./env-vars.js')

// const PushMsgEvent = new CustomEvent('pushMsgEvent', { detail: null });
const ENVERYWHERE_APP_BASE_URL = "http://localhost:8080"
workbox.core.skipWaiting();
workbox.core.clientsClaim();

[{"revision":"515e89d5a234c5c7cc067d963a3c6f8e","url":"css/app.4a33cc81.css"},{"revision":"b588c5c1f9f064526f43eb09cb7c77cc","url":"css/chunk-vendors.b33d17b5.css"},{"revision":"30ef2b4fa829e0ebd68bae7a690c486e","url":"env-vars.js"},{"revision":"29350d137523af2e90b90ee3b27c2746","url":"favicon.ico"},{"revision":"0509ab09c1b0d2200a4135803c91d6ce","url":"fonts/MaterialIcons-Regular.0509ab09.woff2"},{"revision":"29b882f018fa6fe75fd338aaae6235b8","url":"fonts/MaterialIcons-Regular.29b882f0.woff"},{"revision":"96c476804d7a788cc1c05351b287ee41","url":"fonts/MaterialIcons-Regular.96c47680.eot"},{"revision":"da4ea5cdfca6b3baab285741f5ccb59f","url":"fonts/MaterialIcons-Regular.da4ea5cd.ttf"},{"revision":"68786111535a741ab74b8200bceccda2","url":"img/bot_icon.68786111.png"},{"revision":"38be44d7271b7ad5bad84d435ab7afcc","url":"img/ENERGY_cmjn.38be44d7.svg"},{"revision":"2194b686ed9815416c296d1e9a1bff6c","url":"img/home1.2194b686.svg"},{"revision":"68786111535a741ab74b8200bceccda2","url":"img/icons/favicon-192.png"},{"revision":"4603735659a9261f961cd070421049d2","url":"img/icons/manifest-icon-192.png"},{"revision":"0153c3ce638956480fd4cc5126039b4b","url":"img/icons/manifest-icon-512.png"},{"revision":"7f79cd64a7bd2614019868cb23a829e5","url":"img/iSIS_cmjn.7f79cd64.svg"},{"revision":"51793891b7e3399661b47ea262dcb1cf","url":"img/logo_heia.51793891.svg"},{"revision":"0613323726727834dce86408132484a7","url":"img/mental.06133237.svg"},{"revision":"2794ef9519ad70bbad510d0448c66046","url":"img/rocket.2794ef95.svg"},{"revision":"460f7c4a6c094e94fd1a8573056845dc","url":"img/TRANSFORM_cmjn.460f7c4a.svg"},{"revision":"598eaab2b50354d7be0aaa4fb7563299","url":"index.html"},{"revision":"c2dcf4251206c4ff6a5b8db0535d6873","url":"js/app.487ff59f.js"},{"revision":"1babcc51180c352d78a882bfeaeac420","url":"manifest.json"}];

// Stylesheet caching
workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "file-cache",
  })
);

// Image caching
workbox.routing.registerRoute(
  /\.(?:png|jpg|jpeg|svg|gif)$/,
  new workbox.strategies.CacheFirst({
    cacheName: "image-cache",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50, // cache only 50 images
        maxAgeSeconds: 30 * 24 * 60 * 60, // 30 days
      }),
    ],
  })
);

// Listen to push event
self.addEventListener("push", (event) => {
  if (event.data) {
    console.log(JSON.parse(event.data.text()).tag.toString())
    if(JSON.parse(event.data.text()).tag.toString().includes('push_message')){
      self.clients.matchAll().then(clients => {
        clients.forEach(client => {
          client.postMessage(JSON.stringify(event.data.text()));
        });
      });
    }
    if(JSON.parse(event.data.text()).tag.toString().includes('push_notification')){
      const { title, ...options } = JSON.parse(event.data.text());
      self.registration.showNotification(title, options).then(r => console.log("popup ok"));
    }
    if(JSON.parse(event.data.text()).tag.toString().includes('push_alarm_msg')){
      self.clients.matchAll().then(clients => {
        clients.forEach(client => {
          // this part is a bit shady but basically we can only trigger message event so we need to manually add a tag
          // to the message to know whenever it's a chat message (so no buttons) or an alerte message (buttons)
          let newData ="alerte_msg".concat('',JSON.stringify(event.data.text()));
          client.postMessage(newData);
        });
      });
    }
  }
});


// Notification action
self.addEventListener("notificationclick", (e) => {
  const { notification, action } = e;
  if( notification.data.id){
    let req = {
      id: notification.data.id,
      newStatus: 'read',
    };

    // @TODO change it with server address
    fetch('http://localhost:3000/notifications/updatestatus', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(req),
    }).catch(error => {
      console.error(error)
    });

    notification.close();

    if (action === "vote") {
      clients.openWindow(`${ENVERYWHERE_APP_BASE_URL}`);

    }
    else if (action === "invitation") {
      clients.openWindow(`${ENVERYWHERE_APP_BASE_URL}/groups`);
    }
  }else{
    notification.close();
  }
});

workbox.precaching.precacheAndRoute([]);
