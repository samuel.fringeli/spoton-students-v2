import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# SMTP Configuration
EMAIL_HOST = 'smtp.hefr.ch'
EMAIL_HOST_PORT = 25
EMAIL_ADDRESS = 'noreply-spoton@hefr.ch'


def compose_email(subject, recipient_email, html_content):
    message = MIMEMultipart()
    message['From'] = EMAIL_ADDRESS
    message['To'] = recipient_email
    message['Subject'] = subject
    message.attach(MIMEText(html_content, 'html'))
    return message


def send_email(url, recipient_email):
    # HTML Content
    html_content = f"""
    <html>
        <head></head>
        <body>
            <p>Bonjour,</p>
            <p>Voici le lien vers le PDF de la dernière flashnews fournie par l'application Sooze:</p>
            <p><a href='{url}'>Cliquez ici pour accéder au PDF</a></p>
            <p>Cordialement,</p>
            <p>L'équipe Sooze</p>
        </body>
    </html>
    """

    # Compose email with HTML content
    subject = "Lien PDF Flashnews - Sooze"
    message = compose_email(subject, recipient_email, html_content)

    # Send the email
    try:
        with smtplib.SMTP(EMAIL_HOST, EMAIL_HOST_PORT) as server:
            server.starttls()  # Comment this line if TLS is not required
            server.send_message(message)
        print("Email sent successfully.")
        return True
    except smtplib.SMTPException as e:
        print(f"Error sending email: {e}")
        return False


# Main execution
if __name__ == "__main__":
    # Example usage
    send_email('https://sooz.tic.heia-fr.ch/api/pdf/c28ec8e8-f8fe-4bf4-985d-43ce1930a42c', 'me@leonardnoth.com')
