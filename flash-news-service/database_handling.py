from mysql.connector.cursor import Error as DBError
from utils import DbAccess
import sys


class DBInterface:
    def __init__(self):
        self.get_group_id_query_str = 'SELECT id FROM s00z_2_db.`group`;'
        self.get_email_admins_query_str = ('SELECT email FROM s00z_2_db.user WHERE id IN (SELECT userId FROM '
                                           's00z_2_db.userGroup WHERE groupId = {} AND role = "admin");')
        self.store_pdf_query_str = 'INSERT INTO s00z_2_db.pdf (url, groupId) VALUES (%s, %s);'

    def get_group_ids_from_database(self):
        try:
            with DbAccess() as db:
                db.cursor.execute(self.get_group_id_query_str)
                results = db.cursor.fetchall()
                db.close()
                return results

        except DBError as e:
            print('An error occurred while using the DB:', str(e), file=sys.stderr)
            return False

    def get_email_admins_from_database(self, group_id):
        try:
            with DbAccess() as db:
                query = self.get_email_admins_query_str.format(group_id)
                db.cursor.execute(query)
                results = db.cursor.fetchall()
                db.close()
                return results

        except DBError as e:
            print('An error occurred while using the DB:', str(e), file=sys.stderr)
            return False

    def store_pdf_in_database(self, url, group_or_user_id):
        try:
            with DbAccess() as db:
                db.cursor.execute(self.store_pdf_query_str, (url, group_or_user_id))
                db.connection.commit()
                db.close()
                return True

        except DBError as e:
            print('An error occurred while using the DB:', str(e), file=sys.stderr)
            return False
