import requests
from database_handling import DBInterface
from mail import send_email
from utils import Config
import datetime

Config.parse_from_config()
CORRELATION_URL = "http://{}:6001/analyse".format(Config.correlation_url)
CLUSTERING_URL = "http://{}:6004/cluster".format(Config.clustering_url)
PDF_GENERATION_URL = "http://{}:5001/pdfgen".format(Config.pdf_generation_url)
FLASH_NEWS_GATHER_URL = "http://{}:6011/flash-news-gather".format(Config.flash_news_gather_url) + "?groupId={}"


def orchestrator():
    db_interface = DBInterface()
    group_ids = db_interface.get_group_ids_from_database()

    for group_id in group_ids:
        all_flash_news = request_flash_news_gather(group_id=group_id[0])

        if len(all_flash_news) == 0:
            continue

        results = {
            "title": "Flash News - Groupe {}".format(group_id[0]),
            "subtitle": datetime.date.today().strftime("%d.%m.%Y"),
            "section": []
        }

        for i in range(len(all_flash_news)):
            if 'start_date2' in all_flash_news[i][1]:
                response = request_correlation(body=all_flash_news[i][1])
                # print(response.json(), file=sys.stderr)
            else:
                response = request_clustering(body=all_flash_news[i][1])
                # print(response.json(), file=sys.stderr)

            try:
                sentences = response.json()['sentences']

                if sentences == '':
                    continue

            except KeyError:
                continue

            section = {
                "section_title": all_flash_news[i][0],
                "section_text": sentences
            }
            results["section"].append(section)

        if len(results["section"]) == 0:
            continue

        status = request_pdf_generation(body=results)
        url_pdf = status.content.decode()

        db_interface.store_pdf_in_database(url=url_pdf, group_or_user_id=group_id[0])

        admin_emails = db_interface.get_email_admins_from_database(group_id=group_id[0])

        if admin_emails is not False:
            for admin_email in admin_emails:
                # print(admin_email[0])
                # send_email(url=url_pdf, recipient_email=admin_email[0])
                pass

    return '', 200


def request_flash_news_gather(group_id: int):
    url = FLASH_NEWS_GATHER_URL.format(group_id)
    r = requests.get(url=url)
    return r.json()


def request_correlation(body: dict):
    return requests.post(CORRELATION_URL, json=body)


def request_clustering(body: dict):
    # TODO FIX THAT: request not working, may be due to the clustering service
    body['start_date2'] = body['start_date1']
    body['end_date2'] = body['end_date1']
    body['factor_theme_type2'] = body['factor_theme_type1']
    body['factor_theme_id2'] = body['factor_theme_id1']
    body['user_group_type2'] = body['user_group_type1']
    body['user_group_id2'] = body['user_group_id1']

    payload = {
        "clustering_method": "KMeans",
        "analysis_type": "5-point",
        "should_print_interpretations_json": False,
        "should_print_interpretations_string": True,
        "num_iterations": 1000,
        "file_type": "html",
        "gather": body
    }
    return requests.post(CLUSTERING_URL, json=payload)


def request_pdf_generation(body: dict):
    return requests.post(PDF_GENERATION_URL, json=body)
