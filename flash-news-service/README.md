# Flash news orchestrator service

## Description
This service is used to orchestrate the whole process for the flash news.

It is called by a cron job in the backend. 

Then, it will retrieve in the database the IDs of the groups. It will then call the flash news gather service for each group to retrieve the flash news to run.

Each flash news is then run by calling the flash news run service, calling either the clustering or the correlation service depending on the type of flash news.

If these services return something, it will be added to a dict that will be used to generate the PDF filled with the flash news descriptions and the results of the execution.
If all the analysis returned blank, no PDF will be generated.

The URL of the PDF is retrieved from the response and stored in the database.

Finally, each admin of each group is notified by email that the flash news are ready to be read with the link to the PDF.

## How to use
This service is served in the docker-compose files at the root of the repository.

To request the service, you must send a POST request to the following URL:
```
http://localhost:6005/run-flash-news
``` 

It returns nothing, but it will trigger the whole process described above.