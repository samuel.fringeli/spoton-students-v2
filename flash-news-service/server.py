from utils import Config
from flask import Flask
from flash_news_service import orchestrator

app = Flask(__name__)

# Load configuration once when the app starts
Config.parse_from_config()


@app.route('/')
def hello():
    return 'Flash news service - up and running!'


@app.route('/run-flash-news', methods=['POST'])
def run_flash_news():
    return orchestrator()


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=6005, debug=True)
