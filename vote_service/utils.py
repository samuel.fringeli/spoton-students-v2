import os
from requests import Session
from mysql.connector import connect
import toml
from pathlib import Path
from datetime import date


base_dir = Path(__file__).parent


class Config(object):
    DEFAULT_CONFIG_PATH = base_dir / 'conf/config.toml'

    # JSON paths
    users_path: Path
    votes_path: Path
    test_cases_path: Path
    user_profiles_path: Path

    # DB config
    db_host: str
    db_port: int
    db_username: str
    db_password: str
    db_database: str

    # API config
    api_base_url: str
    api_username: str
    api_password: str

    @classmethod
    def parse_from_config(cls, path: Path = DEFAULT_CONFIG_PATH):
        data = toml.load(path)

        base_dir = Path(__file__).parent
        cls.users_path = base_dir / data['json_paths']['users_path']
        cls.votes_path = base_dir / data['json_paths']['votes_path']
        cls.test_cases_path = base_dir / data['json_paths']['test_cases_path']
        cls.user_profiles_path = base_dir / data['json_paths']['user_profiles_path']

        cls.db_host = data['database']['host']
        cls.db_port = data['database']['port']
        cls.db_username = os.environ['MYSQL_USER_NAME']
        cls.db_password = os.environ['MYSQL_ROOT_PASSWORD']
        cls.db_database = os.environ['MYSQL_DATABASE']
        cls.api_base_url = data['api']['base_url']
        cls.api_username = data['api']['username']
        cls.api_password = data['api']['password']


class DbAccess(object):

    def __init__(self):
        self.connection = connect(
            host=Config.db_host,
            port=Config.db_port,
            user=os.environ["MYSQL_USER_NAME"],
            password=os.environ["MYSQL_ROOT_PASSWORD"],
            database=os.environ["MYSQL_DATABASE"],
            ssl_disabled=True)
        self.cursor = self.connection.cursor()

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.close()

    def close(self):
        self.cursor.close()
        self.connection.close()


class ApiAccess(object):

    def __init__(self):
        self.session = Session()

        r = self.session.post(
            f'{Config.api_base_url}/users/login',
            json={'user': {
                'identifier': Config.api_username,
                'token': Config.api_password
            }})
        r.raise_for_status()

    def get_opendays(self, start_date: date, end_date: date, enterprise_id: int):
        r = self.session.get(f'{Config.api_base_url}/enterprises/{enterprise_id}/opendays',
                            params={'begin': start_date, 'end': end_date})
        r.raise_for_status()
        return [date.fromisoformat(s) for s in r.json()]

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.close()

    def close(self):
        r = self.session.post(f"{Config.api_base_url}/users/logout")
        r.raise_for_status()
        self.session.close()
