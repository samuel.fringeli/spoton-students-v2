
from .error_handlers import handle_db_error, handle_api_error, handle_value_error, handle_generic_error
from flask import Flask, request, jsonify
from utils import Config, DbAccess, ApiAccess
from requests import HTTPError
from mysql.connector import Error as DBError

from vote_service_helper import VoteServiceHelper

import logging

app = Flask(__name__)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class VoteApiHelper:

    @staticmethod
    def generate_votes_API_request():
        try:
            json_data = request.get_json()
            with DbAccess() as db, ApiAccess() as api:
                IsGenerated_votes = VoteServiceHelper.generate_votes(db, api, json_data)
                return jsonify({'data': {'status':'success' if IsGenerated_votes else 'failed'}})
        except DBError as e:
            return handle_db_error(e)
        except HTTPError as e:
            return handle_api_error(e)
        except ValueError as e:
            return handle_value_error(e)
        except Exception as e:
            return handle_generic_error(e)
        

    @staticmethod
    def import_votes_API_request():
        try:
            with DbAccess() as db, ApiAccess() as api:
                isImported_votes = VoteServiceHelper.import_votes(db=db)
                return jsonify({'data': {'status':'success' if isImported_votes else 'failed'}})
        except DBError as e:
            return handle_db_error(e)
        except HTTPError as e:
            return handle_api_error(e)
        except ValueError as e:
            return handle_value_error(e) 
        except Exception as e:
            return handle_generic_error(e)
        



    @staticmethod
    def delete_votes_API_request():
        try:
            with DbAccess() as db, ApiAccess() as api:
                IsDeleted_votes = VoteServiceHelper.delete_votes(db=db)
                return jsonify({'data': {'status':'success' if IsDeleted_votes else 'failed'}})
        except DBError as e:
            return handle_db_error(e)
        except HTTPError as e:
            return handle_api_error(e)
        except ValueError as e:
            return handle_value_error(e)
        except Exception as e:
            return handle_generic_error(e)