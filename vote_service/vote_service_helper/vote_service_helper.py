from test_data_generation import generate_test_data, load_json, insert_test_data, delete_test_data
from utils import Config, DbAccess, ApiAccess

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class VoteServiceHelper:

## this function for generate fake votes
    @staticmethod
    def generate_votes(db, api, json_data):
                    try:
                        generate_test_data(db, api, json_data["start_date"],json_data["end_date"],json_data["theme_id"])
                        return True
                    except RuntimeError as e:
                        print('An error occured inserting test data:')
                        print(e)
                        return False   
   
## this function for import fake votes to Database
    @staticmethod
    def import_votes(db):
                    users = load_json(Config.users_path)
                    votes = load_json(Config.votes_path)
                    try:
                        offset = insert_test_data(db, users, votes)
                        print(f'User ID offset to map from the users in the JSON to the DB is {offset} (user 5 in the JSON is now {5 + offset})')
                        return True
                    except RuntimeError as e:
                        print('An error occured inserting test data:')
                        print(e)
                        return False
    


## this function for delete fake votes in Database
    @staticmethod
    def delete_votes(db):
                    users = load_json(Config.users_path)
                    votes = load_json(Config.votes_path)
                    try:
                     delete_test_data(db, users, votes)
                     return True
                    except RuntimeError as e:
                        print('An error occured deleting test data:')
                        print(e)
                        return False