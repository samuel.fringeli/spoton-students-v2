# vote_service

The aim of this part of the project is to generate and (import and delete) votes from the database.


# Run Gather Container
* First of all, to run the "vote_service" container separately, execute the following command: "docker-compose up vote_service." If you prefer to run the "vote_service" container alongside other containers, use the command "docker-compose up."


## How to run

### there are two way to run vote_service methods:
* first way by run comand line inside vote_service container terminal  :
* Run `./main.py generator stard_date end_date  them_id` to generator fake data.
* Run `./main.py import` to import fake data from DB.
* Run `./main.py delete` to delete fake data from DB.

**ex.:**

```json
./main.py generator "2023-10-01" "2023-10-30"  6
./main.py import
./main.py delete
```

* secound way By using API endpoint method :
**generate fake data :By Make a POST request to http://localhost:6002/vote_service/generate with the following JSON payload in the request body and set the Content-Type header to application/json :**

```json
{
    "start_date": "2023-11-01",
    "end_date": "2023-11-30",
    "theme_id": 7
}
```

### Successful Response

- **Status Code**: 200 OK

**Body Example:**

```json
{
    "data": {
        "result": [
            [
                [
                    "Sun, 01 Oct 2023 00:00:00 GMT",
                    "morning",
                    0
                ],
                [
                    "Sun, 01 Oct 2023 00:00:00 GMT",
                    "afternoon",
                    1
                ],
                [
                    "Mon, 02 Oct 2023 00:00:00 GMT",
                    "morning",
                    -1
                ] 
            ]
        ]  
    }
}

```

**import fake data to DB:By Make a POST request to http://localhost:6002/vote_service/import with the following JSON payload in the request body and set the Content-Type header to application/json :**
**import fake data:**

### Successful Response

- **Status Code**: 200 OK

**Body Example:**

```json
{
    "data": {
        "message": "Data imported Successfully" 
        "result": [
            [
                [
                    "Sun, 01 Oct 2023 00:00:00 GMT",
                    "morning",
                    0
                ],
                [
                    "Sun, 01 Oct 2023 00:00:00 GMT",
                    "afternoon",
                    1
                ],
                [
                    "Mon, 02 Oct 2023 00:00:00 GMT",
                    "morning",
                    -1
                ] 
            ]
        ]  
    }
}

```



**delete fake data to DB:By Make a PUT request to http://localhost:6002/vote_service/delete with the following JSON payload in the request body and set the Content-Type header to application/json :**
**import fake data:**

### Successful Response

- **Status Code**: 200 OK

**Body Example:**

```json
{
    "data": {
        "message": "Data deleted Successfully" 
    }
}

```







### Error Response

- **Status Code**: 500 Bad Request

**Body Example:**

```json
{
  "error": "Internal Server Error:...."
}
```

