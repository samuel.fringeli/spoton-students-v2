mysql-connector-python==8.1.0
Flask==3.0.0
numpy==1.25.2
requests==2.28.1
matplotlib==3.7.2
toml==0.10.2
