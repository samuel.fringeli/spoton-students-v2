#!/usr/bin/env python3

from mysql.connector import Error as DBError
from requests import HTTPError
import argparse
from datetime import date
from pathlib import Path
from utils import Config, DbAccess, ApiAccess
from vote_service_helper import VoteServiceHelper
from api_helper import vote_api_helper


def init_argparser() -> argparse.Namespace:
    print("Initializing argument parser")
    parser = argparse.ArgumentParser(
        description='Module to do vote service')
    parser.add_argument('--config', type=str,
                        default=str(Config.DEFAULT_CONFIG_PATH), help='Path to TOML config file')
    subparsers = parser.add_subparsers(
        help='choose a command', required=True, dest='command')

    subparsers.add_parser(
        'import', help='imports the JSON test cases in the DB')

    subparsers.add_parser(
        'delete', help='removes the JSON test cases in the DB')

    generate_parser = subparsers.add_parser(
        'generate', help='generates fake users and vote data')
    generate_parser.add_argument(
        'start_date', type=date.fromisoformat, help='start date (format: YYYY-mm-dd)')
    generate_parser.add_argument(
        'end_date', type=date.fromisoformat, help='end date (format: YYYY-mm-dd)')
    generate_parser.add_argument(
        'theme_id', type=int, help='id of the theme for which the users vote')

    return parser.parse_args()






def main():
    args = init_argparser()
    Config.parse_from_config(Path(args.config))
    try:
        with DbAccess() as db:
            with ApiAccess() as api:
                try:
                    if args.command == 'generate':
                        json_data ={"start_date": args.start_date,"end_date": args.end_date,"theme_id": args.theme_id}
                        generated_votes = VoteServiceHelper.generate_votes(db=db, api=api,json_data=json_data)
                        print(generated_votes)

                    if args.command == 'import':
                        impoted_votes = VoteServiceHelper.import_votes(db=db)
                        print(impoted_votes)

                    if args.command == 'delete':
                        deleted_votes = VoteServiceHelper.delete_votes(db=db)
                        print(deleted_votes)
                       
                except RuntimeError as e:
                        print('An error occured gather')
                        print(e)     
    except DBError as e:
        print('An error occured while using the DB:')
        print(e)
    except HTTPError as e:
        print('An error occured while using the backend API:')
        print(e)
    except ValueError as e:
        print('Invalid arguments passed:')
        print(e)
    except IOError as e:
        print('An error occured while reading the JSON files:')
        print(e)


if __name__ == '__main__':
    main()