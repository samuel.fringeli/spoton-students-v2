import json
from datetime import datetime, date, time
import random
from enum import Enum

from utils import Config, DbAccess, ApiAccess


class FillWith(Enum):
    RANDOM = 'random',
    REPEAT = 'repeat',
    NO_VOTE = 'no_vote',
    POSITIVE = 'positive',
    NEGATIVE = 'negative',

    @classmethod
    def parse_from_json(cls, type: str):
        if type == 'random':
            return cls.RANDOM
        elif type == 'repeat':
            return cls.REPEAT
        elif type == 'no_vote':
            return cls.NO_VOTE
        elif type == 'positive':
            return cls.POSITIVE
        elif type == 'negative':
            return cls.NEGATIVE
        else:
            raise ValueError(f'{type} is an invalid "fill with"')


class PatternPlacement(Enum):
    RANDOM = 'random',
    START = 'start',
    MIDDLE = 'middle',
    END = 'end',

    @classmethod
    def parse_from_json(cls, type: str):
        if type == 'random':
            return cls.RANDOM
        elif type == 'start':
            return cls.START
        elif type == 'middle':
            return cls.MIDDLE
        elif type == 'end':
            return cls.END
        else:
            raise ValueError(f'{type} is an invalid pattern placement')


def generate_votes(votes: list[dict[str, any]], profile: dict[str, any],
                   dates: list[date], factors: dict[int, dict[int, list[int]]],
                   user_id: int, roomId: int) -> list[dict[str, any]]:
    periods = ['morning', 'afternoon']
    votes_len = len(dates) * 2

    pattern = profile['voting_behaviour'].get('pattern')
    pattern_placement = PatternPlacement.parse_from_json(
        profile['voting_behaviour'].get('pattern_placement', 'start'))
    fill_with = FillWith.parse_from_json(
        profile['voting_behaviour'].get('fill_with', 'no_vote'))

    if fill_with is FillWith.RANDOM:
        vote_vals = [random.randint(-1, 1) for _ in range(votes_len)]
    elif fill_with is FillWith.POSITIVE:
        vote_vals = votes_len * [1]
    elif fill_with is FillWith.NEGATIVE:
        vote_vals = votes_len * [-1]
    else:
        vote_vals = votes_len * [0]

    if pattern is not None:
        pattern = pattern[:votes_len]
        if pattern_placement is PatternPlacement.START:
            start = 0
        elif pattern_placement is PatternPlacement.END:
            start = votes_len - len(pattern)
        elif pattern_placement is PatternPlacement.MIDDLE:
            start = (votes_len // 2) - (len(pattern) // 2)
        elif pattern_placement is PatternPlacement.RANDOM:
            start = random.randrange(0, votes_len - len(pattern))

        if fill_with is not FillWith.REPEAT:
            if start + len(pattern) < votes_len:
                vote_vals = vote_vals[:start] + pattern + \
                    vote_vals[start + len(pattern):]
            else:
                vote_vals = vote_vals[:start] + pattern
        else:
            for i in range(votes_len):
                vote_vals[(start + i) % votes_len] = pattern[i % len(pattern)]

    # print(f'{profile["username"]}{user_id} {vote_vals}')

    i = 0
    for date in dates:
        for period in periods:
            if vote_vals[i] != 0:
                factor_id = random.choice(list(factors.keys()))
                ro_id = random.choice(factors[factor_id][vote_vals[i]])
                votes.append({
                    'date': str(date),
                    'datetime': str(datetime.combine(date, time(7) if period == 'morning' else time(14))),
                    'period': period,
                    'userId': user_id,
                    'factorId': factor_id,
                    'responseOptionId': ro_id,
                    'roomId': roomId,
                })
            i += 1


def get_factors(db: DbAccess, theme_id: int) -> dict[int, dict[int, list[int]]]:
    db.cursor.execute(
        'SELECT id, responseOptionSetId FROM factor WHERE themeId = %s', (theme_id,))
    res = {}
    for factor_id, ros_id in db.cursor.fetchall():
        res[factor_id] = {
            -1: [],
            1: [],
        }
        db.cursor.execute(
            'SELECT id, polarizedValue FROM responseOption WHERE responseOptionSetId = %s', (ros_id,))
        for ro_id, val in db.cursor:
            res[factor_id][val].append(ro_id)

    return res


def generate_test_data(db: DbAccess, api: ApiAccess, start_date: date, end_date: date, theme_id: int):
    user_profiles: list[dict[str, any]] = json.load(
        open(Config.user_profiles_path, 'r'))
    dates = api.get_opendays(start_date, end_date,
                             user_profiles['enterpriseId'])
    factors = get_factors(db, theme_id)

    users = []
    votes = []
    user_count = 1
    for profile in user_profiles['profiles']:
        for i in range(1, profile['number_of_occurences'] + 1):
            users.append({
                'id': user_count,
                'firstName': profile['firstName'],
                'lastName': profile['lastName'] + str(i),
                'username': profile['username'] + str(i),
                'email': profile['username'] + str(i) + '@sooze.ch',
                'password': profile['password'],
                'points': profile['points'],
                'activatePopup': profile['activatePopup'],
                'sex': profile['sex'],
                'role': profile['role'],
                'enterpriseId': user_profiles['enterpriseId'],
                'activationToken': profile['activationToken'],
                'activated': profile['activated']
            })

            generate_votes(votes, profile, dates, factors,
                           user_count, user_profiles['roomId'])

            user_count += 1

    with open(Config.users_path, 'w') as f:
        json.dump(users, f, indent=4)

    with open(Config.votes_path, 'w') as f:
        json.dump(votes, f, indent=4)
