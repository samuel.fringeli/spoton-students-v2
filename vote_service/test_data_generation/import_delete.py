from typing import Union
from mysql.connector.cursor import MySQLCursorAbstract
import json
from pathlib import Path

from utils import DbAccess

# Type for the JSON data retrieved from the files
JsonData = list[dict[str, Union[str, int, None]]]


def load_json(path: Path) -> JsonData:
    with open(path) as file:
        return json.loads(file.read())


# not using the decorator because this operation is used within a with state so a rollback can be done if an error occurs
def count_entries(cursor: MySQLCursorAbstract) -> tuple[int, int]:
    cursor.execute('SELECT COUNT(*) FROM user;')
    user_count = cursor.fetchone()[0]
    cursor.execute('SELECT COUNT(*) FROM userFactorResponse;')
    vote_count = cursor.fetchone()[0]

    return (user_count, vote_count)


# returns the offset of the userids
def insert_test_data(db: DbAccess, users: JsonData, votes: JsonData) -> int:
    old_user_count, old_vote_count = count_entries(db.cursor)
    print(
        f'row count before insert -> user: {old_user_count}, vote: {old_vote_count}')

    db.cursor.executemany('''INSERT INTO user (firstName, lastName, username, email, token,
                        points, activatePopup, sex, role, enterpriseId, activationToken, activated)
                        VALUES (%(firstName)s, %(lastName)s, %(username)s, %(email)s,
                        %(password)s, %(points)s, %(activatePopup)s, %(sex)s, %(role)s,
                        %(enterpriseId)s, %(activationToken)s, %(activated)s);''',
                            users)

    user_id_offset = db.cursor.lastrowid - 1
    for vote in votes:
        vote['userId'] += user_id_offset

    db.cursor.executemany('''INSERT INTO userFactorResponse (date, datetime, period, userId, factorId,
                        responseOptionId, roomId) VALUES (%(date)s, %(datetime)s, %(period)s,
                        %(userId)s, %(factorId)s, %(responseOptionId)s, %(roomId)s);''',
                            votes)

    user_count, vote_count = count_entries(db.cursor)
    print(
        f'row count after insert -> user: {user_count}, vote: {vote_count}')

    # check that everything got inserted correctly
    if old_user_count + len(users) != user_count:
        raise RuntimeError('User count assertion failed')
    
    if old_vote_count + len(votes) != vote_count:
        raise RuntimeError('Vote count assertion failed')

    db.connection.commit()

    return user_id_offset


def delete_test_data(db: DbAccess, users: JsonData, votes: JsonData):
    old_user_count, old_vote_count = count_entries(db.cursor)
    print(
        f'row count before delete -> user: {old_user_count}, vote: {old_vote_count}')
    
    if old_user_count < len(users):
        print(f'no test data present')
        return

    db.cursor.executemany(
        'DELETE FROM user WHERE username = %(username)s;', users)

    user_count, vote_count = count_entries(db.cursor)
    print(
        f'row count after delete -> user: {user_count}, vote: {vote_count}')

    # check that everything got inserted correctly
    if old_user_count - len(users) != user_count:
        raise RuntimeError('User count assertion failed')

    if old_vote_count - len(votes) != vote_count:
        raise RuntimeError('Vote count assertion failed')

    db.connection.commit()
