
from mysql.connector import Error as DBError
from utils import Config
from flask import Flask
from api_helper import VoteApiHelper 

app = Flask(__name__)

# Load configuration once when the app starts
Config.parse_from_config()

@app.route('/')
def hello():
    return 'This Compose/Flask demo has been viewed %s time(s) - vote service'

@app.route('/generate', methods=['POST'])
def generate_vote():
    return VoteApiHelper.generate_votes_API_request()

@app.route('/import', methods=['POST'])
def import_vote():
    return VoteApiHelper.import_votes_API_request()

@app.route('/delete', methods=['DELETE'])
def delete_vote():
    return VoteApiHelper.delete_votes_API_request()

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=6003, debug=True)