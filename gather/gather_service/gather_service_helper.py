class GatherServiceHelper:

    @staticmethod
    def get_gather_results(vg, args):
        data1 = vg.gather(args.start_date1, args.end_date1, args.factor_theme_type1,
                          args.factor_theme_id1, args.user_group_type1, args.user_group_id1)
        data2 = vg.gather(args.start_date2, args.end_date2, args.factor_theme_type2,
                          args.factor_theme_id2, args.user_group_type2, args.user_group_id2)
        return data1, data2

    @staticmethod
    def get_gather_results_api(vg, json_data):
        data1 = vg.gather(
            json_data['start_date1'],
            json_data['end_date1'],
            json_data['factor_theme_type1'],
            json_data['factor_theme_id1'],
            json_data['user_group_type1'],
            json_data['user_group_id1'])

        data2 = []
        if 'start_date2' in json_data:
            data2 = vg.gather(
                json_data['start_date2'],
                json_data['end_date2'],
                json_data['factor_theme_type2'],
                json_data['factor_theme_id2'],
                json_data['user_group_type2'],
                json_data['user_group_id2'])

        return data1, data2
