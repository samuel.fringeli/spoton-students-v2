from mysql.connector.cursor import MySQLCursorAbstract
from datetime import date
from utils import ApiAccess, DbAccess

VoteData = list[tuple[date, str, float]]


class VoteGatherer(object):

    def __init__(self, db: DbAccess, api: ApiAccess):
        self.db = db
        self.api = api

    def gather(self, start_date: date, end_date: date, factor_theme_type: str, factor_theme_id: int,
               user_group_type: str, user_group_id: int) -> VoteData:
        query = VoteGatheringQuery(
            start_date, end_date, factor_theme_type, factor_theme_id, user_group_type, user_group_id)
        return self.gather_from_query(query)

    def gather_from_query(self, query) -> VoteData:
        dates = get_open_days(self.db.cursor, self.api, query)

        if query.is_factor and query.is_user:
            return get_factor_user_votes(self.db.cursor, dates, query)
        elif not query.is_factor and query.is_user:
            return get_theme_user_votes(self.db.cursor, dates, query)
        elif query.is_factor and not query.is_user:
            return get_factor_group_votes(self.db.cursor, dates, query)
        elif not query.is_factor and not query.is_user:
            return get_theme_group_votes(self.db.cursor, dates, query)


class VoteGatheringQuery(object):

    def __init__(self, start_date: date, end_date: date, factor_theme_type: str, factor_theme_id: int,
                 user_group_type: str, user_group_id: int):
        self.start_date = start_date
        self.end_date = end_date

        if factor_theme_type == 'factor':
            self.is_factor = True
            self.factor_id = factor_theme_id
        elif factor_theme_type == 'theme':
            self.is_factor = False
            self.theme_id = factor_theme_id
        else:
            raise ValueError("Invalid value for factor_theme_type")

        if user_group_type == 'user':
            self.is_user = True
            self.user_id = user_group_id
        elif user_group_type == 'group':
            self.is_user = False
            self.group_id = user_group_id
        else:
            raise ValueError("Invalid value for user_group_type")


def get_open_days(cursor: MySQLCursorAbstract, api: ApiAccess, query: VoteGatheringQuery) -> list[date]:
    try:
        if query.user_id > -1:
            cursor.execute(
                'SELECT enterpriseId FROM user WHERE id = %s;', (query.user_id,))
    except AttributeError:
        cursor.execute(
            'SELECT enterpriseId FROM `group` WHERE id = %s;', (query.group_id,))

    res = cursor.fetchone()
    if res is None:
        raise ValueError('Invalid user / group id')
    enterprise_id, = res

    return api.get_open_days(query.start_date, query.end_date, enterprise_id)


def get_factor_user_votes(cursor: MySQLCursorAbstract, dates: list[date], query: VoteGatheringQuery) \
        -> list[tuple[date, str, float]]:
    cursor.execute(f'''SELECT vote.date, vote.period, respOpt.polarizedValue
                       FROM userFactorResponse as vote
                       JOIN responseOption as respOpt
                       ON vote.responseOptionId = respOpt.id
                       WHERE userId = %s
                       AND factorId = %s
                       AND date IN ({', '.join(['%s']*len(dates))})
                       ORDER BY date, period;''',
                   tuple([query.user_id, query.factor_id] + dates))

    return fill_missing_entries(dates, cursor.fetchall())


def get_factor_group_votes(cursor: MySQLCursorAbstract, dates: list[date], query: VoteGatheringQuery) \
        -> list[tuple[date, str, float]]:
    cursor.execute(f'''SELECT vote.date, vote.period, respOpt.polarizedValue
                       FROM userFactorResponse as vote
                       JOIN responseOption as respOpt
                       ON vote.responseOptionId = respOpt.id
                       WHERE userId IN (SELECT userId FROM userGroup WHERE groupId = %s)
                       AND factorId = %s
                       AND date IN ({', '.join(['%s']*len(dates))})
                       ORDER BY date, period;''',
                   tuple([query.group_id, query.factor_id] + dates))

    return transform_group_votes(cursor, dates, query.group_id, cursor.fetchall())


def get_theme_user_votes(cursor: MySQLCursorAbstract, dates: list[date], query: VoteGatheringQuery) \
        -> list[tuple[date, str, float]]:
    cursor.execute(f'''SELECT vote.date, vote.period, respOpt.polarizedValue
                       FROM userFactorResponse as vote
                       JOIN responseOption as respOpt
                       ON vote.responseOptionId = respOpt.id
                       WHERE userId = %s
                       AND factorId IN (SELECT id FROM factor WHERE themeId = %s)
                       AND date IN ({', '.join(['%s']*len(dates))})
                       ORDER BY date, period;''',
                   tuple([query.user_id, query.theme_id] + dates))

    return fill_missing_entries(dates, cursor.fetchall())


def get_theme_group_votes(cursor: MySQLCursorAbstract, dates: list[date], query: VoteGatheringQuery) \
        -> list[tuple[date, str, float]]:
    cursor.execute(f'''SELECT vote.date, vote.period, respOpt.polarizedValue
                       FROM userFactorResponse as vote
                       JOIN responseOption as respOpt
                       ON vote.responseOptionId = respOpt.id
                       WHERE userId IN (SELECT userId FROM userGroup WHERE groupId = %s)
                       AND factorId IN (SELECT id FROM factor WHERE themeId = %s)
                       AND date IN ({', '.join(['%s']*len(dates))})
                       ORDER BY date, period;''',
                   tuple([query.group_id, query.theme_id] + dates))

    return transform_group_votes(cursor, dates, query.group_id, cursor.fetchall())


# PRE-CONDITION:
# data should be ordered by date, period
def transform_group_votes(cursor: MySQLCursorAbstract, dates: list[date], group_id: int,
                          data: list[tuple[date, str, int, int]]) -> list[tuple[date, str, float]]:
    if len(data) == 0:
        return []
    n_users = count_users_in_group(cursor, group_id)

    avg_data = []
    crt_date = data[0][0]
    crt_period = data[0][1]
    crt_value = data[0][2] / n_users

    for date, period, value in data[1:]:
        if date == crt_date and period == crt_period:
            crt_value += value / n_users
        else:
            avg_data.append((crt_date, crt_period, crt_value))
            crt_date = date
            crt_period = period
            crt_value = value / n_users
    avg_data.append((crt_date, crt_period, crt_value))

    return fill_missing_entries(dates, avg_data)


def count_users_in_group(cursor: MySQLCursorAbstract, group_id: int) -> int:
    cursor.execute(
        'SELECT COUNT(*) FROM userGroup WHERE groupId = %s;', (group_id,))
    return cursor.fetchone()[0]


def fill_missing_entries(dates: list[date], data: list[tuple[date, str, float]]) -> list[tuple[date, str, float]]:
    dates_half = []
    for date in dates:
        dates_half.append((date, 'morning'))
        dates_half.append((date, 'afternoon'))

    votes = []
    index = 0
    for crt_date, crt_period in dates_half:
        date, period, value = data[index] if index < len(data) else (None, None, None)

        if crt_date == date and crt_period == period:
            votes.append((crt_date, crt_period, value))
            index += 1
        else:
            votes.append((crt_date, crt_period, 0))

    return votes
