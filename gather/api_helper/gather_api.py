from gather_service import VoteGatherer, GatherServiceHelper
from .error_handlers import handle_db_error, handle_api_error, handle_value_error, handle_generic_error
from flask import request, jsonify
from utils import DbAccess, ApiAccess
from requests import HTTPError
from mysql.connector import Error as DBError


class GatherApiHelper:

    @staticmethod
    def gather_api_request():

        try:
            json_data = request.get_json()
            with DbAccess() as db, ApiAccess() as api:
                vg = VoteGatherer(db, api)
                gather_results = GatherServiceHelper.get_gather_results_api(vg, json_data)
                return jsonify({'data': {'result': [gather_results[0], gather_results[1]]}})
                
        except DBError as e:
            return handle_db_error(e)
            
        except HTTPError as e:
            return handle_api_error(e)
            
        except ValueError as e:
            return handle_value_error(e)
            
        except Exception as e:
            return handle_generic_error(e)
        