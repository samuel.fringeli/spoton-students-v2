from utils import Config
from flask import Flask
from api_helper import GatherApiHelper 

app = Flask(__name__)

# Load configuration once when the app starts
Config.parse_from_config()


@app.route('/')
def hello():
    return 'Gather service - up and running!'


@app.route('/gather', methods=['POST'])
def gather_api():
    return GatherApiHelper.gather_api_request()


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=6002, debug=True)
