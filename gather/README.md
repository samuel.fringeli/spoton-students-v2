# Gather

The aim of this part of the project is to gather the votes from the database and return them in the right format.


# Run Gather Container
* First of all, to run the "gather" container separately, execute the following command: "docker-compose up gather." If you prefer to run the "gather" container alongside other containers, use the command "docker-compose up."


## How to run
* Run `./main.py gather` to run gather the fake data.
* Use the `./main.py analyze --help` to get information on how to make a query to analyze voting data

### there are two way to run gather method:
* first way by run comand line inside gather container terminal  : ./main.py gather [PARAMETERS]

**ex.:**

```json
./main.py gather 2023-10-01 2023-10-30 theme 6 user 10 2023-10-01 2023-10-08 theme 6 user 10
```

* secound way By using API endpoint method :
**By Make a POST request to http://localhost:6002/gather with the following JSON payload in the request body and set the Content-Type header to application/json :**

```json
{
    "start_date1": "2023-10-01",
    "end_date1": "2023-10-30",
    "factor_theme_type1": "theme",
    "factor_theme_id1": 6,
    "user_group_type1": "user",
    "user_group_id1": 10,
    "start_date2": "2023-10-08",
    "end_date2": "2023-10-16",
    "factor_theme_type2": "theme",
    "factor_theme_id2": 6,
    "user_group_type2": "user",
    "user_group_id2": 10,
    "visualize": true
}
```

### Successful Response

- **Status Code**: 200 OK

**Body Example:**

```json
{
    "data": {
        "result": [
            [
                [
                    "Sun, 01 Oct 2023 00:00:00 GMT",
                    "morning",
                    0
                ],
                [
                    "Sun, 01 Oct 2023 00:00:00 GMT",
                    "afternoon",
                    1
                ],
                [
                    "Mon, 02 Oct 2023 00:00:00 GMT",
                    "morning",
                    -1
                ] 
            ],

            [  
                [
                    "Mon, 02 Oct 2023 00:00:00 GMT",
                    "afternoon",
                    0
                ],
                [
                    "Tue, 03 Oct 2023 00:00:00 GMT",
                    "morning",
                    0
                ],
                [
                    "Tue, 03 Oct 2023 00:00:00 GMT",
                    "afternoon",
                    0
                ]
            ]

        ]  
    }
}

```

### Error Response

- **Status Code**: 500 Bad Request

**Body Example:**

```json
{
  "error": "Internal Server Error:...."
}
```

