#!/usr/bin/env python3

from mysql.connector import Error as DBError
from requests import HTTPError
import argparse
from datetime import date
from pathlib import Path
from utils import Config, DbAccess, ApiAccess
from gather_service import VoteGatherer
from gather_service import GatherServiceHelper


def init_arg_parser() -> argparse.Namespace:
    print("Initializing argument parser")
    parser = argparse.ArgumentParser(
        description='Module to do gather voting data')

    parser.add_argument('--config', type=str,
                        default=str(Config.DEFAULT_CONFIG_PATH), help='Path to TOML config file')

    subparsers = parser.add_subparsers(
        help='choose a command', required=True, dest='command')

    gather_parser = subparsers.add_parser(
        'gather', help='gather vote data from the DB')
    gather_parser.add_argument(
        'start_date1', type=date.fromisoformat, help='start date (format: YYYY-mm-dd)')
    gather_parser.add_argument(
        'end_date1', type=date.fromisoformat, help='end date (format: YYYY-mm-dd)')
    gather_parser.add_argument('factor_theme_type1', choices=[
                                'factor', 'theme'], help='is the id for a factor or a theme')
    gather_parser.add_argument(
        'factor_theme_id1', type=int, help='id of factor or theme to gather')
    gather_parser.add_argument('user_group_type1', choices=[
                                'user', 'group'], help='is the id for a user or a group')
    gather_parser.add_argument(
        'user_group_id1', type=int, help='id of user or group to gather')
    gather_parser.add_argument(
        'start_date2', type=date.fromisoformat, help='start date (format: YYYY-mm-dd)')
    gather_parser.add_argument(
        'end_date2', type=date.fromisoformat, help='end date (format: YYYY-mm-dd)')
    gather_parser.add_argument('factor_theme_type2', choices=[
                                'factor', 'theme'], help='is the id for a factor or a theme')
    gather_parser.add_argument(
        'factor_theme_id2', type=int, help='id of factor or theme to gather')
    gather_parser.add_argument('user_group_type2', choices=[
                                'user', 'group'], help='is the id for a user or a group')
    gather_parser.add_argument(
        'user_group_id2', type=int, help='id of user or group to gather')

    return parser.parse_args()


def main():
    args = init_arg_parser()
    Config.parse_from_config(Path(args.config))
    try:
        with DbAccess() as db, ApiAccess() as api:
            try:
                if args.command == 'gather':
                    vg = VoteGatherer(db, api)
                    results = GatherServiceHelper.get_gather_results(vg, args)
                    print(results)

            except RuntimeError as e:
                print('An error occurred gather')
                print(e)
                
    except DBError as e:
        print('An error occurred while using the DB:')
        print(e)
    except HTTPError as e:
        print('An error occurred while using the backend API:')
        print(e)
    except ValueError as e:
        print('Invalid arguments passed:')
        print(e)
    except IOError as e:
        print('An error occurred while reading the JSON files:')
        print(e)


if __name__ == '__main__':
    main()
