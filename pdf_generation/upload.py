import boto3
from botocore.exceptions import NoCredentialsError
import os

class S3Session:
    def __init__(self, file_name):
        self.file_name = file_name
        self.access_key = os.environ["S3_USERNAME"]
        self.secret_key =  os.environ["S3_PASSWORD"]
    def s3session(self):
        # Initialize a session with the Minio server.
        client = boto3.client('s3',
                              endpoint_url='https://minio1.isc.heia-fr.ch:9008',
                              aws_access_key_id=self.access_key,
                              aws_secret_access_key=self.secret_key,
                              config=boto3.session.Config(signature_version='s3v4'),
                              use_ssl=True)

        # File to upload
        file_name = f"{self.file_name}.pdf"
        file_path = f'./{file_name}'
        bucket_name = 'spoton-pdf'

        # Upload the file
        try:
            client.upload_file(file_path, bucket_name, file_name)
            os.remove(f'./{file_name}')
        except NoCredentialsError as e:
            return e
        except Exception as e:
            return e

        return 200
