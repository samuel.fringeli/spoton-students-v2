import json
from fpdf import FPDF
import uuid

class DataManagement:
    def __init__(self, input_json):
        self.input_json = input_json
        self.template = {'file_name': '', 'title': '', 'subtitle': ''}
        self.section = []
        self.parsed_file = []

    def parse_data(self):
        try:
            data = self.input_json

            self.template['file_name'] = data.get('file_name', '')
            self.template['title'] = data.get('title', '')
            self.template['subtitle'] = data.get('subtitle', '')

            for section_data in data.get('section', []):
                if section_data.get('section_img', ''):
                    self.section.append({
                        'section_title': section_data.get('section_title', ''),
                        'section_img': section_data.get('section_img', ''),
                        'section_text': section_data.get('section_text', '')
                    })
                else:
                    self.section.append({
                        'section_title': section_data.get('section_title', ''),
                        'section_text': section_data.get('section_text', '')
                    })

        except KeyError:
            return "Key error in JSON data"
        except Exception as e:
            return f"An error occurred: {e}"

        self.parsed_file = {**self.template, 'section': self.section}

        return self.parsed_file

class GeneratePdf:
    def __init__(self, input_file):
        self.input_file = input_file
        self.file_name = uuid.uuid4()
        self.formatted_file = []
        self.output_pdf = []

    def format_to_pdf(self):

        try:
            pdf = FPDF()
            pdf.add_page()

            pdf.set_font('helvetica', 'B', size=18)
            pdf.multi_cell(0, 0, align='C', text=self.input_file['title'])
            pdf.ln(10)

            pdf.set_font('helvetica', 'I', size=16)
            pdf.multi_cell(0, 0, align='C', text=self.input_file['subtitle'])
            pdf.ln(10)

            for section in self.input_file['section']:
                section_title = section.get('section_title', '')
                section_text = section.get('section_text', '')

                pdf.set_font('helvetica', 'B', size=12)
                pdf.multi_cell(0, 0, text=section_title)
                pdf.ln(10)

                if section.get('section_img', ''):
                    section_img = section.get('section_img', '')
                    pdf.image(section_img, x='C' ,w=pdf.epw / 2)
                    pdf.ln(10)

                pdf.set_font('helvetica', size=12)
                pdf.multi_cell(ln=0, h=5.0, align='L', w=0, txt=section_text, border=0)
                pdf.ln(10)

            pdf.output(f"{self.file_name}.pdf")

            return f"{self.file_name}.pdf"

        except FileNotFoundError:
            return "Could not create pdf file"

        except ConnectionError:
            return "Failed to open image URL"
