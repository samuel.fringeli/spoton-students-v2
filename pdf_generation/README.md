# SooZe 
## Pdf generation

The pdf generation service for SooZe uses a Flask app and the [FPDF library for Python](https://pyfpdf.readthedocs.io/en/latest/) to generate a pdf from a json string.

The service is launched automatically with _docker-compose_.