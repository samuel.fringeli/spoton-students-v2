from flask import Flask, request, jsonify, send_file
import os
from pdf_generation import DataManagement, GeneratePdf
from upload import S3Session

app = Flask(__name__)

@app.route('/')
def home():
    return "SooZe pdf generator home page"

@app.route('/pdfgen', methods=['POST'])
def get_file():
    if request.method == 'POST':
        data = request.get_json()
        format_file = GeneratePdf(DataManagement(data).parse_data())
        output_pdf = format_file.format_to_pdf()
        s3_session = S3Session(format_file.file_name)
        response = s3_session.s3session()

        if response == 200:
            return f"https://sooz.tic.heia-fr.ch/api/pdf/{format_file.file_name}"
        else:
            return str(response)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5001, debug=True)
