#!/bin/bash

find / -name *.sql -delete
find / -name *.zip -delete

zipfilename="all-database-$(date +%d-%m-%Y_%H-%M-%S).zip"
sqlfile="/all-database-$(date +%d-%m-%Y_%H-%M-%S).sql"
zipfile="/${zipfilename}"

mysqldump -P 3306 -h $DB_HOST -u root -p$MYSQL_ROOT_PASSWORD --all-databases > $sqlfile

if [ $? == 0 ]; then
  echo 'Sql dump created'
else
  echo 'mysqldump return non-zero code'
  curl --location --request POST 'https://notify.pyme.ch/' --header 'Content-Type: application/json' --data-raw '{ "to": "samuel.fringeli@me.com", "subject": "Spot-On - error with DB backup", "message": "An error occured while trying to connect to the DB." }'
  exit
fi

# Compress backup
zip $zipfile $sqlfile
if [ $? == 0 ]; then
  echo 'The backup was successfully compressed'
else
  echo 'Error compressing backup'
  curl --location --request POST 'https://notify.pyme.ch/' --header 'Content-Type: application/json' --data-raw '{ "to": "samuel.fringeli@me.com", "subject": "Spot-On - error with DB backup", "message": "An error occured while trying to compress to the DB backup." }'
  exit
fi

rm $sqlfile
echo $zipfile

S3_PATH="s3://${BUCKET_NAME}${zipfile}"
aws configure set aws_access_key_id $S3_USERNAME
aws configure set aws_secret_access_key $S3_PASSWORD

aws --endpoint-url $S3_ENDPOINT s3 cp $zipfile $S3_PATH
if [ $? == 0 ]; then
  echo 'The backup transfer to minio was successfull'
else
  echo 'Error compressing backup'
  curl --location --request POST 'https://notify.pyme.ch/' --header 'Content-Type: application/json' --data-raw '{ "to": "samuel.fringeli@me.com", "subject": "Spot-On - error with DB backup", "message": "An error occured while trying to transfer the DB backup to minio." }'
  exit
fi
