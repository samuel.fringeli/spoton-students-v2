# Invitation nouveaux utilisateurs pour "Microsoft identity platform"

<p>Pour inviter un utilisateur il suffit de se rendre dans la section "owners" du service "Microsoft identity platform" sur l'interface d'azure<p>

![](./images/owners_list.png)

<p>En haut à gauche se trouve un bouton "Add owners" qui permet ensuite de rajouter différents membre en les sélectionnant. Une fois les nouveaux membres sélectionné, il suffit de cliquer sur le bouton "select" se trouvant au bas de la page</p>

![](./images/add_owner.png)
