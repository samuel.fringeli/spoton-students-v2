# rules-scheduler
Initial repository: https://gitlab.forge.hefr.ch/spoton/rules-scheduler
## Usage

Example: ` python3 rules_checker.py $(date --iso-8601=seconds) weekly username user_pwd`

Usage:

```bash
$ python3.9 rules_checker.py --help

usage: rules_checker.py [-h] [--enterprise_id ENTERPRISE_ID] datetime periodicity username password

Module to check if rules should be triggered or not.

positional arguments:
  datetime              Datetime used as reference to find valid rules, generally NOW.
  periodicity           Periodicity in ['daily', 'weekly', 'monthly'] to define occurrence of check.
  username              Username or email used to connect to existing SpotOn account.
  password              Password to connect to user SpotOn account.

optional arguments:
  -h, --help            show this help message and exit
  --enterprise_id ENTERPRISE_ID
                        Id of enterprise
```

## What is missing

- Deployment
    - Change url name of backend
    - Create cron jobs: one per periodicity, just execute command like above

## If a new periodicity/operator/votetype is added

- Add it in the corresponding class in `classes.py`
- In `rules_checker.py`, change the conditions in functions `get_periodicity_dates` or`get_alarm_json_if_rule_triggered` 
