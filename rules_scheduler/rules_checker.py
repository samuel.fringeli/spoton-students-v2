import datetime
from datetime import datetime as datetime_lib
from datetime import timezone as tz
from datetime import timedelta
import calendar
from classes import Vote, Periodicity, Alarm, UserVotes, Rule, Enterprise
from api_access import ConfigHelper, ApiAccess
from typing import Any
import argparse

all_periodicity = Periodicity.get_all()


def check_periodicity(func):
    def wrapper(*args, **kwargs):
        assert len(args) > 0 and args[0] in all_periodicity, "Periodicity should be in [" + ", ".join(
            all_periodicity) + "]"
        return func(*args, **kwargs)

    return wrapper


def get_enterprises(api_access: ApiAccess) -> list[Enterprise]:
    enterprises, _ = api_access.enterprise.get_enterprises_members()
    return enterprises


@check_periodicity
def get_periodicity_dates(periodicity: Periodicity, datetime: datetime_lib) -> tuple[
    datetime_lib.date, datetime_lib.date]:
    # Gets from really begin to end (there is no difference between weekday or weekend)
    begin = end = datetime_lib.date(datetime)

    if periodicity == Periodicity.DAILY:
        begin = end = begin - timedelta(days=1)
    elif periodicity == Periodicity.WEEKLY:
        # Monday before: datetime-(7 days)-datetime.weekday()
        # Sunday before: datetime-datetime.weekday()
        begin = begin - timedelta(days=(7 + begin.weekday()))
        end = end - timedelta(days=(7 + end.weekday())) + timedelta(days=6)
    elif periodicity == Periodicity.MONTHLY:
        # 1st day of previous month: (datetime-(1 month)).replace(day=1)
        # Last day of previous month (28, 29, 30, 31):
        # last = calendar.monthrange(year, month-1)[1];
        # (datetime-(1 month)).replace(day=last)
        last_day_of_month = calendar.monthrange(begin.year, begin.month - 1 or 12)[1]
        begin = (begin - timedelta(days=last_day_of_month)).replace(day=1)
        end = (end - timedelta(days=last_day_of_month)).replace(day=last_day_of_month)

    return begin, end


@check_periodicity
def get_active_rules(periodicity: Periodicity, begin: datetime_lib, enterprise: Enterprise, api_access: ApiAccess) -> \
        list[Rule]:
    rules, _ = api_access.enterprise.get_enterprise_rules(periodicity, begin, enterprise)
    return rules


def get_open_days_in_period(begin: datetime_lib, end: datetime_lib, enterprise_id: int, api_access: ApiAccess) -> list[
    str]:
    open_days, _ = api_access.enterprise.get_enterprise_opendays(begin, end, enterprise_id)
    return open_days


def get_votes(open_days: list[datetime_lib], enterprise: Enterprise, rule: Rule, api_access: ApiAccess) -> list[Vote]:
    votes, _ = api_access.responses.get_votes(open_days, enterprise, rule)
    return votes


def pos_neg_responses(rule: Rule, api_access: ApiAccess) -> tuple[list[int], list[int]]:
    responses, _ = api_access.response_options.get_factor_response_options(rule.factor.theme_id, rule.factor_id)

    negatives = []
    positives = []
    # Be careful: this is based on actual project structure (06.09.2022)
    if len(responses) == 5:
        negatives += map(lambda x: x.id, filter(lambda x: x.value in [1, 5], responses))
        positives += map(lambda x: x.id, filter(lambda x: x.value in [2, 3, 4], responses))
    elif len(responses) == 4:
        negatives += map(lambda x: x.id, filter(lambda x: x.value in [1, 2], responses))
        positives += map(lambda x: x.id, filter(lambda x: x.value in [3, 4], responses))
    elif len(responses) == 3:
        negatives += map(lambda x: x.id, filter(lambda x: x.value in [1], responses))
        positives += map(lambda x: x.id, filter(lambda x: x.value in [2, 3], responses))

    return positives, negatives


def refactor_votes(votes: list[Vote], pos_neg_responses: tuple[list[int], list[int]], group=None, person_id=None) -> \
        list[UserVotes]:
    pos, neg = pos_neg_responses

    if group:
        users = {}
        for user in group.users:
            users[user] = UserVotes(user)

        for vote in votes:
            if vote.response_option_id in pos:
                users[vote.user_id].pos_votes += 1
            else:
                users[vote.user_id].neg_votes += 1

        return list(users.values())

    elif person_id:
        user = UserVotes(person_id)
        for vote in votes:
            if vote.response_option_id in pos:
                user.pos_votes += 1
            else:
                user.neg_votes += 1
        return [user]


def get_alarm_json_if_rule_triggered(rule: Rule, users_votes: list[UserVotes], nb_max_of_votes: int) -> dict[str, Any]:
    if len(users_votes) == 0:
        return {}  # not triggered

    # voting rate
    voting_rate = 0
    for user_votes in users_votes:
        voting_rate = round(user_votes.get_total_votes() / nb_max_of_votes * 100)
        user_votes.has_voted_enough = voting_rate >= rule.voting_rate

    participation_rate = sum([user_votes.has_voted_enough for user_votes in users_votes]) / len(users_votes) * 100

    if participation_rate < rule.participation_rate:
        return {}  # not triggered

    # count pos/neg rate
    # we consider only the people that voted enough
    all_votes = UserVotes.merge_users_votes([user_votes for user_votes in users_votes if user_votes.has_voted_enough])
    rate = 0
    if rule.is_positive():
        rate = all_votes.pos_votes
    elif rule.is_negative():
        rate = all_votes.neg_votes

    try:
        rate = round(rate / all_votes.get_total_votes() * 100)
    except ZeroDivisionError:
        pass

    # check if rate is greater/lower than rule rate
    if rule.is_operator_greater_than():
        if rate >= rule.rate:
            return _create_alarm_body(rule, rate, voting_rate, participation_rate)  # create alarm
        else:
            return {}  # not triggered
    elif rule.is_operator_less_than():
        if rate <= rule.rate:  # create alarm
            return _create_alarm_body(rule, rate, voting_rate, participation_rate)
        else:
            return {}  # not triggered

    # alarm not triggered: vote type or operator not recognized
    return {}


def _create_alarm_body(rule: Rule, rate: float, voting_rate: float, participation_rate: float) -> dict[str, any]:
    return {
        "rate": rate,
        "ruleId": rule.id,
        "votingRate": voting_rate,
        "periodicity": rule.periodicity,
        "participationRate": participation_rate,
        "ruleType": rule.group_id,
        "shouldSendNotification": True,
        "notificationMessage": "Une alarme à été levée",
        "shouldSendMessage": True
    }


def create_alarm(parameters_json: dict[str, Any], api_access: ApiAccess) -> Alarm:
    alarm, _ = api_access.alarms.post_alarm(parameters_json)
    return alarm


@check_periodicity
def check_rules_per_period_per_enterprise(periodicity: Periodicity, datetime: datetime_lib, enterprise: Enterprise,
                                          api_access: ApiAccess) -> dict[str, Any]:
    begin, end = get_periodicity_dates(periodicity, datetime)
    rules = get_active_rules(periodicity, begin, enterprise, api_access)
    open_days = get_open_days_in_period(begin, end, enterprise.id, api_access)
    triggered_rules = []
    for rule in rules:
        print("====== Rule ", rule.id)
        votes = get_votes(open_days, enterprise, rule, api_access)
        responses = pos_neg_responses(rule, api_access)

        refactored_votes = []
        if rule.group_id:
            group = [group for group in enterprise.groups_id if group.id == rule.group_id][0]
            refactored_votes = refactor_votes(votes, responses, group=group)
        elif rule.person_id:
            refactored_votes = refactor_votes(votes, responses, person_id=rule.person_id)

        alarm = get_alarm_json_if_rule_triggered(
            rule,
            refactored_votes,
            len(open_days) * 2,  # because we consider half days
        )
        if any(alarm):
            created_alarm: Alarm = create_alarm(alarm, api_access)
            # do whatever you want with created_alarm (stats, etc)
            triggered_rules.append(rule)

    return {"enterpriseId": enterprise.id, "rules": triggered_rules}


@check_periodicity
def check_rules_per_period(periodicity: Periodicity, datetime: datetime_lib, api_access: ApiAccess) -> list[
    dict[str, Any]]:
    rules_triggered = []
    for enterprise in get_enterprises(api_access):
        rules_triggered.append(check_rules_per_period_per_enterprise(periodicity, datetime, enterprise, api_access))

    return rules_triggered


def _instantiate_argparser():
    # Instantiate the parser
    parser = argparse.ArgumentParser(description="Module to check if rules should be triggered or not.")
    parser.add_argument("datetime", type=str, help="Datetime used as reference to find valid rules, generally NOW.")
    parser.add_argument("periodicity", type=str,
                        help="Periodicity in ['daily', 'weekly', 'monthly'] to define occurrence of check.")
    parser.add_argument("username", type=str, help="Username or email used to connect to existing SpotOn account.")
    parser.add_argument("password", help="Password to connect to user SpotOn account.")
    parser.add_argument("--enterprise_id", type=int, help="Id of enterprise")  # Optional argument

    return parser.parse_args()  # parse arguments


def main():
    args = _instantiate_argparser()

    ConfigHelper.host = "http://localhost:3000"  # TODO: configure with right backend
    ConfigHelper.verbose = True

    with ApiAccess(args.username, args.password) as user:
        datetime = datetime_lib.fromisoformat(args.datetime)
        periodicity = Periodicity.from_str(args.periodicity)

        if args.enterprise_id:
            # Actually not used, but useful if we have one machine per enterprise
            # TODO: get enterprise in right format
            enterprise = []
            check_rules_per_period_per_enterprise(periodicity, datetime, enterprise, user)
        else:
            check_rules_per_period(periodicity, datetime, user)


def main_for_api(username, password, periodicity, enterprise_id=None):
    ConfigHelper.host = "http://backend:3000"  # TODO: configure with right backend
    ConfigHelper.verbose = True

    with ApiAccess(username, password) as user:
        datetime = datetime_lib.now(tz.utc).replace(microsecond=0)
        periodicity = Periodicity.from_str(periodicity)

        if enterprise_id is not None:
            # Actually not used, but useful if we have one machine per enterprise
            # TODO: get enterprise in right format
            enterprise = []
            check_rules_per_period_per_enterprise(periodicity, datetime, enterprise, user)
        else:
            check_rules_per_period(periodicity, datetime, user)


if __name__ == "__main__":
    main()
