from classes import Operator, VoteType
from rules_checker import *
import unittest


class TestDateRanges(unittest.TestCase):

    def test_periodicity_dates(self):
        dt_start = datetime_lib.fromisoformat("2022-09-02")

        test_values = [
            (Periodicity.DAILY, dt_start),
            (Periodicity.WEEKLY, dt_start),
            (Periodicity.MONTHLY, dt_start),
        ]
        true_values = [
            ("2022-09-01", "2022-09-01"),
            ("2022-08-22", "2022-08-28"),
            ("2022-08-01", "2022-08-31"),
        ]
        for i in range(len(true_values)):
            periodicity, datetime = test_values[i]
            begin, end = get_periodicity_dates(periodicity, datetime)
            true_begin, true_end = true_values[i]
            self.assertEqual(begin.isoformat(), true_begin)
            self.assertEqual(end.isoformat(), true_end)


class TestRuleTriggered(unittest.TestCase):
    # Test fct get_alarm_json_if_rule_triggered, regarding cases in Excel sheet

    def test_s1(self):
        # weekly, so nb_max_of_votes=10
        periodicity: Periodicity = Periodicity.WEEKLY
        nb_max_of_votes: int = 10

        p1: UserVotes = UserVotes(1, positives=10, negatives=0)
        p2: UserVotes = UserVotes(2, positives=1, negatives=5)
        p3: UserVotes = UserVotes(3, positives=5, negatives=0)
        p4: UserVotes = UserVotes(4, positives=1, negatives=6)
        user_votes_all: list[UserVotes] = [p1, p2, p3, p4]
        rule: Rule = Rule(0, periodicity, 0, Operator.GREATER_THAN, 50, 50, VoteType.NEGATIVE, 50, False)

        alarm = get_alarm_json_if_rule_triggered(rule, user_votes_all, nb_max_of_votes)
        self.assertFalse(any(alarm))

    def test_s2(self):
        # weekly, so nb_max_of_votes=10
        periodicity: Periodicity = Periodicity.WEEKLY
        nb_max_of_votes: int = 10

        p1: UserVotes = UserVotes(1, positives=1, negatives=0)
        p2: UserVotes = UserVotes(2, positives=5, negatives=1)
        p3: UserVotes = UserVotes(3, positives=0, negatives=10)
        p4: UserVotes = UserVotes(4, positives=5, negatives=1)
        user_votes_all: list[UserVotes] = [p1, p2, p3, p4]
        rule: Rule = Rule(0, periodicity, 0, Operator.GREATER_THAN, 50, 50, VoteType.NEGATIVE, 50, False)

        alarm = get_alarm_json_if_rule_triggered(rule, user_votes_all, nb_max_of_votes)
        self.assertTrue(any(alarm))

    def test_s3(self):
        # weekly, so nb_max_of_votes=10
        periodicity: Periodicity = Periodicity.WEEKLY
        nb_max_of_votes: int = 10

        p1: UserVotes = UserVotes(1, positives=6, negatives=0)
        p2: UserVotes = UserVotes(2, positives=3, negatives=0)
        p3: UserVotes = UserVotes(3, positives=0, negatives=0)
        p4: UserVotes = UserVotes(4, positives=0, negatives=3)
        user_votes_all: list[UserVotes] = [p1, p2, p3, p4]
        rule: Rule = Rule(0, periodicity, 0, Operator.GREATER_THAN, 50, 50, VoteType.NEGATIVE, 50, False)

        alarm = get_alarm_json_if_rule_triggered(rule, user_votes_all, nb_max_of_votes)
        self.assertFalse(any(alarm))

    def test_s4(self):
        # weekly, so nb_max_of_votes=10
        periodicity: Periodicity = Periodicity.WEEKLY
        nb_max_of_votes: int = 10

        p1: UserVotes = UserVotes(1, positives=10, negatives=0)
        p2: UserVotes = UserVotes(2, positives=0, negatives=6)
        p3: UserVotes = UserVotes(3, positives=2, negatives=0)
        p4: UserVotes = UserVotes(4, positives=0, negatives=0)
        user_votes_all: list[UserVotes] = [p1, p2, p3, p4]
        rule: Rule = Rule(0, periodicity, 0, Operator.GREATER_THAN, 50, 50, VoteType.NEGATIVE, 50, False)

        alarm = get_alarm_json_if_rule_triggered(rule, user_votes_all, nb_max_of_votes)
        self.assertFalse(any(alarm))


if __name__ == '__main__':
    unittest.main()
