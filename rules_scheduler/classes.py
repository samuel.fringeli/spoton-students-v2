from __future__ import annotations  # because we pass argument of class itself
import json
from dataclasses import dataclass
from dataclasses_json import dataclass_json, LetterCase
from datetime import datetime
from typing import Optional


class Periodicity:
    """
    Class for periodicity
    """
    DAILY = "daily"
    WEEKLY = "weekly"
    MONTHLY = "monthly"

    @staticmethod
    def from_str(label):
        if label == "daily":
            return Periodicity.DAILY
        if label == "weekly":
            return Periodicity.WEEKLY
        if label == "monthly":
            return Periodicity.MONTHLY
        else:
            raise NotImplementedError

    @staticmethod
    def get_all():
        return [Periodicity.DAILY, Periodicity.WEEKLY, Periodicity.MONTHLY]


class Period:
    """
    Class for periodicity
    """
    MORNING = "morning"
    AFTERNOON = "afternoon"


class Operator:
    """
    Class for operators
    """
    GREATER_THAN = ">"
    LESS_THAN = "<"


class VoteType:
    """
    Class for vote types
    """
    POSITIVE = "positive"
    NEGATIVE = "negative"


class UserVotes:
    def __init__(self, user_id, positives=0, negatives=0):
        self.id = user_id  # -1 if used to compute the sum of multiple userVOtes
        self.pos_votes = positives
        self.neg_votes = negatives
        self.has_voted_enough = -1  # -1 for init, otherwise 0 or 1

    def __repr__(self):
        return json.dumps(self.__dict__)

    def get_total_votes(self):
        return self.neg_votes + self.pos_votes

    def has_voted(self):
        return 0 != (self.neg_votes or self.pos_votes)

    @staticmethod
    def merge_users_votes(users_votes: list[UserVotes]):
        positives = sum(user_votes.pos_votes for user_votes in users_votes)
        negatives = sum(user_votes.neg_votes for user_votes in users_votes)
        return UserVotes(-1, positives, negatives)


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Vote:
    factor_id: int
    id: int
    response_option_id: int
    user_id: int


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Enterprise:
    groups_id: list[Group]
    id: int
    users_id: list[int]


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Alarm:
    message: str
    rule_id: int
    status: str
    state: str
    state_date: datetime
    state_message: str


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ResponseOption:
    id: int
    value: int


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Group:
    id: int
    users: list[int]


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Factor:
    theme_id: int
    # users: list[int]


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Rule:
    id: int
    periodicity: Periodicity
    factor_id: int
    operator: Operator
    voting_rate: int
    rate: int
    vote_type: VoteType
    participation_rate: int
    archived: bool
    person_id: Optional[int] = None
    factor: Optional[Factor] = None
    group_id: Optional[int] = None

    def is_positive(self):
        return self.vote_type == VoteType.POSITIVE

    def is_negative(self):
        return self.vote_type == VoteType.NEGATIVE

    def is_operator_greater_than(self):
        return self.operator == Operator.GREATER_THAN

    def is_operator_less_than(self):
        return self.operator == Operator.LESS_THAN

    def is_group(self):
        return self.group_id is not None
