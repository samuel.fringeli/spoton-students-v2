from flask import Flask, request, jsonify
import logging
from rules_checker import main_for_api

app = Flask(__name__)


@app.route('/')
def hello():
    return 'This Compose/Flask demo has been viewed %s time(s).'


@app.route('/run_rule_checker', methods=['POST'])
def foo():
    data = request.json
    app.logger.info(data)
    main_for_api(data['username'], data['password'], data['periodicity'])
    return jsonify(data)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
