import requests

from .alarms_helper import AlarmsHelper
from .config_helper import ConfigHelper
from .enterprise_helper import EnterpriseHelper
from .logger import Logger
from .response_helper import ResponseHelper
from .response_option_helper import ResponseOptionHelper
from .user_helper import UserHelper


class ApiAccess(object):
    def __init__(self, name: str, token: str, force_create: bool = False):
        if ConfigHelper.host == "":
            raise ValueError(
                "Please provide a host before instantiating with 'ConfigHelper.host=\"http://<host>\"'"
            )
        if ConfigHelper.verbose is None:
            raise ValueError(
                "Please provide a boolean value to 'verbose' attribute before instantiating with ''ConfigHelper.verbose = <bool>'"
            )

        self.force_create = force_create
        self.session = requests.session()

        self.alarms = AlarmsHelper(self.session)
        self.enterprise = EnterpriseHelper(self.session)
        self.response_options = ResponseOptionHelper(self.session)
        self.responses = ResponseHelper(self.session)
        self.users = UserHelper(self.session, name, token)

        Logger.display = ConfigHelper.verbose

    def __enter__(self):
        _, r = self.users.login()
        if r.status_code == 200:
            return self

        return None

    def __exit__(self, *args, **kwargs):
        self.users.logout()
