from requests import Session
from requests.exceptions import HTTPError
from .config_helper import ConfigHelper
from classes import *
from datetime import datetime


class ResponseHelper:
    def __init__(self, session: Session):
        self.session = session

    def get_votes(self, open_days: list[datetime], enterprise: Enterprise, rule: Rule):
        """Get all votes that are valid for the rule."""
        content = {
            "openDays": open_days,
            "factorId": rule.factor_id
        }

        if rule.group_id:  # add all users ids if we check for a group
            content["userId"] = [group.users for group in enterprise.groups_id if group.id == rule.group_id][0]
        elif rule.person_id:
            content["userId"] = rule.person_id

        try:
            r = self.session.get(f"{ConfigHelper.host}/oneshot/allresponses", params=content)
            r.raise_for_status()
        except HTTPError as e:
            pass
        else:
            content = json.loads(r.content.decode("utf-8"))
        finally:
            ConfigHelper.show_status("    Get Responses", r)

        return [Vote.from_dict(vote) for vote in content], r
