from datetime import date
from requests import Session
from requests.exceptions import HTTPError
from .config_helper import ConfigHelper
from classes import *
import json


class EnterpriseHelper:
    def __init__(self, session: Session):
        self.session = session

    def get_enterprises_members(self):
        content = {}
        try:
            r = self.session.get(f"{ConfigHelper.host}/enterprises/members")
            r.raise_for_status()
        except HTTPError as e:
            pass
        else:
            content = json.loads(r.content.decode("utf-8"))
            print(content)
        finally:
            ConfigHelper.show_status("    Get Enterprises Members", r)

        return [Enterprise.from_dict(enterprise) for enterprise in content], r

    def get_enterprise_opendays(self, begin: date, end: date, enterprise_id: int):
        content = {
            "begin": begin,
            "end": end
        }

        try:
            r = self.session.get(f"{ConfigHelper.host}/enterprises/{enterprise_id}/opendays", params=content)
            r.raise_for_status()
        except HTTPError as e:
            pass
        else:
            content = list(map(lambda d: date.fromisoformat(d), json.loads(r.content.decode("utf-8"))))
        finally:
            ConfigHelper.show_status("    Get Enterprises Members", r)

        return content, r

    def get_enterprise_rules(self, periodicity: Periodicity, begin: date, enterprise: Enterprise):
        """Get all rules for an enterprise"""
        content = {
            "groupsId": map(lambda group: group.id, enterprise.groups_id),
            "usersId": enterprise.users_id,
            "createdBefore": begin.isoformat(),
            "periodicity": periodicity,
            "archived": False,
        }

        try:
            r = self.session.get(f"{ConfigHelper.host}/enterprises/{enterprise.id}/rules", params=content)
            r.raise_for_status()
        except HTTPError as e:
            pass
        else:
            content = json.loads(r.content.decode("utf-8"))
        finally:
            ConfigHelper.show_status("    Get Rules", r)

        return [Rule.from_dict({k: v for k, v in rule.items() if rule['groupId'] != "None"}) for rule in content], r
