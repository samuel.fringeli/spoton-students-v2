from requests import Session
from requests.exceptions import HTTPError
from .config_helper import ConfigHelper
from classes import *


class ResponseOptionHelper:
    def __init__(self, session: Session):
        self.session = session

    def get_factor_response_options(self, theme_id: int, factor_id: int):
        """Get all possible responses for a particular factor."""

        content = {}

        try:
            r = self.session.get(
                f"{ConfigHelper.host}/themes/{theme_id}/factors/{factor_id}/responseOptions"
            )
            r.raise_for_status()
        except HTTPError as e:
            pass
        else:
            content = json.loads(r.content.decode("utf-8"))
        finally:
            ConfigHelper.show_status("    Get Factor Response Options", r)

        return [ResponseOption.from_dict(responseOption) for responseOption in content], r
