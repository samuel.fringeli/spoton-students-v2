import json
from pprint import pprint
from typing import Tuple

from requests import Response, Session
from requests.exceptions import HTTPError

from classes import Alarm
from .config_helper import ConfigHelper


class AlarmsHelper:
    def __init__(self, session: Session):
        self.session = session

    def post_alarm(self, parameters_json) -> Tuple[dict, Response]:
        """Post an alarm"""
        content = parameters_json
        try:
            r = self.session.post(f"{ConfigHelper.host}/alarms", json=content)
            r.raise_for_status()
        except HTTPError as e:
            pass
        else:
            content = json.loads(r.content.decode("utf-8"))
            pprint(content)
        finally:
            ConfigHelper.show_status("    Post alarm", r)

        return content, r
        # TODO: when request is working, change it !
        # return Alarm.from_dict(content), r
