# Icon Generation
To generate the different icons, this npm module is used: https://github.com/onderceylan/pwa-asset-generator.

The source file for the rendered logo is a https://app.diagrams.net/ file with the ending `.drawio`.
